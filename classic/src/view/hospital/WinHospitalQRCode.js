Ext.define('Admin.view.hospital.WinHospitalQRCode', {
    extend: 'Ext.window.Window',
    controller: 'hospital',
    width:400,
    height:400,
    layout:"fit",
    modal:true,
    items: [
        {
            xtype: "container",
            html:"<div id='qrcode' style='margin:50px 100px'></div>",
            listeners:{
                render: function(arguments) {
                    var id = this.ownerCt.record.id;
                    var qrcode = new QRCode(document.getElementById("qrcode"), {
                        width : 200,
                        height : 200
                    });
                    if(this.ownerCt.title == '查看Web版二维码'){
                        qrcode.makeCode(Admin.proxy.API.WebPath +`?${id}#/`);
                    }else if(this.ownerCt.title == '查看微信网页版二维码'){
                        qrcode.makeCode(Admin.proxy.API.WechatWebPath + `?${id}#/signin`);
                    }
                }
            }
        }
    ]
});
