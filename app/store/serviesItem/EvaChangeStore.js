Ext.define("Admin.store.serviesItem.EvaChangeStore",{
	extend: 'Admin.store.BaseStore',
    alias: 'store.evaChangeStore',

    fields: [
        { name:"Id",mapping:"id"},                
        { name:"ServiceId",mapping:"serviceId" },        
        { name:"BefValidDateStart",mapping:"befValidDateStart" },                                       
        { name:"BefValidDateEnd",mapping:"befValidDateEnd" },                                   
        { name:"AftValidDateStart",mapping:"aftValidDateStart" },                                      
        { name:"AftValidDateEnd",mapping:"aftValidDateEnd" },                                        
        { name:"BefIsDateUnlimited",mapping:"befIsDateUnlimited" },                              
        { name:"AftIsDateUnlimited",mapping:"aftIsDateUnlimited" },                                          
        { name:"CreateTime",mapping:"createTime" },                                               
        { name:"Memo",mapping:"memo" },      
        { name:"BefServiceCount",mapping:"befServiceCount" },               
        { name:"AftServiceCount",mapping:"aftServiceCount" },                                    
        { name:"BefName",mapping:"befName"},       
        { name:"AftName",mapping:"aftName" },                 
    ],

    autoLoad: false,
    storeId: 'evaChangeStore',
    proxy: {
        type: 'ajax',
        cors:true,
        useDefaultXhrHeader:false,
        url: Admin.proxy.API.MgmtPath+"serviceManage/findServiesChange",
        reader: {
            type: 'json',
            rootProperty: 'data',
            totalProperty  : 'totalCount',
            successProperty: 'success'
        }
     }
})