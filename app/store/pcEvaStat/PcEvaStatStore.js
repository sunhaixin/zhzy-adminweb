Ext.define('Admin.store.pcEvaStat.PcEvaStatStore', {
    extend: 'Admin.store.BaseStore',
    alias: 'store.pcEvaStat',

    fields: [
        {name: "hospitalId"},
        {name: "serviceStartTime"},
        {name: "serviceEndTime"},
        {name: "list"}
    ],

    autoLoad:false,
    storeId: 'pcEvaStat',
    proxy: {
         type: 'ajax',
         cors:true,
         useDefaultXhrHeader:false,
         url: Admin.proxy.API.MgmtPath+'serviceManage/stat',
         reader: {
             type: 'json',
             rootProperty: 'data',
             totalProperty  : 'totalCount',
             successProperty: 'success'
         }
     }
});
