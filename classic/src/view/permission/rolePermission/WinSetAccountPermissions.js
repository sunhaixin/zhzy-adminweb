Ext.define("Admin.view.permission.rolePermission.WinSetAccountPermissions",{
	extend: "Ext.window.Window",
	layout: "fit",
	controller: "privilegeMgr",
	width: 600,
	height: 600,
	modal: true,
	title:"设置权限",
	autoScroll:true,
	listeners: {
		render: function(){

			var tree = Ext.getCmp("setsccountpermission");
            var store = tree.getStore();

            store.getProxy().extraParams = {
                accountTypeId: this.TypeId
            };
            store.remove();
            store.load({
            	callback:function(){
            		store.getRoot().expand([true,true]);
            	}
            });
            
		}
	},
	bbar:[
        {
            xtype:"container",
            flex:1,
			style: "border:1",
            layout:{
                type:"hbox",
                pack:"center",
                align:"center"
            },
            padding:"0 0 10 0",
            items:[
                {
                    xtype:"tbspacer"
                },{
                    xtype:"button",
                    text:"提交",
                    handler:"onSetAccountPermissions",
					width: "40%",
					listeners: {
						render: function(){
							var prvileges=sessionStorage.getItem("MyPrvileges").split(",");
							if (prvileges.indexOf("rolepermission_revisions_power") == -1) {
								this.hide();
							}
						}
					}
                }
            ]
        }
    ],
    items: [
    	{
    		xtype:"treepanel",
            id: "setsccountpermission",
            itemId: "AccountPermissions",
            flex:1,
            margin:"0 0 0 10",
            height:"100%",
            // style:"border:1px solid #d0d0d0 !important",
            rootVisible:false,
            store: Ext.create("Admin.store.permission.SetAccountPermission",{autoLoad: false}),
		    listeners: {//为类添加监听
		    	
		    	checkchange: function(){
		    		var me = arguments[0];
		    		var nodes = me.parentNode.childNodes;
		    		nodes.forEach(function(bean){
		    			if(bean.get("GroupName") == me.get("GroupName") && me.get("GroupName") != ""){
		    				if(bean.get("Id") != me.get("Id")){
		    					bean.set("checked",false);
		    				}
		    			}
		    		});

		    		// var nodes = Ext.getCmp("setsccountpermission").getChecked();
		    		// for (var i = nodes.length - 2; i >= 0; i--) {
		    		// 	if (nodes[i].get("GroupName") == nodes[nodes.length-1].get("GroupName")) {
		    		// 		// nodes[i].set("checked",false)
		    		// 	}
		    		// }
		    	}
		    }
    	}
    ]
})