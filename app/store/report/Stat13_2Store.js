Ext.define('Admin.store.report.Stat13_2Store', {
    extend: 'Admin.store.BaseStore',
    alias: 'store.stat13_2report',

    model: 'Admin.model.report.Stat13Model',

    autoLoad:false,
    storeId: 'stat13_2report',
    proxy: {
         type: 'ajax',
         cors:true,
         useDefaultXhrHeader:false,
         url: Admin.proxy.API.BasePaths + 'api/admin/stat/13',
         reader: {
             type: 'json',
             rootProperty: 'Data',
             totalProperty  : 'Total',
             successProperty: 'IsSuccess'
         }
     }
});
