Ext.define('Admin.view.hospital.system.WinAccount', {
    extend: 'Ext.window.Window',
    controller: 'account',
    width:500,
    height:490,
    layout:"fit",
    modal:true,
    listeners: {
        activate:function(){
            var grid = this.down("#AccountHospital");
            if (sessionStorage.getItem("AccountType") != "system" && sessionStorage.getItem("AccountType") != "Operator") {                                                                           
                grid.getStore().proxy.url = Admin.proxy.API.MgmtPath+"MedicalTreatmentCombination/hospital/findAll";
                var aid = sessionStorage.getItem("Aid");
                if ( sessionStorage.getItem("MyPrvileges").indexOf("yilianti_hospital_lower_self_see") !=-1) {
                    grid.getStore().getProxy().extraParams = {
                        aid: aid,
                        search: "yilianti_hospital_lower_self_see"
                    };
                }else{
                    grid.getStore().getProxy().extraParams = {
                        aid: aid,
                        start: 0,
                        limit: 1000,
                        page: 10
                    };
                }
            }
            grid.getStore().removeAll();
            grid.getStore().loadPage(1);
        }
    },
    items: [
        {
            xtype:"form",
            layout: 'anchor',
            defaultType: 'textfield',
            padding:"10 20",
            itemId:"frmAccount",
            defaults: {
                anchor: '100%',
                labelWidth:65
            },
            autoScroll:true,
            items:[
                {
                    xtype:"combobox",
                    fieldLabel:"<span style='color:red'>*</span>账号类型",
                    name:"AccountType",
                    id:"zzcombo",
                    store:Ext.create("Admin.store.system.AccountTypeStore"),
                    displayField:"Name",
                    valueField:"Id",
                    editable:false,
                    allowBlank:false,
                    listeners:{
                        render: function(combo){
                            this.store.load();
                            if (this.ownerCt.ownerCt.record) {
                                var data = this.ownerCt.ownerCt.record.data;
                                combo.select(data.AccountType);
                            }
                        }
                    }
                },
                {
                    xtype:"combobox",
                    fieldLabel:"<span style='color:red'>*</span>医院",
                    name:"HospitalId",
                    store:Ext.create("Admin.store.hospital.HospitalGetStore"),
                    displayField:"Name",
                    valueField:"Id",
                    editable:false,
                    allowBlank:true,
                    itemId:"AccountHospital",
                    listeners:{
                        render : function(combo) {//渲染   
                            var grid = Ext.getCmp("Account-Hospital");
                            this.store.load();
                            if (grid.selection) {
                                var selectrow = grid.selection.data;
                                // combo.setValue(selectrow);//第一个值
                                combo.select(selectrow.Id);
                            }   
                        }
                    }
                },
                {
                    fieldLabel:"<span style='color:red'>*</span>登录账号",
                    name:"LoginId",
                    allowBlank:false
                },
                {
                    xtype: "container",
                    layout: "hbox",
                    margin: "10 0",
                    items: [
                        {
                            xtype: "textfield",
                            fieldLabel:"<span style='color:red'>*</span>密码",
                            name:"Pswd",
                            itemId: "Pswd",
                            width: "80%",
                            labelWidth:65,
                            allowBlank:false
                        },
                        {
                            xtype: "button",
                            text: "随机密码",
                            margin: "0 0 0 20",
                            itemId: "PswdButton",
                            handler:"randomPswd"
                        }
                    ]
                },
                
                {
                    fieldLabel:"<span style='color:red'>*</span>用户名",
                    name:"UserName",
                    allowBlank:false
                },
                {
                    fieldLabel:"数据平台名称",
                    name:"SysName",
                    // allowBlank:false
                },
                {
                    fieldLabel:"备注",
                    name:"Memo",
                    // allowBlank:false
                }
            ]
        }
    ],
    bbar:[
        {
            xtype:"container",
            flex:1,
            layout:{
                type:"hbox",
                pack:"center",
                align:"center"
            },
            padding:"0 0 10 0",
            items:[
                {
                    xtype:"tbspacer"
                },{
                    xtype:"button",
                    text:"提交",
                    handler:"onSubmitButtonClick"
                }
            ]
        }
    ]
});
