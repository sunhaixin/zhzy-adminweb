Ext.define('Admin.store.report.Stat16Store', {
    extend: 'Admin.store.BaseStore',
    alias: 'store.stat16report',

    model: 'Admin.model.report.Stat16Model',

    autoLoad:false,
    storeId: 'stat16report',
    proxy: {
         type: 'ajax',
         cors:true,
         useDefaultXhrHeader:false,
         url: Admin.proxy.API.MgmtPath+'stat/15',
         reader: {
             type: 'json',
             rootProperty: 'data',
             totalProperty  : 'Total',
             successProperty: 'IsSuccess'
         }
     }
});
