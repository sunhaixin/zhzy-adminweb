Ext.define('Admin.store.note.TongueStore', {
    extend: 'Admin.store.BaseStore',
    alias: 'store.tongue',

    model: 'Admin.model.note.TongueModel',

    autoLoad: false,
    storeId: 'tongue',
    proxy: {
         type: 'ajax',
         cors:true,
         useDefaultXhrHeader:false,
         url: Admin.proxy.API.BasePath+"api/admin/tongue/query",
         reader: {
             type: 'json',
             rootProperty: 'Data',
             totalProperty  : 'Total',
             successProperty: 'IsSuccess'
         }
     }
});
