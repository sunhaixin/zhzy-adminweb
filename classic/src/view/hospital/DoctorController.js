Ext.define("Admin.view.hospital.DoctorController",{
    extend:"Ext.app.ViewController",
    alias: 'controller.doctor',

    onHospitalItemClick:function(){
        var view = this.getView();
        var dpGrid = view.down("#GridDoctor");
        var hid = arguments[1].get("Id");
        dpGrid.getStore().getProxy().extraParams = {"hospitalId":hid};
        dpGrid.getStore().remove();
        dpGrid.getStore().loadPage(1);
    },
    onHospitalChange:function(v,val){
        var view = this.getView();
        var cmbDepartment = view.down("#cmbDepartment");
        cmbDepartment.reset();
        cmbDepartment.setDisabled(false);
        if (val != null){
            var store = cmbDepartment.getStore();
            store.getProxy().extraParams = {hospitalId:val};
            store.reload();
        }
    },
    onDoctorHospitalFielts: function(){
        var textfield = Ext.getCmp("doctorhospital").down("[xtype=textfield]").value;
        var dpGrid = Ext.getCmp("doctorhospital");
        dpGrid.getStore().clearFilter();
        var regExp = new RegExp(".*"+textfield+".*");
        if (textfield) {
            dpGrid.getStore().filterBy(function(record) { 
                    // return record.get('Name') == textfield;
                    // var filterBy = regExp.test(record.get("Name","")) || regExp.test(record.get("Id",""));
                    // return filterBy;
                    return regExp.test(record.get("Name"));
            });
        }
        dpGrid.getStore().remove();
        dpGrid.getStore().loadPage(1);
    },
    onDoctorFielts: function(){
        var textfield = this.getView().down("#GridDoctor").down("[xtype=textfield]").value;
        var dpGrid = this.getView().down("#GridDoctor");
        dpGrid.getStore().clearFilter();
        var regExp = new RegExp(".*"+textfield+".*");
        if (textfield) {
            dpGrid.getStore().filterBy(function(record) { 
                    // return record.get('Name') == textfield;
                    var filterBy = regExp.test(record.get("Name","")) || regExp.test(record.get("Id",""));
                    return filterBy;
            });
        }
        dpGrid.getStore().remove();
        dpGrid.getStore().loadPage(1);
    },
    onSubmitButtonClick:function(){
        var view = this.getView();
        if(view.type == "Add"){
            this.onAddDoctorSubmit(arguments);
        }else{
            this.onUpdateDoctorSubmit(arguments);
        }
    },
    onAddDoctorSubmit:function(){
        var view = this.getView();
        var frm = view.down("#frmDoctor");
        var values = frm.getValues();
        values.Department = view.down("#cmbDepartment").getDisplayValue();

        var filterRule= /[^0-9a-zA-Z_[\u4E00-\u9FA5]]/g;  
        var judge= filterRule.test(values.Name);
        if (judge) {
            Ext.Msg.alert("提示","姓名不能出现特殊符号")
            return false;
        }
        if (values.Mobile.length !== 11) {
            Ext.Msg.alert("提示","手机号不是11位");
            return false;
        }
        if(!values.Gender){
            Ext.Msg.alert("提示","请选择性别");
            return false;
        }
        if(!values.IsSSO){
            Ext.Msg.alert("提示","请选择是否为演示账号");
            return false;
        }
        if (!frm.form.isValid()) {
            Ext.Msg.alert("提示","请检查必填项");
            return false;
        }
        Ext.Ajax.request({
            url: Admin.proxy.API.MgmtPath+"doctor/insert",
            method:"Post",
            cors:true,//开启跨域模式
            useDefaultXhrHeader:false,//true的话header会带有X-Requested-With
            //binary:true, //为true后，后端将返回bytes
            //jsonData:{data:valus,token:"123456"},
            params:values,
            success:function(resp){
                var bean = Ext.decode(resp.responseText);
                if(bean.success){
                    Ext.Msg.alert("提示","添加成功");
                    var store = view.BindGrid.getStore();
                    store.reload();
                    view.close();      
                }else{
                    Ext.Msg.alert("提示",bean.message);
                }
            }
        });
    },
    onUpdateDoctorSubmit:function(){
        var view = this.getView();
        var frm = view.down("#frmDoctor");
        var values = frm.getValues();
        values.Id = view.record.get("Id");
        var Department = view.down("#cmbDepartment");
        values.Department = Department.rawValue;
        // valus.DepartmentUid = view.record.get("DepartmentUid");

        var filterRule= /[^0-9a-zA-Z_[\u4E00-\u9FA5]]/g;  
        var judge= filterRule.test(values.Name);
        if (judge) {
            Ext.Msg.alert("提示","姓名不能出现特殊符号")
            return false;
        }
        if (!frm.form.isValid()) {
             Ext.Msg.alert("提示","请检查必填项");
            return false;
        }
        // return false;
        Ext.Ajax.request({
            url: Admin.proxy.API.MgmtPath+"doctor/update",
            method:"Post",
            cors:true,//开启跨域模式
            useDefaultXhrHeader:false,//true的话header会带有X-Requested-With
            //binary:true, //为true后，后端将返回bytes
            params:values,
            success:function(resp){
                var bean = Ext.decode(resp.responseText);
                if(bean.success){
                    Ext.Msg.alert("提示","修改成功");
                    var store = view.BindGrid.getStore();
                    store.reload();
                    view.close();      
                }else{
                    Ext.Msg.alert("提示",bean.message);
                }
            }
        });
    },
    onDoctorDelete:function(){
        var record = arguments[5];
        var grid = this.getView().down("#GridDoctor");
        var store = grid.getStore();
        Ext.Msg.confirm("提示","确定要删除吗？",function(bt){
            var me = this;
            if(bt == "yes"){
                Ext.Ajax.request({
                    url:Admin.proxy.API.MgmtPath+"doctor/delete",
                    cors:true,
                    useDefaultXhrHeader:false,
                    params:{Id:record.get("Id")},
                    method:"Post",
                    success:function(resp){
                        var bean = Ext.decode(resp.responseText);
                        if(bean.success){
                            Ext.Msg.alert("提示","删除成功!");
                            me.close();
                            store.reload();
                        }else{
                            Ext.Msg.alert("提示",bean.message);
                        }
                    }
                });
            }else{
                me.close();
            }
        });
    },
    onBtnSetDoctorPswd:function(){
        var record = arguments[5];
        var win = Ext.Msg.prompt("设置密码","密码",function(btn,text){
            if(btn == "ok"){
                Ext.Ajax.request({
                    url:Admin.proxy.API.MgmtPath+"doctor/updatePwd",
                    cors:true,
                    useDefaultXhrHeader:false,
                    params:{id:record.get("Id"),newPwd:text},
                    method:"Post",
                    success:function(resp){
                        var bean = Ext.decode(resp.responseText);
                        if(bean.success){
                            Ext.Msg.alert("提示","设置成功!");
                        }else{
                            Ext.Msg.alert("提示",bean.message);
                        }
                    }
                });
            }else{
                win.close();
            }
        },this);
    }
});