Ext.define('Admin.view.eva.EvaReport13', {
    extend: 'Ext.window.Window',
    controller: 'EvaData',

    width:600,
    height:600,
    layout:"fit",
    modal:true,
    autoScroll:true,
    listeners:{
        activate: function (argument) {
            var me = this;
            var model = me.getViewModel();
            model.set("name","jack");
            var uid = this.record.data.EvaUid;
            var eva = this.record.data.Eva;
            var report = this.down("#evaReport");
            var urls = me.type && me.type == "app"?Admin.proxy.API.AppDetail:Admin.proxy.API.PcDetail;
            urls += "?uid="+uid+"&eva="+eva;
            Ext.Ajax.request({
                url: urls,
                method:"Get",
                cors:true,//开启跨域模式
                useDefaultXhrHeader:false,//true的话header会带有X-Requested-With
                //binary:true, //为true后，后端将返回bytes
                success:function(resp){
                    var bean = Ext.decode(resp.responseText);
                        if(bean.success){
                        var data = bean.data;
                        let value = [];
                        SCALE_PARTUM_VALUE = Admin.constants.Partum.data;
                            if(data.patientValue && data.patientValue.split("|").length == 6){
                                let patientValue = data.patientValue.split("|")[5].split(";");
                                for( var i in SCALE_PARTUM_VALUE){
                                    if (patientValue.indexOf(SCALE_PARTUM_VALUE[i].id) != -1) {
                                        value.push(SCALE_PARTUM_VALUE[i].title);
                                    }
                                }
                                value = value.join(",");
                            }else{
                                value = "";

                            }

                        var detail = '';
                        if(data.recType == "text"){
                            if (data.programeRecHtml) {
                                detail = data.programeRecHtml.replace(/\r\n/g,"</br>"); 
                            }else{
                                detail = data.programeIntHtml.replace(/\r\n/g,"</br>"); 
                            }
                        }else if(data.recType == "html" || "div"){
                            if (data.programeRecHtml) {
                                detail =  data.programeRecHtml; 
                            }else{
                                detail =  data.programeIntHtml; 
                            }
                        }
                        model.setData(
                            {
                                value: value,
                                report1: data.report1,
                                report2: data.report2,
                                report3: data.report3,
                                report4: data.report4,
                                report5: data.report5,
                                recDetail: detail,

                            }
                        )
                        var chart = me.down("[xtype=cartesian]");
                        var store = chart.getStore();
                        var report = data.report2.split(";")
                        var Report1 = report[0].split(",");
                        var Report2 = report[1].split(",");
                        Report1.forEach(function(bean,index){
                            var value = Report2[index] ;
                            var text = Report2[index];
                            store.insert(index, Ext.create('Admin.model.eva.EvaReportModel',{name:bean,value:value,text:text}));
                        });
                    }else{
                        me.close();
                        Ext.Msg.alert("提示",bean.message);
                    }
                }
            });
        }
    },
    viewModel: {
        data: {
            name: ""
        }
    },
    items: [
        {
            xtype: "container",
            layout: "vbox",
            margin: 20,
            autoScroll:true,
            items: [
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    style: "font-weight: bold;",
                    bind: {  
                        html: '症状' 
                    } 
                },
                {
                    xtype:"container",
                    style: "text-indent: 2em;",
                    bind: {  
                        html: '<p  style="word-wrap: break-word;width: 540px;">{value}</p>',
                    } 
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    style: "font-weight: bold;",
                    bind: {  
                        html: '脏腑健康状态评估结果' 
                    } 
                },
                {
                    xtype:"container",
                    style: "text-indent: 2em;",
                    bind: {  
                        html: '<p  style="word-wrap: break-word;width: 540px;">{report1}</p>',
                    } 
                },
                
                {
                    xtype: 'cartesian',
                    reference: 'chart',
                    width: "100%",
                    height: 200,
                    store: {
                        model: 'Admin.model.eva.EvaReportModel',
                        data: [
                        ]
                    },
                    axes: [ {
                                type: 'numeric',
                                position: 'left',
                                minimum: 0
                            },{
                        type: 'category',
                        position: 'bottom'
                    }],
                    series: [{
                        type: 'bar',
                        xField: 'name',
                        yField: 'value',
                        renderer: function(sprite, storeItem, barAttr, i, store) {    
                            var colors = ['#ffffff','#A7E9FF','#01aaed','#0076c8'];
                                if (arguments[0].attr.dataY[i] < 25) {
                                    colors[i] = "#ffffff";
                                }
                                if (25 <= arguments[0].attr.dataY[i] && arguments[0].attr.dataY[i] < 50) {
                                    colors[i] = "#A7E9FF";
                                }
                                if (50 <= arguments[0].attr.dataY[i] && arguments[0].attr.dataY[i] < 75) {
                                    colors[i] = "#01aaed";
                                }
                                if (arguments[0].attr.dataY[i] >= 75) {
                                    colors[i] = "#0076c8";
                                }
                                barAttr.fill = colors[i];
                                return barAttr; 
                               
                        },
                        style: {
                            minGapWidth: 50,
                        },
                        label: {
                            field: 'text',
                            display: 'insideEnd',
                            orientation:"horizontal"
                        }
                    }]
                },
                {
                    xtype: "container",
                    style: "border: 1px solid #ccc;",
                    layout: "vbox",
                    padding: 20,
                    items: [
                        {
                            xtype:"container",
                            margin: "0 0 10px 0",
                            html:'<span style="font-weight: bold;">分值：</span>分值越高代表对人体影响越大，不同项目之间的对比毫无意义'
                        },
                        {
                            xtype: "container",
                            layout: "hbox",
                            items: [
                                {
                                    xtype: "container",
                                    width: "50px",
                                    height: "20px",
                                    margin: "0 30px 0 0",
                                    style: "background: #A7E9FF",
                                    html: '<div style="height: 20px; width: 50px;"></div>'
                                },
                                {
                                    xtype: "container",
                                    margin: "0 0 10px 0",
                                    html: '<div style="font-weight: bold;">浅蓝色</div><div>表示轻度影响</div>'
                                }
                            ]
                        },
                        {
                            xtype: "container",
                            layout: "hbox",
                            items: [
                                {
                                    xtype: "container",
                                    width: "50px",
                                    height: "20px",
                                    margin: "0 30px 0 0",
                                    style: "background: #01aaed",
                                    html: '<div style="height: 20px; width: 50px;"></div>'
                                },
                                {
                                    xtype: "container",
                                    margin: "0 0 10px 0",
                                    html: '<div style="font-weight: bold;">蓝色</div><div>表示中度影响</div>'
                                }
                            ]
                        },
                        {
                            xtype: "container",
                            layout: "hbox",
                            items: [
                                {
                                    xtype: "container",
                                    width: "50px",
                                    height: "20px",
                                    margin: "0 30px 0 0",
                                    style: "background: #0076c8",
                                    html: '<div style="height: 20px; width: 50px;"></div>'
                                },
                                {
                                    xtype: "container",
                                    margin: "0 0 10px 0",
                                    html: '<div style="font-weight: bold;">深蓝色</div><div>表示影响较大</div>'
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    width: 540,
                    style: "font-weight: bold;text-align: center;",
                    bind: {  
                        html: '方案' 
                    } 
                },
                {
                    xtype:"container",
                    style: "text-indent: 2em;",
                    bind: {  
                        html: '<div style="overflow:hidden;white-space：normal;width: 540px;">{recDetail}</div>',
                    } 
                }
            ]
        }
    ],
   
});
