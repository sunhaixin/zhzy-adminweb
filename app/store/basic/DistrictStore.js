Ext.define('Admin.store.basic.DistrictStore', {
    extend: 'Admin.store.BaseStore',
    alias: 'store.district',

    model: 'Admin.model.basic.DistrictModel',

    autoLoad:false,
    storeId: 'district',
    proxy: {
         type: 'ajax',
         cors:true,
         useDefaultXhrHeader:false,
         url: Admin.proxy.API.MgmtPath+'basic/district',
        //  api: {
        //     create  : 'http://localhost:49712/api/admin/hospital/query',
        //     read  : 'http://localhost:49712/api/admin/hospital/query',
        //     update  : '/controller/update',
        //     destroy : '/controller/destroy_action'
        // },
         reader: {
             type: 'json',
             rootProperty: 'data',
             totalProperty  : 'totalCount',
             successProperty: 'success'
         }
     }
});
