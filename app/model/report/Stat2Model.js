Ext.define('Admin.model.report.Stat2Model', {
    //extend: 'Admin.model.Base',
    extend:'Ext.data.Model',

    idProperty:"name",
    fields: [
        { name: "name"},
        { name: "value1", mapping: 'count1'},
        { name: "value2", mapping: 'count2'},
        { name: "value3", mapping: 'count3'}
    ]
});
