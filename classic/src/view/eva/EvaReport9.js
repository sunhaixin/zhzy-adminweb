Ext.define('Admin.view.eva.EvaReport9', {
    extend: 'Ext.window.Window',
    controller: 'EvaData',
    width:650,
    height:600,
    layout:"fit",
    modal:true,
    listeners:{
        activate: function (argument) {
            var me = this;
            var model = me.getViewModel();
            var uid = this.record.data.EvaUid;
            var eva = this.record.data.Eva;
            var report = this.down("#evaReport");
            var urls = me.type && me.type == "app"?Admin.proxy.API.AppDetail:Admin.proxy.API.PcDetail;
            urls += "?uid="+uid+"&eva="+eva;
            Ext.Ajax.request({
                url: urls,
                method:"Get",
                cors:true,//开启跨域模式
                useDefaultXhrHeader:false,//true的话header会带有X-Requested-With
                //binary:true, //为true后，后端将返回bytes
                success:function(resp){
                    var bean = Ext.decode(resp.responseText);
                    if(bean.success){
                        var data = bean.data;
                        let Ecchymosis,Thickness,Curdygreasiness,Exfoliation,Fatthin,Indentation,Pepeckneedling,Crack;
                        let bianjian;
                        
                        if (data.ecchymosis) {   
                            Ecchymosis=data.ecchymosis.split("|")[0];
                            if (Ecchymosis.indexOf("无")) {
                                Ecchymosis = "无";
                            }
                            Thickness=data.thickness.split("|")[0];

                            Curdygreasiness=data.curdyGreasiness.split("|")[0];
                            if (Curdygreasiness != "无") {
                                Curdygreasiness = "有";
                            }

                            var fu = "无", ni = "无";
                            var s = data.curdyGreasiness.split("|")[0];
                            if(s.indexOf("腻") >= 0){
                                ni = "有";
                            }
                            if(s.indexOf("腐") >= 0){
                                fu = "有";
                            }

                            Exfoliation=data.exfoliation.split("|")[0];
                            Fatthin=data.fatThin.split("|")[0];
                            Indentation=data.indentation.split("|")[0].substr(0,1);
                            Pepeckneedling=data.pepeckNeedling.split("|")[0].substr(0,1);
                            Crack=data.crack.split("|")[0].substr(0,1);

                            if (data.quanSheC.indexOf("边尖红") != -1) {
                                bianjian="红";
                            }else {
                                bianjian="无";
                            }
                        }
                        if (data.step1Pic) {
                            data.image1 = data.step1Pic;
                            data.image2 = data.step2Pic;
                        }
                        model.setData(
                            {
                                Ecchymosis: Ecchymosis,
                                Thickness: Thickness,
                                Curdygreasiness: Curdygreasiness,
                                Exfoliation: Exfoliation,
                                Fatthin: Fatthin,
                                Indentation: Indentation,
                                Pepeckneedling: Pepeckneedling,
                                Crack: Crack,
                                bianjian: bianjian,

                                Image1: data.image1,
                                Image2: data.image2,

                                Quanshel: data.quanSheL,
                                Shezhongl: data.sheZhongL,
                                Shegenl: data.sheGenL,
                                Lshebianl: data.lSheBianL,
                                Rshebianl: data.rSheBianL,
                                Shejianl: data.sheJianL,

                                Quanshea: data.quanSheA,
                                Shezhonga: data.sheZhongA,
                                Shegena: data.sheGenA,
                                Lshebiana: data.lSheBianA,
                                Rshebiana: data.rSheBianA,
                                Shejiana: data.sheJianA,
                                Quansheb: data.quanSheB,
                                Shezhongb: data.quanSheB,
                                Shegenb: data.sheGenB,
                                Lshebianb: data.lSheBianB,
                                Rshebianb: data.rSheBianB,
                                Shejianb: data.sheJianB,
                                Quanshec: data.quanSheC,
                                Shezhongc: data.sheZhongC,
                                Shegenc: data.sheGenC,
                                Lshebianc: data.lSheBianC,
                                Rshebianc: data.rSheBianC,
                                Shejianc: data.sheJianC,

                                CoatquansheL: data.coatQuanSheL,
                                Coatshezhongl: data.coatSheZhongL,
                                Coatshegenl: data.coatSheGenL,
                                Coatlshebianl: data.coatLSheBianL,
                                Coatrshebianl: data.coatRSheBianL,
                                Coatshejianl: data.coatSheJianL,
                                Coatquanshea: data.coatQuanSheA,
                                Coatshezhonga: data.coatSheZhongA,
                                Coatshegena: data.coatSheGenA,
                                Coatlshebiana: data.coatLSheBianA,
                                Coatrshebiana: data.coatRSheBianA,
                                Coatshejiana: data.coatSheJianA,
                                Coatquansheb: data.coatQuanSheB,
                                Coatshezhongb: data.coatSheZhongB,
                                Coatshegenb: data.coatSheGenB,
                                Coatlshebianb: data.coatLSheBianB,
                                Coatrshebianb: data.coatRSheBianB,
                                Coatshejianb: data.coatSheJianB,
                                Coatquanshec: data.coatQuanSheC,
                                Coatshezhongc: data.coatSheZhongC,
                                Coatshegenc: data.coatSheGenC,
                                Coatlshebianc: data.coatLSheBianC,
                                Coatrshebianc: data.coatRSheBianC,
                                Coatshejianc: data.coatSheJianC,

                                ni: ni,
                                fu: fu

                            }
                        )
                        
                    }else{
                        me.close();
                        Ext.Msg.alert("提示",bean.message);
                    }
                }
            });
        }
    },
    viewModel: {
        data: {
            name: ""
        }
    },
    items: [
        {
            xtype: "container",
            layout: "vbox",
            margin: 20,
            autoScroll:true,
            items: [
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    style: "font-weight: bold;",
                    bind: {  
                        html: '舌象照片' 
                    } 
                },
                {
                    xtype:"container",
                    // height: 200,
                    bind: {  
                        html: '<img src="{Image1}" height="510px" width="500px">',
                    } 
                },
                {
                    xtype:"container",
                    // height: 200,
                    margin: "20px 0 0 0",
                    bind: {  
                        html: '<img src="{Image2}" height="510px"  width="500px">',
                    } 
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    style: "font-weight: bold;",
                    bind: {  
                        html: '舌色参数' 
                    } 
                },
                {
                    xtype:"container",
                    bind: {  
                        html: '<table border="1px solid #000" cellpadding="0" cellspacing="0" >'
                        +'<tr>'
                          +'<td style="padding: 10px; text-align: center;">色彩空间</td>'
                          +'<td style="padding: 10px; text-align: center;">测量标准</td>'
                          +'<td style="padding: 10px; text-align: center;">全舌</td>'
                          +'<td style="padding: 10px; text-align: center;">舌中</td>'
                          +'<td style="padding: 10px; text-align: center;">舌根</td>'
                          +'<td style="padding: 10px; text-align: center;">舌边(左)</td>'
                          +'<td style="padding: 10px; text-align: center;">舌边(右)</td>'
                          +'<td style="padding: 10px; text-align: center;">舌尖</td>'
                        +'</tr>'
                        
                        +'<tr>'
                          +'<td style="padding: 10px; text-align: center;" ></td>'
                          +'<td style="padding: 10px; text-align: center;">L</td>'
                          +'<td style="padding: 10px; text-align: center;">{Quanshel}</td>'
                          +'<td style="padding: 10px; text-align: center;">{Shezhongl}</td>'
                          +'<td style="padding: 10px; text-align: center;">{Shegenl}</td>'
                          +'<td style="padding: 10px; text-align: center;">{Lshebianl}</td>'
                          +'<td style="padding: 10px; text-align: center;">{Rshebianl}</td>'
                          +'<td style="padding: 10px; text-align: center;">{Shejianl}</td>'
                        +'</tr>'
                        +'<tr>'
                          +'<td style="padding: 10px; text-align: center;" >Lab</td>'
                          +'<td style="padding: 10px; text-align: center;">a</td>'
                          +'<td style="padding: 10px; text-align: center;">{Quanshea}</td>'
                          +'<td style="padding: 10px; text-align: center;">{Shezhonga}</td>'
                          +'<td style="padding: 10px; text-align: center;">{Shegena}</td>'
                          +'<td style="padding: 10px; text-align: center;">{Lshebiana}</td>'
                          +'<td style="padding: 10px; text-align: center;">{Rshebiana}</td>'
                          +'<td style="padding: 10px; text-align: center;">{Shejiana}</td>'
                        +'</tr>'
                        +'<tr>'
                          +'<td style="padding: 10px; text-align: center;" ></td>'
                          +'<td style="padding: 10px; text-align: center;">b</td>'
                          +'<td style="padding: 10px; text-align: center;">{Quansheb}</td>'
                          +'<td style="padding: 10px; text-align: center;">{Shezhongb}</td>'
                          +'<td style="padding: 10px; text-align: center;">{Shegenb}</td>'
                          +'<td style="padding: 10px; text-align: center;">{Lshebianb}</td>'
                          +'<td style="padding: 10px; text-align: center;">{Rshebianb}</td>'
                          +'<td style="padding: 10px; text-align: center;">{Shejianb}</td>'
                        +'</tr>'

                        +'<tr>'
                          +'<td style="padding: 10px; text-align: center;">舌色类别</td>'
                          +'<td style="padding: 10px; text-align: center;"></td>'
                          +'<td style="padding: 10px; text-align: center;">{Quanshec}</td>'
                          +'<td style="padding: 10px; text-align: center;">{Shezhongc}</td>'
                          +'<td style="padding: 10px; text-align: center;">{Shegenc}</td>'
                          +'<td style="padding: 10px; text-align: center;">{Lshebianc}</td>'
                          +'<td style="padding: 10px; text-align: center;">{Rshebianc}</td>'
                          +'<td style="padding: 10px; text-align: center;">{Shejianc}</td>'
                        +'</tr>'
                      +'</table>',
                    } 
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    style: "font-weight: bold;",
                    bind: {  
                        html: '苔色参数' 
                    } 
                },
                {
                    xtype:"container",
                    bind: {  
                        html: '<table border="1px solid #000" cellpadding="0" cellspacing="0">'
                            +'<tr>'
                              +'<td style="padding: 10px; text-align: center;">色彩空间</td>'
                              +'<td style="padding: 10px; text-align: center;">测量标准</td>'
                              +'<td style="padding: 10px; text-align: center;">全舌</td>'
                              +'<td style="padding: 10px; text-align: center;">舌中</td>'
                              +'<td style="padding: 10px; text-align: center;">舌根</td>'
                              +'<td style="padding: 10px; text-align: center;">舌边(左)</td>'
                              +'<td style="padding: 10px; text-align: center;">舌边(右)</td>'
                              +'<td style="padding: 10px; text-align: center;">舌尖</td>'
                            +'</tr>'
                            
                            +'<tr>'
                              +'<td style="padding: 10px; text-align: center;" ></td>'
                              +'<td style="padding: 10px; text-align: center;">L</td>'
                              +'<td style="padding: 10px; text-align: center;">{CoatquansheL}</td>'
                              +'<td style="padding: 10px; text-align: center;">{Coatshezhongl}</td>'
                              +'<td style="padding: 10px; text-align: center;">{Coatshegenl}</td>'
                              +'<td style="padding: 10px; text-align: center;">{Coatlshebianl}</td>'
                              +'<td style="padding: 10px; text-align: center;">{Coatrshebianl}</td>'
                              +'<td style="padding: 10px; text-align: center;">{Coatshejianl}</td>'
                            +'</tr>'
                            +'<tr>'
                              +'<td style="padding: 10px; text-align: center;" >Lab</td>'
                              +'<td style="padding: 10px; text-align: center;">a</td>'
                              +'<td style="padding: 10px; text-align: center;">{Coatquanshea}</td>'
                              +'<td style="padding: 10px; text-align: center;">{Coatshezhonga}</td>'
                              +'<td style="padding: 10px; text-align: center;">{Coatshegena}</td>'
                              +'<td style="padding: 10px; text-align: center;">{Coatlshebiana}</td>'
                              +'<td style="padding: 10px; text-align: center;">{Coatrshebiana}</td>'
                              +'<td style="padding: 10px; text-align: center;">{Coatshejiana}</td>'
                            +'</tr>'
                            +'<tr>'
                              +'<td style="padding: 10px; text-align: center;" ></td>'
                              +'<td style="padding: 10px; text-align: center;">b</td>'
                              +'<td style="padding: 10px; text-align: center;">{Coatquansheb}</td>'
                              +'<td style="padding: 10px; text-align: center;">{Coatshezhongb}</td>'
                              +'<td style="padding: 10px; text-align: center;">{Coatshegenb}</td>'
                              +'<td style="padding: 10px; text-align: center;">{Coatlshebianb}</td>'
                              +'<td style="padding: 10px; text-align: center;">{Coatrshebianb}</td>'
                              +'<td style="padding: 10px; text-align: center;">{Coatshejianb}</td>'
                            +'</tr>'

                            +'<tr>'
                              +'<td style="padding: 10px; text-align: center;">苔色类别</td>'
                              +'<td style="padding: 10px; text-align: center;"></td>'
                              +'<td style="padding: 10px; text-align: center;">{Coatquanshec}</td>'
                              +'<td style="padding: 10px; text-align: center;">{Coatshezhongc}</td>'
                              +'<td style="padding: 10px; text-align: center;">{Coatshegenc}</td>'
                              +'<td style="padding: 10px; text-align: center;">{Coatlshebianc}</td>'
                              +'<td style="padding: 10px; text-align: center;">{Coatrshebianc}</td>'
                              +'<td style="padding: 10px; text-align: center;">{Coatshejianc}</td>'
                            +'</tr>'
                          +'</table>',
                    } 
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    style: "font-weight: bold;",
                    bind: {  
                        html: '舌象信息输出' 
                    } 
                },
                {
                    xtype:"container",
                    bind: {  
                        html: '<table border="1px solid #000" cellpadding="0" cellspacing="0" >'
                +'<tr>'
                  +'<td style="padding: 3px; text-align: center;" rowspan="2">舌色</td>'
                  +'<td style="padding: 3px; text-align: center;" colspan="2">局部特征</td>'
                  +'<td style="padding: 3px; text-align: center;" rowspan="2">苔色</td>'
                  +'<td style="padding: 3px; text-align: center;" colspan="4">苔质</td>'
                  +'<td style="padding: 3px; text-align: center;" colspan="4">舌形</td>'
                +'</tr>'
                
                +'<tr>'
                  +'<td style="padding: 3px; text-align: center;">边尖红</td>'
                  +'<td style="padding: 3px; text-align: center;">瘀斑瘀点</td>'
                  +'<td style="padding: 3px; text-align: center;">厚薄</td>'
                  +'<td style="padding: 3px; text-align: center;">腻</td>'
                  +'<td style="padding: 3px; text-align: center;">腐</td>'
                  +'<td style="padding: 3px; text-align: center;">苔剥</td>'
                  +'<td style="padding: 3px; text-align: center;">胖瘦</td>'
                  +'<td style="padding: 3px; text-align: center;">齿痕</td>'
                  +'<td style="padding: 3px; text-align: center;">点刺</td>'
                  +'<td style="padding: 3px; text-align: center;">裂纹</td>'
                +'</tr>'
                +'<tr>'
                  +'<td style="padding: 3px; text-align: center;" >{Quanshec}</td>'
                  +'<td style="padding: 3px; text-align: center;">{bianjian}</td>'
                  +'<td style="padding: 3px; text-align: center;">{Ecchymosis}</td>'
                  +'<td style="padding: 3px; text-align: center;">{Coatquanshec}</td>'
                  +'<td style="padding: 3px; text-align: center;">{Thickness}</td>'
                  +'<td style="padding: 3px; text-align: center;">{ni}</td>'
                  +'<td style="padding: 3px; text-align: center;">{fu}</td>'
                  +'<td style="padding: 3px; text-align: center;">{Exfoliation}</td>'
                  +'<td style="padding: 3px; text-align: center;">{Fatthin}</td>'
                  +'<td style="padding: 3px; text-align: center;">{Indentation}</td>'
                  +'<td style="padding: 3px; text-align: center;">{Pepeckneedling}</td>'
                  +'<td style="padding: 3px; text-align: center;">{Crack}</td>'
                +'</tr>'
              +'</table>',
                    } 
                },
            ]
        }
    ],
   
});
