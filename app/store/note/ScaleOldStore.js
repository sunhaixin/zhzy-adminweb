Ext.define('Admin.store.note.ScaleOldStore', {
    extend: 'Admin.store.BaseStore',
    alias: 'store.scaleold',

    model: 'Admin.model.note.ScaleOldModel',

    autoLoad: false,
    storeId: 'scaleold',
    proxy: {
         type: 'ajax',
         cors:true,
         useDefaultXhrHeader:false,
         url: Admin.proxy.API.BasePath+"api/admin/scale/old/query",
         reader: {
             type: 'json',
             rootProperty: 'Data',
             totalProperty  : 'Total',
             successProperty: 'IsSuccess'
         }
     }
});
