Ext.define('Admin.view.telemedicine.Group', {
    extend:"Admin.view.main.BaseContainer",
    xtype: 'telemedicine_group',
    controller: 'group',
    layout: 'hbox',
    listeners:{
        activate:function(){
            var grid = this.down("[xtype=listpanel]");
            if (grid) {
                var store = grid.getStore();
                var hid = sessionStorage.getItem("Hid");
                if (sessionStorage.getItem("MyPrvileges").indexOf("telemedicine_group_see_self") !=-1) {
                    store.proxy.url = Admin.proxy.API.MgmtPath + "/telemedicineGroup/findByHid?hid=" + hid;
                }else{
                    store.getProxy().extraParams = {
                        "hid":hid,
                        "code": "system"
                    };
                }
                store.load();
            }
        }
    },
    margin:20,
    items: [
        {
            xtype: "container",
            layout: 'vbox',
            cls:"content-Panel-BorderStyle",
            height:"100%",
            autoScroll:true,
            items: [
                {
                    xtype:"listpanel",
                    width:300,
                    
                    id:"grdGroupLeft",
                    privileges:["telemedicine_group_see_all","telemedicine_group_see_self"],
                    title:"远程医疗小组名称",
                    tbar:[
                        {
                            xtype:"container",
                            layout: 'vbox',
                            width:"100%",
                            items:[
                                {
                                    xtype:"button",
                                    text:"新增远程医疗小组",
                                    privileges:["telemedicine_group_add"],
                                    width:"100%",
                                    handler:function(){
                                        var win = Ext.create("Admin.view.telemedicine.WinGroup");
                                        win.setTitle("添加远程医疗小组");
                                        win.BindGrid = this.ownerCt.ownerCt.ownerCt;
                                        win.type = "Add"
                                        win.show();
                                    }
                                },{
                                    xtype: "container",
                                    layout: 'hbox',
                                    margin: "10 0 0 0",
                                    width:"100%",
                                    items: [
                                        {
                                            xtype: "textfield",
                                            flex: 1,
                                            fieldLabel: '搜索',
                                            labelWidth: 50,
                                            listeners: {
                                                change: "onTelemedicineHospitalFielts"
                                            }
                                        }
                                    ]
                                }
                            ]
                        }
                    ],
                    store:Ext.create("Admin.store.telemedicine.GroupStore",{
                        pageSize:1000
                        
                    }),
                    columns:[
                        {
                            xtype:"templatecolumn",
                            menuDisabled:true,
                            flex:1,
                            tpl:"<div style='height:30px;line-height:30px;cursor:pointer;'>{Name}</div>"
                        }
                    ],
                    listeners:{
                        itemClick:"onHospitalItemClick",
                        itemcontextmenu:function(cmp, record, item, index, e, eOpts ){
                            var me = this;
                            //禁止浏览器右键菜单事件
                            e.preventDefault();  
                            e.stopEvent();
                            var myprivileges = Ext.decode(sessionStorage.getItem("privileges"));
                            var privileges = [];
                            myprivileges.forEach(function(bean){
                                privileges.push(bean.code);
                            });
                            if (privileges.indexOf("telemedicine_group_delete") != -1) {
                                var menu = Ext.create("Admin.view.telemedicne.TelemedicneDelete").showAt(e.getXY());
                                menu.selectedNodeId = record.data.id;
                                menu.type = 'groupDelete';
                                var item = menu.down('#telemedicnedelete');
                                item.setText('删除该远程医疗小组');
                            }else{
                            }
                        }
                    }
                }
            ]
            
        },
        {
            xtype:"grid",
            title:"远程医疗小组成员",
            privileges:["telemedicine_group_see_all","telemedicine_group_see_self"],
            cls:"content-Panel-BorderStyle",
            flex:1,
            margin:"0 0 0 20",
            height:"100%",
            itemId:"GridDepartment",
            id: "grdGroupright",
            style:"border:1px solid #d0d0d0 !important",
            //padding:"20 200",
            tbar:[
                {
                    xtype:"container",
                    layout:"hbox",
                    defaults:{
                        margin:"0 5"
                    },
                    items:[
                        {
                            xtype:"button",
                            text:"添加成员",
                            privileges:["telemedicine_group_add"],
                            handler:function(){
                                var grid = Ext.getCmp("grdGroupLeft");
                                var record = grid.getSelectionModel().getLastSelected();
                                var win = Ext.create("Admin.view.telemedicine.WinGroupHospital");
                                var frm = Ext.getCmp("idfrmGroupHospital");
                                frm.InitFormGrid = record;
                                win.setTitle("添加成员");
                                win.BindGrid = this.ownerCt.ownerCt.ownerCt;
                                win.type = "HospitalAdd";
                                win.show();
                            }
                        }
                    ]
                }
            ],
            store:Ext.create("Admin.store.telemedicine.GroupHospitalStore",{
                autoLoad: false
            }),
            columns:[
                {
                    width:60,
                    xtype:"rownumberer"
                },{
                    text:"医院名称",
                    flex:1,
                    dataIndex:"HospitalName"
                },
            ],
            bbar:[
                {
                    xtype: 'pagingtoolbar',
                    store: Ext.data.StoreManager.lookup('grouphospital'),
                    displayInfo: true,
                    displayMsg: '显示第 {0} 条到 {1} 条记录，一共 {2} 条',
                    emptyMsg: "没有数据可以显示"
                }
            ],
            listeners:{
                itemcontextmenu:function(cmp, record, item, index, e, eOpts ){
                    var me = this;
                    //禁止浏览器右键菜单事件
                    e.preventDefault();  
                    e.stopEvent();
                    var myprivileges = Ext.decode(sessionStorage.getItem("privileges"));
                    var privileges = [];
                    myprivileges.forEach(function(bean){
                        privileges.push(bean.code);
                    });
                    if (privileges.indexOf("telemedicine_group_hospital_delete") != -1) {
                        var menu = Ext.create("Admin.view.telemedicne.TelemedicneDelete").showAt(e.getXY());
                        menu.selectedNodeId = record.data.id;
                        menu.type = 'hospitalDelete';
                        var item = menu.down('#telemedicnedelete');
                        item.setText('删除该成员');
                    }
                }
            }
        }
    ]
});
