Ext.define('Admin.view.yilianti.WinYiliantiGroup', {
    extend: 'Ext.window.Window',
    controller: 'yilianti',
    width:400,
    height:160,
    layout:"fit",
    modal:true,
    items: [
        {
            xtype:"form",
            layout: 'anchor',
            defaultType: 'textfield',
            padding:"10 20",
            itemId:"frmYiliantiGroup",
            defaults: {
                anchor: '100%',
                labelWidth:80
            },
            items:[
                {
                    xtype:"textfield",
                    fieldLabel:"<span style='color:red'>*</span>医联体名称",
                    name:"name",
                    allowBlank: false
                }
            ]
        }
    ],
    bbar:[
        {
            xtype:"container",
            flex:1,
            layout:{
                type:"hbox",
                pack:"center",
                align:"center"
            },
            padding:"0 0 10 0",
            items:[
                {
                    xtype:"tbspacer"
                },{
                    xtype:"button",
                    text:"提交",
                    handler:"onSubmitButtonClick"
                }
            ]
        }
    ]
});
