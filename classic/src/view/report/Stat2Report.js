Ext.define("Admin.view.report.Stat2Report",{
    extend: 'Ext.container.Container',
    xtype: 'stat2report',
    height:"100%",
    layout:"hbox",
    cls:"content-Panel-BorderStyle",
    margin: 20,
    controller:"report",
    gridId: "stat2report_grid",
    listeners:{
        afterrender:"onReportRender"
    },
    items:[
        {
            xtype:"panel",
            title:"地区常住人口年龄分布",
            flex:1,
            height:"100%",
            style:"border:1px solid #ccc",
            layout:{
                type:"hbox",
                pack:"center"
            },
            autoScroll:true,
            items:[
                {
                    width:900,
                    height:500,
                    layout:{
                        type:"hbox",
                    },
                    autoScroll: true,
                    items:[
                        {
                            xtype:"grid",
                            width:300,
                            // height:"100%",
                            id: "stat2report_grid",
                            store:Ext.create("Admin.store.report.Stat2Store",{autoLoad:false}),
                            style:"border:1px solid #ccc;",
                            // height:"100%",
                            margin:10,
                            columns:[
                                {
                                    text:"年龄组段",
                                    dataIndex:"name",
                                    flex:1,
                                },{
                                    text:"男",
                                    dataIndex:"value1"
                                },{
                                    text:"女",
                                    dataIndex:"value2"
                                }
                            ]
                        },
                        {
                            xtype:"grid",
                            width:300,
                            // height:"100%",
                            id: "stat2report_1_grid",
                            store:Ext.create("Admin.store.report.Stat2_1Store",{autoLoad:false}),
                            style:"border:1px solid #ccc;",
                            // height:"100%",
                            margin:10,
                            columns:[
                                {
                                    text:"年龄组段",
                                    dataIndex:"name",
                                    flex:1
                                },{
                                    text:"男",
                                    dataIndex:"value1",
                                    flex:1
                                },{
                                    text:"女",
                                    dataIndex:"value2",
                                    flex:1
                                },{
                                    text:"合计",
                                    dataIndex:"value3",
                                    flex:1
                                }
                            ]
                        },{
                            xtype: 'cartesian',
                            flex:1,
                            // width: 800,
                            reference: 'chart',
                            height: "100%",
                            insetPadding: {
                                top: 40,
                                bottom: 40,
                                left: 20,
                                right: 40
                            },
                            store:Ext.data.StoreManager.lookup("stat2report"),
                            axes: [{
                                type: 'numeric',
                                position: 'left',
                                adjustByMajorUnit: true,
                                fields: ['value1'],
                                grid: true,
                                minimum: 0
                            }, {
                                type: 'category',
                                fields: ['name'],
                                grid:true,
                                position: 'bottom'
                            }],
                            series: [{
                                type: 'bar',
                                stacked:false,
                                xField: 'name',
                                yField: ['value1','value2'],
                                label: {
                                    field: ['value1','value2'],
                                    display: 'insideEnd',
                                    orientation:"horizontal"
                                }
                            }]
                        }
                    ]
                }
                
            ]
        }
    ]
});