Ext.define('Admin.view.eva.EvaReport18', {
    extend: 'Ext.window.Window',
    controller: 'EvaData',
    width:650,
    height:600,
    layout:"fit",
    modal:true,
    listeners:{
        activate: function (argument) {
            var me = this;
            var model = me.getViewModel();
            var uid = this.record.data.EvaUid;
            var eva = this.record.data.Eva;
            var urls = me.type && me.type == "app"?Admin.proxy.API.AppDetail:Admin.proxy.API.PcDetail;
            urls += "?uid="+uid+"&eva="+eva;
            Ext.Ajax.request({
                url: urls,
                method:"Get",
                cors:true,//开启跨域模式
                useDefaultXhrHeader:false,//true的话header会带有X-Requested-With
                //binary:true, //为true后，后端将返回bytes
                success:function(resp){
                    var bean = Ext.decode(resp.responseText);
                    if(bean.success){
                        var data = bean.data;
                        var Rectitles = Ext.getCmp("Rectitles1");
                        var items = [];
                        Rectitles.removeAll();
                        if(data){
                            model.setData(
                                {
                                    unit: data.unit ? data.unit : "暂无数据",
                                    photoList: data.photoList,
                                    comment: data.comment
                                }
                            )
                            var photoList = data.photoList;
                            sessionStorage.removeItem("photoList");
                            sessionStorage.setItem("photoList", JSON.stringify(photoList));
                            photoList ? photoList.forEach(function(bean,index){
                                items[items.length] = Admin.util.ProgrameList.imageFile18(bean);
                            }):"";
                            Rectitles.add(items);
                            
                        }else{
                            sessionStorage.removeItem("photoList");
                            let obj = {mageFile: "",part: "",uid: ""};
                            items[items.length] = Admin.util.ProgrameList.imageFile18(obj);
                            Rectitles.add(items);
                        }
                        
                    }else{
                        me.close();
                        Ext.Msg.alert("提示",bean.message);
                    }
                }
            });
        }
    },
    viewModel: {
        data: {
            name: ""
        }
    },
    items: [
        {
            xtype: "container",
            layout: "vbox",
            margin: 20,
            autoScroll:true,
            items: [
                {	
                    xtype: "combobox",
                    labelWidth: 80,
                    fieldLabel: "图片规格",
                    name:"ImageType",
                    editable:false,
                    allowBland:false,
                    displayField:"value",
                    valueField: "imgtype",
                    itemId:"ImageTypes",
                    value: 0,
                    store: {
                        fields:[
                            "imgtype","value"
                        ],
                        autoLoad: true,
                        data: [
                            { imgtype: 0, value: "大图", width: 125, height:175 },
                            { imgtype: 1, value: "小图", width: 250, height:350 },
                        ]
                    },
                    listeners: {
                        change: function(v,n,o){
                            var Rectitles = Ext.getCmp("Rectitles1");
                            let obj = {mageFile: "",part: "",uid: ""};
                            Rectitles.removeAll(); var items = [];
                            var photoList = JSON.parse(sessionStorage.getItem("photoList"));
                            if(photoList){
                                photoList.forEach(function(bean,index){
                                    if(n== 1){
                                        items[items.length] = Admin.util.ProgrameList.imageFile18(bean,175,125);
                                    }else{
                                        items[items.length] = Admin.util.ProgrameList.imageFile18(bean,350,250);
                                    }
                                });
                                Rectitles.add(items);
                            }else{
                                if(n == 1){
                                    items[items.length] = Admin.util.ProgrameList.imageFile18(obj,175,125);  
                                }else{
                                    items[items.length] = Admin.util.ProgrameList.imageFile18(obj,350,250);
                                }
                                Rectitles.add(items);
                            }
                        }
                    }
                },
                {
                    xtype:"container",
                    id: "Rectitles1",
                    layout:{
                        type: "table",
                        columns: 2
                    }
                },
            ]
        }
    ],
   
});
