Ext.define('Admin.view.note.ScaleOld', {
    extend: 'Ext.container.Container',
    xtype: 'scaleold',
    layout: 'fit',
    listeners:{
        activate:function(){
            var grid = this.down("[xtype=grid]");
            grid.getStore().load();
        }
    },
    padding:10,
    items: [
        {
            xtype:"grid",
            title:"老年人测评管理",
            selModel: {
                selType: 'checkboxmodel',
                checkOnly: true,
                showHeaderCheckbox: true
            },
            store:Ext.create("Admin.store.note.ScaleOldStore"),
            columns:[
                {
                    text:"医生姓名",
                    dataIndex:"DoctorName"
                },
                {
                    text:"患者姓名",
                    dataIndex:"PatientName"
                },
                {
                    text:"来源",
                    dataIndex:"Source"
                },
                {
                    text:"版本号",
                    dataIndex:"Version"
                },
                {
                    text:"报告1",
                    dataIndex:"Report1",
                    flex:1
                },
                {
                    text:"报告2",
                    dataIndex:"Report2",
                    flex:1
                },
                {
                    text:"报告3",
                    dataIndex:"Report3",
                    flex:1
                },
                {
                    text:"创建时间",
                    dataIndex:"CreateTime",
                    width:160,
                    xtype:"datecolumn",
                    format:"Y-m-d H:i:s"
                }
            ],
            bbar:[
                {
                    xtype: 'pagingtoolbar',
                    store: Ext.data.StoreManager.lookup('scaleold'),
                    displayInfo: true,
                    displayMsg: '显示第 {0} 条到 {1} 条记录，一共 {2} 条',
                    emptyMsg: "没有数据可以显示"
                }
            ]
        }
    ]
});
