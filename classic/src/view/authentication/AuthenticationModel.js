Ext.define('Admin.view.authentication.AuthenticationModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.authentication',

    data: {
        Loginid : '',
        Pswd : '',
        persist: false,
        agrees : false
    }
});