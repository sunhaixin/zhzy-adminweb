Ext.define('Admin.model.report.Stat18Model', {
    //extend: 'Admin.model.Base',
    extend:'Ext.data.Model',
    
    idProperty:"name",
    fields: [
        { name: "name" },
        { name: "value",type:"int" }
    ]
});
