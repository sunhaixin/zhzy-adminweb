Ext.define('Admin.store.report.StatNo1Store', {
    extend: 'Admin.store.BaseStore',
    alias: 'store.statno1report',

    model: 'Admin.model.report.Stat1Model',

    autoLoad:false,
    storeId: 'statno1report',
    proxy: {
         type: 'ajax',
         cors:true,
         useDefaultXhrHeader:false,
         url: Admin.proxy.API.MgmtPath + 'stat/1',
         reader: {
             type: 'json',
             rootProperty: 'data',
             totalProperty  : 'Total',
             successProperty: 'IsSuccess'
         }
     }
});
