Ext.define('Admin.store.hospital.DepartmentStore', {
    extend: 'Admin.store.BaseStore',
    alias: 'store.department',

    model: 'Admin.model.hospital.DepartmentModel',

    autoLoad: false,
    storeId: 'department',
    proxy: {
         type: 'ajax',
         cors:true,
         useDefaultXhrHeader:false,
         url: Admin.proxy.API.MgmtPath+"department/pageDepartment",
         reader: {
             type: 'json',
             rootProperty: 'data',
             totalProperty  : 'totalCount',
             successProperty: 'success'
         }
     }
});
