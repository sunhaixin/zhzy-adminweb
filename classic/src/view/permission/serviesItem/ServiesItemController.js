Ext.define("Admin.view.permission.serviesItem.ServiesItemController",{
    extend: "Ext.app.ViewController",
    alias: "controller.serviesItem",
   	
   	onSubmitButtonClick: function(argument) {
   		var view = this.getView();
        if(view.type == "Add"){
            this.onAddServiesItemSubmit(arguments);
        }else{
            this.onUpdateServiesItemSubmit(arguments);
        }
   	},
   	onAddServiesItemSubmit: function(){
   		var view = this.getView();
        var frm = view.down("#frmServiesItem");
        var valus = frm.getValues();
         Ext.Ajax.request({
            url:  Admin.proxy.API.MgmtPath+"serviesItem/add",
            method:"Post",
            cors:true,//开启跨域模式
            useDefaultXhrHeader:false,
            params: valus,
            success:function(resp){
                var bean = Ext.decode(resp.responseText);
                if(bean.code == 200){
                    Ext.Msg.alert("提示",bean.message);
                    var store = view.BindGrid.getStore();
                    if (store) {
                        store.reload();
                    }
                    if (view) {
                        view.close(); 
                    }  
                }else{
                    Ext.Msg.alert("提示",bean.message);
                }
            }
        });
   	}
})