Ext.define("Admin.store.yilianti.YiliantiHospitalStore",{
    extend: 'Ext.data.TreeStore',
    alias: 'store.yiliantihospital',

    fields: [
        { name: 'id', mapping: "id", type: 'string'},
		{ name: 'text', mapping: "hospitalName", type: 'string'},
		{ name: 'parentId', mapping: "parentNode" },
        { name: 'path', mapping: "path", type: 'string' },
        { name: 'index', mapping: "mtcIndex", type: 'int' },
        { name: 'leaf', mapping: "leaf", type: "boolean" }
    ],

    autoLoad: false,
    storeId: 'yiliantihospital',
    proxy: {
         type: 'ajax',
         cors:true,
         useDefaultXhrHeader:false,
         url: Admin.proxy.API.MgmtPath + "MedicalTreatmentCombination/hospital/query",
         reader: {
             type: 'json',
             rootProperty: 'children',
             successProperty: 'success'
         }
     }
})