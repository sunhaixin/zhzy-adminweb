Ext.define('Admin.store.hospital.HospitalGetStore', {
    extend: 'Admin.store.BaseStore',
    alias: 'store.hospital',

    model: 'Admin.model.hospital.HospitalGetModel',
    // autoLoad: false,
    autoLoad: true,
    sorters: [{  
        //先按id降序  
        property: 'Id',  
        direction: 'DESC'  
    }],
    storeId: 'hospital',
    proxy: {
         type: 'ajax',
         cors:true,
         useDefaultXhrHeader:false,
         url: Admin.proxy.API.MgmtPath+"hospital/getList",
         reader: {
             type: 'json',
             rootProperty: 'data',
             totalProperty  : 'totalCount',
             successProperty: "success"
         }
     }
});
