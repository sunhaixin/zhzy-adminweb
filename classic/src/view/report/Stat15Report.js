Ext.define("Admin.view.report.Stat15Report",{
    extend: 'Ext.container.Container',
    xtype: 'stat15report',
    height:"100%",
    layout:"hbox",
    controller:"report",
    cls:"content-Panel-BorderStyle",
    margin: 20,
    gridId: "stat15report_grid",
    listeners:{
        afterrender:"onReportRender"
    },
    items:[
        {
            xtype:"panel",
            title:"糖尿病常见证型分布Top5",
            flex:1,
            height:"100%",
            style:"border:1px solid #ccc",
            layout:{
                type:"hbox",
                pack:"center"
            },
            autoScroll:true,
            items:[
                {
                    width:1100,
                    height:500,
                    layout:"vbox",
                    items:[
                        {
                            xtype:"HGrid",
                            autoHeight:true,
                            width:"100%",
                            id: "stat15report_grid",
                            BindStore:Ext.create("Admin.store.report.Stat15Store",{autoLoad:false}),
                            style:"border:1px solid #ccc",
                            margin:10
                        }
                        ,{
                            xtype: 'cartesian',
                            flex:1,
                            reference: 'chart',
                            width: "100%",
                            insetPadding: {
                                top: 40,
                                bottom: 40,
                                left: 20,
                                right: 40
                            },
                            store:Ext.data.StoreManager.lookup("stat15report"),
                            axes: [{
                                type: 'numeric',
                                position: 'left',
                                minimum: 0
                            }, 
                            {
                                type: 'category',
                                position: 'bottom'
                            }
                            ],
                            series: [
                                {
                                    type: 'bar',
                                    xField: 'name',
                                    yField: 'value',
                                    style: {
                                        minGapWidth: 100
                                    },
                                    label: {
                                        field: 'value',
                                        display: 'insideEnd',
                                        orientation:"horizontal"
                                    }
                                }
                                
                            ]
                        }
                    ]
                }
            ]
        }
    ]
});

