Ext.define('Admin.view.permission.privilegeMgr.WinAddPrivilege', {
    extend: 'Ext.window.Window',
    controller: 'privilegeMgr',
    width:500,
    height:270,
    layout:"fit",
    modal:true,
    items: [
        {
            xtype:"form",
            layout: 'anchor',
            defaultType: 'textfield',
            padding:"10 20",
            itemId:"frmAddPrivilege",
            defaults: {
                anchor: '100%',
                labelWidth:100
            },
            items:[
                {
                    fieldLabel:"<span style='color:red'>*</span>权限名称",
                    name:"name",
                    allowBlank: false,
                    listeners:{
                        render:function(){
                            var record = this.ownerCt.ownerCt.record;
                            if (record) {
                                var value = this.ownerCt.ownerCt.record.data.Name;
                                this.setValue(value);
                            }
                        }
                    }
                },
                {
                    fieldLabel:"<span style='color:red'>*</span>Code",
                    name:"code",
                    allowBlank: false,
                    listeners:{
                        render:function(){
                            var record = this.ownerCt.ownerCt.record;
                            if (record) {
                                var value = this.ownerCt.ownerCt.record.data.Code;
                                this.setValue(value);
                            }
                        }
                    }
                },
                {
                    xtype: "container",
                    html: "<span style='color:#CCC;font-size:'>互斥项GroupName必填且相同</span>"
                },
                {
                    fieldLabel:"GroupName",
                    name:"groupName",
                    allowBlank:true,
                    listeners:{
                        render:function(){
                            var record = this.ownerCt.ownerCt.record;
                            if (record) {
                                var value = this.ownerCt.ownerCt.record.data.GroupName;
                                this.setValue(value);
                            }
                        }
                    }
                }
            ]
        }
    ],
    bbar:[
        {
            xtype:"container",
            flex:1,
            layout:{
                type:"hbox",
                pack:"center",
                align:"center"
            },
            padding:"0 0 10 0",
            items:[
                {
                    xtype:"tbspacer"
                },{
                    xtype:"button",
                    text:"提交",
                    handler:"onPrivilegeSubmit"
                }
            ]
        }
    ]
});
