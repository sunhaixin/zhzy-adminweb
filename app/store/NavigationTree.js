Ext.define('Admin.store.NavigationTree', {
    extend: 'Ext.data.TreeStore',

    storeId: 'NavigationTree',
    fields: [
        { name: 'id', mapping: "id", type: 'string'},
        { name: 'text', mapping: "name", type: 'string'},
        { name: 'parentId', mapping: "parentId" },
        { name: 'path', mapping: "path", type: 'string' },
        { name: 'index', mapping: "index", type: 'int' },
        { name: 'leaf', mapping: "isLeaf", type: "boolean" },
        { name: 'checked', mapping: "checked", type: "boolean" },
        { name: 'children', mapping: "children" },
        { name: 'viewType', mapping: "viewType", type: "string" },
        { name: "rowCls" },
        { name: "iconCls" }
    ],
    root:{
        expanded:true
    }
});
