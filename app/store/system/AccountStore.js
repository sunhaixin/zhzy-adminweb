Ext.define('Admin.store.system.AccountStore', {
    extend: 'Admin.store.BaseStore',
    alias: 'store.account',

    model: 'Admin.model.system.AccountModel',

    autoLoad:false,
    storeId: 'account',
    proxy: {
         type: 'ajax',
         cors:true,
         useDefaultXhrHeader:false,
         url: Admin.proxy.API.MgmtPath+'account/query',
         reader: {
             type: 'json',
             rootProperty: 'data',
             totalProperty  : 'totalCount',
             successProperty: 'success'
         }
     }
});
