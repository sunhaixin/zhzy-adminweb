Ext.define('Admin.store.pcEvaStat.ServiceHospitalStore', {
    extend: 'Admin.store.BaseStore',
    alias: 'store.serviceHospital',

    fields: [
        {name: "Name", mapping: "name"},
        {name: "Hid", mapping: "hid"},
    ],

    autoLoad:false,
    storeId: 'serviceHospital',
    proxy: {
         type: 'ajax',
         cors:true,
         useDefaultXhrHeader:false,
         url: Admin.proxy.API.MgmtPath+'serviceManage/hospital',
         reader: {
             type: 'json',
             rootProperty: 'data',
             totalProperty  : 'totalCount',
             successProperty: 'success'
         }
     }
});
