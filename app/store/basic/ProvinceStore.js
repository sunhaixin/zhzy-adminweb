Ext.define('Admin.store.basic.ProvinceStore', {
    extend: 'Admin.store.BaseStore',
    alias: 'store.province',

    model: 'Admin.model.basic.ProvinceModel',

    autoLoad: false,
    storeId: 'province',
    proxy: {
         type: 'ajax',
         cors:true,
         useDefaultXhrHeader:false,
         url: Admin.proxy.API.MgmtPath+'basic/province',
         reader: {
             type: 'json',
             rootProperty: 'data',
             totalProperty  : 'totalCount',
             successProperty: 'success'
         }
     }
});
