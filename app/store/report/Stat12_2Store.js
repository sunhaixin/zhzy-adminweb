Ext.define('Admin.store.report.Stat12_2Store', {
    extend: 'Admin.store.BaseStore',
    alias: 'store.stat12report',

    model: 'Admin.model.report.Stat12Model',

    autoLoad:false,
    storeId: 'stat12_2report',
    proxy: {
         type: 'ajax',
         cors:true,
         useDefaultXhrHeader:false,
         url: Admin.proxy.API.MgmtPath + 'stat/12',
         reader: {
             type: 'json',
             rootProperty: 'data',
             totalProperty  : 'Total',
             successProperty: 'IsSuccess'
         }
     }
});
