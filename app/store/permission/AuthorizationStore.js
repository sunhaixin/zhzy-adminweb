Ext.define("Admin.store.permission.AuthorizationStore",{
	extend: 'Admin.store.BaseStore',
	alias: "store.authorization",
	fields: [
		{name: "Id"},
		{name: "Name"},
		{name: "Code"},
		{name: "CreateTime"},
		{name: "UpdataTime"},
		{name: "IsActive"}
	],
	autoLoad: false,
	storeId: "authorization",
	proxy: {
         type: 'ajax',
         cors:true,
         useDefaultXhrHeader:false,
         url: Admin.proxy.API.BasePath+"api/admin/accounttype/query",
         reader: {
             type: 'json',
             rootProperty: 'Data',
             totalProperty  : 'Total',
             successProperty: 'success'
         }
     }
})