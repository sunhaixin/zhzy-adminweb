Ext.define('Admin.view.hospital.HospitalType', {
    extend: 'Admin.view.main.BaseContainer',
    xtype: 'hospitalType',
    controller: 'hospitalType',
    layout: 'fit',
    cls:"content-Panel-BorderStyle",
    margin: 20,
    style:"background:#f6f6f6",
    listeners: {
        activate: function(){
            var store = this.down('[xtype=grid]').getStore();
            store.load();
        }
    },
    items: [
        {
            xtype:"grid",
            title:"医院类型信息",
            autoScroll:true,
            height:"100%",
            id:"GridHospitalType",
            privileges:["hospitalType_see"],
            // flex:1,
            viewConfig:{  
                enableTextSelection:true
            },
            tbar:[
                {
                    xtype:"container",
                    layout:"hbox",
                    defaults:{
                        margin:"0 5"
                    },
                    items:[
                        {
                            xtype:"button",
                            text:"新增",
                            privileges:["hospitalType_add"],
                            handler:function(){
                                var win = Ext.create("Admin.view.hospital.WinHospitalType");
                                win.BindGrid = this.ownerCt.ownerCt.ownerCt;
                                win.setTitle("新增医院类型");
                                win.type = "Add";
                                win.show();
                            }
                        }
                    ]
                }
            ],
            store:Ext.create("Admin.store.hospital.HospitalTypeStore"),
            columns:{
                defaults:{
                    align:"center"
                },
                items:[
                    {
                        text:"类型名称",
                        dataIndex:"Name",
                        flex:1,
                        minWidth: 100,
                        sortable : true,
                    },
                    {
                        text:"创建时间",
                        dataIndex:"CreateTime",
                        width:160,
                    },{
                        xtype:"actioncolumn",
                        text:"操作",
                        width:115,
                        items:[
                            {
                                iconCls: 'x-fa fa-pencil-square-o',
                                tooltip: '修改',
                                privileges:["hospitalType_revisions"],
                                disabled: (sessionStorage.getItem("MyPrvileges")?(sessionStorage.getItem("MyPrvileges").split(",").indexOf("hospitalType_revisions")==-1?true:false):true),
                                handler:function(){
                                    var win = Ext.create("Admin.view.hospital.WinHospitalType");
                                    win.BindGrid = this.ownerCt.ownerCt;
                                    win.setTitle("修改医院类型");
                                    win.type = "update";
                                    win.record = arguments[5];
                                    win.down("#frmHospitalType").getForm().reset(true);
                                    win.down("#frmHospitalType").loadRecord(arguments[5]);
                                    win.show();
                                }
                            },
                            
                        ]
                    }
                ]
            },
            bbar:[
                {
                    xtype: 'pagingtoolbar',
                    store: Ext.data.StoreManager.lookup('hospitalType'),
                    displayInfo: true,
                    displayMsg: '显示第 {0} 条到 {1} 条记录，一共 {2} 条',
                    emptyMsg: "没有数据可以显示"
                }
            ]
        }
    ]
});
