Ext.define('Admin.view.eva.EvaReport10', {
    extend: 'Ext.window.Window',
    controller: 'EvaData',
    width:650,
    height:600,
    layout:"fit",
    modal:true,
    listeners:{
        activate: function (argument) {
            var me = this;
            var model = me.getViewModel();
            var uid = this.record.data.EvaUid;
            var eva = this.record.data.Eva;
            var report = this.down("#evaReport");
            var urls = me.type && me.type == "app"?Admin.proxy.API.AppDetail:Admin.proxy.API.PcDetail;
            urls += "?uid="+uid+"&eva="+eva;
            Ext.Ajax.request({
                url: urls,
                method:"Get",
                cors:true,//开启跨域模式
                useDefaultXhrHeader:false,//true的话header会带有X-Requested-With
                //binary:true, //为true后，后端将返回bytes
                success:function(resp){
                    var bean = Ext.decode(resp.responseText);
                    if(bean.success){
                        var data = bean.data;
                        model.setData(
                            {
                                Zygomatic: data.zygomatic == "" ? "无":  data.zygomatic,
                                Orbital: data.orbital  == "" ? "无":  data.orbital,
                                Luster: data.luster,
                                Image1: data.image1,
                                Image2: data.image2,
                                Zongtil: data.zongtiL,
                                Etoul: data.etouL,
                                Llianjial: data.lLianJiaL,
                                Rlianjial: data.rLianJiaL,
                                Yankuangl: data.yanKuangL,
                                Bil: data.biL,
                                Chunsel: data.chunSeL,
                                Zongtia: data.zongtiA,
                                Etoua: data.etouA,
                                Llianjiaa: data.lLianJiaA,
                                Rlianjiaa: data.rLianJiaA,
                                Yankuanga: data.yanKuangA,
                                Bia: data.biA,
                                Chunsea: data.chunSeA,
                                Zongtib: data.zongtiB,
                                Etoub: data.etouB,
                                Llianjiab: data.lLianJiaB,
                                Rlianjiab: data.rLianJiaB,
                                Yankuangb: data.yanKuangB,
                                Bib: data.biB,
                                Chunseb: data.chunSeB,
                                Zongtic: data.zongtiC,
                                Etouc: data.etouC,
                                Llianjiac: data.lLianJiaC,
                                Rlianjiac: data.rLianJiaC,
                                Yankuangc: data.yanKuangC,
                                Bic: data.biC,
                                Chunsec: data.chunSeC
                            }
                        )
                    }else{
                        me.close();
                        Ext.Msg.alert("提示",bean.message);
                    }
                }
            });
        }
    },
    viewModel: {
        data: {
            name: ""
        }
    },
    items: [
        {
            xtype: "container",
            layout: "vbox",
            margin: 20,
            autoScroll:true,
            items: [
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    style: "font-weight: bold;",
                    html: '面色照片'  
                },
                {
                    xtype:"container",
                    // height: 200,
                    bind: {  
                        html: '<img src="{Image1}" height="750px" width="500px">',
                    } 
                },
                {
                    xtype:"container",
                    style: "text-indent: 2em;",
                    // height: 200,
                    margin: "20px 0 0 0",
                    bind: {  
                        html: '<img src="{Image2}" height="750px" width="500px">',
                    } 
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    style: "font-weight: bold;",
                    html: '面色唇色参数'  
                },
                {
                    xtype:"container",
                    bind: {  
                        html: '<table  border="1px solid #000" cellpadding="0" cellspacing="0" >'
                            +'<tr>'
                              +'<td style="padding: 10px; text-align: center;">色彩空间</td>'
                              +'<td style="padding: 10px; text-align: center;">测量标准</td>'
                              +'<td style="padding: 10px; text-align: center;">总体</td>'
                              +'<td style="padding: 10px; text-align: center;">额头</td>'
                              +'<td style="padding: 10px; text-align: center;">脸颊(左)</td>'
                              +'<td style="padding: 10px; text-align: center;">脸颊(右)</td>'
                              +'<td style="padding: 10px; text-align: center;">眼眶</td>'
                              +'<td style="padding: 10px; text-align: center;">鼻</td>'
                              +'<td style="padding: 10px; text-align: center;">唇色</td>'
                            +'</tr>'
                            
                            +'<tr>'
                              +'<td style="padding: 10px; text-align: center;" ></td>'
                              +'<td style="padding: 10px; text-align: center;">L</td>'
                              +'<td style="padding: 10px; text-align: center;">{Zongtil}</td>'
                              +'<td style="padding: 10px; text-align: center;">{Etoul}</td>'
                              +'<td style="padding: 10px; text-align: center;">{Llianjial}</td>'
                              +'<td style="padding: 10px; text-align: center;">{Rlianjial}</td>'
                              +'<td style="padding: 10px; text-align: center;">{Yankuangl}</td>'
                              +'<td style="padding: 10px; text-align: center;">{Bil}</td>'
                              +'<td style="padding: 10px; text-align: center;">{Chunsel}</td>'
                            +'</tr>'
                            +'<tr>'
                              +'<td style="padding: 10px; text-align: center;" >Lab</td>'
                              +'<td style="padding: 10px; text-align: center;">a</td>'
                              +'<td style="padding: 10px; text-align: center;">{Zongtia}</td>'
                              +'<td style="padding: 10px; text-align: center;">{Etoua}</td>'
                              +'<td style="padding: 10px; text-align: center;">{Llianjiaa}</td>'
                              +'<td style="padding: 10px; text-align: center;">{Rlianjiaa}</td>'
                              +'<td style="padding: 10px; text-align: center;">{Yankuanga}</td>'
                              +'<td style="padding: 10px; text-align: center;">{Bia}</td>'
                              +'<td style="padding: 10px; text-align: center;">{Chunsea}</td>'
                            +'</tr>'
                            +'<tr>'
                              +'<td style="padding: 10px; text-align: center;" ></td>'
                              +'<td style="padding: 10px; text-align: center;">b</td>'
                              +'<td style="padding: 10px; text-align: center;">{Zongtib}</td>'
                              +'<td style="padding: 10px; text-align: center;">{Etoub}</td>'
                              +'<td style="padding: 10px; text-align: center;">{Llianjiab}</td>'
                              +'<td style="padding: 10px; text-align: center;">{Rlianjiab}</td>'
                              +'<td style="padding: 10px; text-align: center;">{Yankuangb}</td>'
                              +'<td style="padding: 10px; text-align: center;">{Bib}</td>'
                              +'<td style="padding: 10px; text-align: center;">{Chunseb}</td>'
                            +'</tr>'

                            +'<tr>'
                              +'<td style="padding: 10px; text-align: center;">颜色类别</td>'
                              +'<td style="padding: 10px; text-align: center;"></td>'
                             +'<td style="padding: 10px; text-align: center;">{Zongtic}</td>'
                              +'<td style="padding: 10px; text-align: center;">{Etouc}</td>'
                              +'<td style="padding: 10px; text-align: center;">{Llianjiac}</td>'
                              +'<td style="padding: 10px; text-align: center;">{Rlianjiac}</td>'
                              +'<td style="padding: 10px; text-align: center;">{Yankuangc}</td>'
                              +'<td style="padding: 10px; text-align: center;">{Bic}</td>'
                              +'<td style="padding: 10px; text-align: center;">{Chunsec}</td>'
                            +'</tr>'
                          +'</table>',
                    } 
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    style: "font-weight: bold;",
                    html: '面色信息输出'
                },

                {
                    xtype:"container",
                    bind: {  
                        html: '<table  border="1px solid #000" cellpadding="0" cellspacing="0" >'
                            +'<tr>'
                                +'<td style="padding: 10px; text-align: center;" rowspan="2">面色</td>'
                                +'<td style="padding: 10px; text-align: center;" colspan="2">局部特征</td>'
                                +'<td style="padding: 10px; text-align: center;" rowspan="2">光泽</td>'
                                +'<td style="padding: 10px; text-align: center;" rowspan="2">唇色</td>'
                            +'</tr>'
                            +'<tr>'
                            +'<td style="padding: 10px; text-align: center;">两颧红</td>'
                            +'<td style="padding: 10px; text-align: center;">眼眶黑</td>'
                          +'</tr>'
                            +'<tr>'
                                +'<td style="padding: 10px; text-align: center;">{Zongtic}</td>'
                                +'<td style="padding: 10px; text-align: center;">{Zygomatic}</td>'
                                +'<td style="padding: 10px; text-align: center;">{Orbital}</td>'
                                +'<td style="padding: 10px; text-align: center;">{Luster}</td>'
                                +'<td style="padding: 10px; text-align: center;">{Chunsec}</td>'
                            +'</tr>'
                          +'</table>'
                    } 
                }
            ]
        }
    ],
   
});
