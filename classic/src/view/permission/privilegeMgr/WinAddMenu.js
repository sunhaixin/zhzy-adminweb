Ext.define('Admin.view.permission.privilegeMgr.WinAddMenu', {
    extend: 'Ext.window.Window',
    controller: 'privilegeMgr',
    width:500,
    height:200,
    layout:"fit",
    modal:true,
    items: [
        {
            xtype:"form",
            layout: 'anchor',
            defaultType: 'textfield',
            padding:"10 20",
            itemId:"frmAddMenu",
            defaults: {
                anchor: '100%',
                labelWidth:65
            },
            items:[
                {
                    fieldLabel:"<span style='color:red'>*</span>菜单名称",
                    name:"name",
                    allowBlank:false
                },
                {
                    fieldLabel:"<span style='color:red'>*</span>ViewType",
                    name:"viewType",
                    allowBlank:false,
                    value:""
                }
            ]
        }
    ],
    bbar:[
        {
            xtype:"container",
            flex:1,
            layout:{
                type:"hbox",
                pack:"center",
                align:"center"
            },
            padding:"0 0 10 0",
            items:[
                {
                    xtype:"tbspacer"
                },{
                    xtype:"button",
                    text:"提交",
                    handler:"onMenuSubmit"
                }
            ]
        }
    ]
});
