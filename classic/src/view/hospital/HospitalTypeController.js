Ext.define("Admin.view.hospital.HospitalTypeController",{
    extend:"Ext.app.ViewController",
    alias: 'controller.hospitalType',

    onSubmitButtonClick: function (argument) {
    	var view = this.getView();
        if(view.type == "Add"){
            this.onAddHospitalSubmit(arguments);
        }else{
            this.onUpdateHospitalSubmit(arguments);
        }
    },
    onAddHospitalSubmit:function(){
        var view = this.getView();
        var frm = view.down("#frmHospitalType");
        var valus = frm.getValues();

        if (!frm.form.isValid()) {
             Ext.Msg.alert("提示","请检查必填项");
            return false;
        }

        var controller = Ext.create("Admin.view.controller.admincontroller");
        var url = 'hospitalType/add';
        var store = view.BindGrid.getStore();
        controller.post(valus, url, store, view);
    },
    onUpdateHospitalSubmit:function(){
        var view = this.getView();
        var frm = view.down("#frmHospitalType");
        var valus = frm.getValues();

        if (!frm.form.isValid()) {
             Ext.Msg.alert("提示","请检查必填项");
            return false;
        }
        valus.id = view.record.get("Id");
        // return false;
        var controller = Ext.create("Admin.view.controller.admincontroller");
        var url = 'hospitalType/update';
        var store = view.BindGrid.getStore();
        controller.post(valus, url, store, view);
    },
})