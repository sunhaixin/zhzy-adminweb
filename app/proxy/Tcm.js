Ext.define('Admin.constants.Tcm', {
    statics:{
        data: [
		      {
		        id: '1',
		        title: '胖',
		      },
		      {
		        id: '2',
		        title: '瘦',
		      },
		      {
		        id: '202',
		        title: '正常',
		      },
		      {
		        id: '3',
		        title: '怕冷',
		      },
		      {
		        id: '37',
		        title: '背冷',
		      },
		      {
		        id: '4',
		        title: '怕热',
		      },
		      {
		        id: '5',
		        title: '既怕冷又怕热',
		      },
		      {
		        id: '6',
		        title: '两手心、脚心发热及自觉心胸烦热',
		      },
		      {
		        id: '46',
		        title: '手脚心发热',
		      },
		      {
		        id: '7',
		        title: '自觉发热',
		      },
		      {
		        id: '8',
		        title: '活动量稍大就容易出虚汗',
		      },
		      {
		        id: '9',
		        title: '睡眠时出汗',
		      },
		      {
		        id: '10',
		        title: '容易疲劳',
		      },
		      {
		        id: '11',
		        title: '容易感冒',
		      },
		      {
		        id: '12',
		        title: '容易抽筋',
		      },
		      {
		        id: '13',
		        title: '容易忧思',
		      },
		      {
		        id: '14',
		        title: '容易动怒、发脾气',
		      },
		      {
		        id: '15',
		        title: '气短',
		      },
		      {
		        id: '16',
		        title: '喜欢叹气或叹气后觉得舒服',
		      },
		      {
		        id: '17',
		        title: '心慌',
		      },
		      {
		        id: '18',
		        title: '心烦',
		      },
		      {
		        id: '19',
		        title: '多梦',
		      },
		      {
		        id: '20',
		        title: '睡眠不好',
		      },
		      {
		        id: '21',
		        title: '健忘',
		      },
		      {
		        id: '22',
		        title: '平时痰多或者清晨容易吐痰',
		      },
		      {
		        id: '23',
		        title: '自觉口中唾液较多，或者频频不自主吐唾液',
		      },
		      {
		        id: '24',
		        title: '睡眠时流涎',
		      },
		      {
		        id: '25',
		        title: '睡眠时磨牙',
		      },
		      {
		        id: '26',
		        title: '嗳气',
		      },
		      {
		        id: '27',
		        title: '吞酸',
		      },
		      {
		        id: '28',
		        title: '食欲减退或者食欲不好',
		      },
		      {
		        id: '29',
		        title: '善食易饥',
		      },
		      {
		        id: '30',
		        title: '头痛',
		      },
		      {
		        id: '31',
		        title: '头昏',
		      },
		      {
		        id: '32',
		        title: '头重',
		      },
		      {
		        id: '33',
		        title: '头皮屑多',
		      },
		      {
		        id: '34',
		        title: '容易脱发',
		      },
		      {
		        id: '35',
		        title: '须发早白【中年见少量白发，老年发白，是正常生理现象】',
		      },
		      {
		        id: '36',
		        title: '肩痛',
		      },
		      {
		        id: '38',
		        title: '腰酸',
		      },
		      {
		        id: '39',
		        title: '腰痛',
		      },
		      {
		        id: '40',
		        title: '腰膝软弱无力，不耐久行久立',
		      },
		      {
		        id: '41',
		        title: '胸闷',
		      },
		      {
		        id: '42',
		        title: '腹胀',
		      },
		      {
		        id: '43',
		        title: '肠鸣',
		      },
		      {
		        id: '44',
		        title: '感觉四肢麻木',
		      },
		      {
		        id: '45',
		        title: '感觉四肢沉重',
		      },
		      {
		        id: '47',
		        title: '手指甲、脚趾甲颜色淡',
		      },
		      {
		        id: '48',
		        title: '小便颜色清、量多',
		      },
		      {
		        id: '49',
		        title: '小便颜色黄',
		      },
		      {
		        id: '50',
		        title: '夜间小便次数多（≥3次）',
		      },
		      {
		        id: '51',
		        title: '尿不尽',
		      },
		      {
		        id: '52',
		        title: '大便稀薄不成形',
		      },
		      {
		        id: '53',
		        title: '便秘',
		      },
		      {
		        id: '54',
		        title: '大便粘滞不爽，粘马桶',
		      },
		      {
		        id: '55',
		        title: '大便艰难，不容易解出',
		      },
		      {
		        id: '56',
		        title: '前阴瘙痒',
		      },
		      {
		        id: '57',
		        title: '前阴潮湿',
		      },
		      {
		        id: '58',
		        title: '肛门生痔疮',
		      },
		      {
		        id: '59',
		        title: '肛门瘙痒',
		      },
		      {
		        id: '60',
		        title: '容易遗精【身体健康者，1-2次/月是正常生理现象】',
		      },
		      {
		        id: '61',
		        title: '容易眼睛干涩',
		      },
		      {
		        id: '62',
		        title: '眼屎较多',
		      },
		      {
		        id: '63',
		        title: '容易目流泪水，或见风更多',
		      },
		      {
		        id: '64',
		        title: '耳鸣',
		      },
		      {
		        id: '65',
		        title: '咽干',
		      },
		      {
		        id: '66',
		        title: '咽喉有异物感',
		      },
		      {
		        id: '67',
		        title: '口干',
		      },
		      {
		        id: '68',
		        title: '口淡',
		      },
		      {
		        id: '69',
		        title: '口苦',
		      },
		      {
		        id: '70',
		        title: '口甜',
		      },
		      {
		        id: '71',
		        title: '口中粘腻',
		      },
		      {
		        id: '72',
		        title: '口臭',
		      },
		      {
		        id: '73',
		        title: '嘴唇干燥，容易起皮脱屑',
		      },
		      {
		        id: '74',
		        title: '嘴唇颜色淡',
		      },
		      {
		        id: '75',
		        title: '嘴唇颜色暗沉',
		      },
		      {
		        id: '76',
		        title: '牙痛',
		      },
		      {
		        id: '77',
		        title: '牙龈出血',
		      },
		      {
		        id: '78',
		        title: '牙龈肿痛',
		      },
		      {
		        id: '79',
		        title: '牙龈萎缩【60岁及以上老年人是正常生理现象】',
		      },
		      {
		        id: '80',
		        title: '牙齿浮动【60岁及以上老年人是正常生理现象】',
		      },
		      {
		        id: '81',
		        title: '干性皮肤',
		      },
		      {
		        id: '82',
		        title: '油性皮肤',
		      },
		      {
		        id: '83',
		        title: '混合性皮肤',
		      },
		      {
		        id: '203',
		        title: '正常',
		      },
		      {
		        id: '84',
		        title: '皮肤容易起瘀斑',
		      },
		      {
		        id: '85',
		        title: '皮肤容易生疣等赘生物',
		      },
		      {
		        id: '86',
		        title: '皮肤容易生痤疮或热疮、疖等',
		      },
		      {
		        id: '301',
		        title: '容易过敏（药物、食物、气味、花粉、季节交替时、气候变化等）',
		      },
		      {
		        id: '302',
		        title: '没有感冒也会打喷嚏',
		      },
		      {
		        id: '303',
		        title: '没有感冒也会鼻塞、流鼻涕',
		      },
		      {
		        id: '304',
		        title: '因季节变化、温度变化或异味等原因而咳喘',
		      },
		      {
		        id: '305',
		        title: '皮肤起荨麻疹（风团、风疹块、风疙瘩）',
		      },
		      {
		        id: '306',
		        title: '皮肤因过敏出现紫癜（紫红色瘀点、瘀斑）',
		      },
		      {
		        id: '307',
		        title: '皮肤一抓就红，并出现抓痕',
		      },
		      {
		        id: '87',
		        title: '带下颜色白、量多',
		      },
		      {
		        id: '88',
		        title: '带下颜色黄、量多',
		      },
		      {
		        id: '89',
		        title: '带下清稀',
		      },
		      {
		        id: '90',
		        title: '带下稠厚',
		      },
		      {
		        id: '91',
		        title: '月经量少',
		      },
		      {
		        id: '92',
		        title: '月经量多',
		      },
		      {
		        id: '93',
		        title: '月经颜色淡',
		      },
		      {
		        id: '94',
		        title: '月经颜色深',
		      },
		      {
		        id: '95',
		        title: '月经有血块',
		      },
		      {
		        id: '96',
		        title: '月经期间腰酸',
		      },
		      {
		        id: '97',
		        title: '月经前或月经期间乳房胀痛',
		      },
		      {
		        id: '98',
		        title: '月经前或月经期间腹痛',
		      },
		      {
		        id: '99',
		        title: '月经过后腹痛',
		      },
		      {
		        id: '100',
		        title: '月经期情志异常改变，如烦躁易怒、悲伤欲哭等',
		      },
		      {
		        id: '101',
		        title: '月经前或月经期间面目、四肢浮肿',
		      },
		      {
		        id: '102',
		        title: '舌胖',
		      },
		      {
		        id: '103',
		        title: '齿痕舌',
		      },
		      {
		        id: '104',
		        title: '舌裂',
		      },
		      {
		        id: '105',
		        title: '舌边尖红',
		      },
		      {
		        id: '106',
		        title: '舌苔白',
		      },
		      {
		        id: '107',
		        title: '舌苔白腻',
		      },
		      {
		        id: '108',
		        title: '舌苔黄',
		      },
		      {
		        id: '109',
		        title: '舌苔黄腻',
		      },
		      {
		        id: '110',
		        title: '脉紧',
		      },
		      {
		        id: '111',
		        title: '脉弦',
		      },
		      {
		        id: '112',
		        title: '脉滑',
		      },
		      {
		        id: '113',
		        title: '脉数',
		      },
		      {
		        id: '114',
		        title: '脉结代',
		      },
		],
        
        
    }
});