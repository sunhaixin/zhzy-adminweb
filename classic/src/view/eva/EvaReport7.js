Ext.define('Admin.view.eva.EvaReport7', {
    extend: 'Ext.window.Window',
    controller: 'EvaData',

    width:600,
    height:600,
    layout:"fit",
    autoScroll:true,
    modal:true,
    listeners:{
        activate: function (argument) {
            var me = this;
            var model = me.getViewModel();

            var uid = this.record.data.EvaUid;
            var eva = this.record.data.Eva;
            var report = this.down("#evaReport");
            var urls = me.type && me.type == "app"?Admin.proxy.API.AppDetail:Admin.proxy.API.PcDetail;
            urls += "?uid="+uid+"&eva="+eva;
            Ext.Ajax.request({
                url: urls,
                method:"Get",
                cors:true,//开启跨域模式
                useDefaultXhrHeader:false,//true的话header会带有X-Requested-With
                //binary:true, //为true后，后端将返回bytes
                success:function(resp){
                    var bean = Ext.decode(resp.responseText);
                    if(bean.success){
                        var data = bean.data;
                        var midicen = '';
                        if (me.type == 'app') {
                            if (!data.medSug) {data.medSug = ""};
                            if (!data.medResult) {data.medResult = ""};
                            var MedSug = data.medSug.split('|');
                            var MedResult = data.medResult.split(';');
                            if (MedResult.length != 0 && MedSug.length != 0) {
                                for(var i = 0; i < MedResult.length; i++){
                                    midicen += "<div>" +  MedResult[i] + '</div><div>' + MedSug[i] + '</div>'
                                }
                            }
                        }else{
                            if (!data.medSug) {data.medSug = ""};
                            var MedSug = data.medSug.split('\n');
                            var ss = data.medSug.split('\n');
                            if ( MedSug.length != 0) {
                                for(var i = 0; i < MedSug.length; i++){
                                    midicen += "<div>" + MedSug[i] + '</div>'
                                }
                            }
                        }
                        
                        model.setData(
                            {
                                maincom: data.mainCom,
                                primsym: data.primSym,
                                acpnsym: data.acpnSym,
                                tonguecon: data.tongueCon,
                                pulsecon: data.pulseCon,
                                synresult: data.synResult,
                                disresult: data.disResult,
                                medsug: midicen,
                                comment: data.comment,

                            }
                        )
                    }else{
                        me.close();
                        Ext.Msg.alert("提示",bean.message);
                    }
                }
            });
        }
    },
    viewModel: {
        data: {
            name: ""
        }
    },
    items: [
        {
            xtype: "container",
            layout: "vbox",
            margin: 20,
            autoScroll:true,
            items: [
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    style: "font-weight: bold;",
                    bind: {  
                        html: '主诉' 
                    } 
                },
                {
                    xtype:"container",
                    style: "text-indent: 2em;",
                    bind: {  
                        html: '<p>{maincom}</p>',
                    } 
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    style: "font-weight: bold;",
                    bind: {  
                        html: '主症' 
                    } 
                },
                {
                    xtype:"container",
                    style: "text-indent: 2em;",
                    bind: {  
                        html: '<p>{primsym}</p>',
                    } 
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    style: "font-weight: bold;",
                    bind: {  
                        html: '兼症' 
                    } 
                },
                {
                    xtype:"container",
                    style: "text-indent: 2em;",
                    bind: {  
                        html: '<p>{acpnsym}</p>',
                    } 
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    style: "font-weight: bold;",
                    bind: {  
                        html: '舌象' 
                    } 
                },
                {
                    xtype:"container",
                    style: "text-indent: 2em;",
                    bind: {  
                        html: '<p>{tonguecon}</p>',
                    } 
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    style: "font-weight: bold;",
                    bind: {  
                        html: '脉象' 
                    } 
                },
                {
                    xtype:"container",
                    style: "text-indent: 2em;",
                    bind: {  
                        html: '<p>{pulsecon}</p>',
                    } 
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    style: "font-weight: bold;",
                    bind: {  
                        html: '辨证结果' 
                    } 
                },
                {
                    xtype:"container",
                    style: "text-indent: 2em;",
                    bind: {  
                        html: '<p>{synresult}</p>',
                    } 
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    style: "font-weight: bold;",
                    bind: {  
                        html: '辨病结果' 
                    } 
                },
                {
                    xtype:"container",
                    style: "text-indent: 2em;",
                    bind: {  
                        html: '<p>{disresult}</p>',
                    } 
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    style: "font-weight: bold;",
                    bind: {  
                        html: '用药建议' 
                    } 
                },
                {
                    xtype:"container",
                    style: "text-indent: 2em;",
                    bind: {  
                        html: '<p style="word-wrap: break-word;width: 540px;">{medsug}</p>',
                    } 
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    style: "font-weight: bold;",
                    bind: {  
                        html: '调养建议' 
                    } 
                },
                {
                    xtype:"container",
                    style: "text-indent: 2em;",
                    bind: {  
                        html: '<p>{comment}</p>',
                    } 
                },
            ]
        }
    ],
   
});
