Ext.define("Admin.view.report.Stat12Report",{
    extend: 'Ext.panel.Panel',
    xtype: 'stat12report',
    height:"100%",
    layout:"hbox",
    controller:"report",
    cls:"content-Panel-BorderStyle",
    margin: 20,
    gridId: "stat12report_grid",
    listeners:{
        afterrender:"onReportRender"
    },
    grdid:"stat12report_grid",
    items:[
        {
            xtype:"panel",
            title:"居民常见脉象分布Top7",
            flex:1,
            height:"100%",
            style:"border:1px solid #ccc",
            layout:{
                type:"hbox",
                pack:"center"
            },
            autoScroll:true,
            items:[
                {
                    width:550,
                    height:500,
                    layout: {
                        type: 'hbox',
                    },
                    items:[
                        {
                            width:550,
                            height:500,
                            layout:"vbox",
                            items:[
                                {
                                    xtype:"HGrid",
                                    autoHeight:true,
                                    width:"100%",
                                    title:"居民左手脉象前七分布",
                                    id: "stat12report_1_grid",
                                    BindStore:Ext.create("Admin.store.report.Stat12_1Store",{autoLoad:false}),
                                    style:"border:1px solid #ccc",
                                    margin:10
                                },{
                                    xtype: 'cartesian',
                                    flex:1,
                                    reference: 'chart',
                                    width: "100%",
                                    insetPadding: {
                                        top: 40,
                                        bottom: 40,
                                        left: 20,
                                        right: 40
                                    },
                                    store:Ext.data.StoreManager.lookup("stat12_1report"),
                                    axes: [{
                                        type: 'numeric',
                                        position: 'left',
                                        minimum: 0
                                    }, 
                                    {
                                        type: 'category',
                                        position: 'bottom'
                                    }
                                    ],
                                    series: [
                                        {
                                            type: 'bar',
                                            xField: 'name',
                                            yField: 'value',
                                            style: {
                                                minGapWidth: 50
                                            },
                                            label: {
                                                field: 'value',
                                                display: 'insideEnd',
                                                orientation:"horizontal"
                                            }
                                        }
                                        
                                    ]
                                }
                            ]
                        }
                    ]
                },
                {
                    width:550,
                    height:500,
                    layout: {
                        type: 'vbox',
                    },
                    items:[
                        {
                            width:550,
                            height:500,
                            layout:"vbox",
                            items:[
                                {
                                    xtype:"HGrid",
                                    autoHeight:true,
                                    width:"100%",
                                    id: "stat12report_2_grid",
                                    type: "stat12_1report_grid",
                                    title: "居民右手脉象前七分布",
                                    BindStore:Ext.create("Admin.store.report.Stat12_1Store",{autoLoad:false}),
                                    style:"border:1px solid #ccc",
                                    margin:10
                                },{
                                    xtype: 'cartesian',
                                    flex:1,
                                    reference: 'chart',
                                    width: "100%",
                                    insetPadding: {
                                        top: 40,
                                        bottom: 40,
                                        left: 20,
                                        right: 40
                                    },
                                    store:Ext.data.StoreManager.lookup("stat12_1report"),
                                    axes: [{
                                        type: 'numeric',
                                        position: 'left',
                                        minimum: 0
                                    }, 
                                    {
                                        type: 'category',
                                        position: 'bottom'
                                    }
                                    ],
                                    series: [
                                        {
                                            type: 'bar',
                                            xField: 'name',
                                            yField: 'value',
                                            style: {
                                                minGapWidth: 50
                                            },
                                            label: {
                                                field: 'value',
                                                display: 'insideEnd',
                                                orientation:"horizontal"
                                            }
                                        }
                                        
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ]
        }
    ]
});