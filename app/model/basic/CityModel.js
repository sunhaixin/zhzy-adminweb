Ext.define('Admin.model.basic.CityModel', {
    //extend: 'Admin.model.Base',
    extend:'Ext.data.Model',

    idProperty:"CityId",
    fields: [
        {name:"CityId",type:"int", mapping: "cityId"},
        {name:"CityName",mapping:"cityName"},
        {name:"ZipCode",mapping:"zipCode"},
        {name:"ProvinceId",mapping:"provinceId"}
    ]
});
