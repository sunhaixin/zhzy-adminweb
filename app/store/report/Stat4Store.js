Ext.define('Admin.store.report.Stat4Store', {
    extend: 'Admin.store.BaseStore',
    alias: 'store.stat4report',

    model: 'Admin.model.report.Stat4Model',

    autoLoad:false,
    storeId: 'stat4report',
    proxy: {
         type: 'ajax',
         cors:true,
         useDefaultXhrHeader:false,
         url: Admin.proxy.API.MgmtPath + 'stat/4',
         reader: {
             type: 'json',
             rootProperty: 'data',
             totalProperty  : 'Total',
             successProperty: 'IsSuccess'
         }
     }
});
