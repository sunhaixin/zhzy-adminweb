Ext.define("Admin.view.eva.EvaTypeCombobox",{
	extend: "Ext.form.field.ComboBox",
	xtype: "EvaTypeCombobox",
	id: "EvaTypeCombobox",
	editable:false,
	allowBland:false,
	displayField:"Name",
	// controller: "EvaData",
	valueField: "eva",
	emptyText: "全部测评",
	width: 240,
	store: {
		fields:[
			"Eva","Name"
		],
		autoLoad: true,
		data: [
			{ eva: 0, Name: "全部测评" },
			{ eva: 1, Name: "老年人体质辨识" },
			{ eva: 2, Name: "中医体质辨识" },
			{ eva: 3, Name: "五态性格问卷测评(简)" },
			{ eva: 4, Name: "孕期女性健康状态测评" },
			{ eva: 5, Name: "高血压病辅助辨证" },
			{ eva: 6, Name: "儿童体质辨识" },
			{ eva: 7, Name: "中成药辅助辨证" },
			{ eva: 8, Name: "脉象数据采集" },
			{ eva: 9, Name: "舌象数据采集" },
			{ eva: 10, Name: "面色数据采集" },
			{ eva: 11, Name: "糖尿病辅助辨证" },
			{ eva: 12, Name: "四诊合参体质辨识" },
			{ eva: 13, Name: "产后女性健康状态测评" },
			{ eva: 14, Name: "备孕女性健康状态测评" },
			// { eva: 19, Name: "经典处方辅助辨证"},
			{ eva: 16, Name: "常态女性健康状态测评" },
			{ eva: 17, Name: "五态性格问卷测评" },
			{ eva: 18, Name: "图像采集"},
			{ eva: 19, Name: "7-12岁儿童体质辨识"}
		]
	},
	listeners: {
		change: "onEvaTypeChange"
	}
})