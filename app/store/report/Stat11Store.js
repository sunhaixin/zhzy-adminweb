Ext.define('Admin.store.report.Stat11Store', {
    extend: 'Admin.store.BaseStore',
    alias: 'store.stat11report',

    model: 'Admin.model.report.Stat11Model',

    autoLoad:false,
    storeId: 'stat11report',
    proxy: {
         type: 'ajax',
         cors:true,
         useDefaultXhrHeader:false,
         url: Admin.proxy.API.BasePaths + 'api/admin/stat/11',
         reader: {
             type: 'json',
             rootProperty: 'Data',
             totalProperty  : 'Total',
             successProperty: 'IsSuccess'
         }
     }
});
