Ext.define("Admin.view.eva.PcWebEva",{
	extend: "Admin.view.main.BaseContainer",
	xtype: "pcwebeva",
	requires: [
        "Admin.view.eva.EvaTypeCombobox",
        "Ext.form.field.ComboBox",
        "Ext.grid.Panel",
        "Ext.form.field.Text",
        "Ext.form.field.Date",
        "Ext.button.Button"
    ],
	layout: "fit",
	controller: 'EvaData',
	cls: "content-Panel-BorderStyle",
	margin:20,
	items: [
		{
			xtype: "grid",
			title:"PCWeb测评数据(有身份证号)",
			flex:1,
			tbar:{
				xtype: "container",
				padding: "10",
				style:"background:white",
				layout: "hbox",
				autoScroll:true,
				items: [
					{
						xtype: "combobox",
						store: Ext.create("Admin.store.data.HospitalStore"),
						changid: "pc_webeva",
						allowBland:false,
						displayField:"Name",
						valueField: "Id",
						fieldLabel: "请选择医院",
						emptyText: "全部医院",
						labelWidth: 80,
						labelStyle: "font-size:10px",
						listeners: {
							change:"onHospitalItemClick",
							'beforequery':function(e){
	                            var combo = e.combo;  
	                            combo.getStore().clearFilter();
	                            if(!e.forceAll){  
	                                var input = e.query;  
	                                // 检索的正则
	                                if(input != ""){
	                                    var regExp = new RegExp(".*" + input + ".*");
	                                    // 执行检索
	                                    combo.store.filterBy(function(record,id){  
	                                        // 得到每个record的项目名称值
	                                        return regExp.test(record.get("Name"));
	                                    });
	                                    combo.expand();  
	                                    return false;
	                                }
	                                return false;
	                            }
	                        }
						}
					},
					{
						xtype: "EvaTypeCombobox",
						fieldLabel: "测评类型",
						id: "EvaTypeCombobox_pcweb",
						labelStyle: "font-size:10px",
						margin: "0 10",
						labelWidth: 60
					},
					{
						xtype: 'datefield',
			            name: 'startDate',
			            id: 'PCWeb_startDate',
			            fieldLabel: '起始时间',
			            value: "起始时间",
			            format: "Y-m-d",
			            width: 200,
			            labelStyle: "font-size:10px",
			            labelWidth: 60,
			            allowBlank: false,
			            listeners: {
			            	change: "onEvaDateChange"
			            }
					},
					{
						xtype: 'datefield',
			            name: 'endDate',
			            id: 'PCWeb_endDate',
			            fieldLabel: '截止时间',
			            labelStyle: "font-size:10px",
			            format: "Y-m-d",
			            margin: '0 0 0 5',
			            labelWidth: 60,
			            width: 200,
			            allowBlank: false,
			            listeners: {
			            	change: "onEvaDateChange"
			            }
					},
					{
						xtype: "button",
						text: "导出数据",
						source: 8,
						margin: "0 0 0 15",
						privileges:["pcwebeva_export"],
						handler: "onEvaExportHandler"
					}
				]
			},
			id:"grdPcWebData",
			store:Ext.create("Admin.store.data.PcWebDataStore",{
				pageSize:100,
				// autoLoad:false
			}),
			viewConfig:{  
                enableTextSelection:true
            },
			columns: [
				{
					text: "记录类型",
					dataIndex: "Title",
                    width: 150
				},
				{
					text: "医院",
					dataIndex: "HospitalName",
					flex: 1
				},
				{
					text: "姓名",
					dataIndex: "PatientName",
					width: 120
				},
				{
					text: "医生",
					dataIndex: "DoctorName",
					width: 120
				},
				{
					text: "结论",
					dataIndex: "Result",
					flex: 1
				},
				{
					text: "测评日期",
					dataIndex: "CreateTime",
                    width:160,
				},
				{
					xtype:"actioncolumn",
                    text:"操作",
                    width:115,
                    items:[
                        {
                            iconCls: 'x-fa fa-pencil-square-o',
                            tooltip: '查看报告', 
                            handler:function(){
                            	switch(arguments[5].data.Eva)
								{
									case 1:
										var win = Ext.create("Admin.view.eva.EvaReport1");
										break;
									case 2:
										var win = Ext.create("Admin.view.eva.EvaReport2");
										break;
									case 3:
										var win = Ext.create("Admin.view.eva.EvaReport3");
										break;
									case 4:
										var win = Ext.create("Admin.view.eva.EvaReport4");
										break;
									case 5:
										var win = Ext.create("Admin.view.eva.EvaReport5");
										break;
									case 6:
										var win = Ext.create("Admin.view.eva.EvaReport6");
										break;
									case 7:
										var win = Ext.create("Admin.view.eva.EvaReport7");
										win.type = 'pc';
										break;
									case 8:
										var win = Ext.create("Admin.view.eva.EvaReport8");
										break;
									case 9:
										var win = Ext.create("Admin.view.eva.EvaReport9");
										break;
									case 10:
										var win = Ext.create("Admin.view.eva.EvaReport10");
										break;
									case 11:
										var win = Ext.create("Admin.view.eva.EvaReport11");
										break;
									case 12:
										var win = Ext.create("Admin.view.eva.EvaReport12");
										break;
									case 13:
										var win = Ext.create("Admin.view.eva.EvaReport13");
										break;
									default:
										var win = Ext.create("Admin.view.eva.EvaReport1");
								};
                                win.setTitle(arguments[5].data.Title);
                                win.record = arguments[5];
                                win.show();
                            }
                        },
                    ]
				}
			],
			bbar:[
				{
					xtype: 'pagingtoolbar',
					store: Ext.data.StoreManager.lookup('pcwebdatatore'),
					displayInfo: true,
                    displayMsg: '显示第 {0} 条到 {1} 条记录，一共 {2} 条',
                    emptyMsg: "没有数据可以显示"
				}
			]

		}
		
	],
})
