Ext.define('Admin.store.basic.CityStore', {
    extend: 'Admin.store.BaseStore',
    alias: 'store.city',

    model: 'Admin.model.basic.CityModel',

    autoLoad:false,
    storeId: 'city',
    proxy: {
         type: 'ajax',
         cors:true,
         useDefaultXhrHeader:false,
         url: Admin.proxy.API.MgmtPath+'basic/city',
         reader: {
             type: 'json',
             rootProperty: 'data',
             totalProperty  : 'totalCount',
             successProperty: 'success'
         }
     }
});
