Ext.define("Admin.view.eva.DataController",{
    extend: "Ext.app.ViewController",
    alias: "controller.EvaData",
    
    onHospitalItemClick:function(view,newVal,oldVal){
        var store, combobox = this.getView().down("[xtype=combobox]").value;
        var grid = this.getView().down("[xtype=grid]").getStore().getProxy().extraParams;
        if(view.changid == "pc_eva"){
            store = Ext.getCmp("grdPcData").getStore();
        }else if(view.changid == "app_eva"){
            store = Ext.getCmp("grdAppData").getStore();
        }else if(view.changid == "pc_webeva"){
            store = Ext.getCmp("grdPcWebData").getStore();
        }else{
            store = Ext.getCmp("grdPcWeb2Data").getStore();
        }
        if(newVal == null || newVal == 0){
            delete store.getProxy().extraParams.hid;
            if(grid.eva){
                store.getProxy().extraParams = {"eva": grid.eva};
            }
        }else{
            if(grid.eva){
                store.getProxy().extraParams = {"hid":newVal,"eva": grid.eva};
            }else{
                store.getProxy().extraParams = {"hid":newVal};
            }
        }
        store.remove();
        store.loadPage(1);  
    },
    onEvaTypeChange: function(view,newVal,oldVal){
        var grid = this.getView().down("[xtype=grid]");
        var store = grid.getStore();
        if(newVal == 0){
            delete store.getProxy().extraParams.eva;
        }else{
            store.getProxy().extraParams.eva = newVal;
        }
        store.removeAll();
        store.loadPage(1);
    },
    onEvaDateChange: function(view,newVal,oldVal){
        var grid = this.getView().down("[xtype=grid]");
        var store = grid.getStore();

        if(!newVal){
            if(view.name == "startDate"){
                delete store.getProxy().extraParams.starttime;
            }
            if(view.name == "endDate"){
                delete store.getProxy().extraParams.endtime;
            }
            store.removeAll();
            store.loadPage(1);
            return; 
        }
        var dtArr = grid.query("container > datefield");
        
        var start = dtArr[0];
        var end = dtArr[1];
        
        var startDate = start.lastValue;
        var endDate = end.lastValue;
        //2把字符串格式转换为日期类
        var startTime = new Date(Date.parse(startDate));
        var endTime = new Date(Date.parse(endDate));
        if(startTime>=endTime){
            var msg = "";
            if (view.name == "startDate") {
                start.reset();
                msg = "起止日期不能大于截止日期";            
            }
            if (view.name == "endDate") {
                end.reset();
                msg = "截止日期不能小于起止日期";
            }
            Ext.Msg.alert("提示",msg);
            return;
        }
        if(startDate){
            store.getProxy().extraParams.starttime = startDate;
        }
        if(endDate){
            store.getProxy().extraParams.endtime = endDate;
        }
        
        store.removeAll();
        store.loadPage(1);
    },
    onEvaExportHandler: function(view){
        var hid = this.getView().query("combobox")[0].getValue();
        var eva = this.getView().query("EvaTypeCombobox")[0].getValue();
        var startTime = this.getView().down("grid").query("container > datefield")[0].lastValue;
        var endTime = this.getView().down("grid").query("container > datefield")[1].lastValue;
        let source = this.getView().query("button")[0].config.source;
        let token = Admin.TokenStorage.retrieve();
        var params = [`token=${token}`];

        if(hid> 0){
            params.push("hid="+hid);
        }
        if(eva> 0){
            params.push("eva="+eva);
        }
        if(startTime){
            params.push("starttime="+startTime);
        }
        if(endTime){
            params.push("endtime="+endTime);
        }
         if(source){
            params.push("source="+source);
        }
        var str = params.join("&");
        window.location.href = Admin.proxy.API.BasePath+`mgmt/pc/excel?`+str;
    }
})