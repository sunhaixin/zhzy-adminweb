Ext.define('Admin.model.report.Stat9Model', {
    //extend: 'Admin.model.Base',
    extend:'Ext.data.Model',

    idProperty:"name",
    fields: [
        { name: "name" },
        { name: "value", mapping: 'count',type:"int" }
    ]
});
