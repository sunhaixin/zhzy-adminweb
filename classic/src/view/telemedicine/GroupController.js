Ext.define("Admin.view.telemidicine.GroupController",{
	extend: "Ext.app.ViewController",
	alias: "controller.group",
	onHospitalItemClick:function(){
        var view = this.getView();
        var dpGrid = view.down("#GridDepartment");
        var id = arguments[1].get("id");
        dpGrid.getStore().getProxy().extraParams = {"gid":id};
        dpGrid.getStore().remove();
        dpGrid.getStore().loadPage(1);
        // var filters = dpGrid.getStore().getFilters();
        // filters.clearFilters();
        // filters.add({property:"hid",value:1});
        
    },
    onTelemedicineHospitalFielts: function(){
        var textfield = Ext.getCmp("grdGroupLeft").down("[xtype=textfield]").value;
        var dpGrid = Ext.getCmp("grdGroupLeft");
        dpGrid.getStore().clearFilter();
        var regExp = new RegExp(".*"+textfield+".*");
        if (textfield) {
            dpGrid.getStore().filterBy(function(record) { 
                    // return record.get('Name') == textfield;
                    // var filterBy = regExp.test(record.get("Name","")) || regExp.test(record.get("Id",""));
                    // return filterBy;
                    return regExp.test(record.get("name"));
            });
        }
        dpGrid.getStore().remove();
        dpGrid.getStore().loadPage(1);
    },
    onSubmitButtonClick:function(){
        var view = this.getView();
        if(view.type == "Add"){
            this.onAddTelemedicineGroupSubmit(arguments);
        }else if(view.type = "HospitalAdd"){
            this.onAddTelemedicineHospitalSubmit(arguments);
        }
    },
    onAddTelemedicineGroupSubmit:function(){
        var view = this.getView();
        var frm = view.down("#frmGroupHospital");
        var valus = frm.getValues();

        if (!frm.form.isValid()) {
            Ext.Msg.alert("提示","请检查必填项")
            return false;
        }

        var controller = Ext.create("Admin.view.controller.admincontroller");
        var url = 'telemedicineGroup/insert';
        var store = view.BindGrid.getStore();
        controller.post(valus, url, store, view);
    },
    onAddTelemedicineHospitalSubmit:function(){
        var view = this.getView();
        var frm = view.down("#frmGroupHospital");
        var valus = frm.getValues();
        if (!frm.form.isValid()) {
            Ext.Msg.alert("提示","请检查必填项")
            return false;
        }

        var controller = Ext.create("Admin.view.controller.admincontroller");
        var url = 'telemedicineGroupHospital/inserts';
        var store = view.BindGrid.getStore();
        controller.post(valus, url, store, view);
    },
    onTelemedicineDelete: function(){
        var view = this.getView();
        var me = this;
        var grid = Ext.getCmp("grdGroupLeft");
        var hgrid = Ext.getCmp("grdGroupright")
        var store = grid.getStore();
        var hstore = hgrid.getStore();
        var valus = {
            id: view.selectedNodeId
        }

        var url = '';
        if (view.type == 'groupDelete') {
            url = 'telemedicineGroup/delete';
        }
        if (view.type == 'hospitalDelete') {
            url = 'telemedicineGroupHospital/delete';
        }
        //controller.post(valus, url, store, hstore);
        Ext.Ajax.request({
            url: Admin.proxy.API.MgmtPath + url,
            method:"Post",
            cors:true,//开启跨域模式
            useDefaultXhrHeader:false,
            params: valus,
            success:function(resp){
                var bean = Ext.decode(resp.responseText);
                if(bean.code == 200){
                    Ext.Msg.alert("提示",bean.message);
                     if (view.type == 'groupDelete' && store) {
                        url = 'telemedicineGroup/delete';
                        store.reload();
                    }
                    if (view.type == 'hospitalDelete' && hstore) {
                        url = 'telemedicineGroupHospital/delete';
                        hstore.reload()
                     }
                    if (view) {
                        view.close(); 
                    }  
                }else{
                    Ext.Msg.alert("提示",bean.message);
                }
            }
        });



    },
})