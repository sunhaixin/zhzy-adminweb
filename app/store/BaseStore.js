Ext.define('Admin.store.BaseStore', {
    extend: 'Ext.data.Store',

    autoLoad:false,
    
    loadSync:async function(){
        var me = this;
        return new Promise((resolve,reject)=>{
            me.load({
                callback:function(records,operation,success){
                    if(success){
                        resolve(records);
                    }else{
                        reject();
                    }
                }
            });
        });
    }
});
