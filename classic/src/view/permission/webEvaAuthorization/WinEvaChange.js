Ext.define('Admin.view.permission.webEvaAuthorization.WinEvaChange', {
    extend: 'Ext.window.Window',
    requires: [
        "Ext.grid.plugin.RowExpander",
        "Ext.grid.Panel"
    ],
    listeners: {
        activate: function(argument) {
            var me = this;
            var grid = me.down("[xtype=grid]");
            serviceId = me.record.data.ServicesId;
            grid.getStore().getProxy().extraParams = {"serviceId":serviceId};
            grid.getStore().remove();
            grid.getStore().loadPage(1);
        }
    },
    controller: 'authorization',
    width:900,
    height:600,
    layout:"fit",
    modal:true,
    items: [
        {
            xtype:"grid",
            autoScroll:true,
            height:"100%",
            width: "100%",
            id:"GridHospital",
            privileges:["hospital_see_all","hospital_see_self"],
            viewConfig:{  
                enableTextSelection:true
            },
            store:Ext.create("Admin.store.serviesItem.EvaChangeStore"),
            
            columns:{
                defaults:{
                    align:"center"
                },
                items:[
                    {
                        text:"修改时间",
                        dataIndex:"CreateTime",
                        width: 200
                    },
                    {
                        text:"授权时间变更前",
                        dataIndex:"BefValidDateEnd",
                        width: 200
                    },
                    {
                        text:"授权时间变更后",
                        dataIndex:"AftValidDateEnd",
                        width: 200
                    },
                    {
                        text:"服务次数变更前",
                        dataIndex:"BefServiceCount",
                        flex: 1
                    },
                    {
                        text:"服务次数变更后",
                        dataIndex:"AftServiceCount",
                        flex: 1
                    },
                    // {
                    //     text:"服务项目变更前",
                    //     dataIndex:"BeforeItems"
                    // },
                    // {
                    //     text:"服务项目变更后",
                    //     dataIndex:"AfterItems",
                    // }
                ]
            },
            
            plugins: [{
                    ptype: 'rowexpander',
                    rowBodyTpl : [
                            '<p><b>服务项目变更前:</b> {befName}</p><br>',
                            '<p><b>服务项目变更后:</b> {aftName}</p>'
                    ]
            }],
            bbar:[
                {
                    xtype: 'pagingtoolbar',
                    store: Ext.data.StoreManager.lookup('evaChangeStore'),
                    displayInfo: true,
                    displayMsg: '显示第 {0} 条到 {1} 条记录，一共 {2} 条',
                    emptyMsg: "没有数据可以显示"
                }
            ]
        }
    ]
    
});
