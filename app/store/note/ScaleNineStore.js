Ext.define('Admin.store.note.ScaleNineStore', {
    extend: 'Admin.store.BaseStore',
    alias: 'store.scalenine',

    model: 'Admin.model.note.ScaleNineModel',

    autoLoad: false,
    storeId: 'scalenine',
    proxy: {
         type: 'ajax',
         cors:true,
         useDefaultXhrHeader:false,
         url: Admin.proxy.API.BasePath+"api/admin/scale/nine/query",
         reader: {
             type: 'json',
             rootProperty: 'Data',
             totalProperty  : 'Total',
             successProperty: 'IsSuccess'
         }
     }
});
