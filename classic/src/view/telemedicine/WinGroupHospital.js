Ext.define('Admin.view.telemedicine.WinGroupHospital', {
    extend: 'Ext.window.Window',
    controller: 'group',
    width:400,
    height:220,
    layout:"fit",
    listeners: {
        activate: function(){
            var store = Ext.getCmp("cmbGroup").getStore();
            store.getProxy().extraParams = {
                "hid":sessionStorage.getItem("Hid"),
                "code": "system"
            };
            var InitFormGrid = Ext.getCmp("idfrmGroupHospital").InitFormGrid;
            if (InitFormGrid) {
                var groupid = InitFormGrid.get("Id")
                var hospital = Ext.getCmp("telemedicine_hospital").getStore();
                hospital.getProxy().extraParams = {
                    groupid: groupid,
                    type: "telemedicine"
                }
                hospital.load();
            }else{
               
            }
            
        }
    },
    modal:true,
    items: [
        {
            xtype:"form",
            layout: 'anchor',
            defaultType: 'textfield',
            padding:"10 20",
            id:"idfrmGroupHospital",
            itemId:"frmGroupHospital",
            defaults: {
                anchor: '100%',
                labelWidth:90
            },
            items:[
                {
                    xtype:"combobox",
                    fieldLabel:"<span style='color:red'>*</span>远程医疗小组",
                    name:"groupId",
                    id:"cmbGroup",
                    store:Ext.data.StoreManager.lookup('group'),
                    displayField:"Name",
                    valueField:"Id",
                    editable:false,
                    allowBlank:false,
                    listeners:{
                        render : function(combo) {//渲染   
                            combo.getStore().load();
                            var grid = Ext.getCmp("grdGroupLeft");
                            // this.store.load();
                            if (grid.selection) {
                                var selectrow = grid.selection.data;
                                // combo.setValue(selectrow);//第一个值
                                combo.select(selectrow.Id);
                            }   
                        },
                        change: function(combo){
                        }
                    }
                },
                {
                    xtype:"combobox",
                    fieldLabel:"<span style='color:red'>*</span>医院",
                    name:"hospitalIds",
                    id: "telemedicine_hospital",
                    store:Ext.create("Admin.store.hospital.HospitalStore",{
                         pageSize: 1000
                    }),
                    displayField:"Name",
                    valueField:"Id",
                    // editable:false,
                    allowBland:true,
                    allowBlank:false,
                    multiSelect: true,
                    listConfig : {    
                        itemTpl : Ext.create('Ext.XTemplate','<input type=checkbox>{[values.Name]}'),
                        onItemSelect : function(record) {  
                            var node = this.getNode(record);    
                            if (node) {    
                                Ext.fly(node).addCls(this.selectedItemCls);    
                                var checkboxs = node.getElementsByTagName("input");    
                                if (checkboxs != null)    
                                    var checkbox = checkboxs[0];    
                                checkbox.checked = true;    
                            }    
                        },  
                        listeners : {   
                            itemclick : function(view, record, item, index, e, eOpts) {    
                                var isSelected = view.isSelected(item);    
                                var checkboxs = item.getElementsByTagName("input");    
                                if (checkboxs != null) {    
                                    var checkbox = checkboxs[0];    
                                    if (!isSelected) {    
                                        checkbox.checked = true;    
                                    } else {    
                                        checkbox.checked = false;    
                                    }    
                                }    
                            }    
                        }   
                    },
                    listeners : {
                        'beforequery':function(e){
                            var combo = e.combo;  
                            combo.getStore().clearFilter();
                            if(!e.forceAll){  
                                var input = e.query;  
                                // 检索的正则
                                if(input != ""){
                                    var regExp = new RegExp(".*" + input + ".*");
                                    // 执行检索
                                    combo.store.filterBy(function(record,id){  
                                        return regExp.test(record.get("Name"));
                                    });
                                    combo.expand();  
                                    return false;
                                }
                                return false;
                            }
                        }
                    }
                }
            ]
        }
    ],
    bbar:[
        {
            xtype:"container",
            flex:1,
            layout:{
                type:"hbox",
                pack:"center",
                align:"center"
            },
            padding:"0 0 10 0",
            items:[
                {
                    xtype:"tbspacer"
                },{
                    xtype:"button",
                    text:"提交",
                    handler:"onSubmitButtonClick"
                }
            ]
        }
    ]
});
