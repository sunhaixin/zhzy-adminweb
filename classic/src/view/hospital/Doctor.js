Ext.define('Admin.view.hospital.Doctor', {
    extend: 'Admin.view.main.BaseContainer',
    xtype: 'doctor',
    controller: 'doctor',
    layout: 'hbox',
    cls:"content-Panel-BorderStyle",
    margin:20,
    listeners:{
        activate:function(){
            var grid = this.down("[xtype=listpanel]");
            grid.getStore().removeAll();
            grid.getStore().loadPage(1);
        }
    },

    items: [
        {
            xtype:"listpanel",
            width:300,
            height:"100%",
            cls:"content-Panel-BorderStyle",
            id: "listpanel_doctor",
            title:"医院名称",
            privileges:["doctor_see_all","doctor_see_lower","doctor_see_self"],
            id: "doctorhospital",
            store:Ext.create("Admin.store.hospital.HospitalStore",{
                pageSize: 1000, 
                baseParams:{
                    "type":"all"
                }
            }),
            tbar: [
                {
                    xtype: "container",
                    layout: "hbox",
                    width: "100%",
                    items: [
                        {
                            xtype: "textfield",
                            id: "doctor_hospital_textfield",
                            fieldLabel: '搜索',
                            labelWidth: 50,
                            listeners: {
                                change: "onDoctorHospitalFielts",
                                render: function(){
                                    document.getElementById("doctor_hospital_textfield-inputEl").setAttribute("placeholder",'按医院名称搜索');
                                }
                            },
                            flex: 1
                        }
                    ]
                }
            ],
            columns:[
                {
                    xtype:"templatecolumn",
                    menuDisabled:true,
                    flex:1,
                    tpl:"<div style='height:30px;line-height:30px;cursor:pointer;'>{name}</div>"
                }
            ],
            bbar:[
                {
                    xtype: 'pagingtoolbar',
                    store: Ext.data.StoreManager.lookup('hospital'),
                    displayInfo: true,
                    inputItemWidth: 30,//这里是改变gridPanel底部分页栏方框的大小 
                    displayMsg: '显示第 {0} 条到 {1} 条记录，一共 {2} 条',
                    emptyMsg: "没有数据可以显示"
                }
            ],
            listeners:{
                itemClick:"onHospitalItemClick"
            }
        },
        {
            xtype:"grid",
            title:"医生管理",
            flex:1,
            itemId:"GridDoctor",
            margin:"0 0 0 10",
            height:"100%",
            cls:"content-Panel-BorderStyle",
            style:"border:1px solid #d0d0d0 !important",
            tbar:[
                {
                    xtype:"container",
                    layout:"hbox",
                    defaults:{
                        margin:"0 5"
                    },
                    items:[
                        {
                            xtype:"button",
                            text:"新增",
                            privileges:["doctor_add"],
                            handler:function(){
                                var win = Ext.create("Admin.view.hospital.WinDoctor");
                                win.setTitle("新增医生");
                                win.BindGrid = this.ownerCt.ownerCt.ownerCt;
                                win.type = "Add"
                                win.show();
                            }
                        },
                        {
                            xtype: 'textfield',
                            id:"doctor_textfield",
                            fieldLabel: '搜索',
                            margin: "0 30px",
                            labelWidth: "10px",
                            listeners: {
                                change: "onDoctorFielts",
                                render: function(){
                                    document.getElementById("doctor_textfield-inputEl").setAttribute("placeholder",'按医生名称搜索');
                                }
                            }
                        }
                    ]
                }
            ],
            viewConfig:{  
                enableTextSelection:true
            },
            store:Ext.create("Admin.store.hospital.DoctorStore",{
                 pageSize: 100,
            }),
            columns:[
                {
                    text:"医院名称",
                    width:200,
                    dataIndex:"HospitalName"
                },
                {
                    text:"科室名称",
                    width:150,
                    dataIndex:"Department"
                },
                {
                    text:"姓名",
                    dataIndex:"Name"
                },
                {
                    text: "医生编号",
                    width:100,
                    dataIndex: "Id"
                },
                {
                    text:"手机",
                    dataIndex:"Mobile"
                },
                {
                    text: "擅长",
                    dataIndex: "Expert"
                },
                {
                    text:"Email",
                    dataIndex:"Email"
                },
                {
                    text:"性别",
                    dataIndex:"Gender",
                    renderer:function(v){
                        if(v == "1"){
                            return "男";
                        }
                        if(v == "2"){
                            return "女";
                        }
                        return "未知";
                    }
                },
                {
                    text:"简介",
                    dataIndex:"Comment"
                },
                {
                        xtype:"actioncolumn",
                        text:"操作",
                        items:[
                            {
                                iconCls: 'x-fa fa-pencil-square-o',
                                tooltip: '修改',
                                privileges:["doctor_revisions"],
                                disabled: (sessionStorage.getItem("MyPrvileges")?(sessionStorage.getItem("MyPrvileges").split(",").indexOf("doctor_revisions")==-1?true:false):true),
                                handler:function(){
                                    var win = Ext.create("Admin.view.hospital.WinDoctor");
                                    win.BindGrid = this.ownerCt.ownerCt;
                                    win.setTitle("修改医生信息");
                                    win.type = "update";
                                    win.record = arguments[5];
                                    win.down("#frmDoctor").loadRecord(arguments[5]);
                                    win.down("#cmbDepartment").hide();
                                    win.down("#cmbDepartment").allowBlank = true;
                                    win.down("#cmbHospital").hide();
                                    win.down("#cmbHospital").allowBlank = true;
                                    win.show();
                                }
                            },{
                                iconCls:'x-fa fa-trash-o',
                                tooltip:"删除",
                                privileges:["doctor_delete"],
                                disabled: (sessionStorage.getItem("MyPrvileges")?(sessionStorage.getItem("MyPrvileges").split(",").indexOf("doctor_delete")==-1?true:false):true),
                                handler:"onDoctorDelete"
                            },{
                                iconCls:"x-fa fa-cog",
                                tooltip:"重置密码",
                                privileges:["doctor_pswd"],
                                disabled: (sessionStorage.getItem("MyPrvileges")?(sessionStorage.getItem("MyPrvileges").split(",").indexOf("doctor_pswd")==-1?true:false):true),
                                handler:"onBtnSetDoctorPswd"
                            }
                        ]
                    }
            ],
            bbar:[
            {
                xtype: "container",
                padding: "10",
                style:"background:white",
                layout: "hbox",
                autoScroll:true,
                items: [
                    {
                        xtype: 'pagingtoolbar',
                        autoScroll:true,
                        store: Ext.data.StoreManager.lookup('doctor'),
                        displayInfo: true,
                        displayMsg: '显示第 {0} 条到 {1} 条记录，一共 {2} 条',
                        emptyMsg: "没有数据可以显示"
                    }
                ]
            }
                
            ]
        }
    ]
});
