Ext.define('Admin.store.report.Stat17Store', {
    extend: 'Admin.store.BaseStore',
    alias: 'store.stat17report',

    model: 'Admin.model.report.Stat17Model',

    autoLoad:false,
    storeId: 'stat17report',
    proxy: {
         type: 'ajax',
         cors:true,
         useDefaultXhrHeader:false,
         url: Admin.proxy.API.MgmtPath + 'stat/17',
         reader: {
             type: 'json',
             rootProperty: 'data',
             totalProperty  : 'Total',
             successProperty: 'IsSuccess'
         }
     }
});
