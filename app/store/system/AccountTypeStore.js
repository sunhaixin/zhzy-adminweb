Ext.define('Admin.store.system.AccountTypeStore', {
    extend: 'Admin.store.BaseStore',
    alias: 'store.accountType',

    model: 'Admin.model.system.AccountTypeModel',

    autoLoad:false,
    storeId: 'accountType',
    proxy: {
         type: 'ajax',
         cors:true,
         useDefaultXhrHeader:false,
         url: Admin.proxy.API.MgmtPath+'accountType/all',
         reader: {
             type: 'json',
             rootProperty: 'data',
             totalProperty  : 'totalCount',
             successProperty: 'success'
         }
     }
});
