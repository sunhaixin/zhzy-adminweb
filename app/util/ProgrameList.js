Ext.define("Admin.util.ProgrameList",{
    statics:{
        createAlltp:(bean) =>{
            let text, styles;
            if(bean == null || bean == "" || bean == "尚未进行客观化信息采集" || bean == "尚未设置智能合参"){
                text = "";
                styles = ""
            }else{
                text = `<textarea readonly rows="5" cols="30" style="width: 400px; border: none; font-size: 13px; padding-top: 7px;resize:none">${bean}</textarea>`;
                styles = "text-indent: 2em;"
            }
            return Ext.create("Ext.container.Container",{
                items: [
                    {
                        xtype:"container",
                        style: styles,
                        html: text
                    }
                ]
            });
        },
        createCmp:(bean)=> {
            var img = "";
            var div = "";
            var title = "";
            var names = /[a-z]/i;
            if(bean){
                img = '<a target="_blank" href=' + bean.url + '>查看方案</a>';
                div = bean.url?img:"暂无方案";
                // title = names.test(bean.prog)? bean.url.split("type=")[1]? "": bean.prog  : bean.prog;
                title =  bean.url? bean.url.indexOf("type=") == - 1 ?  names.test(bean.prog)? "自定义方案": bean.prog : names.test(bean.prog)? bean.url.split("type=")[1]: bean.prog: bean.prog;
                
            }else{
                title = "暂无数据";
            }
            return Ext.create("Ext.container.Container",{
                layout: "hbox",
                style: "border: 1px solid #ccc;",
                width: "100%",
                width: 500,
                padding: 10,
                items: [
                    {
                        xtype:"container",
                        width: "80%",
                        html:title
                    },
                    {
                        xtype:"container",
                        width: "20%",
                        html:div
                    }
                ]
            });
        },
        imageFile18:(bean, a, b) =>{
            var img = "";
            var div = "";
            var part= "";
            if(!a && !b){ a = 350;  b = 250}
            if(bean){
                img = bean.imageFile ? bean.imageFile.toLocaleLowerCase() + `?x-oss-process=image/resize,m_pad,w_${a},h_${b}/rotate,270`: "";
                part= bean.part? bean.part: "暂无数据";
                div = '<img width='+b+' height='+a+' src='+img+'/>';
            }
            return Ext.create("Ext.container.Container",{
                padding: 10,
                width: "100%",
                items: [
                    {
                        xtype:"container", 
                        flex: 1,
                        html: div
                    },
                    {
                        xtype: "container",
                        margin: "13px 0 0 10px",
                        html:  "部位："+ part,
                    }
                ]
            })
        },
        App9ImageCreate:(b) =>{
            var div = "";
            if(b == ""){
                div= '<div></div>'
            }else{
                div = '<img width='+400+' height='+400+' src='+b+'/>';
            }
            return Ext.create("Ext.container.Container",{
                items: [
                    {
                        xtype:"container", 
                        height: 400,
                        margin: "20px 0 0 0",
                        html: div
                    },
                ]
            })
        }
    }
})