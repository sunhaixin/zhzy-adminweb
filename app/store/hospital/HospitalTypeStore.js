Ext.define('Admin.store.hospital.HospitalTypeStore', {
    extend: 'Admin.store.BaseStore',
    alias: 'store.hospitalType',

    model: 'Admin.model.hospital.HospitalTypeModel',

    autoLoad: false,
    storeId: 'hospitalType',
    proxy: {
         type: 'ajax',
         cors:true,
         useDefaultXhrHeader:false,
         url: Admin.proxy.API.MgmtPath+"hospitalType/list",
         reader: {
             type: 'json',
             rootProperty: 'data',
             totalProperty  : 'totalCount',
             successProperty: 'success'
         }
     }
});
