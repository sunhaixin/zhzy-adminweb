Ext.define("Admin.view.pcEvaStat.PcEvaStatController",{
    extend:"Ext.app.ViewController",
    alias: 'controller.pcEvaStat',
    
    onEndDate: function(){
        var grid = Ext.getCmp("PcEvastat_grid");
        var store = grid.getStore();
        var end = Ext.getCmp("PcEvastat_endDate");
        var endDate = Ext.getCmp("PcEvastat_endDate").lastValue;
        var startDate = Ext.getCmp("PcEvastat_startDate").lastValue;
        var curTime = new Date();
        //2把字符串格式转换为日期类
        var startTime = new Date(Date.parse(startDate));
        var endTime = new Date(Date.parse(endDate));
        if (startTime>=endTime) {
            end.setValue ( "" ) ;
            return  Ext.Msg.alert("提示","截至日期不能小于初始日期");
        }
        if(startDate && startDate != ''){
            store.getProxy().extraParams.starttime = startDate;
        }else{
            store.getProxy().extraParams.starttime = '';
        }

        if (endDate && endDate != '') {
            store.getProxy().extraParams.endtime = endDate;
        }else{
            store.getProxy().extraParams.endtime = '';
        }
        
        store.removeAll();
        store.loadPage(1);
       
    },
    onPcEvaUseLogExport: function(argument) {
        var hid = sessionStorage.getItem("Hid");
        if (!hid) {
            return  Ext.Msg.alert("提示","请重新登陆");
        }
        window.location.href = Admin.proxy.API.BasePath+"api/admin/pcwebuse/export?hid="+hid;
    }
});