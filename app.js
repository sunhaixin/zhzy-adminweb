/*
 * This file is responsible for launching the application. Application logic should be
 * placed in the Admin.Application class.
 */
Ext.application({
    name: 'Admin',

    extend: 'Admin.Application',

    // Simply require all classes in the application. This is sufficient to ensure
    // that all Admin classes will be included in the application build. If classes
    // have specific requirements on each other, you may need to still require them
    // explicitly.
    //
    requires: [
        'Admin.*'
    ],
    init:function(){
        Ext.Msg.buttonText.yes = "确定";
        Ext.Msg.buttonText.no = "取消";
        Ext.Msg.buttonText.ok = "确定";
        Ext.Msg.buttonText.cancel = "取消";
        //Ext.data.Request.headers.token = "4444";
        Ext.Ajax.on('beforerequest', function(conn, options) {
            if (Admin.TokenStorage.retrieve()) {
                options.headers = options.headers || {};
                options.headers['Authorization'] = Admin.TokenStorage.retrieve();//'Bearer '+Admin.TokenStorage.retrieve(); 
                if(options.params && !options.action && (!options.convertParams || options.convertParams === true)){
                    var newParams = {};
                    for(var key in options.params){
                        var val = options.params[key];
                        var newKey = key[0].toLowerCase()+key.substr(1,key.length);
                        newParams[newKey] = val;
                    }
                    options.params = newParams;
                }
            }
        });
    }
});
