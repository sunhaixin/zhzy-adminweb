Ext.define("Admin.view.report.ReportController",{
    extend:"Ext.app.ViewController",
    alias: 'controller.report',
    onReportRender:function(){
        var view = this.getView();
        var grid = Ext.getCmp(view.xtype+"_grid") || Ext.getCmp(view.xtype+"_1_grid") || Ext.getCmp(view.xtype+"_2_grid")
        || Ext.getCmp(view.xtype+"_3_grid") || Ext.getCmp(view.xtype+"_4_grid") ;
        var store;//= grid.getStore();// || grid.BindStore;

        if (grid.xtype != "HGrid") {
            if (grid.id == "stat11report_grid") {
                store = grid.getStore();
                store.getProxy().extraParams = {
                    aid: sessionStorage.getItem("Aid"),
                    type: "气虚体"
                }
            }else{
                store = grid.getStore();
                store.getProxy().extraParams = {
                    aid: sessionStorage.getItem("Aid")
                }
            }
            
            // store.remove();
            store.load();
        }
        var nogrid = Ext.getCmp("stat1report_no_grid");
        if (nogrid) {
            var nostore = nogrid.getStore();
            nostore.getProxy().extraParams = {
                aid: sessionStorage.getItem("Aid")
            }
            nostore.loadPage(1);
            nogrid.hide();
        }
        var stat2report_1_grid = Ext.getCmp("stat2report_1_grid");
        if (stat2report_1_grid) {
            var stat2reportstore = stat2report_1_grid.getStore();
            stat2reportstore.getProxy().extraParams = {
                aid: sessionStorage.getItem("Aid"),
                all: true
            }
            stat2reportstore.load();
            Ext.getCmp("stat2report_grid").hide();
        }
        var s = view.down("[xtype=polar]");
        var stat1 = Ext.getCmp("stat1report_grid");
        var polar = Ext.getCmp("Stat1Polar");
        var polar_no = Ext.getCmp("Stat1Polar_no");
        if (s) {
            
            // statno1report_grid.hide();
            store.getProxy().extraParams = {
                aid: sessionStorage.getItem("Aid")
            }
            store.load({
                callback: function(){
                    var items = store.data.items;
                    var count = 0;
                    
                    items.forEach(function(bean){
                        count += bean.get("value");
                    })
                    if (count == 0) {
                        polar.hide();
                    }else{
                        polar_no.hide();
                    }
                }
            });
        }
        
                                    
        // store.loadPage(1);
    },
    onReport18Render:function(){
        var view = this.getView();
        // var grid = view.down("[xtype=grid]");
        var grid = Ext.getCmp(view.xtype+"_grid");
        var store;//= grid.getStore();// || grid.BindStore;
        var time = new Date();
        var year = time.getFullYear()
        store = grid.getStore();
        store.getProxy().extraParams = {
            hid: sessionStorage.getItem("Hid"),
            year: year
        }
        store.load({
            callback:function(records){
                var sum = 0;
                store.each(function(record){              
                    sum += Number(record.data.value); //把money2列下面的所有值进行加和运算 
                });      
                store.insert(store.getCount(), Ext.create('Admin.model.report.Stat18Model',{value:sum,name:'合计'}));
            }
        });

        var cartesian = Ext.getCmp("stat18report_cartesian");
        var carStore = cartesian.getStore();
        carStore.getProxy().extraParams = {
            hid: sessionStorage.getItem("Hid"),
            year: year
        }
        carStore.load();

        var combo = Ext.getCmp("stat18Start");
        var comboStore = combo.getStore();
        comboStore.getProxy().extraParams = {
            hid: sessionStorage.getItem("Hid"),
        }
        comboStore.load();
    },
    onstat18Change: function(){
        var view = this.getView();
        var grid = Ext.getCmp(view.xtype+"_grid");
        var combo = Ext.getCmp("stat18Start");
        var value = combo.value.substring(0,combo.value.length-1)

        store = grid.getStore();
        store.getProxy().extraParams = {
            hid: sessionStorage.getItem("Hid"),
            year: value
        }
        store.removeAll();
        store.loadPage(1);
        store.on('load', function() { 
            var sum = 0;
                store.each(function(record){              
                    sum += Number(record.data.value); //把money2列下面的所有值进行加和运算 
                }); 
            store.insert(store.getCount(), Ext.create('Admin.model.report.Stat18Model',{value:sum,name:'合计'}));

        });

        var cartesian = Ext.getCmp("stat18report_cartesian");
        var carStore = cartesian.getStore();
        carStore.getProxy().extraParams = {
            hid: sessionStorage.getItem("Hid"),
            year: value
        }
        carStore.removeAll();
        carStore.loadPage(1);
    },
    onEndDate: function(){
        var grid = Ext.getCmp("stat1report_grid");
        var store = grid.getStore();
        var end = Ext.getCmp("stat1_endDate");
        var endDate = Ext.getCmp("stat1_endDate").lastValue;
        var startDate = Ext.getCmp("stat1_startDate").lastValue;
        var curTime = new Date();
        //2把字符串格式转换为日期类
        var startTime = new Date(Date.parse(startDate));
        var endTime = new Date(Date.parse(endDate));
        if (startTime>=endTime) {
            end.setValue ( "" ) ;
            return  Ext.Msg.alert("提示","截至日期不能小于初始日期");
        }
        if(startDate && startDate != ''){
            store.getProxy().extraParams.startTime = startDate;
        }else{
            store.getProxy().extraParams.startTime = '';
        }

        if (endDate && endDate != '') {
            store.getProxy().extraParams.endTime = endDate;
        }else{
            store.getProxy().extraParams.endTime = '';
        }
        
        store.removeAll();
        store.loadPage(1);
        var grid = Ext.getCmp("stat1report_no_grid");
        var store = grid.getStore();
        var endDate = Ext.getCmp("stat1_endDate").lastValue;
        var startDate = Ext.getCmp("stat1_startDate").lastValue;
        if(startDate && startDate != ''){
            store.getProxy().extraParams.startTime = startDate;
        }else{
            store.getProxy().extraParams.startTime = '';
        }

        if (endDate && endDate != '') {
            store.getProxy().extraParams.endTime = endDate;
        }else{
            store.getProxy().extraParams.endTime = '';
        }
        
        store.removeAll();
        store.loadPage(1);
        var polar = Ext.getCmp("Stat1Polar");
        var polar_no = Ext.getCmp("Stat1Polar_no");
        store.load({
                callback: function(){
                    var items = store.data.items;
                    var count = 0;
                    
                    items.forEach(function(bean){
                        count += bean.get("value");
                    })
                    if (count == 0) {
                        polar.hide();
                        polar_no.show();
                    }else{
                        polar_no.hide();
                        polar.show();
                    }
                }
            });
    },
    onstat11Change: function(){
        var type = Ext.getCmp("stat11tizhi").value;
        var HGrid = Ext.getCmp("stat11report_grid");
        var store = HGrid.getStore();
        store.getProxy().extraParams = {
            aid: sessionStorage.getItem("Aid"),
            type: type
        };
        store.removeAll();
        store.reload();
    },
    parseDom:function(dom){
        var div = document.createElement("div");
        div.innerHTML = dom;
        return div.innerHTML;
    }
    ,
    reoprtExport: function(){
         var win = window.open();
         var mydiv = document.getElementById("stat11reportcanvas");
         
         var linkArr = document.getElementsByTagName("link");
         var linkStr = "";
         for (var i = linkArr.length - 1; i >= 0; i--) {
             linkStr += parseDom(linkArr[i]); 
         }
         var domdiv = this.parseDom(mydiv);
         domdiv = linkStr + domdiv;
         win.document.write(domdiv);
    }
});