Ext.define('Admin.store.report.Stat1Store', {
    extend: 'Admin.store.BaseStore',
    alias: 'store.stat1report',

    model: 'Admin.model.report.Stat1Model',

    autoLoad:false,
    storeId: 'stat1report',
    
    proxy: {
         type: 'ajax',
         cors:true,
         useDefaultXhrHeader:false,
         url: Admin.proxy.API.MgmtPath+'stat/1?hasTotal=true',
         baseParams:{
            "hasTotal": true
        },
         reader: {
             type: 'json',
             rootProperty: 'data',
             totalProperty  : 'Total',
             successProperty: 'IsSuccess'
         }
     }
});
