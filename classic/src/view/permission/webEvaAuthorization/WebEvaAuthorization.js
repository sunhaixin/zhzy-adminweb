Ext.define('Admin.view.permission.webEvaAuthorization.WebEvaAuthorization', {
    extend: 'Admin.view.main.BaseContainer',
    xtype: 'webEvaAuthorization',
    controller: 'authorization',
    layout: 'fit',
    cls:"content-Panel-BorderStyle",
    listeners: {
        activate: function(){
            var store = this.down("[xtype=grid]").getStore();
            if (sessionStorage.getItem("AccountType") != "system" && sessionStorage.getItem("AccountType") != "Operator") { 
                var hid = sessionStorage.getItem("Hid");
                store.getProxy().extraParams = {
                    hid: hid
                };
            }
            store.removeAll();
            store.loadPage(1);
        }
    },
    margin: 20,
    style:"background:#f6f6f6",
    items: [
        {
            xtype:"grid",
            title:"PC网页版授权管理",
            autoScroll:true,
            height:"100%",
            privileges:["webEvaAuthorization_see"],
            // flex:1,
            viewConfig:{  
                enableTextSelection:true
            },
            
            store:Ext.create("Admin.store.serviesItem.WebEvaAuthorization",{
                pageSize: 50, 
                autoLoad: false,
            }),
            columns:{
                defaults:{
                    align:"center"
                },
                items:[
                    {
                        width:60,
                        text: "医院编号",
                        dataIndex:"Id"
                    },{
                        text:"医院名称",
                        dataIndex:"Name",
                        flex:1,
                        minWidth: 100,
                        sortable : true,
                    },
                    {
                        text:"web授权地址",
                        dataIndex:"EntryAddress"
                    },
                    {
                        text:"授权可用时间",
                        dataIndex:"ServiesEndTime",
                        width:200
                    },
                    {
                        text:"剩余测评次数",
                        dataIndex:"Remainder",
                    },
                    {
                        text:"总测评次数",
                        dataIndex:"ServiesCount",
                    },
                    {
                        xtype:"actioncolumn",
                        text:"操作",
                        width:115,
                        items:[
                            {
                                iconCls: 'x-fa fa-pencil-square-o',
                                tooltip: '进行服务授权',
                                privileges:["webEvaAuthorization_add"],
                                disabled: (sessionStorage.getItem("MyPrvileges")?(sessionStorage.getItem("MyPrvileges").split(",").indexOf("webEvaAuthorization_add")==-1?true:false):true),
                                handler:function(){
                                    var win = Ext.create("Admin.view.permission.webEvaAuthorization.WinEvaAuthorization");
                                    win.BindGrid = this.ownerCt.ownerCt;
                                    win.setTitle("网页版测评授权信息配置");
                                    win.type = "add";
                                    win.record = arguments[5];
                                    win.down("#comment").hide();
                                    win.show();
                                },
                                getClass : function (v, metadata, r, rowIndex, colIndex, store) {
                                    var ServicesId = arguments[2].data.ServicesId;
                                    if (ServicesId) {
                                        return 'x-hidden';
                                    }else{
                                        return "x-fa fa-pencil-square-o";
                                    }
                                    return "x-fa fa-pencil-square-o";
                                }
                            },
                            {
                                iconCls: 'x-fa fa-pencil-square-o',
                                html: "进行授权",
                                tooltip: '变更服务授权',
                                privileges:["webEvaAuthorization_update"],
                                disabled: (sessionStorage.getItem("MyPrvileges")?(sessionStorage.getItem("MyPrvileges").split(",").indexOf("webEvaAuthorization_update")==-1?true:false):true),
                                handler:function(){
                                    var win = Ext.create("Admin.view.permission.webEvaAuthorization.WinEvaAuthorization");
                                    win.BindGrid = this.ownerCt.ownerCt;
                                    win.setTitle("网页版测评授权信息配置");
                                    win.type = "update";
                                    win.record = arguments[5];
                                    var EntryAddress =win.down("#EntryAddress");
                                    EntryAddress.ownerCt.remove(EntryAddress);
                                    win.down("#frmEvaAuthorization").getForm().reset(true);
                                    win.down("#frmEvaAuthorization").loadRecord(arguments[5]);
                                    Ext.getCmp("serviceCount").fieldLabel = "<span style='color:red'>*</span>增加服务次数";
                                    Ext.getCmp("serviceContainer").setHtml("在原有服务总次数上增加服务次数后并勾选可适用的测评项目）");
                                    win.show();
                                },
                                getClass : function (v, metadata, r, rowIndex, colIndex, store) {
                                    var ServicesId = arguments[2].data.ServicesId;
                                    if (ServicesId) {
                                        return "x-fa fa-pencil-square-o";
                                    }else{
                                        return 'x-hidden';
                                    }
                                    return "x-fa fa-pencil-square-o";
                                }
                            },
                            {
                                iconCls: 'x-fa fa-pencil-square-o',
                                html: "网页版测评授权信息变更详情",
                                tooltip: '变更详情',
                                privileges:["webEvaAuthorization_update_log"],
                                disabled: (sessionStorage.getItem("MyPrvileges")?(sessionStorage.getItem("MyPrvileges").split(",").indexOf("webEvaAuthorization_update_log")==-1?true:false):true),
                                handler:function(){
                                    var win = Ext.create("Admin.view.permission.webEvaAuthorization.WinEvaChange");
                                    win.BindGrid = this.ownerCt.ownerCt;
                                    win.setTitle("修改医院信息");
                                    win.type = "updateDetail";
                                    win.record = arguments[5];
                                    win.show();
                                },
                                getClass : function (v, metadata, r, rowIndex, colIndex, store) {
                                    var IsChange = arguments[2].data.IsChange;
                                    if (IsChange) {
                                        return "x-fa fa-pencil-square-o";
                                    }else{
                                        return 'x-hidden';
                                    }
                                    return "x-fa fa-pencil-square-o";
                                }
                            },
                        ]
                    }
                ]
            },
            bbar:[
                {
                    xtype: 'pagingtoolbar',
                    store: Ext.data.StoreManager.lookup('webEvaAuthorization'),
                    displayInfo: true,
                    displayMsg: '显示第 {0} 条到 {1} 条记录，一共 {2} 条',
                    emptyMsg: "没有数据可以显示"
                }
            ]
        }
    ]
});
