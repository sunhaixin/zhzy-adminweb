Ext.define('Admin.view.eva.EvaReportApp9', {
    extend: 'Ext.window.Window',
    controller: 'EvaData',
    width:650,
    height:600,
    layout:"fit",
    modal:true,
    listeners:{
        activate: function (argument) {
            var me = this;
            var model = me.getViewModel();
            var uid = this.record.data.EvaUid;
            var eva = this.record.data.Eva;
            var report = this.down("#evaReport");
            var urls = me.type && me.type == "app"?Admin.proxy.API.AppDetail:Admin.proxy.API.PcDetail;
            urls += "?uid="+uid+"&eva="+eva;
            Ext.Ajax.request({
                url: urls,
                method:"Get",
                cors:true,//开启跨域模式
                useDefaultXhrHeader:false,//true的话header会带有X-Requested-With
                //binary:true, //为true后，后端将返回bytes
                success:function(resp){
                    var bean = Ext.decode(resp.responseText);
                        if(bean.success){
                        var data = bean.data; var App9Image2 = Ext.getCmp("App9Image2");App9Image2.removeAll();
                        var Comment = data.comment == "" || !data.comment ? "暂无备注" : data.comment; 
                        var BianJianHong = data.bianJianHong == "" || !data.bianJianHong ? "无" : data.bianJianHong;
                        var SheSe = data.sheSe == "" || !data.sheSe ? "无" : data.sheSe;
                        var TaiSe = data.taiSe == "" || !data.taiSe ? "无" : data.taiSe;
                        var HouBao = data.houBao == "" || !data.houBao ? "无" : data.houBao;
                        var yuBanYuDian = data.yuBanYuDian == "" || !data.yuBanYuDian ? "无": data.yuBanYuDian;
                        var ni = data.ni == "" || !data.ni ? "无" : data.ni;
                        var fu = data.fu == "" || !data.fu ? "无" : data.fu;
                        var taiBao = data.taiBao == "" || !data.taiBao ? "无" : data.taiBao;
                        var pangShou = data.pangShou == "" || !data.pangShou ? "无" : data.pangShou;
                        var chiHen = data.chiHen == "" || !data.chiHen ? "无" : data.chiHen;
                        var dianCi = data.dianCi == "" || !data.dianCi ? "无" : data.dianCi;
                        var lieWen = data.lieWen == "" || !data.lieWen ? "无" : data.lieWen;
                        if(!data.step2Pic|| data.step2Pic==""){
                             App9Image2.add(Admin.util.ProgrameList.App9ImageCreate(""))
                        }else{
                             App9Image2.add(Admin.util.ProgrameList.App9ImageCreate(data.step2Pic))
                        }
                        model.setData(
                            {
                            Image1: !data.step1Pic||data.step1Pic==""?"":data.step1Pic,
                            Image2: !data.step2Pic||data.step2Pic==""?"":data.step2Pic,
                            Comment: Comment,
                            Quanshec: SheSe,
                            bianjian: BianJianHong,
                            Ecchymosis: yuBanYuDian,
                            Coatquanshec: TaiSe,
                            Thickness: HouBao,
                            Curdygreasiness: ni,
                            Curdygreasiness2: fu,
                            Exfoliation: taiBao,
                            Fatthin: pangShou,
                            Indentation: chiHen,
                            Pepeckneedling: dianCi,
                            Crack: lieWen,
                            }
                        )
                    }else{
                        me.close(); 
                        Ext.Msg.alert("提示",bean.message);
                    }
                }
            });
        }
    },
    viewModel: {
        data: {
            name: ""
        }
    },
    items: [
        {
            xtype: "container",
            layout: "vbox",
            margin: 20,
            autoScroll:true,
            items: [
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    style: "font-weight: bold;",
                    bind: {  
                        html: '舌象照片' 
                    } 
                },
                {
                    xtype:"container",
                    height: 533,
                    bind: {  
                        html: '<img src="{Image1}" height="533px" width="400px" />',
                    } 
                },
                {
                    xtype:"container",
                    id:"App9Image2"
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    style: "font-weight: bold;",
                    bind: {  
                        html: '备注' 
                    } 
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    bind: {  
                        html: '{Comment}'
                    } 
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    style: "font-weight: bold;",
                    bind: {  
                        html: '舌象信息输出' 
                    } 
                },
                {
                    xtype:"container",
                    bind: {  
                        html: '<table border="1px solid #000" cellpadding="0" cellspacing="0" >'
                +'<tr>'
                  +'<td style="padding: 3px; text-align: center;" rowspan="2">舌色</td>'
                  +'<td style="padding: 3px; text-align: center;" colspan="2">局部特征</td>'
                  +'<td style="padding: 3px; text-align: center;" rowspan="2">苔色</td>'
                  +'<td style="padding: 3px; text-align: center;" colspan="4">苔质</td>'
                  +'<td style="padding: 3px; text-align: center;" colspan="4">舌形</td>'
                +'</tr>'
                
                +'<tr>'
                  +'<td style="padding: 3px; text-align: center;">边尖红</td>'
                  +'<td style="padding: 3px; text-align: center;">瘀斑瘀点</td>'
                  +'<td style="padding: 3px; text-align: center;">厚薄</td>'
                  +'<td style="padding: 3px; text-align: center;">腻</td>'
                  +'<td style="padding: 3px; text-align: center;">腐</td>'
                  +'<td style="padding: 3px; text-align: center;">苔剥</td>'
                  +'<td style="padding: 3px; text-align: center;">胖瘦</td>'
                  +'<td style="padding: 3px; text-align: center;">齿痕</td>'
                  +'<td style="padding: 3px; text-align: center;">点刺</td>'
                  +'<td style="padding: 3px; text-align: center;">裂纹</td>'
                +'</tr>'
                +'<tr>'
                  +'<td style="padding: 3px; text-align: center;" >{Quanshec}</td>'
                  +'<td style="padding: 3px; text-align: center;">{bianjian}</td>'
                  +'<td style="padding: 3px; text-align: center;">{Ecchymosis}</td>'
                  +'<td style="padding: 3px; text-align: center;">{Coatquanshec}</td>'
                  +'<td style="padding: 3px; text-align: center;">{Thickness}</td>'
                  +'<td style="padding: 3px; text-align: center;">{Curdygreasiness}</td>'
                  +'<td style="padding: 3px; text-align: center;">{Curdygreasiness2}</td>'
                  +'<td style="padding: 3px; text-align: center;">{Exfoliation}</td>'
                  +'<td style="padding: 3px; text-align: center;">{Fatthin}</td>'
                  +'<td style="padding: 3px; text-align: center;">{Indentation}</td>'
                  +'<td style="padding: 3px; text-align: center;">{Pepeckneedling}</td>'
                  +'<td style="padding: 3px; text-align: center;">{Crack}</td>'
                +'</tr>'
              +'</table>',
                    } 
                },
            ]
        }
    ],
   
});
