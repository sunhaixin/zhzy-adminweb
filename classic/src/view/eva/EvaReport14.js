Ext.define('Admin.view.eva.EvaReport14', {
    extend: 'Ext.window.Window',
    controller: 'EvaData',
    width:600,
    height:600,
    layout:"fit",
    modal:true,
    listeners:{
        render: function (argument) {
            var me = this;
            var model = me.getViewModel();

            var uid = this.record.data.EvaUid;
            var eva = this.record.data.Eva;
            var report = this.down("#evaReport");
            var urls = me.type && me.type == "app"?Admin.proxy.API.AppDetail:Admin.proxy.API.PcDetail;
            urls += "?uid="+uid+"&eva="+eva;
            Ext.Ajax.request({
                url: urls,
                method:"Get",
                cors:true,//开启跨域模式
                useDefaultXhrHeader:false,//true的话header会带有X-Requested-With
                //binary:true, //为true后，后端将返回bytes
                success:function(resp){
                    var bean = Ext.decode(resp.responseText);
                    if(bean.success){
                        var data = bean.data;
                        let value = [];
                        var SCALE_PRE_VALUE = Admin.constants.PrePre.data;
                        for (var k in SCALE_PRE_VALUE) {
                                if (data.patientValue.indexOf(SCALE_PRE_VALUE[k].id) != -1) {
                                    value.push(SCALE_PRE_VALUE[k].title);
                                }
                            }
                        value = value.join(",");
                        model.setData(
                            {
                                report1: data.result.report1,
                                report2: data.result.report2,
                                report3: data.result.report3,
                                recDetail: data.result.programe || "暂无方案",
                                value: value,

                            }
                        )
                        var chart = me.down("[xtype=cartesian]");
                        var store = chart.getStore();
                        var report = data.result.report2.split(";")
                        var names = report[0].split(",");
                        var values = report[1].split(",");
                        names.forEach(function(bean,index){
                            var Report1 = bean;
                            var Report2 = values[index];
                            var value = Report2 ;
                            var text = value;
                            store.insert(index, Ext.create('Admin.model.eva.EvaReportModel',{name:Report1,value:value,text:text}));
                        });
                    }else{
                        me.close(); 
                        Ext.Msg.alert("提示",bean.message);
                    }
                }
            });
        }
    },
    viewModel: {
        data: {
            name: ""
        }
    },
    items: [
        {
            xtype: "container",
            layout: "vbox",
            margin: 20,
            autoScroll:true,
            items: [
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    style: "font-weight: bold;",
                    bind: {  
                        html: '症状' 
                    } 
                },
                {
                    xtype:"container",
                    style: "text-indent: 2em;",
                    bind: {  
                        html: '<p style="word-wrap: break-word;width: 540px;">{value}</p>',
                    } 
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    style: "font-weight: bold;",
                    bind: {  
                        html: '脏腑健康状态评估结果' 
                    } 
                },
                {
                    xtype:"container",
                    style: "text-indent: 2em;",
                    bind: {  
                        html: '<p style="word-wrap: break-word;width: 540px;">{report1}</p>',
                    } 
                },
                
                {
                    xtype: 'cartesian',
                    reference: 'chart',
                    width: "100%",
                    height: 300,
                    store: {
                        model: 'Admin.model.eva.EvaReportModel',
                        data: [
                        ]
                    },
                    axes: [ {
                                type: 'numeric',
                                position: 'left',
                                minimum: 0
                            },{
                        type: 'category',
                        position: 'bottom'
                    }],
                    series: [{
                        type: 'bar',
                        xField: 'name',
                        yField: 'value',
                        renderer: function(sprite, storeItem, barAttr, i, store) {   
                            var colors = ['#ffffff','#A7E9FF','#01aaed','#0076c8'];
                                if (arguments[0].attr.dataY[i] <25) {
                                    colors[i] = "#ffffff";
                                }
                                if (25 <= arguments[0].attr.dataY[i] && arguments[0].attr.dataY[i] < 50) {
                                    colors[i] = "#A7E9FF";
                                }
                                if ( arguments[0].attr.dataY[i] >= 50 && arguments[0].attr.dataY[i] < 75) {
                                    colors[i] = "#01aaed";
                                }
                                if (arguments[0].attr.dataY[i] >=75) {
                                    colors[i] = "#0076c8";
                                }
                                if (arguments[2].store.data.items[i].data.name.indexOf("热") > -1 && arguments[0].attr.dataY[i] >= 57) {
                                    colors[i] = "#FFCC33";
                                }
                                if (arguments[2].store.data.items[i].data.name.indexOf("热") > -1 && arguments[0].attr.dataY[i] >= 34 && arguments[0].attr.dataY[i] < 57) {
                                    colors[i] = "#FFCCCC";
                                }
                                barAttr.fill = colors[i];
                                return barAttr; 
                               
                        },
                        style: {
                            minGapWidth: 50,
                        },
                        label: {
                            field: 'text',
                            display: 'insideEnd',
                            orientation:"horizontal"
                        }
                    }]
                },
                {
                    xtype: "container",
                    style: "border: 1px solid #ccc;",
                    layout: "vbox",
                    padding: 20,
                    items: [
                        {
                            xtype:"container",
                            margin: "0 0 10px 0",
                            html:'<span style="font-weight: bold;">分值：</span>分值越高代表对人体影响越大，不同项目之间的对比毫无意义'
                        },
                        {
                            xtype: "container",
                            layout: "hbox",
                            items: [
                                {
                                    xtype: "container",
                                    width: "50px",
                                    height: "20px",
                                    margin: "0 30px 0 0",
                                    style: "background: #A7E9FF",
                                    html: '<div style="height: 20px; width: 50px;"></div>'
                                },
                                {
                                    xtype: "container",
                                    margin: "0 0 10px 0",
                                    html: '<div style="font-weight: bold;">浅蓝色</div><div>表示轻度影响</div>'
                                }
                            ]
                        },
                        {
                            xtype: "container",
                            layout: "hbox",
                            items: [
                                {
                                    xtype: "container",
                                    width: "50px",
                                    height: "20px",
                                    margin: "0 30px 0 0",
                                    style: "background: #01aaed",
                                    html: '<div style="height: 20px; width: 50px;"></div>'
                                },
                                {
                                    xtype: "container",
                                    margin: "0 0 10px 0",
                                    html: '<div style="font-weight: bold;">蓝色</div><div>表示中度影响</div>'
                                }
                            ]
                        },
                        {
                            xtype: "container",
                            layout: "hbox",
                            items: [
                                {
                                    xtype: "container",
                                    width: "50px",
                                    height: "20px",
                                    margin: "0 30px 0 0",
                                    style: "background: #0076c8",
                                    html: '<div style="height: 20px; width: 50px;"></div>'
                                },
                                {
                                    xtype: "container",
                                    margin: "0 0 10px 0",
                                    html: '<div style="font-weight: bold;">深蓝色</div><div>表示影响较大</div>'
                                }
                            ]
                        },
                        {
                            xtype: "container",
                            layout: "hbox",
                            items: [
                                {
                                    xtype: "container",
                                    width: "50px",
                                    height: "20px",
                                    margin: "0 30px 0 0",
                                    style: "background: #FFCC33",
                                    html: '<div style="height: 20px; width: 50px;"></div>'
                                },
                                {
                                    xtype: "container",
                                    margin: "0 0 10px 0",
                                    html: '<div style="font-weight: bold;">亮橙色</div><div>表示实热</div>'
                                }
                            ]
                        },
                        {
                            xtype: "container",
                            layout: "hbox",
                            items: [
                                {
                                    xtype: "container",
                                    width: "50px",
                                    height: "20px",
                                    margin: "0 30px 0 0",
                                    style: "background: #FFCCCC",
                                    html: '<div style="height: 20px; width: 50px;"></div>'
                                },
                                {
                                    xtype: "container",
                                    margin: "0 0 10px 0",
                                    html: '<div style="font-weight: bold;">浅粉色</div><div>表示虚热</div>'
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    width: 540,
                    style: "font-weight: bold;text-align: center;",
                    bind: {  
                        html: '方案' 
                    } 
                },
                {
                    xtype:"container",
                    style: "text-indent: 2em;",
                    bind: {  
                        html: '<div style="overflow:hidden;white-space：normal;width: 540px;">{recDetail}</div>',
                    } 
                }
            ]
        }
    ],
   
});
