Ext.define('Admin.model.basic.DistrictModel', {
    //extend: 'Admin.model.Base',
    extend:'Ext.data.Model',

    idProperty:"DistrictId",
    fields: [
        {name:"DistrictId",type:"int",mapping: 'districtId'},
        {name:"DistrictName", mapping: 'districtName'},
        {name:"CityId", mapping: 'cityId'}
    ]
});
