Ext.define('Admin.store.report.Stat3Store', {
    extend: 'Admin.store.BaseStore',
    alias: 'store.stat3report',

    model: 'Admin.model.report.Stat3Model',

    autoLoad:false,
    storeId: 'stat3report',
    proxy: {
         type: 'ajax',
         cors:true,
         useDefaultXhrHeader:false,
         url: Admin.proxy.API.MgmtPath + 'stat/3',
         reader: {
             type: 'json',
             rootProperty: 'data',
             totalProperty  : 'Total',
             successProperty: 'IsSuccess'
         }
     }
});
