Ext.define('Admin.view.hospital.WinDepartment', {
    extend: 'Ext.window.Window',
    controller: 'department',
    width:500,
    height:490,
    layout:"fit",
    modal:true,
    listeners: {
        activate:function(){
            var grid = this.down("#department-hospital");
            if (sessionStorage.getItem("AccountType") != "system" && sessionStorage.getItem("AccountType") != "Operator") {                                                                           
                grid.getStore().proxy.url = Admin.proxy.API.MgmtPath + "MedicalTreatmentCombination/hospital/findAll";
                var hid = sessionStorage.getItem("Hid");

                //如果只看下级医院
                if ( sessionStorage.getItem("MyPrvileges").split(",").indexOf("yilianti_hospital_lower_self_see") !=-1) {
                    grid.getStore().getProxy().extraParams = {
                        hid: hid,
                        b: false,
                    }
                }else{
                    grid.getStore().getProxy().extraParams = {
                        hid: hid,
                        b: true,
                    }
                    
                }
            }
            grid.getStore().removeAll();
            grid.getStore().loadPage(1);
        }
    },
    items: [
        {
            xtype:"form",
            layout: 'anchor',
            defaultType: 'textfield',
            padding:"10 20",
            itemId:"frmDepartment",
            defaults: {
                anchor: '100%',
                labelWidth:65
            },
            items:[
                {
                    xtype:"combobox",
                    fieldLabel:"<span style='color:red'>*</span>医院",
                    name:"hospitalId",
                    store:Ext.create("Admin.store.hospital.HospitalGetStore"),
                    displayField:"name",
                    valueField:"id",
                    editable:false,
                    allowBlank:false,
                    itemId:"department-hospital",
                    listeners: { //监听   
                        render : function(combo) {//渲染   
                            combo.getStore().load();
                            var grid = Ext.getCmp("departmenthospital");
                            if (grid.selection) {
                                // var selectrow = grid.selection.data;
                                var selectrow = grid.selection.get("id");
                                // combo.setRawValue(selectrow.Name);//第一个值
                                combo.select(selectrow);
                            }   
                        }   
                    } 

                },
                {
                    fieldLabel:"<span style='color:red'>*</span>科室名",
                    name:"name",
                    allowBlank:false,
                },
                {
                    fieldLabel:"备注",
                    name:"memo"
                }
            ]
        }
    ],
    bbar:[
        {
            xtype:"container",
            flex:1,
            layout:{
                type:"hbox",
                pack:"center",
                align:"center"
            },
            padding:"0 0 10 0",
            items:[
                {
                    xtype:"tbspacer"
                },{
                    xtype:"button",
                    text:"提交",
                    handler:"onSubmitButtonClick"
                }
            ]
        }
    ]
});
