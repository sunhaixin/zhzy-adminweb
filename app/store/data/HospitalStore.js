Ext.define('Admin.store.data.HospitalStore', {
    extend: 'Admin.store.BaseStore',
    alias: 'store.datahospital',

    model: 'Admin.model.hospital.HospitalModel',

    autoLoad: false,
    storeId: 'datahospital',
    proxy: {
         type: 'ajax',
         cors:true,
         useDefaultXhrHeader:false,
         url: Admin.proxy.API.MgmtPath + "MedicalTreatmentCombination/hospital/findAll",
         reader: {
             type: 'json',
             rootProperty: 'data',
             totalProperty  : 'totalCount',
             successProperty: 'success'
         }
     },
    listeners:{
        "load":function(store, records, options){
            if (records.length > 1) {
                store.insert(0,{Id:0,id:0,Name:'全部医院',phantom:false});
            }
        }
    }
});
