Ext.define('Admin.view.yilianti.WinYiliantiHospital', {
    extend: 'Ext.window.Window',
    controller: 'yilianti',
    width:400,
    height:200,
    layout:"fit",
    listeners:{
        activate:function(){
            var grid = this.down("[xtype=combobox]");
            grid.getStore().load();
            var store = grid.getStore();
            var hid = sessionStorage.getItem("Hid");
            if (sessionStorage.getItem("MyPrvileges").indexOf("yilianti_group_see_self") !=-1) {
                store.getProxy().extraParams = {
                    "hid":hid,
                    "search": "yilianti_group_see_self"
                };
            }else{
                store.getProxy().extraParams = {
                    "hid":hid,
                    "search": "all"
                };
            }

            var hospital = Ext.getCmp("yilianti_hospital").getStore();
            var InitFormGrid = Ext.getCmp("idfrmYiliantiHospital").InitFormGrid;
            if (InitFormGrid) {
                var groupid = InitFormGrid.get("Id");
                hospital.getProxy().extraParams = {
                    groupid: groupid,
                    type: "medicine"
                }
                hospital.load();
            }
            
        }
    },
    modal:true,
    items: [
        {
            xtype:"form",
            layout: 'anchor',
            defaultType: 'textfield',
            padding:"10 20",
            id: "idfrmYiliantiHospital",
            itemId:"frmYiliantiHospital",
            defaults: {
                anchor: '100%',
                labelWidth:80
            },
            items:[
                {
                    xtype:"combobox",
                    fieldLabel:"<span style='color:red'>*</span>医联体名称",
                    name:"MtcId",
                    id:"winYiliantiGroup",
                    store:Ext.create("Admin.store.yilianti.YiliantiGroupStore"),//Ext.data.StoreManager.lookup('yiliantigroup'),
                    displayField:"Name",
                    valueField:"Id",
                    editable:false,
                    allowBlank:false,
                    listeners:{
                        render : function(combo) {//渲染   
                            combo.getStore().load();
                            var grid = Ext.getCmp("lstp-yiliantiHospital");
                            if (grid.selection) {
                                var selectrow = grid.selection.get("Id");
                                combo.select(selectrow);
                            }   
                        }
                    }
                },
                {
                    xtype:"combobox",
                    fieldLabel:"<span style='color:red'>*</span>医院",
                    name:"hids",
                    id: "yilianti_hospital",
                    store:Ext.create("Admin.store.hospital.HospitalStore"),
                    displayField:"Name",
                    valueField:"Id",
                    allowBlank:false,
                    multiSelect: true,
                    queryMode: 'remote',
                    listConfig : {    
                        itemTpl : Ext.create('Ext.XTemplate','<input type=checkbox>{[values.Name]}'),
                        onItemSelect : function(record) {  
                            var node = this.getNode(record);    
                            if (node) {    
                                Ext.fly(node).addCls(this.selectedItemCls);    
                                var checkboxs = node.getElementsByTagName("input");    
                                if (checkboxs != null)    
                                    var checkbox = checkboxs[0];    
                                checkbox.checked = true;    
                            }    
                        },  
                        listeners : {    
                            itemclick : function(view, record, item, index, e, eOpts) {    
                                var isSelected = view.isSelected(item);    
                                var checkboxs = item.getElementsByTagName("input");    
                                if (checkboxs != null) {    
                                    var checkbox = checkboxs[0];    
                                    if (!isSelected) {    
                                        checkbox.checked = true;    
                                    } else {    
                                        checkbox.checked = false;    
                                    }    
                                }    
                            }    
                        }   
                    },
                    listeners : {
                        'beforequery':function(e){
                            var combo = e.combo;  
                            combo.getStore().clearFilter();
                            if(!e.forceAll){  
                                var input = e.query;  
                                // 检索的正则
                                if(input != ""){
                                    var regExp = new RegExp(".*" + input + ".*");
                                    // 执行检索
                                    combo.store.filterBy(function(record,id){  
                                        return regExp.test(record.get("Name"));
                                    });
                                    combo.expand();  
                                    return false;
                                }
                                return false;
                            }
                        }
                    }
                }
            ]
        }
    ],
    bbar:[
        {
            xtype:"container",
            flex:1,
            layout:{
                type:"hbox",
                pack:"center",
                align:"center"
            },
            padding:"0 0 10 0",
            items:[
                {
                    xtype:"tbspacer"
                },{
                    xtype:"button",
                    text:"提交",
                    handler:"onSubmitYiliantiHospitalButtonClick"
                }
            ]
        }
    ]
});
