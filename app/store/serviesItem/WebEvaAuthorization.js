Ext.define('Admin.store.serviesItem.WebEvaAuthorization', {
  extend: 'Admin.store.BaseStore',
  alias: 'store.webEvaAuthorization',

  fields: [
      {name:"Id",mapping:"id"},
      {name:"Name",mapping:"name"},
      {name:"ServiesCount",mapping:"serviceCount"}, 
      {name:"ServicesId",mapping:"serviceId"},
      {name:"ServiesStartTime",mapping:"serviceStartTime"},
      {name:"ServiesEndTime",mapping:"serviceEndTime"},
      {name: "IsDateUnLimited",mapping:"isDateUnLimited"},
      {name: "EntryAddress",mapping:"entryAddress"},
      {name: "IsChange",mapping:"isChange"},
      {name: "Remainder",mapping:"remainder"}
  ],

  autoLoad: false,
  storeId: 'webEvaAuthorization',
  proxy: {
       type: 'ajax',
       cors:true,
       useDefaultXhrHeader:false,
       url: Admin.proxy.API.BasePaths+"api/admin/ServiesByHospitalId/query",
       reader: {
           type: 'json',
           rootProperty: 'Data',
           totalProperty  : 'Total',
           successProperty: 'IsSuccess'
       }
   }
});