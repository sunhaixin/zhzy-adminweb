Ext.define('Admin.store.report.Stat7Store', {
    extend: 'Admin.store.BaseStore',
    alias: 'store.stat7report',

    model: 'Admin.model.report.Stat7Model',

    autoLoad:false,
    storeId: 'stat7report',
    proxy: {
         type: 'ajax',
         cors:true,
         useDefaultXhrHeader:false,
         url: Admin.proxy.API.MgmtPath + 'stat/7',
         reader: {
             type: 'json',
             rootProperty: 'data',
             totalProperty  : 'Total',
             successProperty: 'IsSuccess'
         }
     }
});
