Ext.define("Admin.view.hospital.DepartmentController",{
    extend: 'Ext.app.ViewController',
    alias: 'controller.department',

    onHospitalItemClick:function(){
        var view = this.getView();
        var dpGrid = view.down("#GridDepartment");
        var hid = arguments[1].get("id");
        dpGrid.getStore().getProxy().extraParams = {"hospitalId":hid};
        dpGrid.getStore().remove();
        dpGrid.getStore().loadPage(1);
    },
    onDepartmentfilters: function(){
        var view = this.getView();
        var textfield = view.down("[xtype=textfield]").value;
        var dpGrid = Ext.getCmp("departmenthospital");
        dpGrid.getStore().clearFilter();
        var regExp = new RegExp(".*"+textfield+".*");
        if (textfield) {
            dpGrid.getStore().filterBy(function(record) { 
                    // return record.get('Name') == textfield;
                    return regExp.test(record.get("name"));
            });
        }
        dpGrid.getStore().remove();
        dpGrid.getStore().loadPage(1);
    },
    onSubmitButtonClick:function(){
        var view = this.getView();
        if(view.type == "Add"){
            this.onAddDepartmentSubmit(arguments);
        }else{
            this.onUpdateDepartmentSubmit(arguments);
        }
    },
    onAddDepartmentSubmit:function(){
        var view = this.getView();
        var frm = view.down("#frmDepartment");
        var valus = frm.getValues();
        if (!frm.form.isValid()) {
             Ext.Msg.alert("提示","请检查必填项");
            return false;
        }

        var controller = Ext.create("Admin.view.controller.admincontroller");
        var url = 'department/insert';
        var store = view.BindGrid.getStore();
        controller.post(valus, url, store, view);
    },
    onUpdateDepartmentSubmit:function(){
        var view = this.getView();
        var frm = view.down("#frmDepartment");
        var valus = frm.getValues();
        valus.uid = view.record.get("uid");

        valus.hospitalId = view.record.get("hospitalId");
        if (!frm.form.isValid()) {
             Ext.Msg.alert("提示","请检查必填项");
            return false;
        }

        var controller = Ext.create("Admin.view.controller.admincontroller");
        var url = 'department/update';
        var store = view.BindGrid.getStore();
        controller.post(valus, url, store, view);
    },
    onDepartmentDelete:function(){
        var record = arguments[5];
        var grid = this.getView().down("#GridDepartment");
        var store = grid.getStore();
        Ext.Msg.confirm("提示","确定要删除吗？",function(bt){
            var me = this;
            if(bt == "yes"){
                Ext.Ajax.request({
                    url:Admin.proxy.API.MgmtPath + "api/admin/department/delete",
                    cors:true,
                    useDefaultXhrHeader:false,
                    jsonData:{data:{Uid:record.get("Uid")},token:"123456"},
                    method:"Post",
                    success:function(resp){
                        var bean = Ext.decode(resp.responseText);
                        if(bean.IsSuccess){
                            Ext.Msg.alert("提示","删除成功!");
                            me.close();
                            store.reload();
                        }else{
                            Ext.Msg.alert("提示",bean.Message);
                        }
                    }
                });
            }else{
                me.close();
            }
        });
    }
});