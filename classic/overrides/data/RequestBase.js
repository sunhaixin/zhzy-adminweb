Ext.define("Admin.override.form.Combobox",{
    override: 'Ext.data.request.Ajax',
    setupHeaders:function(xhr, options, data, params){
        var me = this; 
        
        if (options && Admin.TokenStorage.retrieve()) {
            options.headers = options.headers || {};
            options.headers['Authorization'] = Admin.TokenStorage.retrieve();//'Bearer '+Admin.TokenStorage.retrieve(); 
        }
        var headers = me.callOverridden(arguments);
        return headers;
    }
});