Ext.define('Admin.store.report.Stat14_1Store', {
    extend: 'Admin.store.BaseStore',
    alias: 'store.stat14_1report',

    model: 'Admin.model.report.Stat14Model',

    autoLoad:false,
    storeId: 'stat14_1report',
    proxy: {
         type: 'ajax',
         cors:true,
         useDefaultXhrHeader:false,
         url: Admin.proxy.API.BasePaths + 'api/admin/stat/14',
         reader: {
             type: 'json',
             rootProperty: 'Data',
             totalProperty  : 'totalCount',
             successProperty: 'success'
         }
     }
});
