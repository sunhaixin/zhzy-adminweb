Ext.define('Admin.view.authentication.AuthenticationController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.authentication',
    onFaceBookLogin : function() {
        this.redirectTo('dashboard', true);
    },

    onLoginButton: function() {
        var me = this;
        var view = this.getView();
        var frm = Ext.getCmp("authdialog");
        if (!frm.form.isValid()) {
            Ext.Msg.alert("提示","请检查必填项")
            return false;
        }
        model = this.getViewModel();
        var controller = Ext.create("Admin.view.controller.admincontroller");
        Ext.Ajax.request({
            url: Admin.proxy.API.RbacPath + "account/login",
            cors: true,
            useDefaultXhrHeader:false,
            jsonData:{loginId:model.get("Loginid"),pswd:model.get("Pswd")},
            params:{loginId:model.get("Loginid"),pswd:model.get("Pswd")},
            method:"Post",
            success:function(resp){
                var bean = Ext.decode(resp.responseText);
                
                if (bean.code == 200) {
                    var data = bean.data;
                    //当选中记忆密码时保存当前密码，否则删除账号密码
                    var memory = view.down("[xtype=checkboxfield]");
                    if (memory.getValue()) {
                        localStorage.setItem("gyhl_user",model.get("Loginid"));
                        localStorage.setItem("gyhl_pass",model.get("Pswd"));
                        localStorage.setItem("gyhl_persist",memory.getValue());
                    }else{
                        localStorage.removeItem("gyhl_user");
                        localStorage.removeItem("gyhl_pass");
                    }
                    Admin.TokenStorage.save(data.token);
                    sessionStorage.setItem("AccountType",data.accountTypeCode);
                    if(data.hospitalId){
                        sessionStorage.setItem("Hid",data.hospitalId);
                    }
                    if(data.id){
                        sessionStorage.setItem("Aid",data.id);
                    }
                    sessionStorage.setItem("UserName",data.userName);
                    var treeStore = Ext.data.StoreManager.lookup("NavigationTree");
                    var menus = Ext.encode(me.onForEach(data.menus));
                    sessionStorage.setItem("menus",menus);
                    var Menus = me.onForEach(data.menus);

                    sessionStorage.setItem("privileges",Ext.encode(data.privileges));
                    var myprivileges = Ext.decode(sessionStorage.getItem("privileges"));
                    var privileges = [];
                    myprivileges.forEach(function(bean){
                        privileges.push(bean.code);
                    });
                    sessionStorage.setItem("MyPrvileges",privileges.join(','))
                    

                    var mainContainer = view.up("maincontainer");
                    mainContainer.removeAll();
                    var pal = Ext.create("Admin.view.main.Main",{
                        store:Ext.create("Admin.store.NavigationTree",{
                            root:{
                                expanded:true,
                                children:Menus
                            }
                        })
                    });
                    mainContainer.add(pal);
                    var st = Ext.getCmp("leftMenus").getStore();
                    location.reload();
                }
                else{
                    Ext.Msg.alert("提示",bean.message);
                    // return false;
                }
            }
        });
    },

    onLoginAsButton: function() {
        this.redirectTo('login', true);
    },

    onNewAccount:  function() {
        this.redirectTo('register', true);
    },

    onSignupClick:  function() {
        this.redirectTo('dashboard', true);
    },

    onResetClick:  function() {
        this.redirectTo('dashboard', true);
    }
    ,onForEach: function(data){
        var me = this;
        for (var i = data.length - 1; i >= 0; i--) {
            data[i].iconCls = "x-fa fa-h-square";
            if (data[i].IsLeaf) {
                data[i].iconCls = "x-fa fa-h-square";
                data[i].rowCls = "nav-tree-badge";
                data[i].cls = "nav-tree-badge";
            }else{
                // me.onForEach(data[i].Children);
            }
        }
        return data;
    }
});