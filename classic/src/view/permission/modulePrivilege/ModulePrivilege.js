Ext.define("Admin.view.permission.modulePrivilege.ModulePrivilege",{
	extend: "Admin.view.main.BaseContainer",
	layout: "fit",
	cls:"content-Panel-BorderStyle",
	xtype: "modulePrivilege",
	requires: [
		"Admin.view.main.BaseContainer"
	],
	margin: 20,
	items: [
		{
			xtype: 'grid',
			title: '账号权限管理',
			privileges:["rolepermission_see"],
			height: "100%",
			width: "100%",
			// padding: 10,
			id: "AccountTypeGrid",
			cls: "content-Panel-BorderStyle",
			tbar: [
				{
					xtype: 'container',
					items: [
						// {
						// 	xtype: 'button',
						// 	text: '新增账号类型',
						// 	privileges:["rolepermission_add"],
						// 	handler:function(){
						// 		var win = Ext.create("Admin.view.permission.rolePermission.WinAddAccountType");
						// 		win.setTitle = "新增账号类型";
						// 		win.type = "Add";
						// 		win.BindGrid = this.ownerCt.ownerCt.ownerCt;
						// 		win.show();
						// 	}
						// }
					]
				}
			],
			// store: Ext.create("Admin.store.permission.AccountTypeStore",{
			// 	pageSize:1000,
			// 	autoLoad: true
			// }),
			viewConfig:{  
                enableTextSelection:true
            },
			// columns: {
			// 	defaults: {
			// 		align: "center"
			// 	},
			// 	items: [
			// 		{
			// 			text: "账号类型",
			// 			dataIndex: "Name",
			// 			flex: 1
			// 		},
			// 		{
			// 			text: "Code",
			// 			dataIndex: "Code",
			// 			flex: 1
			// 		},
			// 		{
   //                      xtype:"actioncolumn",
   //                      // xtype:"baseactioncolumn",
   //                      text:"操作",
   //                      flex: 1,
   //                      items:[
   //                          {
   //                              iconCls:"x-fa fa-cog",
   //                              // tooltip:"编辑权限",
   //                              tooltip: (sessionStorage.getItem("MyPrvileges")?(sessionStorage.getItem("MyPrvileges").split(",").indexOf("rolepermission_revisions_power")==-1?"查看权限":"编辑权限"):"查看权限"),
   //                              privileges:["rolepermission_revisions_power"],
   //                              // disabled: (sessionStorage.getItem("MyPrvileges")?(sessionStorage.getItem("MyPrvileges").split(",").indexOf("rolepermission_revisions_power")==-1?true:false):true),
   //                              // disabled: (Ext.MyPrvileges?(Ext.MyPrvileges.indexOf("rolepermission_revisions_power")==-1?true:false):true),
   //                              handler:function(){
			// 						var BindGrid = this.ownerCt.ownerCt;
			// 							var grid = Ext.getCmp("AccountTypeGrid");
			// 							grid.getSelectionModel().select(arguments[5],true);
			// 							var win = Ext.create("Admin.view.permission.rolePermission.WinSetAccountPermissions");
			// 							win.setTitle("设置"+BindGrid.selection.get("Name")+"权限");
			// 							win.TypeId = arguments[5].data.Id;
			// 							// win.record = arguments[5];
			// 							// var store = grid.getStore();
			// 							win.show();
   //                              }
   //                          }
                            
   //                      ]
   //                  }
			// 	]
			// },
			bbar:[
                {
                    xtype: 'pagingtoolbar',
                    store: Ext.data.StoreManager.lookup('accounttype'),
                    displayInfo: true,
                    displayMsg: '显示第 {0} 条到 {1} 条记录，一共 {2} 条',
                    emptyMsg: "没有数据可以显示"
                }
            ]
		}
	]
}); 