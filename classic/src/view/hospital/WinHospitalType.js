Ext.define('Admin.view.hospital.WinHospitalType', {
    extend: 'Ext.window.Window',
    controller: 'hospitalType',
    width:500,
    height:490,
    layout:"fit",
    modal:true,
    items: [
        {
            xtype:"form",
            itemId:"frmHospitalType",
            layout: 'anchor',
            defaultType: 'textfield',
            autoScroll:true,
            padding:"10 20",
            method:"POST",
            jsonSubmit:true,
            defaults: {
                anchor: '100%',
                labelWidth:65
            },
            items:[
                {
                    fieldLabel:"<span style='color:red'>*</span>医院类型名称",
                    name:"name",
                    allowBlank:false,               
                    blankText:'该项不能为空!'
                },
            ]
        }
    ],
    bbar:[
        {
            xtype:"container",
            flex:1,
            layout:{
                type:"hbox",
                pack:"center",
                align:"center"
            },
            padding:"0 0 10 0",
            items:[
                {
                    xtype:"tbspacer"
                },{
                    xtype:"button",
                    text:"提交",
                    handler:"onSubmitButtonClick"
                }
            ]
        }
    ]
});
