Ext.define('Admin.view.eva.EvaReport12', {
    extend: 'Ext.window.Window',
    controller: 'EvaData',
    width:600,
    height:600,
    layout:"fit",
    modal:true,
    listeners:{
        render: function (argument) {
            var me = this;
            var model = me.getViewModel();
            var uid = this.record.data.EvaUid;
            var eva = this.record.data.Eva;
            var report = this.down("#evaReport");
            var urls = me.type && me.type == "app"?Admin.proxy.API.AppDetail:Admin.proxy.API.PcDetail;
            urls += "?uid="+uid+"&eva="+eva;
            Ext.Ajax.request({
                url: urls,
                method:"Get",
                cors:true,//开启跨域模式
                useDefaultXhrHeader:false,//true的话header会带有X-Requested-With
                //binary:true, //为true后，后端将返回bytes
                success:function(resp){
                    var bean = Ext.decode(resp.responseText);
                        if(bean.success){
                        var data = bean.data;
                        let patientvalue = [];
                        let a = [], b = [];
                        let SCALE_TCM_VALUE = Admin.constants.Tcm.data;
                        if (data.patientValue1 && data.patientValue1.indexOf("|") == -1) {
                            const Tcm = data.patientValue1.split(',');
                            for (var i in Tcm) {
                                for (var j in SCALE_TCM_VALUE) {
                                    if (SCALE_TCM_VALUE[j].id == Tcm[i]) {
                                        patientvalue.push(SCALE_TCM_VALUE[j]);
                                    }
                                }
                            }
                            for (var z in patientvalue) {
                                if(patientvalue[z].id>=102 && patientvalue[z].id<=144){
                                    b.push(patientvalue[z].title)
                                }else{
                                    a.push(patientvalue[z].title)
                                }
                            }
                        } 

                        a = a.join("; ")
                        b = b.join("; ")
                        model.setData(
                            {
                                patientValue1:  a,
                                patientValue2:  data.patientValue2,
                                patientValue3:  b,
                                allTP: data.allTP,
                                report1: data.report1,
                                report2: data.report2
                            }
                        )

                        var recs = data.programeRec || null;
                        var ints = data.programeInt || null;
                        var alltp = data.allTP || null;

                        var Textareaid = Ext.getCmp("textareaid");
                        var alltp_arr = [];
                        Textareaid.removeAll();
                        alltp_arr[alltp_arr.length] = Admin.util.ProgrameList.createAlltp(alltp);
                        Textareaid.add(alltp_arr);

                        var Rectitles = Ext.getCmp("Rectitles12");
                        var items = [];

                        Rectitles.removeAll();

                        recs.forEach(function(bean,index){
                            items[items.length] = Admin.util.ProgrameList.createCmp(bean);
                        });
                        Rectitles.add(items);

                        var IntTitles = Ext.getCmp("IntTitles12");
                        items = [];

                        IntTitles.removeAll();

                        ints.forEach(function(bean,index){
                            items[items.length] = Admin.util.ProgrameList.createCmp(bean);
                        });
                        IntTitles.add(items);
                        
                    }else{
                        me.close();
                        Ext.Msg.alert("提示",bean.message);
                    }
                }
            });
        }
    },
    viewModel: {
        data: {
            name: ""
        }
    },
    items: [
        {
            xtype: "container",
            layout: "vbox",
            margin: 20,
            autoScroll:true,
            items: [
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    style: "font-weight: bold;",
                    bind: {  
                        html: '症状' 
                    } 
                },
                {
                    xtype:"container",
                    style: "text-indent: 2em;",
                    bind: {  
                        html: '<p style="word-wrap: break-word;width: 540px;">{patientValue1};{patientValue3}</p>',
                    } 
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    style: "text-indent: 2em;",
                    bind: {  
                        html: '<div>{!allTP || allTP == "" || allTP == "尚未进行客观化信息采集" || allTP == "尚未设置智能合参"? "" : "舌面脉客观化信息"}</div>' 
                    } 
                },
                {
                    xtype:"container",
                    id:"textareaid"
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    style: "font-weight: bold;",
                    bind: {  
                        html: '体质辨识结果' 
                    } 
                },
                {
                    xtype:"container",
                    style: "text-indent: 2em;",
                    bind: {  
                        html: '<p style="word-wrap: break-word;width: 540px;">{report2}</p>',
                    } 
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    style: "font-weight: bold;",
                    bind: {  
                        html: '中医辨证结果' 
                    } 
                },
                {
                    xtype:"container",
                    style: "text-indent: 2em;",
                    bind: {  
                        html: '<p style="word-wrap: break-word;width: 540px;">{report1}</p>',
                    } 
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    width: 540,
                    style: "font-weight: bold;text-align: center;",
                    bind: {  
                        html: '养生方案' 
                    } 
                },
                {
                    xtype:"container",
                    
                    id: "Rectitles12",
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    width: 540,
                    style: "font-weight: bold;text-align: center;",
                    bind: {  
                        html: '干预方案' 
                    } 
                },
                {
                    xtype:"container",
                    
                    id: "IntTitles12",
                },
            ]
        }
    ],
   
});
