Ext.define('Admin.store.report.Stat14_3Store', {
    extend: 'Admin.store.BaseStore',
    alias: 'store.stat14_3report',

    model: 'Admin.model.report.Stat14Model',

    autoLoad:false,
    storeId: 'stat14_3report',
    proxy: {
         type: 'ajax',
         cors:true,
         useDefaultXhrHeader:false,
         url: Admin.proxy.API.BasePaths + 'api/admin/stat/14',
         reader: {
             type: 'json',
             rootProperty: 'Data',
             totalProperty  : 'totalCount',
             successProperty: 'success'
         }
     }
});
