Ext.define('Admin.model.report.Stat7Model', {
    //extend: 'Admin.model.Base',
    extend:'Ext.data.Model',

    idProperty:"name",
    fields: [
        { name: "name" },
        { name: "value1", mapping: 'count1' },
        { name: "value2", mapping: 'count2' },
    ]
});
