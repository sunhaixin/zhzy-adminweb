Ext.define("Admin.store.permission.PrivilegeStore",{
    extend: 'Admin.store.BaseStore',
    alias: "store.privilegestore",
    fields: [
        
        {name: "Checked", mapping: 'checked'},
        {name: "Code", mapping: 'code'},
        {name: "CreateTime",type:"date", mapping: 'createTime'},
        {name: "GroupName", mapping: 'groupName'},
        {name: "Id", mapping: 'id'},
        {name: "MenuId", mapping: 'menuId'},
        {name: "Name", mapping: 'name'},
        {name: "UpdateTime",type:"date", mapping: 'updateTime'},
        {name: "Index", mapping: 'pIndex'}
    ],
    autoLoad: false,
    storeId: 'privilegestore',
    proxy: {
         type: 'ajax',
         cors:true,
         useDefaultXhrHeader:false,
         url: Admin.proxy.API.MgmtPath + "privilege/query",
        //  api: {
        //     create  : 'http://localhost:49712/api/admin/hospital/query',
        //     read  : 'http://localhost:49712/api/admin/hospital/query',
        //     update  : '/controller/update',
        //     destroy : '/controller/destroy_action'
        // },
         reader: {
             type: 'json',
             rootProperty: 'data',
             totalProperty  : 'totalCount',
             successProperty: 'success'
         }
     }
})