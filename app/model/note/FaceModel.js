Ext.define('Admin.model.note.FaceModel', {
    //extend: 'Admin.model.Base',
    extend:'Ext.data.Model',

    idProperty:"Id",
    fields:[
        {name:"Id"},
        {name:"DoctorId"},
        {name:"DoctorName"},
        {name:"PatientName"},
        {name:"PatientId"},  
        {name:"PicUrl"},
        {name:"PicMd5"},
        {name:"Comment"},
        {name:"IsActive"}
    ]
});