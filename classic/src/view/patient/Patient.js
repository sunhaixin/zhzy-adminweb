Ext.define('Admin.view.patient.Patient', {
    extend: 'Ext.container.Container',
    xtype: 'patient',
    layout: 'fit',
    items: [
        {
            xtype:"grid",
            title:"患者管理",
            tbar:[
                {
                    xtype:"container",
                    layout:"hbox",
                    defaults:{
                        margin:"0 5"
                    },
                    items:[
                        {
                            xtype:"button",
                            text:"编辑"
                        },
                        {
                            xtype:"button",
                            text:"删除"
                        }
                    ]
                }
            ],
            selModel: {
                selType: 'checkboxmodel',
                checkOnly: true,
                showHeaderCheckbox: true
            },
            columns:[
                {
                    width:60,
                },{
                    text:"姓名",
                    flex:1
                },
                {
                    text:"Email",
                },
                {
                    text:"拼音"
                },
                {
                    text:"性别",
                },
                {
                    text:"出生日期",
                },
                {
                    text:"身份证",
                    flex:1
                },
                {
                    text:"联系方式",
                },
            ]
        }
    ]
});
