Ext.define("Admin.store.permission.SetAccountPermission",{
    extend: "Ext.data.TreeStore",
    alias: 'store.setsccountpermission',

    fields: [
        { name: 'id', mapping: "id", type: 'string'},
		{ name: 'text', mapping: "name", type: 'string'},
		{ name: 'parentId', mapping: "parentNode" },
        { name: 'path', mapping: "path", type: 'string' },
        { name: 'index', mapping: "index", type: 'int' },
        { name: 'leaf', mapping: "leaf", type: "boolean" },
        { name: 'checked', mapping: "checked", type: "boolean" },
        { name: 'groupName', mapping: 'groupNames', type: 'string' },
        { name: "code", mapping: 'code', type: 'string' }
    ],

    autoLoad: false,
    storeId: 'setsccountpermission',
    proxy: {
         type: 'ajax',
         cors:true,
         useDefaultXhrHeader:false,
         url: Admin.proxy.API.MgmtPath + "menu/queryAllByAccountType",
         reader: {
             type: 'json',
             rootProperty: 'children',
             successProperty: 'success'
         }
     },

    //  proxy: {
    //     type: "ajax",
    //     url: Ext.RootUrl + "Knowledge/GetCategoryTree",
    //     reader: {
    //         type: "json",
    //         root: "Children"
    //     }
    //     ,
    //     actionMethods: {
    //         read: 'Get'
    //     }
    // },
    // root: {
    //     text: 'Root',
    //     expanded: true,
    //     id:"null",
    //     rootVisible: false
    // }
})