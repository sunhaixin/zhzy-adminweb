Ext.define("Admin.view.telemedicne.TelemedicneDelete",{
    extend: "Ext.menu.Menu",
    alias: "view.telemedicnedelete",
    // privileges:["yilianti_group_ lower_hospital_add"],
    controller: 'group',
    items: [
        {
            text: "删除该远程医疗小组",
            itemId: 'telemedicnedelete',
            handler: 'onTelemedicineDelete'
        }
    ]
})