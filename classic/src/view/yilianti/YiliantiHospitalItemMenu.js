Ext.define("Admin.view.yilianti.YiliantiHospitalItemMenu",{
    extend: "Ext.menu.Menu",
    alias: "view.yiliantihospitalitemmenu",
    items: [
        {
            text: "添加下级医院",
            handler: function(){
                var addhospitalWin = Ext.create("Admin.view.yilianti.WinYiliantiAddHospital");
                addhospitalWin.parentid = this.ownerCt.selectedNodeId;
                addhospitalWin.MtcId = Ext.getCmp("lstp-yiliantiHospital").selection.data.Id;
                addhospitalWin.type = "AddHospital";
                addhospitalWin.BindGrid = Ext.getCmp("GridDepartment");
                addhospitalWin.show();
                addhospitalWin.query("textfield")[0].setActive(true);
            }
        },
        {
            text: "删除该成员",
            handler: function(){
                var me = this;
                var grid = Ext.getCmp("GridDepartment")
                var data = me.ownerCt.raw.data;
                var store = grid.getStore();
                Ext.Msg.confirm("提示","确定要删除吗？",function(bt){
                    var conf = this;
                    if(bt == "yes"){
                        Ext.Ajax.request({
                        url:Admin.proxy.API.MgmtPath + "MedicalTreatmentCombination/hospital/delete",
                        cors:true,
                        useDefaultXhrHeader:false,
                        params:{
                            id:data.id,
                        },
                        method:"Post",
                        success:function(resp){
                             var bean = Ext.decode(resp.responseText);
                            if(bean.code== 200){
                                Ext.Msg.alert("提示",bean.message);
                                conf.close();
                                store.reload();

                            }else{
                                Ext.Msg.alert("提示",bean.Message);
                            }
                        }
                    });
                    }else{
                        conf.close();
                    }
                });
            }
        }
    ]
})