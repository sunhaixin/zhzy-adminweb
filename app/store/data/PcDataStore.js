Ext.define("Admin.store.data.PcDataStore",{
    extend: 'Admin.store.BaseStore',
    alias: "store.pcdatatore",
    fields: [
        {name: "UId",mapping:"uid"},
        {name: "HospitalId",mapping:"hospitalId"},
        {name: "HospitalName",mapping:"hospitalName"},
        {name: "PatientId",mapping:"patientId"},
        {name: "DoctorId",mapping:"doctorId"},
        {name: "Code",mapping:"code"},
        {name: "Title",mapping:"title"},
        {name: "Result",mapping:"result"},
        {name: "Eva",mapping:"eva"},
        {name: "EvaUid",mapping:"evaUid"},
        {name: "DoctorName",mapping:"doctorName"},
        {name: "PatientName",mapping:"patientName"},
        {name: "CreateTime",mapping:"createTime"},
        {name: "UpdateTime",mapping:"updateTime"},
        {name: "IsFinish",mapping:"isFinish"}
    ],
    autoLoad: true,
    storeId: 'pcdatastore',
    proxy: {
         type: 'ajax',
         cors:true,
         useDefaultXhrHeader:false,
         url: Admin.proxy.API.MgmtPath + "pc/pcQuery",//Admin.proxy.API.BasePath+"api/Admin/data/pc/query",
         timeout: 90000,
         reader: {
             type: 'json',
             rootProperty: 'data',
             totalProperty  : 'totalCount',
             successProperty: 'success'
         }
     }
})