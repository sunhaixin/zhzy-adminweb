Ext.define("Admin.view.system.WinAccountMap",{
    extend:"Ext.window.Window",
    title:"设置数据平台banner图",
    width:600,
    height:200,
    layout:"fit",
    controller:"account",
    modal:true,
    resizeAble:false,
    listeners: {
        render: function (argument) {
            var data = this.record.data;
            // if(data.MapUrl){
                var form = this.down("[xtype=form]");
                var s = Ext.create("Ext.container.Container",{
                    html: `<div>之前的图片</div>${data.mapUrl}`,
                })
                form.add(s);
            // }
        }
    },
    items:[
        {
            xtype:"form",
            // itemId:"frmSetHospitalBanner",
            width:"100%",
            padding:10,
            items:[
                {
                    xtype: 'filefield',
                    // label: "MyPhoto:",
                    fieldLabel: "选择app图片",
                    buttonText: "选择图片",
                    name: 'map',
                    accept: 'image'
                }
            ]
        }
    ],
    bbar:[
        {
            xtype:"container",
            width:"100%",
            layout:{
                type:"hbox",
                pack:"center",
                align:"center"
            },
            items:[
                {
                    xtype:"button",
                    text:"提交",
                    handler: "addBannerSubmit"
                }
            ]
        }
    ]
    
});