Ext.define("Admin.view.hospital.system.AccountController",{
    extend: 'Ext.app.ViewController',
    alias: 'controller.account',

    onHospitalItemClick:function(){
        var view = this.getView();
        var dpGrid = view.down("#GridAccount");
        var hid = arguments[1].get("id");
        dpGrid.getStore().getProxy().extraParams = {"hospitalId":hid};
        dpGrid.getStore().remove();
        dpGrid.getStore().loadPage(1);
        // var filters = dpGrid.getStore().getFilters();
        // filters.clearFilters();
        // filters.add({property:"hid",value:1});
        
    },
    onAccountHospitalFielts: function(){
        var textfield = Ext.getCmp("Account-Hospital").down("[xtype=textfield]").value;
        var dpGrid = Ext.getCmp("Account-Hospital");
        dpGrid.getStore().clearFilter();
        var regExp = new RegExp(".*"+textfield+".*");
        if (textfield) {
            dpGrid.getStore().filterBy(function(record) { 
                    // return record.get('Name') == textfield;
                    // var filterBy = regExp.test(record.get("Name","")) || regExp.test(record.get("Id",""));
                    // return filterBy;
                    return regExp.test(record.get("name"));
            });
        }
        dpGrid.getStore().remove();
        dpGrid.getStore().loadPage(1);
    },
    onSubmitButtonClick:function(){
        var view = this.getView();
        if(view.type == "Add"){
            this.onAddAccountSubmit(arguments);
        }else{
            this.onUpdateAccountSubmit(arguments);
        }
    },
    onAddAccountSubmit:function(){
        var view = this.getView();
        var frm = view.down("#frmAccount");
        var valus = frm.getValues();
        var dataNameCombo = view.down("[xtype=combobox]");
        var s = dataNameCombo.findRecordByValue(dataNameCombo.getValue());

        if (!frm.form.isValid()) {
             Ext.Msg.alert("提示","请检查必填项");
            return false;
        }
        valus.accountTypeCode = s.get("code");
        var controller = Ext.create("Admin.view.controller.admincontroller");
        var url = 'account/add';
        var store = view.BindGrid.getStore();
        controller.post(valus, url, store, view);
    },
    onUpdateAccountSubmit:function(){
        var view = this.getView();
        var frm = view.down("#frmAccount");
        var valus = frm.getValues();
        valus.Id = view.record.get("id");

        // return false;
        if (!frm.form.isValid()) {
             Ext.Msg.alert("提示","请检查必填项");
            return false;
        }
        var com = view.down("#zzcombo");
        valus.accountTypeCode = com.selection.get("code");

        var controller = Ext.create("Admin.view.controller.admincontroller");
        var url = 'account/update';
        var store = view.BindGrid.getStore();
        controller.post(valus, url, store, view);
    },
    onAccountDelete:function(){
        var record = arguments[5];
        var grid = this.getView().down("#GridAccount");
        var store = grid.getStore();

        var controller = Ext.create("Admin.view.controller.admincontroller");
        var url = 'account/delete';
        var valus = {id: record.get('id')};
        controller.deletePost(valus, url, store);
    },
    // onSetHospitalSubmit:function(){
    //     var view = this.getView();
    //     var form = view.down("[xtype=multiselector]");
    //     var items = form.getStore().getData().items;
    //     var values = [];
    //     items.forEach(function(bean){
    //         values.push(bean.get("Id"));
    //     });
    //     var val = values.join(";");
    //     // if(!form.isValid()){
    //     //     return;
    //     // }
    //     // if(!values.Id){
    //     //     values.Id = [];
    //     // }
    //     // var modules = values.Id.join(";");
    //     var store = view.BindGrid.getStore();
    //     var aid = sessionStorage.getItem("Aid");
    //     Ext.Ajax.request({
    //         url:Admin.proxy.API.BasePath+"api/admin/account/hospital",
    //         cors:true,
    //         useDefaultXhrHeader:false,
    //         jsonData:{
    //             data:{
    //                 Aid:aid,
    //                 Hid:val
    //             },
    //             token:"123456"
    //         },
    //         method:"Post",
    //         success:function(resp){
    //             var bean = Ext.decode(resp.responseText);
    //             if(bean.IsSuccess){
    //                 Ext.Msg.alert("提示","设置成功!");
    //                 view.close();
    //                 store.reload();
    //             }else{
    //                 Ext.Msg.alert("提示",bean.Message);
    //             }
    //         }
    //     });
    // },
    onBtnSetAccountPswd:function(){
        var record = arguments[5];
        var win = Ext.Msg.prompt("设置密码","密码",function(btn,text){
            if(btn == "ok"){
                var valus = {id: record.get('id'), newPswd: text};
                var controller = Ext.create("Admin.view.controller.admincontroller");
                var url = 'account/pswd';
                controller.post(valus, url);

            }else{
                win.close();
            }
        },this);
    },
    randomPswd: function(argument) {
        var view = this.getView();
        var pswdField = view.down("#Pswd");
        // var pswdField = Ext.getCmp("accountPswd");
        var pswd = parseInt(Math.random() * 1000000) + "";
        if (pswd.length < 6) {
            pswd = parseInt(Math.random() * 1000000) + "";
        }
        pswdField.setValue(pswd);
    },

    onSetHospitalBanner:function(callback){
        var view = this.getView();
        var frm = view.down("[xtype=form]");
        var form = frm.getForm();
        var value = view.down("[xtype=filefield]");
        var photo = value.value;
        var fileEl = value.fileInputEl.dom;  
        var fd = new FormData();  
        fd.append('uploadFile',fileEl.files[0]);  
        if(photo=="")
        { 
            Ext.Msg.alert("提示","请上传图片");
            return false;
        }
        else
        {
            if(!/\.(gif|jpg|jpeg|png|GIF|JPG|PNG)$/.test(photo))
            {
              Ext.Msg.alert("提示","图片类型必须是.gif,jpeg,jpg,png中的一种");
              return false;
            }
        }
        Ext.Ajax.request({  
            // url:Admin.proxy.API.BasePath + "api/news/upload",  
            url:Admin.proxy.API.MgmtPath + "account/uploadImg",  
            cors:true,  
            useDefaultXhrHeader:false,  
            method:'post',  
            rawData:fd,  
            headers: {  
                "Content-Type":null   /* 最重要的部分，将Content-Type设置成null，ExtJs则会由内部的XMLHTTPRequest自动生成包含boundary的Content-Type，否则在使用rawData的情况下，ExtJs会将Content-Type设置成text/plain */  
            },  
            success:function (resp) {  
                var img = Ext.decode(resp.responseText).data;
                callback(img);
            },  
            failure:function (resp) {  
                Ext.Msg.alert("提示","上传失败"); 
                return false;
            }  
        });
    },
    addBannerSubmit: function(){
        var view = this.getView();
        var values = {id: view.record.id};
        var img = this.onSetHospitalBanner(img=>{
            values.mapUrl = img;
            Ext.Ajax.request({
                url: Admin.proxy.API.MgmtPath+"account/update",
                method:"Post",
                cors:true,//开启跨域模式
                useDefaultXhrHeader:false,//true的话header会带有X-Requested-With
                //binary:true, //为true后，后端将返回bytes
                params:values,
                success:function(resp){
                    var bean = Ext.decode(resp.responseText);
                    if(bean.success){
                        // form.getForm().reset(); 
                        Ext.Msg.alert("提示","设置成功");
                        view.close();
                    }else{
                        Ext.Msg.alert("提示",bean.message);
                        view.close();
                    }
                }
            });
        });
        
    }
});