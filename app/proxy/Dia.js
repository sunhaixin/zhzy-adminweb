Ext.define('Admin.constants.Dia', {
    statics:{
        data: [
		    {
		      id: '76',
		      title: '消瘦',
		    },
		    {
		      id: '62',
		      title: '肥胖',
		    },
		    {
		      id: '72',
		      title: '畏寒'
		    },
		    {
		      id: '191',
		      title: '畏风'
		    },
		    {
		      id: '105',
		      title: '肢冷'
		    },
		    {
		      id: '1',
		      title: '潮热'
		    },
		    {
		      id: '19',
		      title: '烘热'
		    },
		    {
		      id: '73',
		      title: '畏热'
		    },
		    {
		      id: '75',
		      title: '五心烦热'
		    },
		    {
		      id: '58',
		      title: '手足心热'
		    },
		    {
		      id: '130',
		      title: '自觉发热'
		    },
		    {
		      id: '144',
		      title: '心胸烦热'
		    },
		    {
		      id: '10',
		      title: '盗汗',
		    },
		    {
		      id: '111',
		      title: '自汗',
		    },
		    {
		      id: '21',
		      title: '急躁易怒',
		    },
		    {
		      id: '52',
		      title: '善叹息',
		    },
		    {
		      id: '132',
		      title: '善惊恐',
		    },
		    {
		      id: '133',
		      title: '善忧思',
		    },
		    {
		      id: '13',
		      title: '多梦',
		    },
		    {
		      id: '54',
		      title: '失眠'
		    },
		    {
		      id: '36',
		      title: '懒言'
		    },
		    {
		      id: '26',
		      title: '倦怠乏力'
		    },
		    {
		      id: '53',
		      title: '神疲'
		    },
		    {
		      id: '183',
		      title: '嗜睡（嗜卧）'
		    },
		    {
		      id: '100',
		      title: '容易疲劳'
		    },
		    {
		      id: '4',
		      title: '游走痛'
		    },
		    {
		      id: '63',
		      title: '疼痛固定不移'
		    },
		    {
		      id: '186',
		      title: '疼痛拒按'
		    },
		    {
		      id: '187',
		      title: '疼痛喜按'
		    },
		    {
		      id: '65',
		      title: '头痛'
		    },
		    {
		      id: '68',
		      title: '头胀痛'
		    },
		    {
		      id: '49',
		      title: '偏头痛'
		    },
		    {
		      id: '11',
		      title: '巅顶痛'
		    },
		    {
		      id: '91',
		      title: '牙龈肿痛'
		    },  
		    {
		      id: '90',
		      title: '胸痛'
		    },
		    {
		      id: '88',
		      title: '胸刺痛'
		    },
		    {
		      id: '85',
		      title: '胁痛'
		    },
		    {
		      id: '82',
		      title: '胁肋刺痛'
		    },
		    {
		      id: '84',
		      title: '胁肋胀痛'
		    },
		    {
		      id: '95',
		      title: '腰痛'
		    },
		    {
		      id: '197',
		      title: '腰腿酸痛'
		    },
		    {
		      id: '189',
		      title: '脘腹胀痛'
		    },
		    {
		      id: '190',
		      title: '脘腹灼痛'
		    },
		    {
		      id: '200',
		      title: '肢体刺痛'
		    },
		    {
		      id: '201',
		      title: '肢体木痛'
		    },
		    {
		      id: '134',
		      title: '头麻木'
		    }, 
		    {
		      id: '148',
		      title: '手指麻木'
		    }, 
		    {
		      id: '116',
		      title: '口唇麻木'
		    }, 
		    {
		      id: '108',
		      title: '肢体麻木'
		    },       
		    {
		      id: '94',
		      title: '腰酸'
		    }, 
		    {
		      id: '96',
		      title: '腰膝酸软'
		    }, 
		    {
		      id: '147',
		      title: '身重'
		    }, 
		    {
		      id: '69',
		      title: '头重'
		    },  
		    {
		      id: '107',
		      title: '肢体困重'
		    },   
		     {
		        id: '68',
		        title: '头胀痛'
		     }, 
		     {
		        id: '11',
		        title: '巅顶痛'
		     },
		     {
		        id: '64',
		        title: '头昏'
		     },
		     {
		        id: '66',
		        title: '眩晕'
		     }, 
		     {
		        id: '67',
		        title: '头胀'
		     }, 
		     {
		        id: '69',
		        title: '头重'
		     },
		     {
		        id: '134',
		        title: '头麻木'
		     },
		     {
		        id: '91',
		        title: '牙龈肿痛'
		     },
		     {
		        id: '17',
		        title: '脱发'
		     },
		      {
		        id: '38',
		        title: '面红'
		      },
		       {
		        id: '39',
		        title: '颜面浮肿'
		      },
		       {
		        id: '40',
		        title: '面色苍白'
		      },
		       {
		        id: '41',
		        title: '面色黧黑'
		      },
		       {
		        id: '42',
		        title: '面色萎黄'
		      },
		       {
		        id: '123',
		        title: '颧红'
		      },
		       {
		        id: '175',
		        title: '面色无华'
		      },

		      {
		        id: '29',
		        title: '口臭'
		      }, 
		      {
		        id: '34',
		        title: '口苦'
		      }, 
		      {
		        id: '35',
		        title: '口甜'
		      },
		      {
		        id: '118',
		        title: '口淡'
		      },
		      {
		        id: '32',
		        title: '口粘腻'
		      },
		      {
		        id: '119',
		        title: '口舌生疮'
		      },
		      {
		        id: '115',
		        title: '口唇干燥'
		      },
		      {
		        id: '116',
		        title: '口唇麻木'
		      },
		      {
		        id: '117',
		        title: '口唇色淡'
		      },
		      {
		        id: '3',
		        title: '口唇紫'
		      },
		      {
		        id: '167',
		        title: '口唇红'
		      },  
		      {
		        id: '86',
		        title: '心烦'
		      },
		      {
		        id: '87',
		        title: '心悸'
		      },
		      {
		        id: '88',
		        title: '胸刺痛'
		      },
		      {
		        id: '89',
		        title: '胸闷'
		      },
		      {
		        id: '90',
		        title: '胸痛'
		      },
		      {
		        id: '144',
		        title: '心胸烦热'
		      },

		      {
		        id: '143',
		        title: '胃脘痛'
		      }, 
		      {
		        id: '71',
		        title: '脘腹痞满'
		      }, 
		      {
		        id: '188',
		        title: '脘腹坚满'
		      }, 
		      {
		        id: '189',
		        title: '脘腹胀痛'
		      }, 
		      {
		        id: '190',
		        title: '脘腹灼痛'
		      }, 
		      {
		        id: '18',
		        title: '腹胀'
		      }, 
		      {
		        id: '55',
		        title: '食后痞胀'
		      }, 
		      {
		        id: '136',
		        title: '肠鸣'
		      }, 
		      {
		        id: '137',
		        title: '腹痛，喜温喜按'
		      }, 
		      {
		        id: '170',
		        title: '腹胀痛'
		      }, 
		      {
		        id: '178',
		        title: '少腹疼痛'
		      },
		      {
		        id: '179',
		        title: '少腹胀满'
		      },
		      {
		        id: '180',
		        title: '少腹胀痛'
		      },
		      {
		        id: '194',
		        title: '小腹冷痛'
		      },
		    
		      {
		        id: '85',
		        title: '胁痛'
		      }, 
		      {
		        id: '82',
		        title: '胁肋刺痛'
		      }, 
		      {
		        id: '84',
		        title: '胁肋胀痛'
		      }, 
		      {
		        id: '83',
		        title: '胁肋胀满'
		      },
		    
		      {
		        id: '94',
		        title: '腰酸'
		      }, 
		      {
		        id: '196',
		        title: '腰冷'
		      }, 
		      {
		        id: '95',
		        title: '腰痛'
		      }, 
		      {
		        id: '197',
		        title: '腰腿酸痛'
		      },
		      {
		        id: '96',
		        title: '腰膝酸软'
		      },
		      {
		        id: '145',
		        title: '背冷'
		      },
		      {
		        id: '163',
		        title: '背痛  '
		      },
		      
		      {
		        id: '59',
		        title: '四肢乏力'
		      },
		      {
		        id: '105',
		        title: '肢冷'
		      },
		      {
		        id: '107',
		        title: '肢体困重'
		      },
		      {
		        id: '108',
		        title: '肢体麻木'
		      },
		      {
		        id: '199',
		        title: '肢体颤抖'
		      },
		      {
		        id: '200',
		        title: '肢体刺痛'
		      }, 
		      {
		        id: '201',
		        title: '肢体木痛'
		      },
		      {
		        id: '106',
		        title: '肢体浮肿'
		      },
		      {
		        id: '146',
		        title: '两足痿弱'
		      },
		        
		      {
		        id: '109',
		        title: '爪甲少华'
		      }, 
		       {
		        id: '110',
		        title: '爪甲青紫'
		      }, 
		      {
		        id: '15',
		        title: '耳聋'
		      },
		      {
		        id: '16',
		        title: '耳鸣'
		      },
		      {
		        id: '169',
		        title: '耳轮枯焦'
		      },
		      {
		        id: '43',
		        title: '目赤'
		      },
		      {
		        id: '122',
		        title: '目胀'
		      },
		      {
		        id: '44',
		        title: '目干涩'
		      }, 
		      {
		        id: '57',
		        title: '视物模糊'
		      },
		      {
		        id: '121',
		        title: '眼屎较多'
		      },
		      {
		        id: '125',
		        title: '流泪'
		      },

		      {
		        id: '29',
		        title: '口臭'
		      }, 
		      {
		        id: '34',
		        title: '口苦'
		      }, 
		      {
		        id: '35',
		        title: '口甜'
		      }, 
		      {
		        id: '118',
		        title: '口淡'
		      }, 
		      {
		        id: '32',
		        title: '口粘腻'
		      },  
		      {
		        id: '119',
		        title: '口舌生疮'
		      },  
		      {
		        id: '115',
		        title: '口唇干燥'
		      },  
		      {
		        id: '116',
		        title: '口唇麻木'
		      },  
		      {
		        id: '117',
		        title: '口唇色淡'
		      },  
		      {
		        id: '3',
		        title: '口唇紫'
		      },  
		      {
		        id: '167',
		        title: '口唇红'
		      },  
		      {
		        id: '2',
		        title: '牙齿浮动'
		      },  
		      {
		        id: '91',
		        title: '牙龈肿痛'
		      }, 
		    
		      {
		        id: '164',
		        title: '鼻鼾'
		      }, 
		      {
		        id: '92',
		        title: '咽干'
		      },
		      {
		        id: '120',
		        title: '咽喉异物感'
		      },      
		      {
		        id: '0',
		        title: '嗳气'
		      },
		      {
		        id: '14',
		        title: '恶心'
		      },
		      {
		        id: '47',
		        title: '呕吐'
		      },
		      {
		        id: '51',
		        title: '善食易饥'
		      },
		      {
		        id: '56',
		        title: '食欲不振'
		      },
		      {
		        id: '135',
		        title: '嘈杂'
		      }, 
		      {
		        id: '138',
		        title: '呃逆'
		      },
		      {
		        id: '141',
		        title: '吞酸'
		      },
		      {
		        id: '173',
		        title: '饥不能食'
		      },

		        {
		          id: '31',
		          title: '口渴'
		        }, 
		        {
		          id: '27',
		          title: '渴喜冷饮'
		        }, 
		        {
		          id: '28',
		          title: '渴喜热饮'
		        }, 
		        {
		          id: '30',
		          title: '口干'
		        }, 
		        {
		          id: '33',
		          title: '渴欲饮水而不能多'
		        },  
		        {
		          id: '101',
		          title: '饮不解渴'
		        },  
		        {
		          id: '165',
		          title: '不欲饮'
		        },  
		        {
		          id: '174',
		          title: '口渴欲饮但欲饮水不欲咽'
		        },  
		        
		          {
		            id: '37',
		            title: '咯痰'
		          },
		          {
		            id: '60',
		            title: '痰多'
		          },
		          {
		            id: '181',
		            title: '痰少'
		          },
		          {
		            id: '61',
		            title: '痰滑易咯'
		          },
		          {
		            id: '127',
		            title: '痰液清稀'
		          },
		          {
		            id: '128',
		            title: '痰不易咯出'
		          }, 
		          {
		            id: '129',
		            title: '痰粘稠'
		          },
		        
		          {
		            id: '124',
		            title: '多唾'
		          }, 
		          {
		            id: '126',
		            title: '流涎'
		          }, 
		        
		          {
		            id: '5',
		            title: '大便干燥'
		          },
		          {
		            id: '6',
		            title: '大便秘结'
		          },
		          {
		            id: '7',
		            title: '大便溏薄'
		          },
		          {
		            id: '8',
		            title: '大便粘腻不爽'
		          },
		          {
		            id: '184',
		            title: '溏结不调'
		          },
		          {
		            id: '74',
		            title: '五更泄泻'
		          }, 
		          {
		            id: '168',
		            title: '大便泄泻'
		          },  
		          {
		            id: '139',
		            title: '食寒腹泻'
		          },  
		          {
		            id: '140',
		            title: '矢气频频'
		          },
		          {
		            id: '142',
		            title: '完谷不化'
		          },
		          {
		            id: '45',
		            title: '小便余沥'
		          }, 
		          {
		            id: '46',
		            title: '小便频数'
		          }, 
		          {
		            id: '77',
		            title: '小便短黄'
		          }, 
		          {
		            id: '78',
		            title: '小便短少'
		          }, 
		          {
		            id: '79',
		            title: '小便黄赤'
		          }, 
		          {
		            id: '80',
		            title: '小便清长'
		          }, 
		          {
		            id: '81',
		            title: '小便涩'
		          }, 
		          {
		            id: '97',
		            title: '夜尿多'
		          }, 
		          {
		            id: '176',
		            title: '小便量多'
		          }, 
		          {
		            id: '192',
		            title: '小便浑浊'
		          }, 
		          {
		            id: '193',
		            title: '小便失禁'
		          },
		  
		          {
		            id: '149',
		            title: '肛门瘙痒'
		          },
		          {
		            id: '70',
		            title: '阴部瘙痒'
		          },
		          {
		            id: '150',
		            title: '阴部潮湿'
		          },
		        
		          {
		            id: '9',
		            title: '带下清稀'
		          },
		          {
		            id: '152',
		            title: '带下稠厚'
		          },
		          {
		            id: '151',
		            title: '白带量多'
		          },
		          {
		            id: '153',
		            title: '黄带'
		          },
		          {
		            id: '24',
		            title: '月经量少'
		          }, 
		          {
		            id: '154',
		            title: '月经量多'
		          }, 
		          {
		            id: '155',
		            title: '月经色淡'
		          }, 
		          {
		            id: '25',
		            title: '经血紫暗'
		          }, 
		          {
		            id: '103',
		            title: '月经后期'
		          }, 
		          {
		            id: '104',
		            title: '经血夹块'
		          }, 
		          {
		            id: '156',
		            title: '经间期出血'
		          }, 
		          {
		            id: '157',
		            title: '经后腹痛'
		          }, 
		          {
		            id: '158',
		            title: '经前、经行腹痛'
		          }, 
		          {
		            id: '159',
		            title: '经前乳房胀痛'
		          }, 
		          {
		            id: '160',
		            title: '经行浮肿'
		          },
		          {
		            id: '161',
		            title: '经行情志异常'
		          },
		          {
		            id: '162',
		            title: '经行腰酸'
		          },
		          {
		            id: '166',
		            title: '不孕'
		          },
		          {
		            id: '195',
		            title: '性欲减低'
		          },
		          {
		            id: '93',
		            title: '阳痿'
		          }, 
		          {
		            id: '98',
		            title: '遗精'
		          }, 
		          {
		            id: '171',
		            title: '滑精'
		          }, 
		          {
		            id: '195',
		            title: '性欲减低'
		          }, 
		          {
		            id: '198',
		            title: '早泄'
		          },
		       
		          {
		            id: '20',
		            title: '肌肤甲错'
		          }, 
		          {
		            id: '23',
		            title: '皮肤痤疮或疮疖'
		          }, 
		          {
		            id: '48',
		            title: '皮肤干燥'
		          }, 
		          {
		            id: '102',
		            title: '皮肤瘀斑瘀点'
		          }, 
		    
		          {
		            id: '220',
		            title: '血脂高'
		          }, 
		          {
		            id: '223',
		            title: '血流变高凝状态'
		          }, 
		          {
		            id: '172',
		            title: '血糖（尿糖）高'
		          }, 
		          {
		            id: '112',
		            title: '常因情志不畅诱发或加重'
		          },
		          {
		            id: '113',
		            title: '劳则甚'
		          },
		          {
		            id: '114',
		            title: '遇冷尤甚'
		          },
		          {
		            id: '177',
		            title: '入夜尤甚'
		          },
		          {
		            id: '12',
		            title: '动则加重'
		          },
		          {
		            id: '185',
		            title: '疼痛常夜间加剧'
		          },
		          {
		            id: '50',
		            title: '气短'
		          }, 
		          {
		            id: '182',
		            title: '身痒'
		          }, 
		          {
		            id: '99',
		            title: '容易感冒'
		          }, 
		          {
		            id: '131',
		            title: '容易抽筋'
		          },  
		          {
		            id: '202',
		            title: '舌胖'
		          },
		          {
		            id: '203',
		            title: '舌瘦'
		          },
		          {
		            id: '204',
		            title: '齿痕舌'
		          },
		          {
		            id: '205',
		            title: '舌裂'
		          },
		          {
		            id: '206',
		            title: '舌赤红'
		          },
		          {
		            id: '207',
		            title: '舌暗红'
		          },
		          {
		            id: '208',
		            title: '舌紫'
		          },
		          {
		            id: '209',
		            title: '舌有瘀斑或瘀点'
		          }, 
		          {
		            id: '210',
		            title: '舌苔白'
		          }, 
		          {
		            id: '211',
		            title: '舌苔黄'
		          }, 
		          {
		            id: '212',
		            title: '舌苔腻'
		          }, 
		          {
		            id: '213',
		            title: '舌苔腐'
		          },
		          {
		            id: '214',
		            title: '脉弦'
		          }, 
		          {
		            id: '215',
		            title: '脉滑'
		          }, 
		          {
		            id: '216',
		            title: '脉数'
		          }, 
		          {
		            id: '217',
		            title: '脉迟'
		          }, 
		          {
		            id: '218',
		            title: '虚脉'
		          }, 
		          {
		            id: '219',
		            title: '脉结代'
		          },  
		],
        
        
    }
});