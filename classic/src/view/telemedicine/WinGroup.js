Ext.define('Admin.view.telemedicine.WinGroup', {
    extend: 'Ext.window.Window',
    controller: 'group',
    width:400,
    height:160,
    layout:"fit",
    modal:true,
    items: [
        {
            xtype:"form",
            layout: 'anchor',
            defaultType: 'textfield',
            padding:"10 20",
            itemId:"frmGroupHospital",
            defaults: {
                anchor: '100%',
                labelWidth:65
            },
            items:[
                {
                    xtype:"textfield",
                    fieldLabel:"<span style='color:red'>*</span>远程小组",
                    name:"name",
                    allowBlank:false
                }
            ]
        }
    ],
    bbar:[
        {
            xtype:"container",
            flex:1,
            layout:{
                type:"hbox",
                pack:"center",
                align:"center"
            },
            padding:"0 0 10 0",
            items:[
                {
                    xtype:"tbspacer"
                },{
                    xtype:"button",
                    text:"提交",
                    handler:"onSubmitButtonClick"
                }
            ]
        }
    ]
});
