Ext.define('Admin.view.note.Alpha', {
    extend: 'Ext.container.Container',
    xtype: 'alpha',
    layout: 'fit',
    padding:10,
    items: [
        {
            xtype:"grid",
            title:"常见病辅助辩证管理",
            tbar:[
                {
                    xtype:"container",
                    layout:"hbox",
                    defaults:{
                        margin:"0 5"
                    },
                    items:[
                        {
                            xtype:"button",
                            text:"编辑"
                        },
                        {
                            xtype:"button",
                            text:"删除"
                        }
                    ]
                }
            ],
            selModel: {
                selType: 'checkboxmodel',
                checkOnly: true,
                showHeaderCheckbox: true
            },
            columns:[
                {
                    width:60,
                },{
                    text:"医院名称",
                    flex:1
                },
                {
                    text:"医生"
                },
                {
                    text:"患者"
                },
                {
                    text:"来源"
                },
                {
                    text:"主述",
                    dataIndex:"MainCom"
                },
                {
                    text:"主症",
                    dataIndex:"PrimSym"
                },
                {
                    text:"兼症",
                    dataIndex:"AcpnSym"
                },
                {
                    text:"舌象采集图"
                },
                {
                    text:"舌象",
                    dataIndex:"TongueCon"
                },
                {
                    text:"脉象",
                    dataIndex:"PulseCon"
                },
                {
                    text:"辨证结果",
                    dataIndex:"SynResult"
                },
                {
                    text:"辩病结果",
                    dataIndex:"DisResult"
                },
                {
                    text:"中成药建议",
                    dataIndex:"MedSug"
                },
                {
                    text:"中成药结果",
                    dataIndex:"MedResult"
                },
                {
                    text:"汤药建议",
                    dataIndex:"TisaneSug"
                },
                {
                    text:"调养建议",
                    dataIndex:"Comment"
                },
            ]
        }
    ]
});
