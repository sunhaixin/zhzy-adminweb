Ext.define("Admin.view.report.Stat3Report",{
    extend: 'Ext.container.Container',
    xtype: 'stat3report',
    height:"100%",
    layout:"hbox",
    controller:"report",
    cls:"content-Panel-BorderStyle",
    margin: 20,
    gridId: "stat3report_grid",
    listeners:{
        afterrender:"onReportRender"
    },
    items:[
        {
            xtype:"panel",
            title:"成年人9体质类型分布",
            flex:1,
            style:"border:1px solid #ccc",
            layout:{
                type:"hbox",
                pack:"center"
            },
            autoScroll:true,
            height: "100%",
            autoScroll:true,
            items:[
                {
                    width:900,
                    height:500,
                    layout:"vbox",
                    items:[
                        {
                            xtype:"HGrid",
                            // xtype:"grid",
                            autoHeight:true,
                            width:850,
                            id: "stat3report_grid",
                            BindStore:Ext.create("Admin.store.report.Stat3Store",{autoLoad:false}),
                            style:"border:1px solid #ccc",
                            margin:10
                        },
                        {
                            xtype: 'cartesian',
                            flex:1,
                            reference: 'chart',
                            width: 900,
                            insetPadding: {
                                top: 40,
                                bottom: 40,
                                left: 20,
                                right: 40
                            },
                            store:Ext.data.StoreManager.lookup("stat3report"),
                            axes: [{
                                type: 'numeric',
                                position: 'left',
                                minimum: 0
                            }, {
                                type: 'category',
                                position: 'bottom'
                            }],
                            series: [{
                                type: 'bar',
                                stacked:false,
                                xField: 'name',
                                yField: 'value',
                                style: {
                                    minGapWidth: 50,
                                },
                                label: {
                                    field: 'value',
                                    display: 'insideEnd',
                                    orientation:"horizontal"
                                }
                            }]
                        }
                        
                    ]
                }
            ]
        }
    ]
});