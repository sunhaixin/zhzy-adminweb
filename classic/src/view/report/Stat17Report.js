Ext.define("Admin.view.report.Stat17Report",{
    extend: 'Ext.container.Container',
    xtype: 'stat17report',
    height:"100%",
    layout:"hbox",
    controller:"report",
    cls:"content-Panel-BorderStyle",
    margin: 20,
    gridId: "stat17report_grid",
    listeners:{
        afterrender:"onReportRender"
    },
    items:[
        {
            xtype:"panel",
            title:"产后女性常见中医证型分布Top5",
            flex:1,
            height:"100%",
            style:"border:1px solid #ccc",
            layout:{
                type:"hbox",
                pack:"center"
            },
            autoScroll:true,
            items:[
                {
                    width:1100,
                    height:500,
                    layout:"vbox",
                    items:[
                        {
                            xtype:"HGrid",
                            autoHeight:true,
                            width:"100%",
                            id: "stat17report_grid",
                            BindStore:Ext.create("Admin.store.report.Stat17Store",{autoLoad:false}),
                            style:"border:1px solid #ccc",
                            margin:10
                        }
                        ,{
                            xtype: 'cartesian',
                            flex:1,
                            reference: 'chart',
                            width: "100%",
                            insetPadding: {
                                top: 40,
                                bottom: 40,
                                left: 20,
                                right: 40
                            },
                            store:Ext.data.StoreManager.lookup("stat17report"),
                            axes: [{
                                type: 'numeric',
                                position: 'left',
                                minimum: 0
                            }, 
                            {
                                type: 'category',
                                position: 'bottom'
                            }
                            ],
                            series: [
                                {
                                    type: 'bar',
                                    xField: 'name',
                                    yField: 'value',
                                    style: {
                                        minGapWidth: 100
                                    },
                                    label: {
                                        field: 'value',
                                        display: 'insideEnd',
                                        orientation:"horizontal"
                                    }
                                }
                                
                            ]
                        }
                    ]
                }
            ]
        }
    ]
});

