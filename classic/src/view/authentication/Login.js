Ext.define('Admin.view.authentication.Login', {
    extend: 'Admin.view.authentication.LockingWindow',
    xtype: 'login',

    requires: [
        'Admin.view.authentication.Dialog',
        'Ext.container.Container',
        'Ext.form.field.Text',
        'Ext.form.field.Checkbox',
        'Ext.button.Button'
    ],
    header:false,
    //title: 'Let\'s Log In',
    defaultFocus: 'authdialog', // Focus the Auth Form to force field focus as well

    items: [
        {
            xtype: 'authdialog',
            id:"authdialog",
            defaultButton : 'loginButton',
            autoComplete: true,
            bodyPadding: '20 20',
            cls: 'auth-dialog-login',
            header: false,
            width: 415,
            layout: {
                type: 'vbox',
                align: 'stretch'
            },

            defaults : {
                margin : '5 0'
            },

            items: [
                {
                    xtype: 'label',
                    text: '输入您的账号'
                },
                {
                    xtype: 'textfield',
                    cls: 'auth-textbox',
                    name: 'Loginid',
                    bind: '{Loginid}',
                    height: 55,
                    hideLabel: true,
                    allowBlank : false,
                    emptyText: '账号',
                    triggers: {
                        glyphed: {
                            cls: 'trigger-glyph-noop auth-email-trigger'
                        }
                    },
                    listeners:{
                        render: function(){
                            if (localStorage.getItem("gyhl_user")) {
                                this.setValue(localStorage.getItem("gyhl_user"));
                            }
                        }
                    }
                },
                {
                    xtype: 'textfield',
                    cls: 'auth-textbox',
                    height: 55,
                    hideLabel: true,
                    emptyText: '密码',
                    inputType: 'password',
                    name: 'Pswd',
                    bind: '{Pswd}',
                    allowBlank : false,
                    triggers: {
                        glyphed: {
                            cls: 'trigger-glyph-noop auth-password-trigger'
                        }
                    },
                    listeners:{
                        render: function(){
                            if (localStorage.getItem("gyhl_pass")) {
                                this.setValue(localStorage.getItem("gyhl_pass"));
                            }
                        }
                    }
                },
                {
                    xtype: 'container',
                    layout: 'hbox',
                    items: [
                        {
                            xtype: 'checkboxfield',
                            flex : 1,
                            cls: 'form-panel-font-color rememberMeCheckbox',
                            height: 30,
                            bind: '{persist}',
                            boxLabel: '记住密码',
                            listeners:{
                                render:function(){
                                    if (localStorage.getItem("gyhl_persist")) {
                                        this.setValue(localStorage.getItem("gyhl_persist"));
                                    }
                                }
                            }
                        }
                    ]
                },
                {
                    xtype: 'button',
                    reference: 'loginButton',
                    scale: 'large',
                    ui: 'soft-green',
                    iconAlign: 'right',
                    iconCls: 'x-fa fa-angle-right',
                    text: '登录',
                    // formBind: true,
                    listeners: {
                        click: 'onLoginButton'
                    }
                }
            ]
        }
    ],

    initComponent: function() {
        this.addCls('user-login-register-container');
        this.callParent(arguments);
    }
});
