Ext.define("Admin.view.permission.privilegeMgr.PrivilegeMgr",{
	extend: "Admin.view.main.BaseContainer",
	xtype: "privilegeMgr",
	layout: "hbox",
	cls:"content-Panel-BorderStyle",
	controller: "privilegeMgr",
	items: [
		{
            xtype:"treepanel",
            cls:"content-Panel-BorderStyle",
            title:"菜单",
            privileges:["menu_see"],
			style:"background:red",
            //flex:1,
            margin: "10 5 10 10",
			width:400,
            height:"100%",
            itemId:"grdPrivilegeTree",
			id:"grdPrivilegeTree",
            style:"border:1px solid #d0d0d0 !important",
            rootVisible:false,
			store: Ext.create("Admin.store.permission.MenuStore"),
            viewConfig: {
			    plugins: {
                    treeviewdragdrop: {
                        containerScroll: true
                    },
			        ptype: 'treeviewdragdrop',
			        appendOnly: false   //只能拖拽到非叶节点上
			    },
			    listeners: {
			        drop: function (Node, data, overModel, dropPosition, options) {
			        }
			    }
			},
            tbar: [
                {
                    xtype: "container",
                    lyout: "hbox",
                    width: "100%",
                    items: [
                        {
                            xtype: "button",
                            text: "新增菜单",
                            privileges:["menu_add"],
                            handler:function(){
                                var win = Ext.create("Admin.view.permission.privilegeMgr.WinAddMenu");
                                // var win = Ext.create("Admin.view.permission.MenuItemMenu");
                                win.setTitle("新增菜单");
                                // win.BindGrid = this.ownerCt.ownerCt.ownerCt;
                                win.type = "RootAdd"
                                win.show();
                            }
                        }
                    ]
                }
            ],
            listeners:{
				itemcontextmenu:function(cmp, record, item, index, e, eOpts ){
					var me = this;
					//禁止浏览器右键菜单事件
					e.preventDefault();  
					e.stopEvent();
                     var myprivileges = Ext.decode(sessionStorage.getItem("privileges"));
                    var privileges = [];
                    myprivileges.forEach(function(bean){
                        privileges.push(bean.Code);
                    });
                    if (privileges.indexOf("menu_chrend_add") != -1) {
                        var menu = Ext.create("Admin.view.permission.privilegeMgr.MenuItemMenu").showAt(e.getXY());
						menu.selectedNodeId = record.raw.Id;
                    }else{
                    }
					
				},
				itemClick:"onSubMenuItemClick"
			}
        },
		{
			xtype:"grid",
			cls:"content-Panel-BorderStyle",
			flex:1,
			title:"权限",
			privileges:["menu_see"],
			itemId: "GridMenuPower",
			Id: "grdPrivilege",
			itemId: "grdPrivilege",
			margin: "10 10 10 10 5",
			height: "100%",
			store: Ext.create("Admin.store.permission.PrivilegeStore",{
				pageSize:100,
				// autoLoad:false
			}),
			viewConfig: {  
	            plugins: {  
	                ptype: "gridviewdragdrop",  
	                dragText: "可用鼠标拖拽进行上下排序"  
	            },
	            enableTextSelection:true
	        },
			tbar: [
				{
					xtype: 'container',
					items: [
						{
							xtype: 'button',
							text: '新增',
							privileges:["privilege_add"],
							handler:function(){
								var win = Ext.create("Admin.view.permission.privilegeMgr.WinAddPrivilege");
								win.setTitle("添加权限");
								win.BindGrid = this.ownerCt.ownerCt.ownerCt;
								win.type = "Add";
								var grid = Ext.getCmp("grdPrivilegeTree");
                                if (grid.selection) {
                                    var selectrow = grid.selection.data;
                                    win.MenuId = selectrow.id;
                                    win.show();
                                }else{
                                	Ext.Msg.alert("提示","请选择菜单后添加！");
                                }
								
							}
						},
						{
							xtype: "button",
							text: "确认排序",
							handler: 'onPrivilegeIndex'
						}
					]
				}
			],
			columns: {
				defaults: {
					align: "center"
				},
				items: [
					{
						text: "权限名称",
						dataIndex: "Name",
						flex: 1,
					},
					{
						text: "Code",
						dataIndex: "Code",
						flex: 1,
					},
					{
						text: "GroupName",
						dataIndex: "GroupName",
						flex: 1,
					}
					,
					{
                        xtype:"actioncolumn",
                        text:"修改",
                        items:[
                            {
                                iconCls: 'x-fa fa-pencil-square-o',
                                tooltip: '修改',
                                privileges:["privilege_revisions"],
                                handler:function(){
                                    var win = Ext.create("Admin.view.permission.privilegeMgr.WinAddPrivilege");
                                    win.BindGrid = this.ownerCt.ownerCt;
                                    win.setTitle("修改权限");
                                    win.type = "update";
                                    win.record = arguments[5];
                                    // win.down("#frmHospital").loadRecord(arguments[5]);
                                    win.show();
                                }
                            }
                        ]
                    }
				]
			}
		}

	]
})