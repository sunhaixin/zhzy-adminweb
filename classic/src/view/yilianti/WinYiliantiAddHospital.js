Ext.define('Admin.view.yilianti.WinYiliantiAddHospital', {
    extend: 'Ext.window.Window',
    controller: 'yilianti',
    width:400,
    height:160,
    layout:"fit",
    modal:true,
    listeners: {
        activate: function(){
            var hospital = Ext.getCmp("YiLiantiAdd_hospital").getStore();
            var groupid = this.MtcId;
            hospital.getProxy().extraParams = {
                groupid: groupid,
                type: "medicine"
            }
            hospital.load();
        }
    },
    items: [
        {
            xtype:"form",
            layout: 'anchor',
            defaultType: 'textfield',
            padding:"10 20",
            itemId:"frmYiliantiAddHospital",
            defaults: {
                anchor: '100%',
                labelWidth:65
            },
            items:[
                {
                    xtype:"combobox",
                    fieldLabel:"<span style='color:red'>*</span>医院",
                    name:"hids",
                    id: "YiLiantiAdd_hospital",
                    store:Ext.create("Admin.store.hospital.HospitalStore"),
                    displayField:"Name",
                    valueField:"Id",
                    // editable:false,
                    allowBlank: false,
                    multiSelect: true,
                    listConfig : {    
                        itemTpl : Ext.create('Ext.XTemplate','<input type=checkbox>{[values.Name]}'),
                        onItemSelect : function(record) {  
                            var node = this.getNode(record);    
                            if (node) {    
                                Ext.fly(node).addCls(this.selectedItemCls);    
                                var checkboxs = node.getElementsByTagName("input");    
                                if (checkboxs != null)    
                                    var checkbox = checkboxs[0];    
                                checkbox.checked = true;    
                            }    
                        },  
                        listeners : {    
                            itemclick : function(view, record, item, index, e, eOpts) {    
                                var isSelected = view.isSelected(item);    
                                var checkboxs = item.getElementsByTagName("input");    
                                if (checkboxs != null) {    
                                    var checkbox = checkboxs[0];    
                                    if (!isSelected) {    
                                        checkbox.checked = true;    
                                    } else {    
                                        checkbox.checked = false;    
                                    }    
                                }    
                            }    
                        }   
                    },
                    listeners : {
                        'beforequery':function(e){
                            var combo = e.combo;  
                            combo.getStore().clearFilter();
                            if(!e.forceAll){  
                                var input = e.query;  
                                // 检索的正则
                                if(input != ""){
                                    var regExp = new RegExp(".*" + input + ".*");
                                    // 执行检索
                                    combo.store.filterBy(function(record,id){  
                                        return regExp.test(record.get("Name"));
                                    });
                                    combo.expand();  
                                    return false;
                                }
                                return false;
                            }
                        }
                    }
                }
            ]
        }
    ],
    bbar:[
        {
            xtype:"container",
            flex:1,
            layout:{
                type:"hbox",
                pack:"center",
                align:"center"
            },
            padding:"0 0 10 0",
            items:[
                {
                    xtype:"tbspacer"
                },{
                    xtype:"button",
                    text:"提交",
                    handler:"onSubmitYiliantiHospitalButtonClick"
                }
            ]
        }
    ]
});
