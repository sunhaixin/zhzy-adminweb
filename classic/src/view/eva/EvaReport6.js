Ext.define('Admin.view.eva.EvaReport6', {
    extend: 'Ext.window.Window',
    controller: 'EvaData',
    width:600,
    height:600,
    layout:"fit",
    modal:true,
    listeners:{
        activate: function (argument) {
            var me = this;
            var model = me.getViewModel();
            model.set("name","jack");
            var uid = this.record.data.EvaUid;
            var eva = this.record.data.Eva;
            var report = this.down("#evaReport");
            var urls = me.type && me.type == "app"?Admin.proxy.API.AppDetail:Admin.proxy.API.PcDetail;
            urls += "?uid="+uid+"&eva="+eva;
            Ext.Ajax.request({
                url: urls,
                method:"Get",
                cors:true,//开启跨域模式
                useDefaultXhrHeader:false,//true的话header会带有X-Requested-With
                //binary:true, //为true后，后端将返回bytes
                success:function(resp){
                    var bean = Ext.decode(resp.responseText);
                        if(bean.success){
                        var data = bean.data;
                        model.setData(
                            {
                                report1: data.report1,
                                report2: data.report2,
                                report3: data.report3,
                                report4: data.report4,
                                report5: data.report5,
                            }
                        )
                        var recs = data.programeRec || null;
                        var chart = me.down("[xtype=cartesian]");
                        var store = chart.getStore();
                        var Report1 = data.report1.split(";");
                        var Report2 = data.report2.split(";");
                        Report1.forEach(function(bean,index){
                            var value = Report2[index] * 10;
                            var a = Report2[index] *1;
                            var text = a.toFixed(4).split(".")[1] ;
                            text = text.toString();
                            text = text.split("");
                            if (text[0] != 0) {
                                text = text[0]+ "" + text[1] + "." + text[2] + "" + text[3] + "%";
                            }
                            else{
                                text = text[1] + "." + text[2] + "" + text[3] + "%";
                            }                            
                            store.insert(index, Ext.create('Admin.model.eva.EvaReportModel',{name:bean,value:value,text:text}));
                        }) 
                        var Rectitles = Ext.getCmp("Rectitles6");
                        var items = [];
                        Rectitles.removeAll();
                        recs.forEach(function(bean,index){
                            bean.type == "chd";
                            items[items.length] = Admin.util.ProgrameList.createCmp(bean);
                        });
                        Rectitles.add(items);
                    }else{
                        me.close();
                        Ext.Msg.alert("提示",bean.message);
                    }
                }
            });
        }
    },
    viewModel: {
        data: {
            name: ""
        }
    },
    items: [
        {
            xtype: "container",
            layout: "vbox",
            margin: 20,
            items: [
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    style: "font-weight: bold;",
                    bind: {  
                        html: '体质类型' 
                    } 
                },
                {
                    xtype:"container",
                    style: "text-indent: 2em;",
                    bind: {  
                        html: '<p><div>{report3}</div><div>{report4}</div></p>',
                    } 
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    style: "font-weight: bold;",
                    bind: {  
                        html: '辨识分析图' 
                    } 
                },
                {
                    xtype: 'cartesian',
                    flex:1,
                    reference: 'chart',
                    width: "100%",
                    insetPadding: {
                        top: 40,
                        bottom: 40,
                        left: 20,
                        right: 40
                    },
                    store: {
                        model: 'Admin.model.eva.EvaReportModel',
                        data: [
                        ]
                    },
                    axes: [ {
                                type: 'numeric',
                                position: 'left',
                                minimum: 0
                            },{
                        type: 'category',
                        position: 'bottom'
                    }],
                    series: [{
                        type: 'bar',
                        xField: 'name',
                        yField: 'value',
                        style: {
                            minGapWidth: 50,
                        },
                        label: {
                            field: 'text',
                            display: 'insideEnd',
                            orientation:"horizontal"
                        }
                    }]
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    width: 540,
                    itemId: "fanan",
                    style: "font-weight: bold;text-align: center;",
                    bind: {  
                        html: '方案' 
                    }
                },
                {
                    xtype:"container",
                    id: "Rectitles6"
                },
            ]
        }
    ],
   
});
