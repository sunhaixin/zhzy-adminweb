Ext.define('Admin.store.report.Stat2Store', {
    extend: 'Admin.store.BaseStore',
    alias: 'store.stat2report',

    model: 'Admin.model.report.Stat2Model',

    autoLoad:false,
    storeId: 'stat2report',
    proxy: {
         type: 'ajax',
         cors:true,
         useDefaultXhrHeader:false,
         // url: Admin.proxy.API.MgmtPath + 'stat/2',
           url: Admin.proxy.API.BasePaths+'api/admin/stat/2',
         reader: {
             type: 'json',
             rootProperty: 'data',
             rootProperty: 'Data',
             totalProperty  : 'Total',
             successProperty: 'IsSuccess'
         }
     }

});
