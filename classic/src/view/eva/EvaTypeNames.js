Ext.define("Admin.view.eva.EvaTypeNames",{
    statics:{
        HandleTitle:(bean, type)=> {
            let title = "", text;
            if(type){
                text = bean
            }else{
                text = bean.data.Title
            }
            if(text == "中医心理测评" || text == "中医心理体质辨识"){
                title = "五态性格问卷测评（简）"
            }else if(text == "科研版体质辨识" || text =="中医体质辨识(科研版)" || text == "中医体质辨识（科研版）" || text == "中医体质（科研版）辨识"){
                title = "四诊合参体质辨识"
            }else if(text == "常见病辅助辨证" || text == "中成药辅助辨证报告"){
                title = "中成药辅助辨证"
            }else if(text == "常态女性女性健康状态测评"){
                title = "常态女性健康状态测评"
            }else if(text == "产妇健康状态测评"){
                title = "产后女性健康状态测评"
            }else if(text == "面像采集报告"){
                title = "面色数据采集"
            }else{
                title = text
            }
            return title;
        },

        ReportName:(report) =>{
            var report1_ = "";
            if(report){
                if(report.split(';')[1]){
                if(report.indexOf("倾向是") != - 1 && report.split(';')[1].indexOf("是") != - 1){
                    if(report.indexOf("基本是") != -1 && report.indexOf("倾向是") != -1){
                        report1_ = report;
                    }else{
                        if(report[0] == "是"){
                        report1_ = report.substring(1,25)
                        }else{
                        report1_ = report.substring(0,25)
                        }
                    }
                }else if(report.indexOf("是") != - 1 ){
                    let report1_ = report.split('是');
                    let b = report1_[2] ?  report1_[2]: '';
                    let b1 = report1_[3] ?  report1_[3]: '';
                    report1_ = report1_[1] + b + b1
                }else{
                    report1_ = report;
                }
                }else{
                if(report.split('是')[0] == "倾向"){
                    report1_ = report;
                }else if(report.split('是')[0] == ""){
                    report1_ = report.substring(1,25)
                }else{
                    report1_ = report;
                }
                }
            }
            return report1_;
        }
    }
})