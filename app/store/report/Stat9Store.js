Ext.define('Admin.store.report.Stat9Store', {
    extend: 'Admin.store.BaseStore',
    alias: 'store.stat9report',

    model: 'Admin.model.report.Stat9Model',

    autoLoad:false,
    storeId: 'stat9report',
    proxy: {
         type: 'ajax',
         cors:true,
         useDefaultXhrHeader:false,
         url: Admin.proxy.API.MgmtPath + 'stat/9',
         reader: {
             type: 'json',
             rootProperty: 'data',
             totalProperty  : 'Total',
             successProperty: 'IsSuccess'
         }
     }
});
