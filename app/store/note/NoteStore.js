Ext.define('Admin.store.note.NoteStore', {
    extend: 'Admin.store.BaseStore',
    alias: 'store.note',

    model: 'Admin.model.note.NoteModel',

    autoLoad: false,
    storeId: 'note',
    proxy: {
         type: 'ajax',
         cors:true,
         useDefaultXhrHeader:false,
         url: Admin.proxy.API.BasePath+"api/admin/note/query",
         reader: {
             type: 'json',
             rootProperty: 'Data',
             totalProperty  : 'Total',
             successProperty: 'IsSuccess'
         }
     }
});
