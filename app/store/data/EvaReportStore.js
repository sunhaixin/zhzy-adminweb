Ext.define("Admin.store.data.EvaReportStore",{
    extend: 'Admin.store.BaseStore',
    alias: "store.evaReport",
    fields: [
        {name: "Uid"},
    ],
    autoLoad: false,
    storeId: 'evaReport',
    proxy: {
         type: 'ajax',
         cors:true,
         useDefaultXhrHeader:false,
         url: Admin.proxy.API.BasePath+"api/admin/data/pc/detail",
         reader: {
             type: 'json',
             rootProperty: 'data',
             totalProperty  : 'totalCount',
             successProperty: 'success'
         }
     }
})