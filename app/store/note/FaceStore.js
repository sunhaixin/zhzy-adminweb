Ext.define('Admin.store.note.FaceStore', {
    extend: 'Admin.store.BaseStore',
    alias: 'store.face',

    model: 'Admin.model.note.FaceModel',

    autoLoad: false,
    storeId: 'face',
    proxy: {
         type: 'ajax',
         cors:true,
         useDefaultXhrHeader:false,
         url: Admin.proxy.API.BasePath+"api/admin/face/query",
         reader: {
             type: 'json',
             rootProperty: 'Data',
             totalProperty  : 'Total',
             successProperty: 'IsSuccess'
         }
     }
});
