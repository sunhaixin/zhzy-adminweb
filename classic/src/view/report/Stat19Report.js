Ext.define("Admin.view.report.Stat19Report",{
  extend: 'Ext.container.Container',
  xtype: 'stat19report',
  height:"100%",
  layout:"hbox",
  controller:"report",
  cls:"content-Panel-BorderStyle",
  margin: 20,
  gridId: "stat19report_grid",
  listeners:{
      afterrender:"onReportRender"
  },
  items:[
      {
          xtype:"panel",
          title:"备孕女性常见中医证型分布Top5",
          flex:1,
          height:"100%",
          style:"border:1px solid #ccc",
          layout:{
              type:"hbox",
              pack:"center"
          },
          autoScroll:true,
      }
  ]
});

