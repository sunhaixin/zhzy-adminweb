Ext.define('Admin.view.note.Tongue', {
    extend: 'Ext.container.Container',
    xtype: 'tongue',
    layout: 'fit',
    listeners:{
        activate:function(){
            var grid = this.down("[xtype=grid]");
            grid.getStore().load();
        }
    },
    padding:10,
    items: [
        {
            xtype:"grid",
            title:"智能舌象管理",
            selModel: {
                selType: 'checkboxmodel',
                checkOnly: true,
                showHeaderCheckbox: true
            },
            store:Ext.create("Admin.store.note.TongueStore"),
            columns:[
                {
                    text:"医生姓名",
                    dataIndex:"DoctorName"
                },
                {
                    text:"患者姓名",
                    dataIndex:"PatientName"
                },
                {
                    text:"来源",
                    dataIndex:"Source"
                },
                {
                    text:"图片1",
                    dataIndex:"Step1Pic"
                },
                {
                    text:"图片2",
                    dataIndex:"Step2Pic"
                },
                {
                    text:"图片3",
                    dataIndex:"ImageOut"
                },
                {
                    text:"评论",
                    dataIndex:"Comment"
                },
                {
                    text:"创建时间",
                    dataIndex:"CreateTime",
                    width:160,
                    xtype:"datecolumn",
                    format:"Y-m-d H:i:s"
                }
            ],
            bbar:[
                {
                    xtype: 'pagingtoolbar',
                    store: Ext.data.StoreManager.lookup('tongue'),
                    displayInfo: true,
                    displayMsg: '显示第 {0} 条到 {1} 条记录，一共 {2} 条',
                    emptyMsg: "没有数据可以显示"
                }
            ]
        }
    ]
});
