Ext.define('Admin.model.basic.ProvinceModel', {
    //extend: 'Admin.model.Base',
    extend:'Ext.data.Model',

    idProperty:"ProvinceId",
    autoLoad:true,
    fields: [
        {name:"ProvinceId",type:"int",mapping: 'provinceId'},
        {name:"ProvinceName",mapping:"provinceName"}
    ]
});
