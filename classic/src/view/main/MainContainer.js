Ext.define("Admin.view.main.MainContainer",{
    extend:"Ext.container.Container",
    xtype:"maincontainer",
    id: "maincontainer",
    // items:[
    //     {
    //         xtype:"login"
    //     }
    // ],
    layout:"fit",
    initComponent:function(){
        var me = this;
        var view = this;
        if(window.location.search && window.location.search.split("?").length == 2){
            var arrs = window.location.search.split("?")[1].split("&");
            var params = {};
            arrs.forEach(function(bean){
                var arr = bean.split("=");
                var key = arr[0];
                var val = arr[1];
                if(key == "sign"){
                    params.Sign = val; 
                }
                if(key == "time"){
                    params.Time = val; 
                }
                if(key == "loginid"){
                    params.Loginid = decodeURI(val); 
                }
            });
            if(params.Sign && params.Time && params.Loginid){
                Ext.Ajax.request({
                    url:Admin.proxy.API.RbacPath+"account/auth",
                    cors:true,
                    useDefaultXhrHeader:false,
                    // jsonData:{data:{Loginid:params.Loginid,Sign:params.Sign,Time:params.Time}},
                    params: {
                        loginId: params.Loginid,
                        sign: params.Sign,
                        timeSpan: params.Time,
                    },
                    method:"Post",
                    success:function(resp){
                        var bean = Ext.decode(resp.responseText);
                        if(bean.IsSuccess){
                            Admin.TokenStorage.save(data.token);
                            sessionStorage.setItem("AccountType",data.accountTypeCode);
                            if(data.hospitalId){
                                sessionStorage.setItem("Hid",data.hospitalId);
                            }
                            if(data.id){
                                sessionStorage.setItem("Aid",data.id);
                            }
                            sessionStorage.setItem("UserName",data.userName);
                            var treeStore = Ext.data.StoreManager.lookup("NavigationTree");
                            var menus = Ext.encode(me.onForEach(data.menus));
                            sessionStorage.setItem("menus",menus);
                            var Menus = me.onForEach(data.menus);
                            sessionStorage.setItem("privileges",Ext.encode(data.privileges));
                            var myprivileges = Ext.decode(sessionStorage.getItem("privileges"));
                            var privileges = [];
                            myprivileges.forEach(function(bean){
                                privileges.push(bean.code);
                            });
                            sessionStorage.setItem("MyPrvileges",privileges.join(','))
                            

                            var mainContainer = view.up("maincontainer");
                            mainContainer.removeAll();
                            var pal = Ext.create("Admin.view.main.Main",{
                                store:Ext.create("Admin.store.NavigationTree",{
                                    root:{
                                        expanded:true,
                                        children:Menus
                                    }
                                })
                            });
                            mainContainer.add(pal);
                            var st = Ext.getCmp("leftMenus").getStore();
                            location.reload();
                        }else{
                            Ext.Msg.alert("提示",bean.message);
                        }
                    }
                });
            }
        }else{
            if(sessionStorage.getItem("Aid")){
                var Menus = Ext.decode(sessionStorage.getItem("menus"));
                var pal = Ext.create("Admin.view.main.Main",{
                    store:Ext.create("Admin.store.NavigationTree",{
                        root:{
                            expanded:true,
                            children:Menus
                        }
                    })
                });
                var st = Ext.getCmp("leftMenus").getStore();
                me.items = [pal];
                // me.items = [
                //     {
                //         xtype: "main"
                //     }
                // ]
            }else{
                me.items = {
                    xtype:"login"
                };
            }
        }
        
        me.callParent(arguments);
    }
    
})

