Ext.define('Admin.store.report.Stat2_1Store', {
    extend: 'Admin.store.BaseStore',
    alias: 'store.stat2_1report',

    model: 'Admin.model.report.Stat2Model',

    autoLoad:false,
    storeId: 'stat2_1report',
    proxy: {
         type: 'ajax',
         cors:true,
         useDefaultXhrHeader:false,
         // url: Admin.proxy.API.MgmtPath + 'stat/2?hasTotal=true',
         url: Admin.proxy.API.BasePaths+'api/admin/stat/2_1',
         reader: {
             type: 'json',
             // rootProperty: 'data',
             rootProperty: 'Data',
             totalProperty  : 'Total',
             successProperty: 'IsSuccess'
         }
     }

});
