Ext.define('Admin.store.report.Stat10Store', {
    extend: 'Admin.store.BaseStore',
    alias: 'store.stat10report',

    model: 'Admin.model.report.Stat10Model',

    autoLoad:false,
    storeId: 'stat10report',
    proxy: {
         type: 'ajax',
         cors:true,
         useDefaultXhrHeader:false,
         url: Admin.proxy.API.MgmtPath + 'stat/10',
         reader: {
             type: 'json',
             rootProperty: 'data',
             totalProperty  : 'Total',
             successProperty: 'IsSuccess'
         }
     }
});
