Ext.define('Admin.view.note.Inquiry', {
    extend: 'Ext.container.Container',
    xtype: 'inquiry',
    layout: 'fit',
    padding:10,
    listeners:{
        activate:function(){
            var grid = this.down("[xtype=grid]");
            grid.getStore().load();
        }
    },
    items: [
        {
            xtype:"grid",
            title:"问诊单管理",
            selModel: {
                selType: 'checkboxmodel',
                checkOnly: true,
                showHeaderCheckbox: true
            },
            store:Ext.create("Admin.store.note.InquiryStore"),
            columns:[
                {
                    text:"患者",
                    dataIndex:"PatientName"
                },
                {
                    text:"舌象",
                    dataIndex:"TonguePic",
                    width:300
                },
                {
                    text:"面象",
                    dataIndex:"FacePic",
                    width:300
                },
                {
                    text:"备注",
                    dataIndex:"Comment",
                    flex:1,
                },

                {
                    text:"创建时间",
                    dataIndex:"CreateTime",
                    width:160,
                    xtype:"datecolumn",
                    format:"Y-m-d H:i:s"
                }
            ],
            bbar:[
                {
                    xtype: 'pagingtoolbar',
                    store: Ext.data.StoreManager.lookup('inquiry'),
                    displayInfo: true,
                    displayMsg: '显示第 {0} 条到 {1} 条记录，一共 {2} 条',
                    emptyMsg: "没有数据可以显示"
                }
            ]
        }
    ]
});
