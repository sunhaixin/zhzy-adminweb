Ext.define('Admin.store.report.Stat8Store', {
    extend: 'Admin.store.BaseStore',
    alias: 'store.stat8report',

    model: 'Admin.model.report.Stat8Model',

    autoLoad:false,
    storeId: 'stat8report',
    proxy: {
         type: 'ajax',
         cors:true,
         useDefaultXhrHeader:false,
         url: Admin.proxy.API.MgmtPath + 'stat/8',
         reader: {
             type: 'json',
             rootProperty: 'data',
             totalProperty  : 'Total',
             successProperty: 'IsSuccess'
         }
     }
});
