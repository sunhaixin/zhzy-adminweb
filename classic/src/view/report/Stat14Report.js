Ext.define("Admin.view.report.Stat14Report",{
    extend: 'Ext.container.Container',
    xtype: 'stat14report',
    height:"100%",
    layout:"hbox",
    controller:"report",
    cls:"content-Panel-BorderStyle",
    margin: 20,
    gridId: "stat14report_grid",
    listeners:{
        afterrender:"onReportRender"
    },
    items:[
        {
            xtype:"panel",
            title:"居民面色分布",
            flex:1,
            height:"100%",
            style:"border:1px solid #ccc",
            layout:{
                type:"hbox",
                pack:"center"
            },
            autoScroll:true,   
            items:[
                {
                    width:350,
                    height:500,
                    layout:{
                        type:"vbox",
                        align: 'center'
                    },
                    items:[
                        {
                            xtype:"HGrid",
                            title:'面色（包含局部特征）排列前五指标',
                            autoHeight:true,
                            width:"100%",
                            id: "stat14report_1_grid",
                            BindStore:Ext.create("Admin.store.report.Stat14_1Store",{autoLoad:false}),
                            style:"border:1px solid #ccc",
                            margin:"30px 0 0 10px"
                        }
                        ,{
                            xtype: 'cartesian',
                            flex:1,
                            reference: 'chart',
                            width: "100%",
                            insetPadding: {
                                top: 40,
                                bottom: 40,
                                left: 20,
                                right: 40
                            },
                            store:Ext.data.StoreManager.lookup("stat14_1report"),
                            axes: [{
                                type: 'numeric',
                                position: 'left',
                                minimum: 0
                            }, 
                            {
                                type: 'category',
                                position: 'bottom'
                            }
                            ],
                            series: [
                                {
                                    type: 'bar',
                                    xField: 'name',
                                    yField: 'value',
                                    style: {
                                        minGapWidth: 50
                                    },
                                    label: {
                                        field: 'value',
                                        display: 'insideEnd',
                                        orientation:"horizontal"
                                    }
                                }
                                
                            ]
                        }
                    ]
                },
                {
                    width:350,
                    height:500,
                    layout:"vbox",
                    items:[
                        {
                            xtype:"HGrid",
                            autoHeight:true,
                            width:"100%",
                            title:"面部光泽",
                            id: "stat14report_2_grid",
                            BindStore:Ext.create("Admin.store.report.Stat14_2Store",{autoLoad:false}),
                            style:"border:1px solid #ccc",
                            margin:"30px 0 0 0"
                        }
                        ,{
                            xtype: 'cartesian',
                            flex:1,
                            reference: 'chart',
                            width: "100%",
                            insetPadding: {
                                top: 40,
                                bottom: 40,
                                left: 20,
                                right: 40
                            },
                            store:Ext.data.StoreManager.lookup("stat14_2report"),
                            axes: [{
                                type: 'numeric',
                                position: 'left',
                                minimum: 0
                            }, 
                            {
                                type: 'category',
                                position: 'bottom'
                            }
                            ],
                            series: [
                                {
                                    type: 'bar',
                                    xField: 'name',
                                    yField: 'value',
                                    style: {
                                        minGapWidth: 50
                                    },
                                    label: {
                                        field: 'value',
                                        display: 'insideEnd',
                                        orientation:"horizontal"
                                    }
                                }
                                
                            ]
                        }
                    ]
                },
                {
                    width:350,
                    height:500,
                    layout:"vbox",
                    items:[
                        {
                            xtype:"HGrid",
                            autoHeight:true,
                            width:"100%",
                            title:"唇色",
                            id: "stat14report_3_grid",
                            BindStore:Ext.create("Admin.store.report.Stat14_3Store",{autoLoad:false}),
                            style:"border:1px solid #ccc",
                            margin:"30px 0 0 0"
                        }
                        ,{
                            xtype: 'cartesian',
                            flex:1,
                            reference: 'chart',
                            width: "100%",
                            insetPadding: {
                                top: 40,
                                bottom: 40,
                                left: 20,
                                right: 40
                            },
                            store:Ext.data.StoreManager.lookup("stat14_3report"),
                            axes: [{
                                type: 'numeric',
                                position: 'left',
                                minimum: 0
                            }, 
                            {
                                type: 'category',
                                position: 'bottom'
                            }
                            ],
                            series: [
                                {
                                    type: 'bar',
                                    xField: 'name',
                                    yField: 'value',
                                    style: {
                                        minGapWidth: 50
                                    },
                                    label: {
                                        field: 'value',
                                        display: 'insideEnd',
                                        orientation:"horizontal"
                                    }
                                }
                                
                            ]
                        }
                    ]
                }
            ]
        }
    ]
});




