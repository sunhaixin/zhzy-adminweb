Ext.define('Admin.model.note.InquiryModel', {
    //extend: 'Admin.model.Base',
    extend:'Ext.data.Model',

    idProperty:"Id",
    fields: [
        {name:"Id"},
        {name:"DoctorId"},
        {name:"PatientId"},
        {name:"DoctorName"},
        {name:"PatientName"},
        {name:"Description"},
        {name:"Q1"},
        {name:"Q2"},
        {name:"Q3"},
        {name:"Q4"},
        {name:"Q5"},
        {name:"Q6"},
        {name:"Q7"},
        {name:"Q8"},
        {name:"Q9"},
        {name:"Q10"},
        {name:"Q11"},
        {name:"Q12"},
        {name:"TonguePic"},
        {name:"FacePic"},
        {name:"SynResult"},
        {name:"DisResult"},
        {name:"MediSug"},
        {name:"MedResult"},
        {name:"Comment"},
        {name:"MainCom"},
        {name:"Status"},
        {name:"IsActivePatient"},
        {name:"IsActiveDoctor"},
        {name:"CreateTime",type:"date"},
        {name:"UpdateTime",type:"date"}
    ]
});