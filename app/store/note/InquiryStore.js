Ext.define('Admin.store.note.InquiryStore', {
    extend: 'Admin.store.BaseStore',
    alias: 'store.inquiry',

    model: 'Admin.model.note.InquiryModel',

    autoLoad: false,
    storeId: 'inquiry',
    proxy: {
         type: 'ajax',
         cors:true,
         useDefaultXhrHeader:false,
         url: Admin.proxy.API.BasePath+"api/admin/inquiry/query",
         reader: {
             type: 'json',
             rootProperty: 'Data',
             totalProperty  : 'Total',
             successProperty: 'IsSuccess'
         }
     }
});
