Ext.define("Admin.view.pcEvaStat.PcEvaStat",{
    extend: 'Ext.container.Container',
    xtype: 'pcEvaStat',
    requires: [
        'Ext.chart.PolarChart',
        'Ext.chart.series.Pie',
        'Ext.chart.interactions.Rotate'
    ],
    layout:"hbox",
    height:"100%",
    margin: 20,
    cls:"content-Panel-BorderStyle",
    controller:"pcEvaStat",
    viewModel:{},
    id:"pcEvaStat",
    autoScroll: true,
    items:[
        {
            xtype: "container",
            width: "100%",
            items: [
                {
                    xtype:"panel",
                    title: "Web测评使用情况",
                    width: "100%",
                    height: "50%",
                    autoScroll:true,
                    border: true,
                    layout: "hbox",
                    tbar:{
                        xtype:"container",
                        layout:"hbox",
                        style:{
                            "background":"white"
                        },
                        items:[{
                            xtype:"combobox",
                            store:Ext.create("Admin.store.pcEvaStat.ServiceHospitalStore",{
                                pageSize: 100
                            }),
                            displayField:"Name",
                            width:300,
                            labelWidth:80,
                            margin:"10 50",
                            valueField:"Hid",
                            fieldLabel:"请选择医院",
                            listeners:{
                                change:async function(){
                                    var grid = Ext.getCmp("PcEvastat_grid");
                                    var pie = Ext.getCmp("PcEvaStatPolar");
                                    var store = Ext.create("Admin.store.pcEvaStat.PcEvaStatStore");
                                    store.getProxy().extraParams.hid = arguments[1];
                                    store.removeAll();
                                    var records = await store.loadSync();
                                    var data = records[0].data;
                                    var pal = Ext.getCmp("pcEvaStat");
                                    var viewModel = pal.getViewModel();
                                    data.count = 0;
                                    data.list.forEach(n=>{
                                        data.count += n.count;
                                    });
                                    //data.list.push({name:"总计",count:data.count});
                                    viewModel.setData(data);
                                    grid.setStore({
                                        data:data.list
                                    });
                                    pie.setStore({
                                        data:data.list
                                    });
                                }
                            }
                        }]
                    },
                    items: [
                        {
                            xtype: "container",
                            width: "10%",
                            height: "100%",
                            // style: "background: red"
                        },
                        {
                            xtype: "container",
                            width: "80%",
                            layout: "vbox",

                            items: [
                                {
                                    xtype: "container",
                                    style: "padding:20px 20px 0",
                                    html: "有效使用时间:",
                                    bind: {
                                        html: "有效使用时间:{serviceEndTime}"
                                    },
                                    width: 400
                                },
                                {
                                    xtype: "container",
                                    style: "padding:0 20px 20px",
                                    layout: "hbox",
                                    width: 300,
                                    items: [
                                        {
                                            xtype: "container",
                                            html: "剩余服务总次数:",
                                            bind: {
                                                html: "剩余服务总次数:{count}"
                                            }
                                        },
                                        // {
                                        //     xtype: 'button',
                                        //     padding: 0,
                                        //     margin: "0 0 0 10px",
                                        //     text: "使用记录导出",
                                        //     handler: "onPcEvaUseLogExport"
                                        // }
                                    ]
                                },
                                {
                                    xtype: "container",
                                    layout: "hbox",
                                    width: "100%",
                                    itemId: "WebEvaUse",
                                    margin: "0 0 10 0"
                                }
                            ]
                        }
                        
                    ]
                },
                {
                    xtype:"panel",
                    title:"工作量统计",
                    flex:1,
                    width: "100%",
                    height:"50%",
                    style: "background: #fff",
                    
                    // layout:{
                    //     type:"hbox",
                    //     pack:"center"
                    // },
                    layout: "hbox",
                    autoScroll:true,
                    tbar: {
                        xtype: "container",
                        layout: {
                            type: "hbox",
                            pack: "center"
                        },
                        padding: 10, 
                        items: [
                            {
                                xtype: 'datefield',
                                name: 'PcEvastat_startDate',
                                id: 'PcEvastat_startDate',
                                fieldLabel: '起始时间',
                                format: "Y-m-d",
                                width: 200,
                                // margin: '0 5 0 0',
                                labelWidth: 60,
                                allowBlank: false,
                                listeners: {
                                    change: "onEndDate"
                                }
                            },
                            {
                                xtype: 'datefield',
                                name: 'PcEvastat_endDate',
                                id: 'PcEvastat_endDate',
                                fieldLabel: '截止时间',
                                format: "Y-m-d",
                                margin: '0 0 0 5',
                                labelWidth: 60,
                                width: 200,
                                allowBlank: false,
                                listeners: {
                                    change: "onEndDate"
                                }
                            }
                        ]
                    },
                    items:[
                        {
                            xtype: "container",
                            width: "10%",
                            height: "100%",
                            // style: "background: red",
                        },
                        {
                            width: "80%",
                            height:500,
                            layout:{
                                type:"hbox"
                            },
                            items:[
                                {
                                    xtype:"grid",
                                    width:300,
                                    height:"100%",
                                    id: "PcEvastat_grid",
                                    //store:Ext.create("Admin.store.pcEvaStat.PcEvaStatStore"),
                                    // store:Ext.create("Admin.store.report.StatNo1Store",{autoLoad:false}),
                                    style:"border:1px solid #ccc",
                                    height:"100%",
                                    margin:10,
                                    columns:[
                                        {
                                            text:"项目",
                                            dataIndex:"name",
                                            flex:1,
                                        },{
                                            text:"工作量（人次）",
                                            dataIndex:"count",
                                            width:115
                                        }
                                    ]
                                },
                                {
                                    xtype: 'polar',
                                    flex:1,
                                    id: "PcEvaStatPolar",
                                    height: 400,
                                    width: 300,
                                    insetPadding: 20,
                                    innerPadding: 20,
                                    // store:Ext.data.StoreManager.lookup("stat1report"),
                                    //store:Ext.data.StoreManager.lookup("pcEvaStat"),
                                    // store: Ext.create("Admin.store.report.StatNo1Store"),
                                    legend: {
                                        docked: 'bottom'
                                    },
                                    interactions: ['rotate'],
                                    series: [{
                                        type: 'pie',
                                        angleField: 'count',
                                        label: {
                                            field: 'name',
                                            calloutLine: {
                                                length: 0,
                                                width: 0
                                                // specifying 'color' is also possible here
                                            },
                                            renderer:function(sprite, config, rendererData,data,index){
                                                var me = this;
                                                var count = 0;
                                                var records = data.store.getData().items;
                                                records.forEach(function(bean){
                                                    count += bean.get("count");
                                                });
                                                me.TotalCount = count;

                                                me.records = records;

                                                var val = me.records[arguments[4]];
                                                var percent = (val.get("count")/me.TotalCount)*100;
                                                if (sprite.length > 5) {
                                                    sprite = sprite.substring(0,sprite.length/2) + "\n" + sprite.substring(sprite.length/2, name.length);
                                                }
                                                return sprite+ "\n" + percent.toFixed(2)+"%";
                                            }
                                        },
                                        highlight: true,
                                        tooltip: {
                                            trackMouse: true,
                                            renderer: function (tooltip, record, item) {
                                                var me = this;
                                                var store = Ext.data.StoreManager.lookup("pcEvaStat");
                                                if(!me.TotalCount){
                                                    var count = 0;
                                                    var records = store.getData().items;
                                                    records.forEach(function(bean){
                                                        count += bean.get("count");
                                                        // count += 1;
                                                    });
                                                    me.TotalCount = count;
                                                }
                                                
                                                //var percent = (record.get('value')/me.TotalCount)*100;
                                                //tooltip.setHtml(record.get('name') + ': ' + percent.toFixed(2) + '%');
                                                tooltip.setHtml(record.get('name') + ': ' + record.get("count"));
                                                // tooltip.setHtml(record.get('name') + ': ' + 1);
                                            }
                                        }
                                    }]
                                },
                                
                            ]
                        },
                        {
                            xtype: "container",
                            width: "10%",
                            height: "100%",
                            // style: "background: red",
                        },
                    ]
                }
            ]
        }
    ]
});