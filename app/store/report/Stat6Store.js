Ext.define('Admin.store.report.Stat6Store', {
    extend: 'Admin.store.BaseStore',
    alias: 'store.stat6report',

    model: 'Admin.model.report.Stat6Model',

    autoLoad:false,
    storeId: 'stat6report',
    proxy: {
         type: 'ajax',
         cors:true,
         useDefaultXhrHeader:false,
         url: Admin.proxy.API.MgmtPath + 'stat/6',
         reader: {
             type: 'json',
             rootProperty: 'data',
             totalProperty  : 'Total',
             successProperty: 'IsSuccess'
         }
     }
});
