Ext.define('Admin.view.permission.rolePermission.WinAddAccountType', {
    extend: 'Ext.window.Window',
    controller: 'privilegeMgr',
    width:500,
    height:200,
    layout:"fit",
    modal:true,
    items: [
        {
            xtype:"form",
            layout: 'anchor',
            defaultType: 'textfield',
            padding:"10 20",
            itemId:"frmAddAccountType",
            defaults: {
                anchor: '100%',
                labelWidth:65
            },
            items:[
                {
                    fieldLabel:"<span style='color:red'>*</span>账号类型",
                    name:"name",
                    allowBlank:false
                },
                {
                    fieldLabel:"<span style='color:red'>*</span>Code",
                    name:"code",
                    allowBlank:false,
                    value:""
                }
            ]
        }
    ],
    bbar:[
        {
            xtype:"container",
            flex:1,
            layout:{
                type:"hbox",
                pack:"center",
                align:"center"
            },
            padding:"0 0 10 0",
            items:[
                {
                    xtype:"tbspacer"
                },{
                    xtype:"button",
                    text:"提交",
                    handler:"onAccountTypeAddSubmit"
                }
            ]
        }
    ]
});
