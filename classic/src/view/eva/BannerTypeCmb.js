Ext.define("Admin.view.eva.BannerTypeCmb",{
	extend: "Ext.form.field.ComboBox",
	xtype: "BannerTypeCmb",
	id: "BannerTypeCmb",
	editable:false,
	allowBland:false,
	displayField:"Name",
	valueField: "type",
	emptyText: "微信端",
	width: 100,
	store: {
		fields:[
			"Type","Name"
		],
		autoLoad: true,
		data: [
			{ type: 'wechat', Name: "微信端" },
			{ type: 'app', Name: "app端" },
		]
	}
})