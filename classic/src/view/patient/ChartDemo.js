Ext.define("Admin.view.patient.ChartDemo",{
    extend: 'Ext.Panel',
    xtype: 'column-stacked',
    controller: 'column-stacked',

    width: 650,

    items: [{
        xtype: 'cartesian',
        reference: 'chart',

        width: '100%',
        height: 460,

        store: {
            type: 'browsers'
        },
        insetPadding: {
            top: 40,
            left: 40,
            right: 40,
            bottom: 40
        },
        axes: [{
            type: 'numeric',
            position: 'left',
            adjustByMajorUnit: true,
            grid: true,
            fields: ['data1'],
            minimum: 0
        }, {
            type: 'category',
            position: 'bottom',
            grid: true,
            fields: ['month']
        }],
        series: [{
            type: 'bar',
            xField: 'month',
            yField: [ 'data1', 'data2', 'data3', 'data4' ],
            stacked: false
        }]
    }]
});