Ext.define('Admin.model.eva.EvaReportModel', {
    //extend: 'Admin.model.Base',
    extend:'Ext.data.Model',

    idProperty:"name",
    fields: [
        { name: "name" },
        { name: "value" },
        { name: "text" },
    ]
});
