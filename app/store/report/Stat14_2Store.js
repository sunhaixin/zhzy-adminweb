Ext.define('Admin.store.report.Stat14_2Store', {
    extend: 'Admin.store.BaseStore',
    alias: 'store.stat14_2report',

    model: 'Admin.model.report.Stat14Model',

    autoLoad:false,
    storeId: 'stat14_2report',
    proxy: {
         type: 'ajax',
         cors:true,
         useDefaultXhrHeader:false,
         url: Admin.proxy.API.BasePaths + 'api/admin/stat/14',
         reader: {
             type: 'json',
             rootProperty: 'Data',
             totalProperty  : 'totalCount',
             successProperty: 'success'
         }
     }
});
