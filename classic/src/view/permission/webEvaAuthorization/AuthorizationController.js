Ext.define("Admin.view.permission.webEvaAuthorization.AuthorizationController",{
    extend: "Ext.app.ViewController",
    alias: "controller.authorization",
   
	onSubmitEvaAuthorization: function (argument) {
		var view = this.getView();
		if(view.type == "add"){
            this.onAddEvaSubmit(arguments);
        }else{
            this.onUpdateEvaSubmit(arguments);
        }
	},
	onAddEvaSubmit: function(argument) {
		var view = this.getView();
        var frm = view.down("#frmEvaAuthorization");
        var values = frm.getValues();
        values.hospitalId = view.record.get("Id");
        if (!frm.form.isValid()) {
            Ext.Msg.alert("提示","请检查必填项")
            return false;
        }
        if (values.ItemIds.length <= 0) {
        	Ext.Msg.alert("提示","请检查必填项")
            return false;
        }
        if(typeof values.ItemIds == "string"){
            values.ItemIds = [values.ItemIds];
        }

        Ext.Ajax.request({
            url: Admin.proxy.API.MgmtPath+ "serviceManage/add",//"api/admin/ServiesByHospital/add",
            method:"Post",
            cors:true,//开启跨域模式
            useDefaultXhrHeader:false,//true的话header会带有X-Requested-With
            //binary:true, //为true后，后端将返回bytes
            //jsonData:{data:values,token:"123456"},
            params:values,
            success:function(resp){
                var bean = Ext.decode(resp.responseText);
                if(bean.success){
                    Ext.Msg.alert("提示","添加成功");
                    var store = view.BindGrid.getStore();
                    store.reload();
                    view.close();      
                }else{
                    Ext.Msg.alert("提示",bean.message);
                }
            }
        });
	},
	onUpdateEvaSubmit: function(argument) {
		var view = this.getView();
        var frm = view.down("#frmEvaAuthorization");
        var values = frm.getValues();
        values.hospitalId = view.record.get("Id");
        if (values.serviceCount + view.record.get("Remainder") < 0) {
            Ext.Msg.alert("提示","增加的改服务会使该医院的剩余服务测试小于0请重新设置")
            return false;
        }
        values.serviceId = view.record.get("ServicesId");
        if (!frm.form.isValid()) {
            Ext.Msg.alert("提示","请检查必填项")
            return false;
        }
        if (typeof values.ItemIds == "string") {
            values.ItemIds = [values.ItemIds];
        }
        if (values.ItemIds.length <= 0) {
            Ext.Msg.alert("提示","请检查必填项")
            return false;
        }

        Ext.Ajax.request({
            url: Admin.proxy.API.BasePaths+"api/admin/ServiesByHospital/update",
            method:"Post",
            cors:true,//开启跨域模式
            useDefaultXhrHeader:false,//true的话header会带有X-Requested-With
            //binary:true, //为true后，后端将返回bytes
            //jsonData:{data:values,token:"123456"},
            jsonData:values,
            success:function(resp){
                var bean = Ext.decode(resp.responseText);
                if(bean.IsSuccess){
                    Ext.Msg.alert("提示","添加成功");
                    var store = view.BindGrid.getStore();
                    store.reload();
                    view.close();      
                }else{
                    Ext.Msg.alert("提示",bean.Message);
                }
            }
        });
	}
})