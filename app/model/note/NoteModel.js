Ext.define('Admin.model.note.NoteModel', {
    //extend: 'Admin.model.Base',
    extend:'Ext.data.Model',

    idProperty:"Id",
    fields:[
        {name:"Id"},
        {name:"DoctorId"},
        {name:"PatientId"},
        {name:"DoctorName"},
        {name:"PatientName"},
        {name:"Memo"},
        {name:"IsActive"}
    ]
});