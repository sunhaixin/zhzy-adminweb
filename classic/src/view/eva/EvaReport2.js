Ext.define('Admin.view.eva.EvaReport2', {
    extend: 'Ext.window.Window',
    controller: 'EvaData',
    width:600,
    height:600,
    layout:"fit",
    modal:true,
    listeners:{
       render: function (argument) {
            var me = this;
            var model = me.getViewModel();
            
            var uid = this.record.data.EvaUid;
            var eva = this.record.data.Eva;
            var report = this.down("#evaReport");
            var urls = me.type && me.type == "app"?Admin.proxy.API.AppDetail:Admin.proxy.API.PcDetail;
            urls += "?uid="+uid+"&eva="+eva;
            Ext.Ajax.request({
                url: urls,
                method:"Get",
                cors:true,//开启跨域模式
                useDefaultXhrHeader:false,//true的话header会带有X-Requested-With
                //binary:true, //为true后，后端将返回bytes
                success:function(resp){
                    var bean = Ext.decode(resp.responseText);
                    if(bean.success){
                        var data = bean.data;
                        model.setData(
                            {
                                report1: Admin.view.eva.EvaTypeNames.ReportName(data.report1),      
                                report2: data.report2
                            }
                        )
                        var chart = me.down("[xtype=cartesian]");
                        var store = chart.getStore();
                        var Report1 = ["平和质","气虚质","阳虚质","阴虚质","痰湿质","湿热质","血瘀质","气郁质","特禀质"];
                        var Report2 = data.report3.split(";");
                        Report1.forEach(function(bean,index){
                            var value = Report2[index] ;
                            var text = Report2[index];
                            store.insert(index, Ext.create('Admin.model.eva.EvaReportModel',{name:bean,value:value,text:text}));
                        });

                        var recs = data.programeRec || [null];
                        var ints = data.programeInt || [null];

                        var Rectitles = Ext.getCmp("Rectitles2");
                        var items = [];

                        Rectitles.removeAll();

                        recs.forEach(function(bean){
                            items[items.length] = Admin.util.ProgrameList.createCmp(bean);
                        });
                        Rectitles.add(items)
                        

                        var IntTitles = Ext.getCmp("IntTitles2");
                        items = [];

                        IntTitles.removeAll();

                        ints.forEach(function(bean,index){
                            items[items.length] = Admin.util.ProgrameList.createCmp(bean);
                        });
                        IntTitles.add(items);
                    
                    }else{
                        me.close();
                        Ext.Msg.alert("提示",bean.message);
                    }
                }
            });
        }
    },
    viewModel: {
        data: {
            name: ""
        }
    },
    items: [
        {
            xtype: "container",
            layout: "vbox",
            margin: 20,
            autoScroll: true,
            items: [
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    style: "font-weight: bold;",
                    bind: {  
                        html: '体质类型' 
                    } 
                },
                {
                    xtype:"container",
                    style: "text-indent: 2em;",
                    bind: {  
                        html: '<p><div>{report1}</div></p>',
                    } 
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    style: "font-weight: bold;",
                    bind: {  
                        html: '易发疾病' 
                    } 
                },
                {
                    xtype:"container",
                    style: "text-indent: 2em;",
                    bind: {  
                        html: '<p style="word-wrap: break-word;width: 540px;">{report2}</p>',
                    } 
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    style: "font-weight: bold;",
                    bind: {  
                        html: '体质得分' 
                    } 
                },
                {
                    xtype: 'cartesian',
                    reference: 'chart',
                    width: "100%",
                    height: 300,
                    store: {
                        model: 'Admin.model.eva.EvaReportModel',
                        data: [
                        ]
                    },
                    axes: [ {
                                type: 'numeric',
                                position: 'left',
                                minimum: 0
                            },{
                        type: 'category',
                        position: 'bottom'
                    }],
                    series: [{
                        type: 'bar',
                        xField: 'name',
                        yField: 'value',
                        style: {
                            minGapWidth: 50,
                        },
                        label: {
                            field: 'text',
                            display: 'insideEnd',
                            orientation:"horizontal"
                        }
                    }]
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    width: 540,
                    style: "font-weight: bold;text-align: center;",
                    bind: {  
                        html: '养生方案' 
                    } 
                },
                {
                    xtype:"container",
                    
                    id: "Rectitles2",
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    width: 540,
                    style: "font-weight: bold;text-align: center;",
                    bind: {  
                        html: '干预方案' 
                    } 
                },
                {
                    xtype:"container",
                    
                    id: "IntTitles2",
                },
            ]
        }
    ],
   
});
