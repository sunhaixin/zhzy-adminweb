Ext.define('Admin.view.hospital.Department', {
    extend: 'Admin.view.main.BaseContainer',
    xtype: 'department',
    controller: 'department',
    layout: 'hbox',
    cls:"content-Panel-BorderStyle",
    listeners:{
        activate:function(){
            var grid = this.down("[xtype=listpanel]");
            grid.getStore().removeAll();
            grid.getStore().loadPage(1);
        }
    },
    margin:20,
    items: [
        {
            xtype:"listpanel",
            width:300,
            title:"医院名称",
            privileges:["department_see_all","department_see_lower","department_see_self"],
            id: "departmenthospital",
            store:Ext.create("Admin.store.hospital.HospitalStore",{
                pageSize:1000
            }),
            cls:"content-Panel-BorderStyle",
            tbar: [
                {
                    xtype: "container",
                    layout: "hbox",
                    width: "100%",
                    items: [
                        {
                            xtype: "textfield",
                            flex: 1,
                            id: "department_hospital_textfield",
                            fieldLabel: '搜索',
                            labelWidth: 50,
                            listeners: {
                                change: "onDepartmentfilters",
                                render: function(){
                                    document.getElementById("department_hospital_textfield-inputEl").setAttribute("placeholder",'按医院名称搜索');
                                }
                            }
                        }
                    ]
                }
            ],
            columns:[
                {
                    xtype:"templatecolumn",
                    menuDisabled:true,
                    flex:1,
                    tpl:"<div style='height:30px;line-height:30px;cursor:pointer;'>{name}</div>"
                }
            ],
            bbar:[
                {
                    xtype: 'pagingtoolbar',
                    store: Ext.data.StoreManager.lookup('hospital'),
                    displayInfo: true,
                    inputItemWidth: 30,//这里是改变gridPanel底部分页栏方框的大小  
                    displayMsg: '显示第 {0} 条到 {1} 条记录，一共 {2} 条',
                    emptyMsg: "没有数据可以显示"
                }
            ],
            listeners:{
                itemClick:"onHospitalItemClick"
            }
        },
        {
            xtype:"grid",
            title:"科室管理",
            flex:1,
            margin:"0 0 0 10",
            height:"100%",
            itemId:"GridDepartment",
            cls:"content-Panel-BorderStyle",
            viewConfig:{  
                enableTextSelection:true
            },
            tbar:[
                {
                    xtype:"container",
                    layout:"hbox",
                    defaults:{
                        margin:"0 5"
                    },
                    items:[
                        {
                            xtype:"button",
                            text:"新增",
                            privileges:["department_add"],
                            handler:function(){
                                var win = Ext.create("Admin.view.hospital.WinDepartment");
                                win.setTitle("新增科室");
                                win.BindGrid = this.ownerCt.ownerCt.ownerCt;
                                win.type = "Add"
                                win.show();
                            }
                        }
                    ]
                }
            ],
            store:Ext.create("Admin.store.hospital.DepartmentStore",{
                pageSize: 100,
                autoLoad:true
            }),
            columns:[
                {
                    text:"科室名称",
                    flex:1,
                    dataIndex:"Name"
                },
                {
                    text:"备注",
                    flex:1,
                    dataIndex:"Memo"
                },
                {
                    xtype:"actioncolumn",
                    text:"操作",
                    items:[
                        {
                            iconCls: 'x-fa fa-pencil-square-o',
                            tooltip: '修改',
                            privileges:["department_revisions"],
                            disabled: (sessionStorage.getItem("MyPrvileges")?(sessionStorage.getItem("MyPrvileges").split(",").indexOf("department_revisions")==-1?true:false):true),
                            handler:function(){
                                var win = Ext.create("Admin.view.hospital.WinDepartment");
                                win.BindGrid = this.ownerCt.ownerCt;
                                win.setTitle("修改科室信息");
                                win.type = "update";
                                win.record = arguments[5];
                                win.down("#frmDepartment").loadRecord(arguments[5]);
                                win.down("#department-hospital").hide();
                                win.down("#department-hospital").allowBlank = true;
                                win.show();
                            }
                        }
                    ]
                }
            ],
            bbar:[
                {
                    xtype: 'pagingtoolbar',
                    store: Ext.data.StoreManager.lookup('department'),
                    displayInfo: true,
                    displayMsg: '显示第 {0} 条到 {1} 条记录，一共 {2} 条',
                    emptyMsg: "没有数据可以显示"
                }
            ]
        }
    ]
});
