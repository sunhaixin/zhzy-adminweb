Ext.define('Admin.model.basic.ModuleModel', {
    //extend: 'Admin.model.Base',
    extend:'Ext.data.Model',

    idProperty:"Id",
    fields: [
        {name:"Id"},
        {name:"Name"}
    ]
});
