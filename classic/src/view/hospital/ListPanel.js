Ext.define("Admin.view.hospital.ListPanel",{
    extend:"Ext.grid.Panel",
    xtype:"listpanel",
    header:{ xtype:"header" },
    rowLines:false,
    menuDisabled:true,
    viewConfig:{
        stripeRows:false,//取消在表格中显示斑马线
        enableTextSelection:true, //可以复制单元格文字
    },
    defaults:{
    },
    height:"100%",
    border:false,
    bodyStyle:"border-top-width:1px !important",
    style:"border:1px solid #d0d0d0 !important",
    autoScroll:true
    
});