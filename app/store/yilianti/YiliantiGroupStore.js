Ext.define("Admin.store.yilianti.YiliantiGroupStore",{
    extend: 'Admin.store.BaseStore',
    alias: 'store.yiliantigroup',

    fields: [
        {name:"Id", mapping: 'id'},
        {name:"Name", mapping: 'name'},
        {name:"CreateTime",type:"date", mapping: 'createTime'},
        {name:"UpdateTime",type:"date", mapping: 'updateTime'}
    ],

    autoLoad: false,
    storeId: 'yiliantigroup',
    proxy: {
         type: 'ajax',
         cors:true,
         useDefaultXhrHeader:false,
         url: Admin.proxy.API.MgmtPath + "MedicalTreatmentCombination/all",
        //  api: {
        //     create  : 'http://localhost:49712/api/admin/hospital/query',
        //     read  : 'http://localhost:49712/api/admin/hospital/query',
        //     update  : '/controller/update',
        //     destroy : '/controller/destroy_action'
        // },
         reader: {
             type: 'json',
             rootProperty: 'data',
             totalProperty  : 'totalCount',
             successProperty: 'success'
         }
     }
})