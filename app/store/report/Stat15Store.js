Ext.define('Admin.store.report.Stat15Store', {
    extend: 'Admin.store.BaseStore',
    alias: 'store.stat15report',

    model: 'Admin.model.report.Stat15Model',

    autoLoad:false,
    storeId: 'stat15report',
    proxy: {
         type: 'ajax',
         cors:true,
         useDefaultXhrHeader:false,
         url: Admin.proxy.API.MgmtPath+'stat/16',
         reader: {
             type: 'json',
             rootProperty: 'data',
             totalProperty  : 'Total',
             successProperty: 'IsSuccess'
         }
     }
});
