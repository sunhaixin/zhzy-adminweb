Ext.define('Admin.TokenStorage', {
    singleton: true,
    storageKey: 'json-web-token',

    clear: function () {
        sessionStorage.removeItem(this.storageKey);
    },

    retrieve: function() {
        return sessionStorage.getItem(this.storageKey);
    },

    save: function (token) {
        sessionStorage.setItem(this.storageKey, token);
    }
});