Ext.define("Admin.view.controller.admincontroller",{
	extend: "Ext.app.ViewController",
	alias: "admincontroller",

	abc: function(){
		return true, 1;
	},
	IsSuccess: function(repText){
		if (repText.code == 200) {
			return true;
		}else{
			Ext.Msg.alert("提示",repText.message);
			return false;
		}
	},
	login: function(loginId,pswd){
		var me = this;
        Ext.Ajax.request({
            url: Admin.proxy.API.RbacPath + "account/login",
            cors: true,
            useDefaultXhrHeader:false,
            jsonData:{loginId:loginId,pswd:pswd},
            params:{loginId:loginId,pswd:pswd},
            method:"Post",
            success:function(resp){
                var bean = Ext.decode(resp.responseText);
                var isSuccess = me.IsSuccess(bean)
                if (isSuccess) {
                    var data = bean.data;
                    return data;
                }
                else{
                    return false;
                }
            }
        });
	},
    post: function(valus, url, store, view){
        Ext.Ajax.request({
            url: Admin.proxy.API.MgmtPath + url,
            method:"Post",
            cors:true,//开启跨域模式
            useDefaultXhrHeader:false,
            params: valus,
            success:function(resp){
                var bean = Ext.decode(resp.responseText);
                if(bean.code == 200){
                    Ext.Msg.alert("提示",bean.message);
                    if (store) {
                        store.reload();
                    }
                    if (view) {
                        view.close(); 
                    }  
                }else{
                    Ext.Msg.alert("提示",bean.message);
                }
            }
        });
    },
    post2: function(valus, url, store, view, type){
        Ext.Ajax.request({
            url: Admin.proxy.API.MgmtPath + url,
            method: "Post",
            cors: true,
            useDefaultXhrHeader: false,
            params: valus,
            success: function(resp){
                var bean = Ext.decode(resp.responseText);
                if(bean.code == 200){
                    if(type == "Add"){
                        Ext.Msg.alert("提示", bean.message);
                        var store = Ext.getCmp("GridDepartment").getStore();
                        store.remove();
                        store.reload();
                        view.close();
                    }else{
                        //当添加下级医院是重新load回失败
                        Ext.Msg.alert("提示", bean.message, function(){
                            var store = Ext.getCmp("GridDepartment").getStore();
                            store.remove();
                            store.reload();
                            view.close();
                        });
                    }
                    
                }else {
                    Ext.Msg.alert("提示", bean.message);
                }
            }
        })
    },
    delete: function(params, url,store){
        Ext.Msg.confirm("提示","确定要删除吗？",function(bt){
            var me = this;
            if(bt == "yes"){
                Ext.Ajax.request({
                    url:Admin.proxy.API.MgmtPath + url,
                    cors:true,
                    useDefaultXhrHeader:false,
                    params: params,
                    method:"Delete",
                    success:function(resp){
                        var bean = Ext.decode(resp.responseText);
                        if(bean.code == 200){
                            Ext.Msg.alert("提示",bean.message);
                            me.close();
                            store.reload();
                        }else{
                            Ext.Msg.alert("提示",bean.message);
                        }
                    }
                });
            }else{
                me.close();
            }
        });
    },
    deletePost: function(params, url,store){
        Ext.Msg.confirm("提示","确定要删除吗？",function(bt){
            var me = this;
            if(bt == "yes"){
                Ext.Ajax.request({
                    url:Admin.proxy.API.MgmtPath + url,
                    cors:true,
                    useDefaultXhrHeader:false,
                    params: params,
                    method:"Post",
                    success:function(resp){
                        var bean = Ext.decode(resp.responseText);
                        if(bean.code == 200){
                            Ext.Msg.alert("提示",bean.message);
                            me.close();
                            store.reload();
                        }else{
                            Ext.Msg.alert("提示",bean.message);
                        }
                    }
                });
            }else{
                me.close();
            }
        });
    },
    delete2: function(valus, url,store, hstore){
        Ext.Msg.confirm("提示","确定要删除吗？",function(bt){
            var conf = this;
            if(bt == "yes"){
                Ext.Ajax.request({
                    url:Admin.proxy.API.MgmtPath + url,
                    cors:true,
                    useDefaultXhrHeader:false,
                    params: valus,
                    method:"delete",
                    success:function(resp){
                        var bean = Ext.decode(resp.responseText);
                        if(bean.code == 200){
                            Ext.Msg.alert("提示", bean.message);
                            conf.close();
                            store.reload();
                            hstore.reload();
                        }else{
                            Ext.Msg.alert("提示", bean.message);
                        }
                    }
                });
            }else{
                conf.close();
            }
        });
    },
})