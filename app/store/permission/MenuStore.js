Ext.define("Admin.store.permission.MenuStore",{
    extend: 'Ext.data.TreeStore',
    alias: "store.menustore",
    fields: [
        { name: 'id', mapping: "id", type: 'string'},
		{ name: 'text', mapping: "name", type: 'string'},
		{ name: 'parentId', mapping: "parentNode" },
        { name: 'path', mapping: "path", type: 'string' },
        { name: 'index', mapping: "menuIndex", type: 'int' },
        { name: 'leaf', mapping: "leaf", type: "boolean" }
    ],
    autoLoad: true,
    storeId: 'menustore',
    proxy: {
         type: 'ajax',
         cors:true,
         useDefaultXhrHeader:false,
         url: Admin.proxy.API.MgmtPath + "menu/query",
         reader: {
             type: 'json',
             rootProperty: 'children',
             successProperty: 'success'
         }
     }
})