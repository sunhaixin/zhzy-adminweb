Ext.define("Admin.view.yilianti.YiliantiController",{
    extend: "Ext.app.ViewController",
    alias: "controller.yilianti",
    onHospitalItemClick:function(){
        var view = this.getView();
        var dpGrid = view.down("#GridDepartment");
        var id = arguments[1].get("id");
        dpGrid.setTitle(arguments[1].get("name")+"成员");
        dpGrid.getStore().getProxy().extraParams = {"mtcId":id};
        dpGrid.getStore().remove();
        // dpGrid.getStore().loadPage(1);
        dpGrid.getStore().load({
            callback:function(){
                dpGrid.getStore().getRoot().expand();
            }
        })
    },
    onYiliantiHospitalFielts: function(){
        var textfield = Ext.getCmp("lstp-yiliantiHospital").down("[xtype=textfield]").value;
        var dpGrid = Ext.getCmp("lstp-yiliantiHospital");
        dpGrid.getStore().clearFilter();
        var regExp = new RegExp(".*"+textfield+".*");
        if (textfield) {
            dpGrid.getStore().filterBy(function(record) { 
                    return regExp.test(record.get("name"));
            });
        }
        dpGrid.getStore().remove();
        dpGrid.getStore().loadPage(1);
    },
    onSubmitButtonClick:function(){
        var view = this.getView();
        if(view.type == "Add"){
            this.onAddDepartmentSubmit(arguments);
        }else{
            this.onUpdateDepartmentSubmit(arguments);
        }
    },
    onAddDepartmentSubmit:function(){
        var view = this.getView();
        var frm = view.down("#frmYiliantiGroup");
        var valus = frm.getValues();

        if (!frm.form.isValid()) {
            Ext.Msg.alert("提示","请检查必填项")
            return false;
        }

        var controller = Ext.create("Admin.view.controller.admincontroller");
        var url = 'MedicalTreatmentCombination/add';
        var store = view.BindGrid.getStore();
        controller.post(valus, url, store, view);
    },
    
    onSubmitYiliantiHospitalButtonClick: function(){
        var view = this.getView();
        if(view.type == "Add" || view.type == "AddHospital"){
            this.onAddYiliantiHospital(view.type,arguments);
        }
    },
    onAddYiliantiHospital: function(type,argument){
        var view = this.getView(), frm;
        if (view.type == "Add") {
            frm = view.down("#frmYiliantiHospital");
            var values = frm.getValues();
            values.parentId = '';
        }
        if (view.type == "AddHospital") {
            frm = view.down("#frmYiliantiAddHospital");
            var values = frm.getValues();
            values.parentId = view.parentid;
            values.mtcId = view.MtcId;
        };
        if (!frm.form.isValid()) {
            Ext.Msg.alert("提示","请检查必填项")
            return false;
        }

        var controller = Ext.create("Admin.view.controller.admincontroller");
        var url = 'MedicalTreatmentCombination/hospital/addAll';
        var store = Ext.getCmp("GridDepartment").getStore();
        controller.post2(values, url, store, view, type);

        
    }
})