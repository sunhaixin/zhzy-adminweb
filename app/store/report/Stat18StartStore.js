Ext.define('Admin.store.report.Stat18StartStore', {
    extend: 'Admin.store.BaseStore',
    alias: 'store.stat18Startreport',

    model: 'Admin.model.report.Stat18StartModel',

    autoLoad:false,
    storeId: 'stat18Startreport',
    proxy: {
         type: 'ajax',
         cors:true,
         useDefaultXhrHeader:false,
         url: Admin.proxy.API.MgmtPath+'stat/18',
         reader: {
             type: 'json',
             rootProperty: 'data',
             totalProperty  : 'Total',
             successProperty: 'IsSuccess'
         }
     }
});
