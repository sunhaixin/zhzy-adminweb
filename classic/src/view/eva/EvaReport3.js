Ext.define('Admin.view.eva.EvaReport3', {
    extend: 'Ext.window.Window',
    controller: 'EvaData',
    width:600,
    height:600,
    layout:"fit",
    modal:true,
    listeners:{
        activate: function (argument) {
            var me = this;
            var model = me.getViewModel();
            var uid = this.record.data.EvaUid;
            var eva = this.record.data.Eva;
            var report = this.down("#evaReport");
            var urls = me.type && me.type == "app"?Admin.proxy.API.AppDetail:Admin.proxy.API.PcDetail;
            urls += "?uid="+uid+"&eva="+eva;
            Ext.Ajax.request({
                url: urls,
                method:"Get",
                cors:true,//开启跨域模式
                useDefaultXhrHeader:false,//true的话header会带有X-Requested-With
                //binary:true, //为true后，后端将返回bytes
                success:function(resp){
                    var bean = Ext.decode(resp.responseText);
                        if(bean.success){
                        var data = bean.data;
                        Report31 = data.report3.split(";")[0];
                        Report32 = data.report3.split(";")[1];
                        Report33 = data.report3.split(";")[2];
                        Report34 = data.report3.split(";")[3];
                        Report35 = data.report3.split(";")[4];

                        Report21 = data.report2.split(";")[0];
                        Report22 = data.report2.split(";")[1];
                        Report23 = data.report2.split(";")[2];
                        Report24 = data.report2.split(";")[3];
                        Report25 = data.report2.split(";")[4];
                        var detail = '';
                        if(data.recType == "text"){
                            if (data.programeRec) {
                                detail = data.programeRec.replace(/\r\n/g,"</br>"); 
                            }else{
                                detail = data.programeInt.replace(/\r\n/g,"</br>"); 
                            }
                        }else if(data.recType == "html" || "div"){
                            if (data.programeRec) {
                                detail =  data.programeRec; 
                            }else{
                                detail =  data.programeInt; 
                            }
                        }
                        model.setData(
                        {
                            Report31: Report31,
                            Report32: Report32,
                            Report33: Report33,
                            Report34: Report34,
                            Report35: Report35,

                            Report21: Report21,
                            Report22: Report22,
                            Report23: Report23,
                            Report24: Report24,
                            Report25: Report25,
                            report5: data.report5,
                            recDetail: detail,
                            eva: eva
                        })
                    }else{
                        me.close();
                        Ext.Msg.alert("提示",bean.message);
                    }
                }
            });
        }
    },
    viewModel: {
        data: {
            name: ""
        }
    },
    items: [
        {
            xtype: "container",
            layout: "vbox",
            margin: 20,
            autoScroll:true,
            items: [
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    style: "font-weight: bold;",
                    bind: {  
                        html: '测试结果' 
                    } 
                },
                {
                    xtype:"container",
                    bind: {  
                        html: '<table border="1px solid #000" cellpadding="0" cellspacing="0" >'
                        +'<tr>'
                          +'<td style="padding: 10px; font-size:16px;">测评项目</td>'
                          +'<td style="padding: 10px; font-size:16px;">太阳</td>'
                          +'<td style="padding: 10px; font-size:16px;">少阳</td>'
                          +'<td style="padding: 10px; font-size:16px;">阴阳和平</td>'
                          +'<td style="padding: 10px; font-size:16px;">少阴</td>'
                          +'<td style="padding: 10px; font-size:16px;">太阴</td>'
                        +'</tr>'
                        
                        +'<tr>'
                          +'<td style="padding: 10px; font-size:16px;">评分</td>'
                          +'<td style="padding: 10px; font-size:16px;">{Report21}</td>'
                          +'<td style="padding: 10px; font-size:16px;">{Report22}</td>'
                          +'<td style="padding: 10px; font-size:16px;">{Report23}</td>'
                          +'<td style="padding: 10px; font-size:16px;">{Report24}</td>'
                          +'<td style="padding: 10px; font-size:16px;">{Report25}</td>'
                        +'</tr>'
                        +'<tr>'
                          +'<td style="padding: 10px; font-size:16px;">程度</td>'
                          +'<td style="padding: 10px; font-size:16px;">{Report31}</td>'
                          +'<td style="padding: 10px; font-size:16px;">{Report32}</td>'
                          +'<td style="padding: 10px; font-size:16px;">{Report33}</td>'
                          +'<td style="padding: 10px; font-size:16px;">{Report34}</td>'
                          +'<td style="padding: 10px; font-size:16px;">{Report35}</td>'
                        +'</tr>'
                      +'</table>',
                    } 
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    style: "font-weight: bold;",
                    bind: {  
                        html: '判定标准' 
                    } 
                },
                {
                    xtype:"container",
                    bind: {  
                        html: '<table border="1px solid #000" cellpadding="0" cellspacing="0" >'
                        +'<tr>'
                          +'<td style="padding: 9.5px; font-size:16px;">T分</td>'
                          +'<td style="padding: 1px; font-size:16px;">{eva == 3 ? "<12.50": "＜38.50"}</td>'
                          +'<td style="padding: 9.5px; font-size:16px;">{eva == 3 ? "12.50-25.00": "38.50-43.30"}</td>'
                          +'<td style="padding: 9.5px; font-size:16px;">{eva == 3 ? "25.00-75.00": "43.31-56.70"}</td>'
                          +'<td style="padding: 9.5px; font-size:16px;">{eva == 3 ? "75.00-87.50": "56.71-61.50"}</td>'
                          +'<td style="padding: 1px; font-size:16px;">{eva == 3 ? "＞87.50": "＞61.50"}</td>'
                        +'</tr>'
                        
                        +'<tr>'
                          +'<td style="padding: 9.5px; font-size:16px;">程度</td>'
                          +'<td style="padding: 1px; font-size:16px;">极低</td>'
                          +'<td style="padding: 9.5px; font-size:16px;">较低</td>'
                          +'<td style="padding: 9.5px; font-size:16px;">中等</td>'
                          +'<td style="padding: 9.5px; font-size:16px;">较高</td>'
                          +'<td style="padding: 1px; font-size:16px;">极高</td>'
                        +'</tr>'
                      +'</table>',
                    } 
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    style: "font-weight: bold;",
                    bind: {  
                        html: '人格（性格）特征提升' 
                    } 
                },
                {
                    xtype:"container",
                    style: "text-indent: 2em;",
                    bind: {  
                        html: '<p style="word-wrap: break-word;width: 540px;">{report5}</p>',
                    } 
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    width: 540,
                    style: "font-weight: bold;text-align: center;",
                    bind: {  
                        html: '方案' 
                    } 
                },
                {
                    xtype:"container",
                    style: "text-indent: 2em;",
                    bind: {  
                        html: '<div style="overflow:hidden;white-space：normal;width: 540px;">{recDetail}</div>',
                    } 
                }
            ]
        }
    ],
   
});
