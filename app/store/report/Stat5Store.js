Ext.define('Admin.store.report.Stat5Store', {
    extend: 'Admin.store.BaseStore',
    alias: 'store.stat5report',

    model: 'Admin.model.report.Stat5Model',

    autoLoad:false,
    storeId: 'stat5report',
    proxy: {
         type: 'ajax',
         cors:true,
         useDefaultXhrHeader:false,
         url: Admin.proxy.API.MgmtPath + 'stat/5',
         reader: {
             type: 'json',
             rootProperty: 'data',
             totalProperty  : 'Total',
             successProperty: 'IsSuccess'
         }
     }
});
