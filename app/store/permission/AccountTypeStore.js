Ext.define("Admin.store.permission.AccountTypeStore",{
	extend: 'Admin.store.BaseStore',
	alias: "store.accounttype",
	fields: [
		{name: "Id", mapping: 'id'},
		{name: "Name", mapping: 'name'},
		{name: "Code", mapping: 'code'},
		{name: "CreateTime", mapping: 'createTime'},
		{name: "UpdataTime", mapping: 'updataTime'},
		{name: "IsActive", mapping: 'isActive'}
	],
	autoLoad: false,
	storeId: "accounttype",
	proxy: {
         type: 'ajax',
         cors:true,
         useDefaultXhrHeader:false,
         url: Admin.proxy.API.MgmtPath+"accountType/page",
         reader: {
             type: 'json',
             rootProperty: 'data',
             totalProperty  : 'totalCount',
             successProperty: 'IsSuccess'
         }
     }
})