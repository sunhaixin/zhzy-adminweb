Ext.define('Admin.view.hospital.WinHospital', {
    extend: 'Ext.window.Window',
    controller: 'hospital',
    width:500,
    height:490,
    layout:"fit",
    modal:true,
    items: [
        {
            xtype:"form",
            itemId:"frmHospital",
            layout: 'anchor',
            defaultType: 'textfield',
            autoScroll:true,
            padding:"10 20",
            method:"POST",
            jsonSubmit:true,
            defaults: {
                anchor: '100%',
                labelWidth:65
            },
            items:[
                {
                    fieldLabel:"<span style='color:red'>*</span>医院名称",
                    name:"Name",
                    allowBlank:false,               //不允许为空
                    blankText:'该项不能为空!'
                },
                {
                    xtype:"combobox",
                    fieldLabel:"<span style='color:red'>*</span>医院类型",
                    name:"Type",
                    store:Ext.create("Admin.store.hospital.HospitalTypeStore"),
                    displayField:"Name",
                    valueField:"Id",
                    editable:false,
                    allowBlank:false,
                    itemId:"department-hospital-type",
                    listeners: { //监听   
                        render : function(combo) {//渲染   
                            combo.getStore().load();
                            var record = this.ownerCt.ownerCt.record;
                            if (record && record != '') {
                                var type = record.data.Type
                                if (type) {
                                    combo.select(type);
                                }
                            }   
                        }   
                    } 

                },

                {
                    xtype:"combobox",
                    displayField:"ProvinceName",
                    valueField:"ProvinceId",
                    store:Ext.create("Admin.store.basic.ProvinceStore",{autoLoad:true}),
                    fieldLabel:"<span style='color:red'>*</span>省份",
                    name:"ProvinceId",
                    editable:false,
                    allowBlank:false,
                    listeners:{
                        change:"onProvinceChange"
                    }
                },
                {
                    xtype:"combobox",
                    displayField:"CityName",
                    valueField:"CityId",
                    itemId:"cmbCity",
                    store:Ext.create("Admin.store.basic.CityStore"),
                    fieldLabel:"<span style='color:red'>*</span>城市",
                    disabled:true,
                    name:"CityId",
                    editable:false,
                    allowBlank:false,
                    listeners:{
                        change:"onCityChange"
                    }
                },
                {
                    xtype:"combobox",
                    displayField:"districtName",
                    valueField:"districtId",
                    itemId:"cmbDis",
                    store:Ext.create("Admin.store.basic.DistrictStore"),
                    fieldLabel:"区县",
                    disabled:true,
                    editable:false,
                    name:"DistrictId"
                },
                {
                    xtype: "numberfield",
                    fieldLabel:"<span style='color:red'>*</span>AgeBaby",
                    name:"AgeBaby",
                    allowBlank: false,
                    value: 6
                },
                {
                    xtype: "numberfield",
                    fieldLabel:"<span style='color:red'>*</span>AgeChild",
                    name:"AgeChild",
                    allowBlank: false,
                    value: 18
                },
                {
                    xtype: "numberfield",
                    fieldLabel:"<span style='color:red'>*</span>AgeOld",
                    name:"AgeOld",
                    allowBlank: false,
                    value: 65
                },
                {
                    xtype: "numberfield",
                    fieldLabel:"<span style='color:red'>*</span>同步时间",
                    name:"SyncInterval",
                    allowBlank: false,
                    value: 1
                },
                {
                    xtype: "numberfield",
                    fieldLabel:"<span style='color:red'>*</span>房间数",
                    name:"RoomCount",
                    allowBlank: false,
                    value: 1
                },
                {
                    xtype: "numberfield",
                    fieldLabel:"<span style='color:red'>*</span>开通pc数",
                    name:"PcCount",
                    allowBlank: false,
                    value: 1
                },
                {
                    xtype: "numberfield",
                    fieldLabel:"<span style='color:red'>*</span>激活医院数据库数",
                    name:"DcCount",
                    allowBlank: false,
                    value: 1
                },
                {
                    fieldLabel:"地址",
                    name:"Address"
                },
                {
                    fieldLabel:"电话",
                    name:"Telephone"
                },
                {
                    fieldLabel:"院长",
                    name:"Agent"
                },
                {
                    fieldLabel:"手机",
                    name:"Mobile"
                },
                {
                    fieldLabel:"微3二维码提示语",
                    labelWidth: 110,
                    name:"w3Tips",
                    value: "请去四诊仪导入测评记录并获取报告"
                },
                {	
                    xtype: "combobox",
                    labelWidth: 80,
                    fieldLabel: "微3登录方式",
                    name:"w3CardType",
                    editable:false,
                    allowBland:false,
                    displayField:"value",
                    valueField: "w3type",
                    itemId:"w3CardTypes",
                    value: 5,
                    store: {
                        fields:[
                            "W3type","value"
                        ],
                        autoLoad: true,
                        data: [
                            { w3type: 5, value: "手机号" },
                            { w3type: 0, value: "身份证" },
                            { w3type: 1, value: "体检卡" },
                            { w3type: 2, value: "社保卡" },
                            { w3type: 3, value: "就诊卡" },
                        ]
                    },
                    listeners: {
                        change: "onW3TypeChange"
                    }
                },
                {
                    xtype: "checkboxgroup",
                    fieldLabel: "<span style='color:red'>*</span>可用平台",
                    name: "Platform",
                    columns: 3,
                    labelWidth: 100,
                    allowBlank:false,  
                    defaults:{
                        xtype:"checkboxfield",
                        name:"Platforms"
                    },
                    listeners: {
                        render: function(){
                            var rec = this.ownerCt.ownerCt.record;
                            if (rec && rec.data && rec.data.Platforms) {
                                var platforms = rec.data.Platforms.split(";");
                                var items = this.query("checkboxfield");
                                items.forEach(function(item){
                                    item.setValue(false);
                                    if (platforms.indexOf(item.inputValue) >= 0) {
                                        item.setValue(true);
                                    }
                                });
                            }
                        },
                        focus: function(){
                        }
                    },
                    items: [
                        {
                            boxLabel: "app",
                            inputValue: "app",
                            checked: true,
                            mutex: "jianhe,WebEva",
                            listeners: {
                                focus: "platfromFocus"
                            }
                        },{
                            boxLabel: "wechat",
                            inputValue: "wechat",
                            mutex: "",
                            checked: true
                        },
                        {
                            boxLabel: "pc",
                            inputValue: "pc",
                            mutex: "WebEva",   //互斥项
                            checked: true,
                            listeners: {
                                focus: "platfromFocus"
                            }
                        },
                        {
                            boxLabel: "远程医疗",
                            inputValue: "telemedicne",
                            mutex: "WebEva",
                            checked: true,
                            listeners: {
                                focus: "platfromFocus"
                            }
                        },
                        {
                            boxLabel: "健和APP",
                            inputValue: "jianhe",
                            mutex: "app,WebEva",
                            listeners: {
                                focus: "platfromFocus"
                            }
                        },
                        {
                            boxLabel: "网页版测评",
                            inputValue: "WebEva",
                            mutex: "app,pc,telemedicne,jianhe",
                            listeners: {
                                focus: "platfromFocus"
                            }
                        }
                    ]
                },
                {
                    xtype: 'checkboxgroup',
                    fieldLabel: "<span style='color:red'>*</span>设置测评模块",
                    name: "Module",
                    columns: 3,
                    labelWidth: 100,
                    allowBlank:false,
                    defaults:{
                        xtype: "checkboxfield",
                        name:"Modules",
                    },
                    items: [
                        {
                            boxLabel: '老年人',
                            inputValue: '1',
                            checked: true
                        }, {
                            boxLabel: '中医体质',
                            inputValue: '2',
                            checked: true
                        }, {
                            boxLabel: '五态(简)',
                            inputValue: '3',
                            checked: true
                        }, {
                            boxLabel: '孕期',
                            inputValue: '4',
                            checked: true
                        }, {
                            boxLabel: '高血压',
                            inputValue: '5',
                            checked: true
                        }, {
                            boxLabel: '儿童',
                            inputValue: '6',
                            checked: true
                        }, {
                            boxLabel: '中成药',
                            inputValue: '7',
                            checked: true
                        }, {
                            boxLabel: '脉',
                            inputValue: '8',
                            checked: true
                        }, {
                            boxLabel: '舌',
                            inputValue: '9',
                            checked: true
                        }, {
                            boxLabel: '面',
                            inputValue: '10',
                            checked: true
                        }, {
                            boxLabel: '糖尿病',
                            inputValue: '11',
                            checked: true
                        }, {
                            boxLabel: '四诊合参',
                            inputValue: '12',
                            checked: true
                        }, {
                            boxLabel: '产后',
                            inputValue: '13',
                            checked: true
                        },
                        {
                            boxLabel: '备孕',
                            inputValue: '14',
                            checked: true
                        },
                        {
                            boxLabel: '经典处方',
                            inputValue: '15',
                            checked: true
                        },
                        {
                            boxLabel: '常态',
                            inputValue: '16',
                            checked: true
                        },
                        {
                            boxLabel: '五态',
                            inputValue: '17',
                            checked: true
                        },
                        {
                            boxLabel: '图象采集',
                            inputValue: '18',
                            checked: true
                        },
                    ],
                    listeners: {
                        render: function(){
                            var rec = this.ownerCt.ownerCt.record;
                            if (rec && rec.data && rec.data.Modules) {
                                var record = rec.data.Modules.split(";");
                                var items = this.query("checkboxfield");
                                items.forEach(function(item){
                                    item.setValue(false);
                                    if (record.indexOf(item.inputValue) >=0) {
                                        item.setValue(true);
                                    }
                                })
                            }
                        }
                    },
                },
                {
                    xtype: "fieldset",
                    width: "100%",
                    title: "<span style='color:red'>*</span>微信能否看报告，0-不可看，1-可看报告，2-可看报告+养生方案，3-可看报告+养生方案+干预方案，8、9、10代表舌面脉，没有方案",
                    defaultType: 'textfield',
                    defaults: {anchor: '100%'},
                    columnWidth: 0.5,
                    collapsible: true,
                    layout: 'anchor',
                    itemId: "addWechatRadio",
                    listeners:{
                        render:function(){
                            var me = this;
                            var wc = ["Wc1","Wc2","Wc3","Wc4","Wc5","Wc6","Wc7","Wc8","Wc9","Wc10","Wc11","Wc12","Wc13","Wc14","Wc15","Wc16","Wc17"];
                            var wcitems = [];
                            if (me.ownerCt.ownerCt.type == "update") {
                                wc.forEach(function(data){
                                    var item = [
                                            { boxLabel: '0', name: data, inputValue: '0' },
                                            { boxLabel: '1', name: data, inputValue: '1' }
                                        ];
                                    if (data!="Wc8" && data!="Wc9" && data!="Wc10") {
                                        item.push({ boxLabel: '2', name: data, inputValue: '2' },{ boxLabel: '3', name: data, inputValue: '3' })
                                    }
                                    wcitems[wcitems.length] = Ext.create("Ext.form.RadioGroup",{
                                        fieldLabel: data,
                                        columns: 3,
                                        autoScroll: true,
                                        vertical: true,
                                        defaults:{xtype: "radio",name: data},
                                        items: item,
                                        listeners:{
                                            render: function(v){
                                                var me = this;
                                                if (this.ownerCt.ownerCt.ownerCt.record) {
                                                    this.items.each(function(item){ 
                                                        item.setValue(item.inputValue == me.ownerCt.ownerCt.ownerCt.record.data[item.name]);   
                                                    });
                                                }
                                            }
                                        }
                                    });
                                });
                            }else{
                                wc.forEach(function(data){
                                    var item = [
                                            { boxLabel: '0', name: data, inputValue: '0' },
                                            { boxLabel: '1', name: data, inputValue: '1' }
                                        ];
                                    if (data!="Wc8" && data!="Wc9" && data!="Wc10") {
                                        item.push({ boxLabel: '2', name: data, inputValue: '2', checked: true })
                                    }
                                    else{
                                        item[1].checked = true;
                                    }
                                    wcitems[wcitems.length] = Ext.create("Ext.form.RadioGroup",{
                                        fieldLabel: data,
                                        columns: 3,
                                        autoScroll: true,
                                        vertical: true,
                                        defaults:{xtype: "radio",name: data},
                                        items: item,
                                        listeners:{
                                            render: function(v){
                                                var me = this;
                                                if (this.ownerCt.ownerCt.ownerCt.record) {
                                                    this.items.each(function(item){ 
                                                        item.setValue(item.inputValue == me.ownerCt.ownerCt.ownerCt.record.data[item.name]);   
                                                    });
                                                }
                                            }
                                        }
                                    });
                                });
                            }
                            
                            me.add(wcitems);
                        }
                    }
                },
                {
                    xtype: "fieldset",
                    width: "100%",
                    title: "<span style='color:red'>*</span>app报告类型，0-简版方案，1-详版方案",
                    defaultType: 'textfield',
                    defaults: {anchor: '100%'},
                    columnWidth: 0.5,
                    collapsible: true,
                    layout: 'anchor',
                    itemId: "UpdateHospital",
                    id: "WinHospitalAppRadio",
                    listeners:{
                        render:function(){
                            var me = this;
                            var app = ["App1","App2","App3","App4","App5","App6","App7","App11","App12","App13","App14","App16","App17"];
                            var appitems = [];

                            if (me.ownerCt.ownerCt.type == "update") {
                                app.forEach(function(data){
                                    appitems[appitems.length] = Ext.create("Ext.form.RadioGroup",{
                                        fieldLabel: data,
                                        columns: 3,
                                        autoScroll: true,
                                        vertical: true,
                                        items: [
                                            { boxLabel: '0', name: data, inputValue: '0' },
                                            { boxLabel: '1', name: data, inputValue: '1', },
                                        ],
                                        listeners:{
                                            render: function(v){
                                                var me = this;
                                                if (this.ownerCt.ownerCt.ownerCt.record) {
                                                    this.items.each(function(item){ 
                                                        item.setValue(item.inputValue == me.ownerCt.ownerCt.ownerCt.record.data[item.name]);   
                                                    });
                                                }
                                            }
                                        }
                                    });
                                });
                            }else {
                                app.forEach(function(data){
                                    appitems[appitems.length] = Ext.create("Ext.form.RadioGroup",{
                                        fieldLabel: data,
                                        columns: 3,
                                        autoScroll: true,
                                        vertical: true,
                                        items: [
                                            { boxLabel: '0', name: data, inputValue: '0', checked: true },
                                            { boxLabel: '1', name: data, inputValue: '1' }
                                        ],
                                        listeners:{
                                            render: function(v){
                                                var me = this;
                                                if (this.ownerCt.ownerCt.ownerCt.record) {
                                                    this.items.each(function(item){ 
                                                       
                                                    });
                                                }
                                            }
                                        }
                                    });
                                });
                            }
                            me.add(appitems);
                        }
                    }
                }
            ]
        }
    ],
    bbar:[
        {
            xtype:"container",
            flex:1,
            layout:{
                type:"hbox",
                pack:"center",
                align:"center"
            },
            padding:"0 0 10 0",
            items:[
                {
                    xtype:"tbspacer"
                },{
                    xtype:"button",
                    text:"提交",
                    handler:"onSubmitButtonClick"
                }
            ]
        }
    ]
});
