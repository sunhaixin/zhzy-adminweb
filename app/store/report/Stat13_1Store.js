Ext.define('Admin.store.report.Stat13_1Store', {
    extend: 'Admin.store.BaseStore',
    alias: 'store.stat13_1report',

    model: 'Admin.model.report.Stat13Model',

    autoLoad:false,
    storeId: 'stat13_1report',
    proxy: {
         type: 'ajax',
         cors:true,
         useDefaultXhrHeader:false,
         url: Admin.proxy.API.BasePaths + 'api/admin/stat/13',
         reader: {
             type: 'json',
             rootProperty: 'Data',
             totalProperty  : 'Total',
             successProperty: 'IsSuccess'
         }
     }
});
