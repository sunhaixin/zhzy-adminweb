Ext.define("Admin.store.telemedicine.GroupHospitalStore",{
	extend: 'Admin.store.BaseStore',
    alias: 'store.grouphospital',

    fields: [
        {name:"Id", mapping: 'id'},
        {name:"HospitalId", mapping: 'hospitalId'},
        {name:"HospitalName", mapping: 'hospital.name'},
        {name:"GroupId", mapping: 'groupId'},
        {name:"Memo"},
        {name:"CreateTime",type:"date"}
    ],

    autoLoad: false,
    storeId: 'grouphospital',
    proxy: {
         type: 'ajax',
         cors:true,
         useDefaultXhrHeader:false,
         url: Admin.proxy.API.MgmtPath + "/telemedicineGroupHospital/findPage",
        //  api: {
        //     create  : 'http://localhost:49712/api/admin/hospital/query',
        //     read  : 'http://localhost:49712/api/admin/hospital/query',
        //     update  : '/controller/update',
        //     destroy : '/controller/destroy_action'
        // },
         reader: {
             type: 'json',
             rootProperty: 'data',
             totalProperty  : 'totalCount',
             successProperty: 'IsSuccess'
         }
     }
})