Ext.define("Admin.view.main.BaseContainer",{
	extend: "Ext.container.Container",
	xtype: "basecontainer",
	listeners:{
		activate:function(){
			var me = this;
			var list = me.query("[privileges]");
        	var myprivileges = Ext.decode(sessionStorage.getItem("privileges"));
        	var privileges = [];
        	myprivileges.forEach(function(bean){
        		privileges.push(bean.code);
        	});
        	if(sessionStorage.getItem("MyPrvileges") == null){
        		sessionStorage.setItem("MyPrvileges",privileges.join(','))
        	}
            // var privileges = ["a","b","c","d"];
        	list.forEach(function(bean){
	            var flag = false;
	            bean.privileges.forEach(function(itm){
	                if(privileges.indexOf(itm) != -1){
	                    flag = true;
		            }
		        });
	            if(!flag){
	            	if (bean.ownerCt) {
	            		bean.ownerCt.remove(bean);
	            	}
	                
	            }
       		});
		}
	}
})