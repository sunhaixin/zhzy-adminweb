Ext.define('Admin.store.hospital.HospitalStore', {
    extend: 'Admin.store.BaseStore',
    alias: 'store.hospital',

    model: 'Admin.model.hospital.HospitalModel',

    autoLoad: false,
    storeId: 'hospital',
    proxy: {
         type: 'ajax',
         cors:true,
         useDefaultXhrHeader:false,
         url: Admin.proxy.API.MgmtPath+"hospital/findPageByName",
         reader: {
             type: 'json',
             rootProperty: 'data',
             totalProperty  : 'totalCount',
             successProperty: "success"
         }
     }
});
