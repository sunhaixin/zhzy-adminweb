Ext.define('Admin.view.permission.webEvaAuthorization.WinEvaAuthorization', {
    extend: 'Ext.window.Window',
    controller: 'authorization',
    listeners: {
        render: function (argument) {
            var me = this;
            var params = {};
            if (me.record) {
                params.serviceId = me.record.data.ServicesId
            }
            Ext.Ajax.request({
                url: Admin.proxy.API.MgmtPath+"serviceManage/detail",
                method:"Get",
                cors:true,//开启跨域模式
                useDefaultXhrHeader:false,//true的话header会带有X-Requested-With
                //binary:true, //为true后，后端将返回bytes
                params:params,
                success:function(resp){
                    var bean = Ext.decode(resp.responseText);
                    if(bean.success){
                        var data = bean.data;
                        var serviesItem = me.down("#serviesItem");
                        var items = [];
                        data.forEach(function(bean, index){
                            var it = Ext.create("Ext.container.Container",{
                                    layout: "hbox",
                                    padding: 10,
                                    items: [
                                        {
                                            xtype:"checkboxfield",
                                            boxLabel: bean.name,
                                            inputValue: bean.id,
                                            checked: bean.isCheck
                                        }
                                    ],
                                });
                            items.push(it);
                        })
                        serviesItem.add(items);

                    }else{
                        Ext.Msg.alert("提示",bean.message);
                    }
                }
            });
        }
    },
    width:700,
    height:600,
    layout:"fit",
    modal:true,
    items: [
        {
            xtype:"form",
            layout: 'anchor',
            defaultType: 'textfield',
            padding:"10 20",
            itemId:"frmEvaAuthorization",
            items:[
                {
                    xtype: "container",
                    layout: "hbox",
                    margin: "20px 0",
                    items: [
                        {
                            xtype: 'datefield',
                            name: 'ValidDateStart',
                            fieldLabel: '起始时间',
                            value: "起始时间",
                            format: "Y-m-d",
                            width: 200,
                            // margin: '0 5 0 0',
                            labelStyle: "font-size:10px",
                            labelWidth: 60,
                            allowBlank: false,
                            listeners: {
                                render: function(argument) {
                                    var view = this.ownerCt.ownerCt.ownerCt;
                                    if (view.record) {
                                        var startTime = view.record.data.ServiesStartTime;
                                        if (startTime) {
                                            var start = startTime.split(" ")[0]
                                            this.format = 'Y-m-d';
                                            this.setValue(start);
                                        }
                                    }
                                }
                            }
                        },
                        {
                            xtype: 'datefield',
                            name: 'ValidDateEnd',
                            fieldLabel: '截止时间',
                            labelStyle: "font-size:10px",
                            format: "Y-m-d",
                            margin: '0 0 0 5',
                            labelWidth: 60,
                            width: 200,
                            allowBlank: false,
                            listeners: {
                                render: function(argument) {
                                    var view = this.ownerCt.ownerCt.ownerCt;
                                    if (view.record) {
                                        var endTime = view.record.data.ServiesEndTime;
                                        if (endTime) {
                                            var end = endTime.split(" ")[0]
                                            this.format = 'Y-m-d';
                                            this.setValue(end);
                                        }
                                    }
                                }
                            }
                        },
                        {
                            xtype: "checkboxfield",
                            name:"IsDateUnlimited",
                            boxLabel: "不限时",
                            margin: "0 0 0 30",
                            inputValue: true,
                            listeners: {
                                render: function(argument) {
                                    var view = this.ownerCt.ownerCt.ownerCt;
                                    if (view.record) {
                                        var IsDateUnLimited = view.record.data.IsDateUnLimited;
                                        if (IsDateUnLimited) {
                                            this.setValue(true);
                                        }
                                    }
                                }
                            }
                            // checked: true
                        }
                    ]
                },
                {
                    xtype: "container",
                    layout: "hbox",
                    items: [
                        {
                            xtype: "numberfield",
                            fieldLabel:"<span style='color:red'>*</span>服务次数",
                            name:"ServiceCount",
                            allowBlank: false,
                            value: 1,
                            id: "serviceCount",
                        },
                        {
                            xtype: "container",
                            id: "serviceContainer",
                            style: "height: 32px;line-height: 32px;",
                            html: "（填写总次数后并勾选可适用的测评项目）"
                        }
                    ]
                },
                
                {
                    xtype: "checkboxgroup",
                    fieldLabel: "<span style='color:red'>*</span>服务项目",
                    name: "ItemIds",
                    columns: 3,
                    itemId: "serviesItem",
                    labelWidth: 75,
                    allowBlank:false,  
                    padding: "10 0",
                },
                {
                    fieldLabel:"<span style='color:red'>*</span>Web授权地址",
                    name:"EntryAddress",
                    itemId: "EntryAddress",
                    labelWidth: 100,
                    allowBlank:false
                },
                {
                    xtype: "textarea",
                    fieldLabel: "<span style='color:red'>*</span>备注",
                    itemId: "comment",
                    name: "Comment",
                    labelWidth: 60,
                    width: 400,
                }

            ]
        }
    ],
    bbar:[
        {
            xtype:"container",
            flex:1,
            layout:{
                type:"hbox",
                pack:"center",
                align:"center"
            },
            padding:"0 0 10 0",
            items:[
                {
                    xtype:"tbspacer"
                },{
                    xtype:"button",
                    text:"提交",
                    handler: "onSubmitEvaAuthorization"
                }
            ]
        }
    ]
});
