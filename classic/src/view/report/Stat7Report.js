Ext.define("Admin.view.report.Stat7Report",{
    extend: 'Ext.container.Container',
    xtype: 'stat7report',
    height:"100%",
    layout:"hbox",
    controller:"report",
    cls:"content-Panel-BorderStyle",
    margin: 20,cls:"content-Panel-BorderStyle",
    margin: 20,
    gridId: "stat7report_grid",
    listeners:{
        afterrender:"onReportRender"
    },
    items:[
        {
            xtype:"panel",
            title:"居民心理类型分布",
            flex:1,
            height:"100%",
            style:"border:1px solid #ccc",
            layout:{
                type:"hbox",
                pack:"center"
            },
            autoScroll:true,
            items:[
                {
                    width:900,
                    height:500,
                    layout:{
                        type:"hbox",
                        pack:"center",
                        align: 'center'
                    },
                    items:[
                        {
                            xtype:"grid",
                            width:400,
                            // height:"50%",
                            height:"202px",
                            id: "stat7report_grid",
                            store:Ext.create("Admin.store.report.Stat7Store",{autoLoad:false}),
                            style:"border:1px solid #ccc",
                            margin:10,
                            columns:[
                                {
                                    text:"心理类型",
                                    dataIndex:"name",
                                    flex:1,
                                },{
                                    text:"高分人群",
                                    dataIndex:"value1",
                                    width:115
                                },{
                                    text:"低分人群",
                                    dataIndex:"value2",
                                    width:115
                                }
                            ]
                        },{
                            xtype: 'cartesian',
                            flex:1,
                            reference: 'chart',
                            height: "100%",
                            insetPadding: {
                                top: 40,
                                bottom: 40,
                                left: 20,
                                right: 40
                            },
                            store:Ext.data.StoreManager.lookup("stat7report"),
                            axes: [{
                                type: 'numeric',
                                position: 'left',
                                adjustByMajorUnit: true,
                                fields: ['value1'],
                                grid: true,
                                minimum: 0
                            }, {
                                type: 'category',
                                fields: ['name'],
                                grid:true,
                                position: 'bottom'
                            }],
                            series: [{
                                type: 'bar',
                                stacked:false,
                                xField: 'name',
                                yField: ['value1','value2'],
                                label: {
                                    field: ['value1','value2'],
                                    display: 'insideEnd',
                                    orientation:"horizontal"
                                }
                            }]
                        }
                    ]
                }
            ]
        }
    ]
});