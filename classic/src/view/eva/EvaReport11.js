Ext.define('Admin.view.eva.EvaReport11', {
    extend: 'Ext.window.Window',
    controller: 'EvaData',
    width:600,
    height:600,
    layout:"fit",
    modal:true,
    listeners:{
        activate: function (argument) {
            var me = this;
            var model = me.getViewModel();
            var uid = this.record.data.EvaUid;
            var eva = this.record.data.Eva;
            var report = this.down("#evaReport");
            var urls = me.type && me.type == "app"?Admin.proxy.API.AppDetail:Admin.proxy.API.PcDetail;
            urls += "?uid="+uid+"&eva="+eva;
            Ext.Ajax.request({
                url: urls,
                method:"Get",
                cors:true,//开启跨域模式
                useDefaultXhrHeader:false,//true的话header会带有X-Requested-With
                //binary:true, //为true后，后端将返回bytes
                success:function(resp){
                    var bean = Ext.decode(resp.responseText);
                        if(bean.success){
                        var data = bean.data;

                        let normal = [], tongues = [], main = [];
                        var SCALE_DIA_VALUE = Admin.constants.Dia.data;
                        const Dia = data.patientValue1.split(',');
                            for (var i in Dia) {
                            for (var j in SCALE_DIA_VALUE) {
                                if (SCALE_DIA_VALUE[j].id == Dia[i]) {
                                main.push(SCALE_DIA_VALUE[j]);
                                }
                            }
                            }
                        for (var z in main) {
                            if(main[z].id>=202 && main[z].id<=219){
                                tongues.push(main[z].title);
                            }else{
                                if (normal.indexOf(main[z].title) == -1) {
                                    normal.push(main[z].title);
                                }
                            }
                            }
                        // }
                        normal = normal.join("; ");
                        tongues = tongues.join("; ");
                        model.setData(
                            {
                                patientValue1:  normal==""? "": normal+";",
                                patientValue2:  data.patientValue2,
                                patientValue3:  tongues==""? "": tongues,
                                allTP: data.allTP,
                                report: data.report1

                            }
                        )
                        var recs = data.programeRec || null;
                        var ints = data.programeInt || null;
                        var alltp = data.allTP || null;

                        var Textareaid = Ext.getCmp("textareaid");
                        var alltp_arr = [];
                        Textareaid.removeAll();
                        alltp_arr[alltp_arr.length] = Admin.util.ProgrameList.createAlltp(alltp);
                        Textareaid.add(alltp_arr);

                        var Rectitles = Ext.getCmp("Rectitles11");
                        var items = [];

                        Rectitles.removeAll();

                        recs.forEach(function(bean,index){
                            items[items.length] = Admin.util.ProgrameList.createCmp(bean);
                        });
                        Rectitles.add(items);

                        var IntTitles = Ext.getCmp("IntTitles11");
                        var items = [];

                        IntTitles.removeAll();

                        ints.forEach(function(bean,index){
                            items[items.length] = Admin.util.ProgrameList.createCmp(bean);
                        });
                        IntTitles.add(items);
                        
                    }else{
                        me.close();
                        Ext.Msg.alert("提示",bean.message);
                    }
                }
            });
        }
    },
    viewModel: {
        data: {
            name: ""
        }
    },
    items: [
        {
            xtype: "container",
            layout: "vbox",
            margin: 20,
            items: [
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    style: "font-weight: bold;",
                    bind: {  
                        html: '症状' 
                    } 
                },
                {
                    xtype:"container",
                    style: "text-indent: 2em;",
                    bind: {  
                        html: '<p style="word-wrap: break-word;width: 540px;">{patientValue1}{patientValue3}</p>',
                    } 
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    style: "text-indent: 2em;",
                    bind: {  
                        html: '<div>{!allTP || allTP == "" || allTP == "尚未进行客观化信息采集" || allTP == "尚未设置智能合参"? "" : "舌面脉客观化信息"}</div>' 
                    } 
                },
                {
                    xtype:"container",
                    id:"textareaid"
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    style: "font-weight: bold;",
                    bind: {  
                        html: '辩证分型' 
                    } 
                },
                {
                    xtype:"container",
                    style: "text-indent: 2em;",
                    bind: {  
                        html: '<p style="word-wrap: break-word;width: 540px;">{report}</p>',
                    } 
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    width: 540,
                    style: "font-weight: bold;text-align: center;",
                    bind: {  
                        html: '养生方案' 
                    } 
                },
                {
                    xtype:"container",
                    
                    id: "Rectitles11",
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    width: 540,
                    style: "font-weight: bold;text-align: center;",
                    bind: {  
                        html: '干预方案' 
                    } 
                },
                {
                    xtype:"container",
                    
                    id: "IntTitles11",
                },
            ]
        }
    ],
   
});
