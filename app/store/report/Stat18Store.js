Ext.define('Admin.store.report.Stat18Store', {
    extend: 'Admin.store.BaseStore',
    alias: 'store.stat18report',

    model: 'Admin.model.report.Stat18Model',

    autoLoad:false,
    storeId: 'stat18report',
    proxy: {
         type: 'ajax',
         cors:true,
         useDefaultXhrHeader:false,
         url: Admin.proxy.API.MgmtPath+'stat/18',
         reader: {
             type: 'json',
             rootProperty: 'data',
             totalProperty  : 'Total',
             successProperty: 'IsSuccess'
         }
     }
});
