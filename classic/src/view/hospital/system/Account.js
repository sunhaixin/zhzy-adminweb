Ext.define("Admin.view.hospital.system.Account",{
    extend: 'Admin.view.main.BaseContainer',
    xtype: 'account',
    controller: 'account',
    layout: 'hbox',
    cls:"content-Panel-BorderStyle",
    listeners:{
        activate:function(){
            var grid = this.down("[xtype=listpanel]");
            grid.getStore().removeAll();
            grid.getStore().loadPage(1);
        }
    },
    margin:10,
    items: [
        {
            xtype:"listpanel",
            width:300,
            height:"100%",
            title:"医院名称",
            privileges:["account_see_all","account_see_lower","account_see_self"],
            id: "Account-Hospital",
            store:Ext.create("Admin.store.hospital.HospitalStore",{
                pageSize: 1000,
            }),
            cls:"content-Panel-BorderStyle",
            tbar: [
                {
                    xtype: "container",
                    layout: "hbox",
                    width: "100%",
                    items: [
                        {
                            xtype: "textfield",
                            flex: 1,
                            id:"account_hospital_textfield",
                            fieldLabel: '搜索',
                            labelWidth: 50,
                            listeners: {
                                change: "onAccountHospitalFielts",
                                render: function(){
                                    document.getElementById("account_hospital_textfield-inputEl").setAttribute("placeholder",'按医院名称搜索');
                                }
                            }
                        }
                    ]
                }
            ],
            columns:[
                {
                    xtype:"templatecolumn",
                    menuDisabled:true,
                    flex:1,
                    tpl:"<div style='height:30px;line-height:30px;cursor:pointer;'>{name}</div>"
                }
            ],
            listeners:{
                itemClick:"onHospitalItemClick"
            }
        },
        {
            xtype:"grid",
            title:"系统账号管理",
            flex:1,
            margin:"0 0 0 10",
            height:"100%",
            itemId:"GridAccount",
            cls:"content-Panel-BorderStyle",
            viewConfig:{  
                enableTextSelection:true
            },
            tbar:[
                {
                    xtype:"container",
                    layout:"hbox",
                    defaults:{
                        margin:"0 5"
                    },
                    items:[
                        {
                            xtype:"button",
                            text:"新增",
                            privileges:["account_add"],
                            handler:function(){
                                var win = Ext.create("Admin.view.hospital.system.WinAccount");
                                win.setTitle("新增管理员账号");
                                win.BindGrid = this.ownerCt.ownerCt.ownerCt;
                                win.type = "Add"
                                win.show();
                            }
                        }
                    ]
                }
            ],
            store:Ext.create("Admin.store.system.AccountStore"),
            columns:[
                {
                    width:60,
                    xtype:"rownumberer"
                }
                ,{
                    text:"登录账号",
                    flex:1,
                    dataIndex:"LoginId"
                }
                ,{
                    text:"账号类型",
                    flex:1,
                    dataIndex:"AccountType",
                    renderer:function(){
                        return arguments[2].data.role.name || "无";
                    }
                }
                ,{
                    text:"用户名",
                    flex:1,
                    dataIndex:"UserName"
                }
                ,{
                     text:"备注",
                     flex:1,
                     dataIndex:"Memo"
                },
                {
                    text:"创建时间",
                    dataIndex:"CreateTime",
                    width:160,
                    xtype:"datecolumn",
                    format:"Y-m-d H:i:s"
                }
                ,{
                    xtype:"actioncolumn",
                    text:"操作",
                    items:[
                        {
                            iconCls: 'x-fa fa-pencil-square-o',
                            tooltip: '修改',
                            privileges:["account_revisions"],
                            disabled: (sessionStorage.getItem("MyPrvileges")?(sessionStorage.getItem("MyPrvileges").split(",").indexOf("account_revisions")==-1?true:false):true),
                            handler:function(){
                                var win = Ext.create("Admin.view.hospital.system.WinAccount");
                                win.BindGrid = this.ownerCt.ownerCt;
                                win.setTitle("修改管理员账号信息");
                                win.type = "update";
                                var field = win.down("[name=Pswd]");
                                var button = win.down("[itemId=PswdButton]");
                                field.ownerCt.remove(field);
                                button.hide();
                                win.record = arguments[5];
                                win.down("#frmAccount").loadRecord(arguments[5]);
                                win.show();
                            }
                        }
                        ,{
                            iconCls:"x-fa fa-cog",
                            tooltip:"重置密码",
                            privileges:["account_revisions_pswd"],
                            disabled: (sessionStorage.getItem("MyPrvileges")?(sessionStorage.getItem("MyPrvileges").split(",").indexOf("account_revisions_pswd")==-1?true:false):true),
                            handler:"onBtnSetAccountPswd"
                        },
                        {
                            iconCls:'x-fa fa-trash-o',
                            tooltip:"删除",
                            privileges:["account_delete"],
                            disabled: (sessionStorage.getItem("MyPrvileges")?(sessionStorage.getItem("MyPrvileges").split(",").indexOf("account_delete")==-1?true:false):true),
                            handler:"onAccountDelete"
                        },
                        {
                            iconCls:"x-fa fa-cog",
                            tooltip:"设置数据管理平台图片",
                            privileges:["account_mapurl"],
                            disabled: (sessionStorage.getItem("MyPrvileges")?(sessionStorage.getItem("MyPrvileges").split(",").indexOf("account_mapurl")==-1?true:false):true),
                            handler:function(){
                                var win = Ext.create("Admin.view.system.WinAccountMap");
                                win.record = arguments[5];
                                win.BindGrid = this.ownerCt.ownerCt;
                                win.show();
                            }
                        },
                    ]
                }
            
            ],
            bbar:[
                {
                    xtype: 'pagingtoolbar',
                    store: Ext.data.StoreManager.lookup('account'),
                    displayInfo: true,
                    displayMsg: '显示第 {0} 条到 {1} 条记录，一共 {2} 条',
                    emptyMsg: "没有数据可以显示"
                }
            ]
        }
    ]
});

