Ext.define("Admin.view.permission.privilegeMgr.MenuItemMenu",{
    extend: "Ext.menu.Menu",
    alias: "view.menuItemMenu",
    items: [
        {
            text: "添加子菜单",
            handler: function(){
                var win = Ext.create("Admin.view.permission.privilegeMgr.WinAddMenu");
                win.parentid = this.ownerCt.selectedNodeId;
                win.type = "Add",
                win.show();
            }
        }
    ]
})