Ext.define('Admin.view.note.Face', {
    extend: 'Ext.container.Container',
    xtype: 'face',
    layout: 'fit',
    listeners:{
        activate:function(){
            var grid = this.down("[xtype=grid]");
            grid.getStore().load();
        }
    },
    padding:10,
    items: [
        {
            xtype:"grid",
            title:"面象管理",
            selModel: {
                selType: 'checkboxmodel',
                checkOnly: true,
                showHeaderCheckbox: true
            },
            store:Ext.create("Admin.store.note.FaceStore"),
            columns:[
                {
                    text:"医生姓名",
                    dataIndex:"DoctorName"
                },
                {
                    text:"患者姓名",
                    dataIndex:"PatientName"
                },
                {
                    text:"查看面象",
                    dataIndex:"PicUrl",
                    width:200
                },
                {
                    text:"评论",
                    dataIndex:"Comment",
                    flex:1
                },
                {
                    text:"创建时间",
                    dataIndex:"CreateTime",
                    width:160,
                    xtype:"datecolumn",
                    format:"Y-m-d H:i:s"
                }
            ],
            bbar:[
                {
                    xtype: 'pagingtoolbar',
                    store: Ext.data.StoreManager.lookup('face'),
                    displayInfo: true,
                    displayMsg: '显示第 {0} 条到 {1} 条记录，一共 {2} 条',
                    emptyMsg: "没有数据可以显示"
                }
            ]
        }
    ]
});
