Ext.define('Admin.store.basic.ModuleStore', {
    extend: 'Admin.store.BaseStore',
    alias: 'store.module',

    model: 'Admin.model.basic.ModuleModel',

    autoLoad: false,
    storeId: 'module',
    proxy: {
         type: 'ajax',
         cors:true,
         useDefaultXhrHeader:false,
         url: Admin.proxy.API.BasePath+'api/admin/basic/module',
         reader: {
             type: 'json',
             rootProperty: 'Data',
             totalProperty  : 'Total',
             successProperty: 'IsSuccess'
         }
     }
});
