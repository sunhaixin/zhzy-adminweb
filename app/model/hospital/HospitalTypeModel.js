Ext.define('Admin.model.hospital.HospitalTypeModel', {
    //extend: 'Admin.model.Base',
    extend:'Ext.data.Model',

    idProperty:"Id",
    fields: [
        {name:"Id", mapping: 'id'},
        {name:"Name", mapping: 'name'},
        {name:"CreateTime", mapping: 'createTime'},
    ]
});
