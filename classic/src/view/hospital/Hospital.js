﻿Ext.define('Admin.view.hospital.Hospital', {
    extend: 'Admin.view.main.BaseContainer',
    xtype: 'hospital',
    controller: 'hospital',
    layout: 'fit',
    cls:"content-Panel-BorderStyle",
    listeners: {
        activate:function(){
            var grid = this.down("[xtype=grid]");
            grid.getStore().removeAll();
            grid.getStore().loadPage(1);
        }
    },
    margin: 20,
    style:"background:#f6f6f6",
    items: [
        {
            xtype:"grid",
            title:"医院信息",
            autoScroll:true,
            height:"100%",
            id:"GridHospital",
            privileges:["hospital_see_all","hospital_see_self"],
            viewConfig:{  
                enableTextSelection:true
            },
            tbar:[
                {
                    xtype:"container",
                    layout:"hbox",
                    defaults:{
                        margin:"0 5"
                    },
                    items:[
                        {
                            xtype:"button",
                            text:"新增",
                            privileges:["hospital_add"],
                            handler:function(){
                                var win = Ext.create("Admin.view.hospital.WinHospital");
                                win.BindGrid = this.ownerCt.ownerCt.ownerCt;
                                win.setTitle("新增医院信息");
                                win.type = "Add";
                                win.show();
                            }
                        },
                        {
                            xtype: "textfield",
                            width: 300,
                            id: "hospital_textfield",
                            fieldLabel: '搜索',
                            labelWidth: 50,
                            margin: "0 70",
                            listeners: {
                                change: "onHospitalfilters",
                                render: function(){
                                    document.getElementById("hospital_textfield-inputEl").setAttribute("placeholder",'按医院名称搜索');
                                }
                            }
                        }
                    ]
                }
            ],
            store:Ext.create("Admin.store.hospital.HospitalStore",{
                autoLoad:true,
            }),
            columns:{
                defaults:{
                    align:"center"
                },
                items:[
                    {
                        width:60,
                        text: "医院编号",
                        dataIndex:"id"
                    },{
                        text:"医院名称",
                        dataIndex:"name",
                        flex:1,
                        minWidth: 100,
                        sortable : true,
                    },
                    {
                        text:"省份",
                        dataIndex:"provinceName"
                    },
                    {
                        text:"城市",
                        dataIndex:"cityName"
                    },
                    {
                        text:"Token",
                        dataIndex:"token",
                        width:260
                    },
                    {
                        text:"模块",
                        dataIndex:"modules",
                        width:120
                    },
                    {
                        text:"版本",
                        dataIndex:"version"
                    },{
                        text:"创建时间",
                        dataIndex:"createTime",
                        width:160,
                    },{
                        xtype:"actioncolumn",
                        text:"操作",
                        width:150,
                        items:[
                            {
                                iconCls: 'x-fa fa-pencil-square-o',
                                tooltip: '修改',
                                privileges:["hospital_revisions"],
                                disabled: (sessionStorage.getItem("MyPrvileges")?(sessionStorage.getItem("MyPrvileges").split(",").indexOf("hospital_revisions")==-1?true:false):true),
                                handler: function(){
                                    var win = Ext.create("Admin.view.hospital.WinHospital");
                                    win.BindGrid = this.ownerCt.ownerCt;
                                    win.setTitle("修改医院信息");
                                    win.type = "update";
                                    win.record = arguments[5];
                                    win.down("#frmHospital").getForm().reset(true);
                                    win.down("#frmHospital").loadRecord(arguments[5]);
                                    win.show();
                                }
                            },
                            {
                                iconCls:"x-fa fa-cog",
                                tooltip:"设置版本",
                                privileges:["hospital_edition"],
                                disabled: (sessionStorage.getItem("MyPrvileges")?(sessionStorage.getItem("MyPrvileges").split(",").indexOf("hospital_edition")==-1?true:false):true),
                                handler:function(){
                                    var win = Ext.create("Admin.view.hospital.WinSetHospitalVersion");
                                    win.record = arguments[5];
                                    win.BindGrid = this.ownerCt.ownerCt;
                                    win.show();
                                }
                            },{
                                iconCls:"x-fa fa-weixin",
                                tooltip:"设置微信",
                                privileges:["hospital_wechat"],
                                disabled: (sessionStorage.getItem("MyPrvileges")?(sessionStorage.getItem("MyPrvileges").split(",").indexOf("hospital_wechat")==-1?true:false):true),
                                handler:function(){
                                    var win = Ext.create("Admin.view.hospital.WinSetHospitalWechat");
                                    win.record = arguments[5];
                                    win.BindGrid = this.ownerCt.ownerCt;
                                    win.show();
                                }
                            },
                            {
                                iconCls:"x-fa fa-file-image-o",
                                tooltip:"banner图设置",
                                privileges:["hospital_edition"],
                                disabled: (sessionStorage.getItem("MyPrvileges")?(sessionStorage.getItem("MyPrvileges").split(",").indexOf("hospital_wechat_banner")==-1?true:false):true),
                                handler:function(){
                                    var win = Ext.create("Admin.view.hospital.WinSetHospitalBanner");
                                    win.record = arguments[5];
                                    win.BindGrid = this.ownerCt.ownerCt;
                                    win.show();
                                }
                            },
                            {
                                iconCls:"x-fa fa-qrcode",
                                tooltip:"查看Web版二维码",
                                handler:function(){
                                    var win = Ext.create("Admin.view.hospital.WinHospitalQRCode");
                                    win.record = arguments[5];
                                    win.title = '查看Web版二维码';
                                    win.show();
                                }
                            },
                            {
                                iconCls:"x-fa fa-qrcode",
                                tooltip:"查看微信网页版二维码",
                                handler:function(){
                                    var win = Ext.create("Admin.view.hospital.WinHospitalQRCode");
                                    win.record = arguments[5];
                                    win.title = '查看微信网页版二维码';
                                    console.log(win)
                                    win.show();
                                }
                            }
                        ]
                    }
                ]
            },
            bbar:[
                {
                    xtype: 'pagingtoolbar',
                    store: Ext.data.StoreManager.lookup('hospital'),
                    displayInfo: true,
                    displayMsg: '显示第 {0} 条到 {1} 条记录，一共 {2} 条',
                    emptyMsg: "没有数据可以显示"
                }
            ]
        }
    ]
});
