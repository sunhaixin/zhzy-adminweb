Ext.define("Admin.view.report.Stat13Report",{
    extend: 'Ext.container.Container',
    xtype: 'stat13report',
    height:"100%",
    layout:"hbox",
    controller:"report",
    cls:"content-Panel-BorderStyle",
    margin: 20,
    gridId: "stat13report_grid",
    listeners:{
        afterrender:"onReportRender"
    },
    items:[
        {
            xtype:"panel",
            title:"居民舌象分布",
            flex:1,
            height:"100%",
            style:"border:1px solid #ccc",
            layout:{
                type:"hbox",
                pack:"center"
            },
            autoScroll:true,   
            items:[
                {
                    width:350,
                    height:500,
                    layout:{
                        type:"vbox",
                        align: 'center'
                    },
                    items:[
                        {
                            xtype:"HGrid",
                            title:'舌色（包括舌色局部特征）',
                            autoHeight:true,
                            width:"100%",
                            id: "stat13report_1_grid",
                            BindStore:Ext.create("Admin.store.report.Stat13_1Store",{autoLoad:false}),
                            style:"border:1px solid #ccc",
                            margin:"30px 0 0 10px"
                        }
                        ,{
                            xtype: 'cartesian',
                            flex:1,
                            reference: 'chart',
                            width: "100%",
                            insetPadding: {
                                top: 40,
                                bottom: 40,
                                left: 20,
                                right: 40
                            },
                            store:Ext.data.StoreManager.lookup("stat13_1report"),
                            axes: [{
                                type: 'numeric',
                                position: 'left',
                                minimum: 0
                            }, 
                            {
                                type: 'category',
                                position: 'bottom'
                            }
                            ],
                            series: [
                                {
                                    type: 'bar',
                                    xField: 'name',
                                    yField: 'value',
                                    style: {
                                        minGapWidth: 50
                                    },
                                    label: {
                                        field: 'value',
                                        display: 'insideEnd',
                                        orientation:"horizontal"
                                    }
                                }
                                
                            ]
                        }
                    ]
                },
                {
                    width:350,
                    height:500,
                    layout:"vbox",
                    items:[
                        {
                            xtype:"HGrid",
                            autoHeight:true,
                            width:"100%",
                            title:"苔色",
                            id: "stat13report_2_grid",
                            BindStore:Ext.create("Admin.store.report.Stat13_2Store",{autoLoad:false}),
                            style:"border:1px solid #ccc",
                            margin:"30px 0 0 0"
                        }
                        ,{
                            xtype: 'cartesian',
                            flex:1,
                            reference: 'chart',
                            width: "100%",
                            insetPadding: {
                                top: 40,
                                bottom: 40,
                                left: 20,
                                right: 40
                            },
                            store:Ext.data.StoreManager.lookup("stat13_2report"),
                            axes: [{
                                type: 'numeric',
                                position: 'left',
                                minimum: 0
                            }, 
                            {
                                type: 'category',
                                position: 'bottom'
                            }
                            ],
                            series: [
                                {
                                    type: 'bar',
                                    xField: 'name',
                                    yField: 'value',
                                    style: {
                                        minGapWidth: 50
                                    },
                                    label: {
                                        field: 'value',
                                        display: 'insideEnd',
                                        orientation:"horizontal"
                                    }
                                }
                                
                            ]
                        }
                    ]
                },
                {
                    width:350,
                    height:500,
                    layout:"vbox",
                    items:[
                        {
                            xtype:"HGrid",
                            autoHeight:true,
                            width:"100%",
                            title:"苔质",
                            id: "stat13report_3_grid",
                            BindStore:Ext.create("Admin.store.report.Stat13_3Store",{autoLoad:false}),
                            style:"border:1px solid #ccc",
                            margin:"30px 0 0 0"
                        }
                        ,{
                            xtype: 'cartesian',
                            flex:1,
                            reference: 'chart',
                            width: "100%",
                            insetPadding: {
                                top: 40,
                                bottom: 40,
                                left: 20,
                                right: 40
                            },
                            store:Ext.data.StoreManager.lookup("stat13_3report"),
                            axes: [{
                                type: 'numeric',
                                position: 'left',
                                minimum: 0
                            }, 
                            {
                                type: 'category',
                                position: 'bottom'
                            }
                            ],
                            series: [
                                {
                                    type: 'bar',
                                    xField: 'name',
                                    yField: 'value',
                                    style: {
                                        minGapWidth: 50
                                    },
                                    label: {
                                        field: 'value',
                                        display: 'insideEnd',
                                        orientation:"horizontal"
                                    }
                                }
                                
                            ]
                        }
                    ]
                },
                {
                    width:350,
                    height:500,
                    layout:"vbox",
                    items:[
                        {
                            xtype:"HGrid",
                            autoHeight:true,
                            width:"100%",
                            title:"舌形",
                            id: "stat13report_4_grid",
                            BindStore:Ext.create("Admin.store.report.Stat13_4Store",{autoLoad:false}),
                            style:"border:1px solid #ccc",
                            margin:"30px 10px 0 0"
                        }
                        ,{
                            xtype: 'cartesian',
                            flex:1,
                            reference: 'chart',
                            width: "100%",
                            insetPadding: {
                                top: 40,
                                bottom: 40,
                                left: 20,
                                right: 40
                            },
                            store:Ext.data.StoreManager.lookup("stat13_4report"),
                            axes: [{
                                type: 'numeric',
                                position: 'left',
                                minimum: 0
                            }, 
                            {
                                type: 'category',
                                position: 'bottom'
                            }
                            ],
                            series: [
                                {
                                    type: 'bar',
                                    xField: 'name',
                                    yField: 'value',
                                    style: {
                                        minGapWidth: 50
                                    },
                                    label: {
                                        field: 'value',
                                        display: 'insideEnd',
                                        orientation:"horizontal"
                                    }
                                }
                                
                            ]
                        }
                    ]
                }
            ]
        }
    ]
});