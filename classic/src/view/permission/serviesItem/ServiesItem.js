Ext.define('Admin.view.permission.serviesItem.ServiesItem', {
    extend: 'Admin.view.main.BaseContainer',
    xtype: 'serviesItem',
    controller: 'serviesItem',
    layout: 'fit',
    cls:"content-Panel-BorderStyle",
    margin: 20,
    style:"background:#f6f6f6",
    items: [
        {
            xtype:"grid",
            title:"服务项目管理",
            autoScroll:true,
            height:"100%",
            privileges:["serviesItem_see"],
            viewConfig:{  
                enableTextSelection:true
            },
            
            store:Ext.create("Admin.store.serviesItem.ServiesItemStore",{
                pageSize: 100, 
                autoLoad: true
            }),
            tbar:[
                {
                    xtype:"container",
                    layout:"hbox",
                    defaults:{
                        margin:"0 5"
                    },
                    items:[
                        {
                            xtype:"button",
                            text:"新增",
                            privileges:["serviesItem_add"],
                            handler:function(){
                                var win = Ext.create("Admin.view.permission.serviesItem.WinServiesItem");
                                win.BindGrid = this.ownerCt.ownerCt.ownerCt;
                                win.setTitle("新增服务");
                                win.type = "Add";
                                win.show();
                            }
                        }
                    ]
                }
            ],
            columns:{
                defaults:{
                    align:"center"
                },
                items:[
                    {
                        width:400,
                        text: "编号",
                        dataIndex:"Eva"
                    },{
                        text:"服务名称",
                        dataIndex:"Name",
                        flex:1,
                        minWidth: 100,
                        sortable : true,
                    },
                    {
                        text:"服务代号",
                        dataIndex:"Id",
                        flex:1,
                        sortable : true,
                    }
                ]
            },
        }
    ]
});
