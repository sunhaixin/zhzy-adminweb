Ext.define("Admin.view.report.Stat18Report",{
    extend: 'Ext.container.Container',
    xtype: 'stat18report',
    requires: [
        'Ext.chart.PolarChart',
        'Ext.chart.series.Pie',
        'Ext.chart.interactions.Rotate'
    ],
    layout:"hbox",
    height:"100%",
    margin: 20,
    cls:"content-Panel-BorderStyle",
    controller:"report",
    gridId: "stat18report_grid",
    listeners:{
        afterrender:"onReport18Render",
        activate: function(){
        }
    },
    items:[
        {
            xtype:"panel",
            title:"服务人次统计",
            flex:1,
            height:"100%",
            style: "background: #fff",
            layout:{
                type:"hbox",
                pack:"center"
            },
            autoScroll:true,
            tbar: {
                xtype: "container",
                layout: {
                    type: "hbox",
                    pack: "center"
                },
                padding: 10, 
                items: [
                    {
                        xtype: 'combobox',
                        displayField:"year",
                        valueField: "year",
                        id: "stat18Start",
                        editable:false,
                        allowBlank:false,
                        // emptyText: "2018",
                        store: Ext.create("Admin.store.report.Stat18StartStore",{autoLoad:false}),
                        listeners: {
                            render: function(combo) {
                                var time = new Date();
                                var year = time.getFullYear() + "年";
                                this.setEmptyText(year);
                            },
                            change: "onstat18Change"
                        }
                    }
                ]
            },
            items:[
                {
                    width:900,
                    height:500,
                    layout:{
                        type:"hbox"
                    },
                    features: [{
                ftype: 'summary'
            }],
                    items:[
                        {
                            xtype:"grid",
                            width:150,
                            height:"100%",
                            id: "stat18report_grid",
                            store:Ext.create("Admin.store.report.Stat18Store",{autoLoad:false}),
                            // store:Ext.create("Admin.store.report.StatNo1Store",{autoLoad:false}),
                            style:"border:1px solid #ccc",
                            height:"100%",
                            margin:10,
                            columns:[
                                {
                                    text:"项目",
                                    dataIndex:"name",
                                    flex:1,
                                },{
                                    text:"服务人次",
                                    dataIndex:"value",
                                }
                            ]
                        },
                        
                        {
                            xtype: 'cartesian',
                            id: "stat18report_cartesian",
                            flex:1,
                            // width: 800,
                            reference: 'chart',
                            height: "100%",
                            insetPadding: {
                                top: 40,
                                bottom: 40,
                                left: 20,
                                right: 40
                            },
                            // store:Ext.data.StoreManager.lookup("stat18report"),
                            store:Ext.create("Admin.store.report.Stat18Store",{autoLoad:false}),
                            axes: [{
                                type: 'numeric',
                                position: 'left',
                                adjustByMajorUnit: true,
                                fields: ['value1'],
                                grid: true,
                                minimum: 10
                            }, {
                                type: 'category',
                                fields: ['name'],
                                grid:true,
                                position: 'bottom'
                            }],
                            series: [
                                {
                                    type: 'bar',
                                    xField: 'name',
                                    yField: 'value',
                                    style: {
                                        minGapWidth: 10
                                    },
                                    label: {
                                        field: 'value',
                                        display: 'insideEnd',
                                        orientation:"horizontal"
                                    }
                                }
                            ]
                        }
                    ]
                }
            ]
        }
    ]
});