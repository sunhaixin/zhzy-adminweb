Ext.define('Admin.view.eva.EvaReport19', {
    extend: 'Ext.window.Window',
    controller: 'EvaData',
    width:600,
    height:600,
    layout:"fit",
    modal:true,
    listeners:{
        activate: function (argument) {
            var me = this;
            var model = me.getViewModel();
            model.set("name","jack");
            var uid = this.record.data.EvaUid;
            var eva = this.record.data.Eva;
            var urls = me.type && me.type == "app"?Admin.proxy.API.AppDetail:Admin.proxy.API.PcDetail;
            urls += "?uid="+uid+"&eva="+eva;
            Ext.Ajax.request({
                url: urls,
                method:"Get",
                cors:true,//开启跨域模式
                useDefaultXhrHeader:false,//true的话header会带有X-Requested-With
                //binary:true, //为true后，后端将返回bytes
                success:function(resp){
                    var bean = Ext.decode(resp.responseText);
                        if(bean.success){
                        var data = bean.data;
                        model.setData(
                            {
                                hint: data.hint,
                                conclusion: data.conclusion,
                                explanation: data.explanation,
                                performance: data.performance
                            }
                        )
                        var recs = data.programeRec || null;
                        var Rectitles = Ext.getCmp("Rectitles19");
                        var items = [];
                        Rectitles.removeAll();
                        recs.forEach(function(bean,index){
                            bean.type == "chd2";
                            items[items.length] = Admin.util.ProgrameList.createCmp(bean);
                        });
                        Rectitles.add(items);
                        var chart = me.down("[xtype=cartesian]");
                        var store = chart.getStore(); var Report2;
                        var Report1 = "平人质;阳热质;阴虚质;阳虚质;痰湿质;气滞质;气虚质;肺气虚;肾气虚;脾气虚".split(";");
                        if(data.scores.indexOf(",") !== -1){
                            Report2 = data.scores.split(",")
                        }else{
                            Report2 = data.scores.split(";")
                        }
                        Report1.forEach(function(bean,index){
                            var value = Report2[index] * 10;
                            var a = Report2[index] *1;
                            var text = a.toFixed(4).split(".")[1] ;
                            text = text.toString();
                            text = text.split("");
                            if (text[0] != 0) {
                                text = text[0]+ "" + text[1] + "." + text[2] + "" + text[3] + "%";
                            }
                            else{
                                text = text[1] + "." + text[2] + "" + text[3] + "%";
                            }                            
                            store.insert(index, Ext.create('Admin.model.eva.EvaReportModel',{name:bean,value:value,text:text}));
                        }) 
                    }else{
                        me.close();
                        Ext.Msg.alert("提示",bean.message);
                    }
                }
            });
        }
    },
    viewModel: {
        data: {
            name: ""
        }
    },
    items: [
        {
            xtype: "container",
            layout: "vbox",
            margin: 20,
            autoScroll: true,
            items: [
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    style: "font-weight: bold;",
                    bind: {  
                        html: '尊敬的（监护人）先生/女士' 
                    } 
                },
                {
                    xtype:"container",
                    style: "text-indent: 2em;",
                    bind: {  
                        html: '<p style="word-wrap: break-word;width: 540px;">本测验基于中医理论，结合7-12岁儿童心身特征，由中国中医科学院中医临床基础医学研究所建立而成，为标准化的中医儿童体质测量工具，可参考此结果了解孩子的体质状况，在医生的指导下进行个体化调养。{hint}</p>',
                    } 
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    style: "font-weight: bold;",
                    bind: {  
                        html: '体质结果' 
                    } 
                },
                {
                    xtype:"container",
                    style: "text-indent: 2em;",
                    bind: {  
                        html: '<div><p style="word-wrap: break-word;width: 540px;">{conclusion}</p><p style="word-wrap: break-word;width: 540px;">{explanation}</p</div>',
                    } 
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    style: "font-weight: bold;",
                    bind: {  
                        html: '主要表现' 
                    } 
                },
                {
                    xtype:"container",
                    style: "text-indent: 2em;",
                    bind: {  
                        html: '<p style="word-wrap: break-word;width: 540px;">{performance}</p>',
                    } 
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    style: "font-weight: bold;",
                    bind: {  
                        html: '辨识分析图' 
                    } 
                },
                {
                    xtype: 'cartesian',
                    reference: 'chart',
                    width: "100%",
                    height: 300,
                    store: {
                        model: 'Admin.model.eva.EvaReportModel',
                        data: [
                        ]
                    },
                    axes: [ {
                                type: 'numeric',
                                position: 'left',
                                minimum: 0
                            },{
                        type: 'category',
                        position: 'bottom'
                    }],
                    series: [{
                        type: 'bar',
                        xField: 'name',
                        yField: 'value',
                        style: {
                            minGapWidth: 40,
                        },
                        label: {
                            field: 'text',
                            display: 'insideEnd',
                            orientation:"horizontal"
                        }
                    }]
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    width: 540,
                    itemId: "fanan",
                    style: "font-weight: bold;text-align: center;",
                    bind: {  
                        html: '方案' 
                    }
                },
                {
                    xtype:"container",
                    id: "Rectitles19"
                },
            ]
        }
    ],
   
});
