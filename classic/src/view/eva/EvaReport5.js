Ext.define('Admin.view.eva.EvaReport5', {
    extend: 'Ext.window.Window',
    controller: 'EvaData',
    width:600,
    height:600,
    layout:"fit",
    modal:true,
    listeners:{
        activate: function (argument) {
            var me = this;
            var model = me.getViewModel();
            model.set("name","jack");
            var uid = this.record.data.EvaUid;
            var eva = this.record.data.Eva;
            var report = this.down("#evaReport");
            var urls = me.type && me.type == "app"?Admin.proxy.API.AppDetail:Admin.proxy.API.PcDetail;
            urls += "?uid="+uid+"&eva="+eva;
            Ext.Ajax.request({
                url: urls,
                method:"Get",
                cors:true,//开启跨域模式
                useDefaultXhrHeader:false,//true的话header会带有X-Requested-With
                //binary:true, //为true后，后端将返回bytes
                success:function(resp){
                    var bean = Ext.decode(resp.responseText);
                    if(bean.success){
                        var data = bean.data;
                        model.setData(
                        {
                            patientValue1: data.patientValue1==""? "": data.patientValue1+";",
                            patientValue2: data.patientValue2 || "",
                            patientValue3: data.patientValue3==""? "": data.patientValue3,
                            allTP: data.allTP,
                            report: data.report1 || "",
                            comment: data.comment==""?"无":data.comment
                        })
                        var recs = data.programeRec || null;
                        var ints = data.programeInt || null;
                        var alltp = data.allTP || null;

                        var Textareaid = Ext.getCmp("textareaid");
                        var alltp_arr = [];
                        Textareaid.removeAll();
                        alltp_arr[alltp_arr.length] = Admin.util.ProgrameList.createAlltp(alltp);
                        Textareaid.add(alltp_arr);

                        var Rectitles = Ext.getCmp("Rectitles5");
                        var items = [];
                        Rectitles.removeAll();
                        recs.forEach(function(bean,index){
                            bean.type == "hyp";
                            items[items.length] = Admin.util.ProgrameList.createCmp(bean);
                        });
                        Rectitles.add(items);

                        var IntTitles = Ext.getCmp("IntTitles5");
                        var arritems = [];
                        IntTitles.removeAll();
                        ints.forEach(function(bean,index){
                            arritems[arritems.length] = Admin.util.ProgrameList.createCmp(bean);
                        });
                        IntTitles.add(arritems);
                    }else{
                        me.close();
                        Ext.Msg.alert("提示",bean.message);
                    }
                }
            });
        }
    },
    viewModel: {
        data: {
            name: ""
        }
    },
    autoScroll:true,
    items: [
        {
            xtype: "container",
            layout: "vbox",
            margin: 20,
            autoScroll:true,
            items: [
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    style: "font-weight: bold;padding: '0 0 0 30px'",
                    html: '主诉' 
                },
                {
                    xtype:"container",
                    style: "text-indent: 2em;",
                    bind: {  
                        html: '<p style="word-wrap: break-word;width: 540px;"><div>{patientValue2}</div></p>',
                    } 
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    style: "font-weight: bold;",
                    html: '症状' 
                },
                {
                    xtype:"container",
                    style: "text-indent: 2em;",
                    bind: {  
                        html: '<p style="word-wrap: break-word;width: 540px;">{patientValue1}{patientValue3}</p>',
                    } 
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    style: "text-indent: 2em;",
                    bind: {  
                        html: '<div>{!allTP || allTP == "" || allTP == "尚未进行客观化信息采集" || allTP == "尚未设置智能合参"? "" : "舌面脉客观化信息"}</div>' 
                    } 
                },
                {
                    xtype:"container",
                    id:"textareaid"
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    style: "font-weight: bold;",
                    html: '辩证分型' 
                },
                {
                    xtype:"container",
                    style: "text-indent: 2em;",
                    bind: {  
                        html: '<p style="word-wrap: break-word;width: 540px;">{report}</p>',
                    } 
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    style: "font-weight: bold;",
                    html: '医生建议' 
                },
                {
                    xtype:"container",
                    style: "text-indent: 2em;",
                    bind: {  
                        html: '<p style="word-wrap: break-word;width: 540px;">{comment}</p>',
                    } 
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    width: 540,
                    style: "font-weight: bold;text-align: center;",
                    html: '养生方案' 
                },
                {
                    xtype:"container",
                    id: "Rectitles5"
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    width: 540,
                    style: "font-weight: bold;text-align: center;",
                    html: '干预方案' 
                },
                {
                    xtype:"container",
                    id: "IntTitles5"
                }
            ]
        }
    ]
   
});
