Ext.define("Admin.view.hospital.WinSetHospitalBanner",{
    extend:"Ext.window.Window",
    title:"banner图设置",
    width:350,
    height:150,
    requires: [
        "Admin.view.eva.BannerTypeCmb"
    ],
    layout:"fit",
    controller:"hospital",
    modal:true,
    resizeAble:false,
    items:[
        {
            xtype:"form",
            itemId:"frmSetHospitalBanner",
            padding:10,
            layout:{
                type:"hbox",
                pack:"center",
                align:"center"
            },

            items:[
                {
                    xtype: "BannerTypeCmb",
                    id: "BannerTypeCmb",
                    labelStyle: "font-size:10px",
                    margin: "0 10",
                    labelWidth: 60
                },
                {
                    xtype: 'filefield',
                    buttonText: "选择图片",
                    name: 'wechatphoto',
                    accept: 'image'
                }
            ]
        }
    ],
    bbar:[
        {
            xtype:"container",
            width:"100%",
            layout:{
                type:"hbox",
                pack:"center",
                align:"center"
            },
            items:[
                {
                    xtype:"button",
                    text:"提交",
                    handler: "addBannerSubmit"
                }
            ]
        }
    ]
    
});