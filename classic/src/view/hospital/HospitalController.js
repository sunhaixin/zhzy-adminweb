Ext.define("Admin.view.hospital.HospitalController",{
    extend:"Ext.app.ViewController",
    alias: 'controller.hospital',

    onProvinceChange: function(v,val){
        var view = this.getView();
        var cmbCity = view.down("#cmbCity");
        // cmbCity.clearvalue();
        if(!cmbCity.Province || cmbCity.Province != val){
            cmbCity.Province = val;
            cmbCity.reset();
            cmbCity.setDisabled(false);
            var cmbDis = view.down("#cmbDis");
            cmbDis.reset();
            cmbDis.setDisabled(true);
            var store = cmbCity.getStore();
            store.getProxy().extraParams = {pid:val};
            store.remove();
            // store.reload();
            store.loadPage(1);
        }
    },
    SoftwareFocus: function(item,obj){
        
    },
    platfromFocus:function(item,obj){
        var ownerCt = item.ownerCt;
        var lastValue = item.lastValue;
        var mutexs = item.mutex;
        if (mutexs != "") {
            mutexs = mutexs.split(",");
        }
        if (!lastValue) {
            var items = ownerCt.query("checkboxfield");
            mutexs.forEach(function(mutex){
                items.forEach(function(bean){
                    if (bean.inputValue == mutex) {
                        bean.setValue(false);
                    }
                })
            })
            
        }
    },
    onHospitalfilters: function(){
        var view = this.getView();
        var textfield = view.down("[xtype=textfield]").value;
        var dpGrid = Ext.getCmp("GridHospital").getStore();
        var aid = sessionStorage.getItem("Aid");
        dpGrid.getProxy().extraParams = {
            aid:aid,
            name:textfield
        };
        dpGrid.remove();
        dpGrid.loadPage(1);
    },
    onCityChange:function(v,val){
        var view = this.getView();
        var cmbDis = view.down("#cmbDis");
        cmbDis.setDisabled(false);
        if (val != null){
            var store = cmbDis.getStore();
            store.getProxy().extraParams = {cid:val};
            // store.reload();
            store.removeAll();
            store.loadPage(1);
        }
    },
    onW3TypeChange: function(v,n,o){
        var view = this.getView();
        var w3CardTypes = view.down("#w3CardTypes");
        w3CardTypes.setDisabled(false);
        var store = w3CardTypes.getStore();
        store.getProxy().extraParams = {w3CardType:n};
        store.removeAll();
        store.loadPage(1);
    },
    onSubmitButtonClick:function(){
        var view = this.getView();
        if(view.type == "Add"){
            this.onAddHospitalSubmit(arguments);
        }else{
            this.onUpdateHospitalSubmit(arguments);
        }
    },
    onAddHospitalSubmit:function(){
        var view = this.getView();
        var frm = view.down("#frmHospital");
        var values = frm.getValues();

        var filterRule= /[^0-9a-zA-Z_[\u4E00-\u9FA5]]/g; 
        var judge= filterRule.test(values.name);
        if (judge) {
            Ext.Msg.alert("提示","医院名不能出现特殊符号")
            return false;
        }
        if (values.Modules) {
            if (typeof(values.Modules) == 'string') {
                values.Modules = values.Modules;
            }else{
                values.Modules = values.Modules.join(";");
            }
        }
        if (values.Platforms) {
            if (typeof(values.Platforms) == 'string') {
                values.Platforms = values.Platforms;
            }else{
                values.Platforms = values.Platforms.join(";");
            }
        }
        
        if (values.District == "") {
            values.District = 0;
        }
        if (!frm.form.isValid()) {
            Ext.Msg.alert("提示","请检查必填项")
            return false;
        }

        Ext.Ajax.request({
            url: Admin.proxy.API.MgmtPath+"hospital/insert",
            method:"Post",
            cors:true,//开启跨域模式
            useDefaultXhrHeader:false,//true的话header会带有X-Requested-With
            //binary:true, //为true后，后端将返回bytes
            //jsonData:{data:valus,token:"123456"},
            params:values,
            success:function(resp){
                var bean = Ext.decode(resp.responseText);
                if(bean.success){
                    Ext.Msg.alert("提示","添加成功");
                    var store = view.BindGrid.getStore();
                    store.reload();
                    view.close();      
                }else{
                    Ext.Msg.alert("提示",bean.message);
                }
            }
        });
    },
    onUpdateHospitalSubmit:function(){
        var view = this.getView();
        var frm = view.down("#frmHospital");
        var values = frm.getValues();
        var module = frm.down("[name=Module]");
        var Platform = frm.down("[name=Platform]");
        var Platforms = frm.getForm().getFieldValues().Platforms;
        var Modules = frm.getForm().getFieldValues().Modules;
            
        values.Platforms = [];
        values.Modules = [];
        for(var i = 0; i< Platforms.length ; i++){
            if (Platforms[i]) {
                values.Platforms.push(Platform.query("checkboxfield")[i].inputValue)
            }
        }
        for(var i = 0; i< Modules.length; i++){
            if (Modules[i]) {
                values.Modules.push(module.query("checkboxfield")[i].inputValue)
            }
        }
        values.Platforms = values.Platforms.join(";");
        values.Modules = values.Modules.join(";");
        if (values.District == "") {
            values.District = 0;
        }

        var filterRule= /[^0-9a-zA-Z_[\u4E00-\u9FA5]]/g; 
        var judge= filterRule.test(values.Name);
        if (judge) {
            Ext.Msg.alert("提示","医院名不能出现特殊符号")
            return false;
        }
        if (!frm.form.isValid()) {
             Ext.Msg.alert("提示","请检查必填项");
            return false;
        }
        values.Id = view.record.get("Id");
        Ext.Ajax.request({
            url:Admin.proxy.API.MgmtPath+"hospital/update",
            method:"Post",
            cors:true,//开启跨域模式
            useDefaultXhrHeader:false,//true的话header会带有X-Requested-With
            //binary:true, //为true后，后端将返回bytes
            //jsonData:{data:valus,token:"123456"},
            params: values,
            success:function(resp){
                var bean = Ext.decode(resp.responseText);
                if(bean.success){
                    Ext.Msg.alert("提示","修改成功");
                    var store = view.BindGrid.getStore();
                    store.reload();
                    view.close();      
                }else{
                    Ext.Msg.alert("提示",bean.message);
                }
            }
        });
    },
    onHospitalDelete:function(){
        var record = arguments[5];
        var grid = this.getView().down("#GridHospital");
        var store = grid.getStore();
        Ext.Msg.confirm("提示","确定要删除吗？",function(bt){
            var me = this;
            if(bt == "yes"){
                Ext.Ajax.request({
                    url:Admin.proxy.API.BasePath+"api/admin/hospital/delete",
                    cors:true,
                    useDefaultXhrHeader:false,
                    jsonData:{data:{Id:record.get("Id")},token:"123456"},
                    method:"Post",
                    success:function(resp){
                        var bean = Ext.decode(resp.responseText);
                        if(bean.success){
                            Ext.Msg.alert("提示","删除成功!");
                            me.close();
                            store.reload();
                        }else{
                            Ext.Msg.alert("提示",bean.Message);
                        }
                    }
                });
            }else{
                me.close();
            }
        });
    },
    onSetHospitalVersionSubmit:function(){
        var view = this.getView();
        var form = view.down("#frmSetHospitalVersion");
        var values = form.getValues();
        if(!form.isValid()){
            return;
        }
        var store = view.BindGrid.getStore();
        values.hid = view.record.get("Id");
        Ext.Ajax.request({
            url:Admin.proxy.API.MgmtPath+"hospital/setVersion",
            cors:true,
            useDefaultXhrHeader:false,
            params:values,
            method:"Post",
            success:function(resp){
                var bean = Ext.decode(resp.responseText);
                if(bean.success){
                    Ext.Msg.alert("提示","设置成功!");
                    view.close();
                    store.reload();
                }else{
                    Ext.Msg.alert("提示",bean.message);
                }
            }
        });
    },
    onSetHospitalWechatSubmit:function(){
        var view = this.getView();
        var form = view.down("#frmSetHospitalWechat");
        var values = form.getValues();
        if(!form.isValid()){
            return;
        }
        values.HospitalId = view.record.get("Id");
        Ext.Ajax.request({
            //url:Admin.proxy.API.BasePath+"api/admin/hospital/wechat",
            url:Admin.proxy.API.MgmtPath + "wechat/insertByHid",
            cors:true,
            useDefaultXhrHeader:false,
            params: values,
            method:"Post",
            success:function(resp){
                var bean = Ext.decode(resp.responseText);
                if(bean.success){
                    Ext.Msg.alert("提示","设置成功!");
                    view.close();
                }else{
                    Ext.Msg.alert("提示",bean.message);
                }
            }
        });
    },
    onSetHospitalBanner:function(callback){
        var view = this.getView();
        var frm = view.down("[xtype=form]");
        var form = frm.getForm();
        var value = view.down("[xtype=filefield]");
        var photo = value.value;
        var fileEl = value.fileInputEl.dom;  
        var fd = new FormData();  
        fd.append('uploadFile',fileEl.files[0]);  
        if(!photo)
        { 
            Ext.Msg.alert("提示","请上传图片");
            return false;
        }
        else
        {
            if(!/\.(gif|jpg|jpeg|png|GIF|JPG|PNG)$/.test(photo))
            {
              Ext.Msg.alert("提示","图片类型必须是.gif,jpeg,jpg,png中的一种");
              return false;
            }
        }
        Ext.Ajax.request({  
            // url:Admin.proxy.API.BasePath + "api/news/upload",  
            //url:Admin.proxy.API.BasePath + "api/admin/hospital/uploadfile",  
            url:Admin.proxy.API.MgmtPath +"hospital/uploadImg",
            cors:true,  
            useDefaultXhrHeader:false,  
            method:'post',  
            rawData:fd,  
            headers: {  
                "Content-Type":null   /* 最重要的部分，将Content-Type设置成null，ExtJs则会由内部的XMLHTTPRequest自动生成包含boundary的Content-Type，否则在使用rawData的情况下，ExtJs会将Content-Type设置成text/plain */  
            },  
            success:function (resp) {  
                var bean = Ext.decode(resp.responseText);
                if(bean.success){
                    callback(bean.data);
                }else{
                    Ext.Msg.alert("提示",bean.message);
                }
                
            },  
            failure:function (resp) {  
                Ext.Msg.alert("提示","上传失败"); 
                return false;
            }  
        });
    },
    addBannerSubmit: function(){
        var view = this.getView();
        var values = {hid: view.record.id};
        var b = view.query("BannerTypeCmb")[0].getValue();
        var Type = b? b: 'wechat';
        var img = this.onSetHospitalBanner(img=>{
            img = img.split("banner/")[1];
            if (Type == "wechat") {
                values.wcfilename = img;
            }
            else if(Type == "app") {
                values.appfilename = img;
            }
            Ext.Ajax.request({
                url: Admin.proxy.API.MgmtPath+"hospital/setBanner",
                method:"Post",
                cors:true,//开启跨域模式
                useDefaultXhrHeader:false,//true的话header会带有X-Requested-With
                //binary:true, //为true后，后端将返回bytes
                params:values,
                success:function(resp){
                    var bean = Ext.decode(resp.responseText);
                    if(bean.success){
                        // form.getForm().reset(); 
                        Ext.Msg.alert("提示","添加成功");
                        view.close();
                    }else{
                        Ext.Msg.alert("提示",bean.message);
                        view.close();
                    }
                }
            });
        });
        
    }
});