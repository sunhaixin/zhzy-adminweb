Ext.define('Admin.view.eva.EvaReport8', {
    extend: 'Ext.window.Window',
    controller: 'EvaData',
    resizable:false,
    width:700,
    height:600,
    layout:"fit",
    modal:true,
    listeners:{
        render: function (argument) {
            var me = this;
            var model = me.getViewModel();
            model.set("name","jack");
            var uid = this.record.data.EvaUid;
            var eva = this.record.data.Eva;
            var report = this.down("#evaReport");
            var urls = me.type && me.type == "app"?Admin.proxy.API.AppDetail:Admin.proxy.API.PcDetail;
            urls += "?uid="+uid+"&eva="+eva;
            Ext.Ajax.request({
                url: urls,
                method:"Get",
                cors:true,//开启跨域模式
                useDefaultXhrHeader:false,//true的话header会带有X-Requested-With
                //binary:true, //为true后，后端将返回bytes
                success:function(resp){
                    var bean = Ext.decode(resp.responseText);
                        if(bean.success){
                        var data = bean.data;
                        if (data.hands == "") {
                            Ext.getCmp("leftPluse").hide();
                        }
                        if (data.handsR == "") {
                            Ext.getCmp("rightPluse").hide();
                        }
                        let A,H31,H41,H51,T1T,W1t,W2t,T54;
                        let AR,H31R,H41R,H51R,T1TR,W1tR,W2tR,T54R;
                            A = parseInt(data.ad+data.ass*1000)/1000;
                            T54 = parseInt(data.t5/data.t4*1000)/1000;
                            H31 = parseInt(data.h3/data.h1*1000)/1000;
                            H41 = parseInt(data.h4/data.h1*1000)/1000;
                            H51 = parseInt(data.h5/data.h1*1000)/1000;
                            T1T = parseInt(data.t1/data.t*1000)/1000;
                            W1t = parseInt(data.w1/data.t*1000)/1000;
                            W2t = parseInt(data.w2/data.t*1000)/1000;
                            AR = parseInt(data.adR+data.assR*1000)/1000;
                            T54R = parseInt(data.t5R/data.t4R*1000)/1000;
                            H31R = parseInt(data.h3R/data.h1R*1000)/1000;
                            H41R = parseInt(data.h4R/data.h1R*1000)/1000;
                            H51R = parseInt(data.h5R/data.h1R*1000)/1000;
                            T1TR = parseInt(data.t1R/data.tR*1000)/1000;
                            W1tR = parseInt(data.w1R/data.tr*1000)/1000;
                            W2tR = parseInt(data.w2R/data.tr*1000)/1000;
                        let jin,liu, jinR,liuR;
                        if(data.pulseName){
                            if(data.pulseName.indexOf("弦") != -1){
                                jin = "弦";
                            } else if(data.pulseName.indexOf("紧") != -1){
                                jin = "紧";
                            } else if(data.pulseName == ""){
                                jin = "";
                            } else {
                                jin = "无弦、紧特征"
                            }
                            if(data.pulseName.indexOf("滑") != -1){
                                liu = "滑";
                            } else if(data.pulseName.indexOf("涩") != -1){
                                liu = "涩";
                            } else if(data.pulseName == ""){
                                liu = "";
                            } else {
                                liu = "无滑、涩特征"
                            }
                        }

                        if(data.pulseNameR){
                            if(data.pulseNameR.indexOf("弦") != -1){
                                jinR = "弦";
                            } else if(data.pulseNameR.indexOf("紧") != -1){
                                jinR = "紧";
                            } else if(data.pulseNameR == ""){
                                jinR = "";
                            } else {
                                jinR = "无弦、紧特征"
                            }
                            if(data.pulseNameR.indexOf("滑") != -1){
                                liuR = "滑";
                            } else if(data.pulseNameR.indexOf("涩") != -1){
                                liuR = "涩";
                            } else if(data.pulseNameR == ""){
                                liuR = "";
                            } else {
                                liuR = "无滑、涩特征"
                            }
                        }
                        let t4s, t4sR;
                        if(data.t){
                            t4s = data.t.toFixed(3);
                        }
                        if(data.tr){
                            t4sR = data.tr.toFixed(3);
                        }
                        let righthards, lefthards; 
                        if(data.h5R){
                            righthards = data.h5R.toFixed(3);
                        }
                        if(data.h5){
                            lefthards  = data.h5.toFixed(3);
                        }
                        if (data.maiLv) {
                            data.maiLv = data.maiLv.split("(")[1].split("t")[0]
                        }
                        if (data.maiLvR) {
                            data.maiLvR = data.maiLvR.split("(")[1].split("t")[0]
                        }
                        model.setData(
                            {
                                ImageSubsetion1: data.imageSubsetion1,
                                ImageSubsetion2: data.imageSubsetion2,
                                ImageTrend1: data.imageTrend1, 
                                ImageTrend2: data.imageTrend2, 
                                ImageRhythm1: data.imageRhythm1, 
                                ImageRhythm2: data.imageRhythm2,
                                ImageBest1: data.imageBest1,
                                ImageBest2: data.imageBest2,
                                T1: data.t1,
                                H1: data.h1,
                                Ass: data.ass,
                                T2: data.t2,
                                H2: data.h2,
                                Ad: data.ad,
                                T3: data.t3,
                                H3: data.h3,
                                T4: data.t4,
                                H4: data.h4,
                                T5: data.t5,
                                W1: data.w1,
                                W2: data.w2,
                                A: A,
                                T54: T54,
                                H31: H31,
                                H41: H41,
                                H51: H51,
                                T1T: T1T, 
                                W1t: W1t,
                                W2t: W2t,

                                t4s: t4s,
                                t4sR: t4sR,
                                lefthards: lefthards,
                                righthards: righthards,

                                T1R: data.t1R,
                                H1R: data.h1R,
                                AssR: data.assR,
                                T2R: data.t2R,
                                H2R: data.h2R,
                                AdR: data.adR,
                                T3R: data.t3R,
                                H3R: data.h3R,
                                AR: data.aR,
                                T4R: data.t4R,
                                H4R: data.h4R,
                                T5R: data.t5R,
                                W1R: data.w1R,
                                W2R: data.w2R,
                                AR: AR,
                                T54R: T54R,
                                H31R: H31R,
                                H41R: H41R,
                                H51R: H51R,
                                T1TR: T1TR, 
                                W1tR: W1tR,
                                W2tR: W2tR,

                                Hands: data.hands,
                                Pulsesite: data.pulseSite,
                                Mailv: data.maiLv,
                                Pulsenumber: data.pulseNumber,
                                Maili: data.maiLi,
                                Pulsename: data.pulseName,

                                HandsR: data.handsR,
                                PulsesiteR: data.pulseSiteR,
                                MailvR: data.maiLvR,
                                PulsenumberR: data.pulseNumberR,
                                MailiR: data.maiLiR,
                                PulsenameR: data.pulseNameR,

                                jin: jin || "",
                                liu: liu || "", 
                                jinR: jinR || "",
                                liuR: liuR || ""
                            }
                        )
                    }else{
                        me.close();
                        Ext.Msg.alert("提示",bean.message);
                    }
                }
            });
        }
    },
    // autoScroll:true,
    viewModel: {
        data: {
            name: ""
        }
    },
    items: [
      {
        xtype: "container",
        autoScroll:true,
        items: [
          {
            xtype: "container",
            layout: "vbox",
            margin: 20,
            id: "leftPluse",
            // autoScroll:true,
            items: [
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    style: "font-weight: bold;",
                    html: '左手分段取压脉图'
                },
                {
                    xtype:"container",
                    // height: 200,
                    bind: {  
                        html: '<img src="{ImageSubsetion1}" height="324px" width="540px">',
                    } 
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    style: "font-weight: bold;",
                    html: '左手脉象p-h趋势图'  
                },
                {
                    xtype:"container",
                    // height: 200,
                    bind: {  
                        html: '<img src="{ImageTrend1}" height="324px" width="540px">',
                    } 
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    style: "font-weight: bold;",
                    html: '左手连续脉图（30秒）' 
                },
                
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    // height: 200,
                    style: "font-weight: bold;text-align: center;",
                    bind: {  
                        html: '<img src="{ImageRhythm1}" height="125px"  width="540px">' 
                    } 
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    style: "font-weight: bold;",
                    html: '左手最佳压力脉图' 
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    // height: 200,
                    style: "font-weight: bold;text-align: center;",
                    bind: {  
                        html: '<img src="{ImageBest1}" height="200px" width="540px">' 
                    } 
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    style: "font-weight: bold;",
                    html: '左手最佳脉图参数' 
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    bind: {  
                        html: '<table border="1px solid #000" cellpadding="0" cellspacing="0">'
                                +'<tr>'
                                  +'<td style="padding: 10px; font-size:16px;">t1(s)</td>'
                                  +'<td style="padding: 10px; font-size:16px;">{T1}</td>'
                                  +'<td style="padding: 10px; font-size:16px;">h1(g)</td>'
                                  +'<td style="padding: 10px; font-size:16px;">{H1}</td>'
                                  +'<td style="padding: 10px; font-size:16px;">As(g*s)</td>'
                                  +'<td style="padding: 10px; font-size:16px;">{Ass}</td>'
                                +'</tr>'
                                 +'<tr>'
                                  +'<td style="padding: 10px; font-size:16px;">t2(s)</td>'
                                  +'<td style="padding: 10px; font-size:16px;">{T2}</td>'
                                  +'<td style="padding: 10px; font-size:16px;">h2(g)</td>'
                                  +'<td style="padding: 10px; font-size:16px;">{H2}</td>'
                                  +'<td style="padding: 10px; font-size:16px;">Ad(g*s)</td>'
                                  +'<td style="padding: 10px; font-size:16px;">{Ad}</td>'
                                +'</tr>'
                                
                                +'<tr>'
                                  +'<td style="padding: 10px; font-size:16px;">t4(s)</td>'
                                  +'<td style="padding: 10px; font-size:16px;">{T4}</td>'
                                  +'<td style="padding: 10px; font-size:16px;">h4(g)</td>'
                                  +'<td style="padding: 10px; font-size:16px;">{H4}</td>'
                                  +'<td style="padding: 10px; font-size:16px;">t(s)</td>'
                                  +'<td style="padding: 10px; font-size:16px;">{t4s}</td>'
                                +'</tr>'

                                +'<tr>'
                                  +'<td style="padding: 10px; font-size:16px;">t5(s)</td>'
                                  +'<td style="padding: 10px; font-size:16px;">{T5}</td>'
                                  +'<td style="padding: 10px; font-size:16px;">h5(g)</td>'
                                  +'<td style="padding: 10px; font-size:16px;">{lefthards}</td>'
                                  +'<td style="padding: 10px; font-size:16px;">t5/t4</td>'
                                  +'<td style="padding: 10px; font-size:16px;">{T54}</td>'
                                +'</tr>'
                                +'<tr>'
                                  +'<td style="padding: 10px; font-size:16px;">h3/h1</td>'
                                  +'<td style="padding: 10px; font-size:16px;">{H31}</td>'
                                  +'<td style="padding: 10px; font-size:16px;">h4/h1</td>'
                                  +'<td style="padding: 10px; font-size:16px;">{H41}</td>'
                                  +'<td style="padding: 10px; font-size:16px;"></td>'
                                  +'<td style="padding: 10px; font-size:16px;"></td>'
                                +'</tr>'
                                +'<tr>'
                                  +'<td style="padding: 10px; font-size:16px;">h5/h1</td>'
                                  +'<td style="padding: 10px; font-size:16px;">{H51}</td>'
                                  +'<td style="padding: 10px; font-size:16px;">t1/t</td>'
                                  +'<td style="padding: 10px; font-size:16px;">{T1T}</td>'
                                  +'<td style="padding: 10px; font-size:16px;"></td>'
                                  +'<td style="padding: 10px; font-size:16px;"></td>'
                                +'</tr>'
                                +'<tr>'
                                  +'<td style="padding: 10px; font-size:16px;">w1/t</td>'
                                  +'<td style="padding: 10px; font-size:16px;">{W1t}</td>'
                                  +'<td style="padding: 10px; font-size:16px;">w2/t</td>'
                                  +'<td style="padding: 10px; font-size:16px;">{W2t}</td>'
                                  +'<td style="padding: 10px; font-size:16px;"></td>'
                                  +'<td style="padding: 10px; font-size:16px;"></td>'
                                +'</tr>'
                                +'<tr>'
                                  +'<td style="padding: 10px; font-size:16px;">w1</td>'
                                  +'<td style="padding: 10px; font-size:16px;">{W1}</td>'
                                  +'<td style="padding: 10px; font-size:16px;">w2</td>'
                                  +'<td style="padding: 10px; font-size:16px;">{W2}</td>'
                                  +'<td style="padding: 10px; font-size:16px;"></td>'
                                  +'<td style="padding: 10px; font-size:16px;"></td>'
                                +'</tr>'
                                +'</table>'
                    } 
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    style: "font-weight: bold;",
                    html: '左手脉象信息输出'  
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    width: "100%",
                    height: 200,
                    bind: {  
                        html: '<table  border="1px solid #000" cellpadding="0" cellspacing="0" >'
                                +'<tr>'
                                  +'<td style="padding: 10px; text-align: center;"></td>'
                                  +'<td style="padding: 10px; text-align: center;">脉位</td>'
                                  +'<td style="padding: 10px; text-align: center;">脉率<br/>(次／分钟)</td>'
                                  +'<td style="padding: 10px; text-align: center;">脉节律</td>'
                                  +'<td style="padding: 10px; text-align: center;">脉力</td>'
                                  +'<td style="padding: 10px; text-align: center;">紧张度</td>'
                                  +'<td style="padding: 10px; text-align: center;">流利度</td>'
                                  +'<td style="padding: 10px; text-align: center;">脉名</td>'
                                +'</tr>'
                                
                                +'<tr>'
                                  +'<td style="padding: 10px; text-align: center;">{Hands}关部</td>'
                                  +'<td style="padding: 10px; text-align: center;">{Pulsesite}</td>'
                                  +'<td style="padding: 10px; text-align: center;">{Mailv}</td>'
                                  +'<td style="padding: 10px; text-align: center;">{Pulsenumber}</td>'
                                  +'<td style="padding: 10px; text-align: center;">{Maili}</td>'
                                  +'<td style="padding: 10px; text-align: center;">{jin}</td>'
                                  +'<td style="padding: 10px; text-align: center;">{liu}</td>'
                                  +'<td style="padding: 10px; text-align: center;">{Pulsename}</td>'
                                +'</tr>'
                              +'</table>'
                    } 
                },
            ]
        },
         {
            xtype: "container",
            layout: "vbox",
            margin: 20,
            id: "rightPluse",
            // autoScroll:true,
            items: [
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    style: "font-weight: bold;",
                    html: '右手分段取压脉图'
                },
                {
                    xtype:"container",
                    style: "text-indent: 2em;",
                    // height: 200,
                    bind: {  
                        html: '<img src="{ImageSubsetion2}" height="324px"  width="540px">',
                    } 
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    style: "font-weight: bold;",
                    html: '右手脉象p-h趋势图' 
                },
                {
                    xtype:"container",
                    style: "text-indent: 2em;",
                    // height: 200,
                    bind: {  
                        html: '<img src="{ImageTrend2}" height="324px" width="540px">',
                    } 
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    style: "font-weight: bold;",
                    html: '右手连续脉图（30秒）' 
                },
                
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    // height: 200,
                    style: "font-weight: bold;text-align: center;",
                    bind: {  
                        html: '<img src="{ImageRhythm2}" height="125px" width="540px">' 
                    } 
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    style: "font-weight: bold;",
                    html: '右手最佳压力脉图'
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    // height: 200,
                    style: "font-weight: bold;text-align: center;",
                    bind: {  
                        html: '<img src="{ImageBest2}" height="200px" width="540px">' 
                    } 
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    style: "font-weight: bold;",
                    html: '右手最佳脉图参数' 
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    bind: {  
                        html: '<table border="1px solid #000" cellpadding="0" cellspacing="0">'
                                +'<tr>'
                                  +'<td style="padding: 10px; font-size:16px;">t1(s)</td>'
                                  +'<td style="padding: 10px; font-size:16px;">{T1R}</td>'
                                  +'<td style="padding: 10px; font-size:16px;">h1(g)</td>'
                                  +'<td style="padding: 10px; font-size:16px;">{H1R}</td>'
                                  +'<td style="padding: 10px; font-size:16px;">As(g*s)</td>'
                                  +'<td style="padding: 10px; font-size:16px;">{AssR}</td>'
                                +'</tr>'
                                 +'<tr>'
                                  +'<td style="padding: 10px; font-size:16px;">t2(s)</td>'
                                  +'<td style="padding: 10px; font-size:16px;">{T2R}</td>'
                                  +'<td style="padding: 10px; font-size:16px;">h2(g)</td>'
                                  +'<td style="padding: 10px; font-size:16px;">{H2R}</td>'
                                  +'<td style="padding: 10px; font-size:16px;">Ad(g*s)</td>'
                                  +'<td style="padding: 10px; font-size:16px;">{AdR}</td>'
                                +'</tr>'
                                
                                +'<tr>'
                                  +'<td style="padding: 10px; font-size:16px;">t4(s)</td>'
                                  +'<td style="padding: 10px; font-size:16px;">{T4R}</td>'
                                  +'<td style="padding: 10px; font-size:16px;">h4(g)</td>'
                                  +'<td style="padding: 10px; font-size:16px;">{H4R}</td>'
                                  +'<td style="padding: 10px; font-size:16px;">t(s)</td>'
                                  +'<td style="padding: 10px; font-size:16px;">{t4sR}</td>'
                                +'</tr>'

                                +'<tr>'
                                  +'<td style="padding: 10px; font-size:16px;">t5(s)</td>'
                                  +'<td style="padding: 10px; font-size:16px;">{T5R}</td>'
                                  +'<td style="padding: 10px; font-size:16px;">h5(g)</td>'
                                  +'<td style="padding: 10px; font-size:16px;">{righthards}</td>'
                                  +'<td style="padding: 10px; font-size:16px;">t5/t4</td>'
                                  +'<td style="padding: 10px; font-size:16px;">{T54R}</td>'
                                +'</tr>'
                                +'<tr>'
                                  +'<td style="padding: 10px; font-size:16px;">h3/h1</td>'
                                  +'<td style="padding: 10px; font-size:16px;">{H31R}</td>'
                                  +'<td style="padding: 10px; font-size:16px;">h4/h1</td>'
                                  +'<td style="padding: 10px; font-size:16px;">{H41R}</td>'
                                  +'<td style="padding: 10px; font-size:16px;"></td>'
                                  +'<td style="padding: 10px; font-size:16px;"></td>'
                                +'</tr>'
                                +'<tr>'
                                  +'<td style="padding: 10px; font-size:16px;">h5/h1</td>'
                                  +'<td style="padding: 10px; font-size:16px;">{H51R}</td>'
                                  +'<td style="padding: 10px; font-size:16px;">t1/t</td>'
                                  +'<td style="padding: 10px; font-size:16px;">{T1TR}</td>'
                                  +'<td style="padding: 10px; font-size:16px;"></td>'
                                  +'<td style="padding: 10px; font-size:16px;"></td>'
                                +'</tr>'
                                +'<tr>'
                                  +'<td style="padding: 10px; font-size:16px;">w1/t</td>'
                                  +'<td style="padding: 10px; font-size:16px;">{W1tR}</td>'
                                  +'<td style="padding: 10px; font-size:16px;">w2/t</td>'
                                  +'<td style="padding: 10px; font-size:16px;">{W2tR}</td>'
                                  +'<td style="padding: 10px; font-size:16px;"></td>'
                                  +'<td style="padding: 10px; font-size:16px;"></td>'
                                +'</tr>'
                                +'<tr>'
                                  +'<td style="padding: 10px; font-size:16px;">w1</td>'
                                  +'<td style="padding: 10px; font-size:16px;">{W1R}</td>'
                                  +'<td style="padding: 10px; font-size:16px;">w2</td>'
                                  +'<td style="padding: 10px; font-size:16px;">{W2R}</td>'
                                  +'<td style="padding: 10px; font-size:16px;"></td>'
                                  +'<td style="padding: 10px; font-size:16px;"></td>'
                                +'</tr>'
                                +'</table>'
                    } 
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    style: "font-weight: bold;",
                    html: '右手脉象信息输出'
                },
                {
                    xtype:"container",
                    margin: "13px 0 0",
                    width: "100%",
                    height: 200,
                    bind: {  
                        html: '<table  border="1px solid #000" cellpadding="0" cellspacing="0" >'
                                +'<tr>'
                                  +'<td style="padding: 10px; font-size:16px; text-align: center;"></td>'
                                  +'<td style="padding: 10px; font-size:16px; text-align: center;">脉位</td>'
                                  +'<td style="padding: 10px; font-size:16px; text-align: center;">脉率<br/>(次／分钟)</td>'
                                  +'<td style="padding: 10px; font-size:16px; text-align: center;">脉节律</td>'
                                  +'<td style="padding: 10px; font-size:16px; text-align: center;">脉力</td>'
                                  +'<td style="padding: 10px; font-size:16px; text-align: center;">紧张度</td>'
                                  +'<td style="padding: 10px; font-size:16px; text-align: center;">流利度</td>'
                                  +'<td style="padding: 10px; font-size:16px; text-align: center;">脉名</td>'
                                +'</tr>'
                                
                                +'<tr>'
                                  +'<td style="padding: 10px; font-size:16px; text-align: center;">{HandsR}关部</td>'
                                  +'<td style="padding: 10px; font-size:16px; text-align: center;">{PulsesiteR}</td>'
                                  +'<td style="padding: 10px; font-size:16px; text-align: center;">{MailvR}</td>'
                                  +'<td style="padding: 10px; font-size:16px; text-align: center;">{PulsenumberR}</td>'
                                  +'<td style="padding: 10px; font-size:16px; text-align: center;">{MailiR}</td>'
                                  +'<td style="padding: 10px; font-size:16px; text-align: center;">{jinR}</td>'
                                  +'<td style="padding: 10px; font-size:16px; text-align: center;">{liuR}</td>'
                                  +'<td style="padding: 10px; font-size:16px; text-align: center;">{PulsenameR}</td>'
                                +'</tr>'
                              +'</table>'
                    } 
                }
            ]
        },
        ]
      }
        
    ],
   
});
