Ext.define('Admin.view.hospital.WinDoctor', {
    extend: 'Ext.window.Window',
    controller: 'doctor',
    width:500,
    height:490,
    layout:"fit",
    modal:true,
    listeners: {
        activate:function(){
            var grid = this.down("#cmbHospital");
            if (sessionStorage.getItem("AccountType") != "system" && sessionStorage.getItem("AccountType") != "Operator") {                                                                           
                grid.getStore().proxy.url = Admin.proxy.API.MgmtPath+"MedicalTreatmentCombination/hospital/findAll";
                var hid = sessionStorage.getItem("Hid");
                //如果只看下级医院
                if (sessionStorage.getItem("MyPrvileges").split(",").indexOf("yilianti_hospital_lower_self_see") !=-1) {
                    grid.getStore().getProxy().extraParams = {
                        hid: hid,
                        b: false,
                    }
                }else{
                    grid.getStore().getProxy().extraParams = {
                        hid: hid,
                        b: true,
                    }
                    
                }
            }
            
            grid.getStore().removeAll();
            grid.getStore().loadPage(1);
        }
    },
    items: [
        {
            xtype:"form",
            layout: 'anchor',
            defaultType: 'textfield',
            padding:"10 20",
            autoScroll:true,
            itemId:"frmDoctor",
            defaults: {
                anchor: '100%',
                labelWidth:65
            },
            items:[
                {
                    xtype:"combobox",
                    fieldLabel:"<span style='color:red'>*</span>医院",
                    name:"hospitalId",
                    store:Ext.create("Admin.store.hospital.HospitalGetStore"),
                    displayField:"name",
                    valueField:"id",
                    itemId:"cmbHospital",
                    allowBlank:false,
                    editable:false,
                    listeners:{
                        change:"onHospitalChange",
                        render : function(combo) {//渲染  
                            combo.getStore().load();
                            var grid = Ext.getCmp("doctorhospital");
                            if (grid.selection) {
                                var selectrow = grid.selection.get("id");
                                // combo.setValue(selectrow);//第一个值
                                combo.select(selectrow);
                            }   
                        }
                    }
                },{
                    xtype:"combobox",
                    fieldLabel:"<span style='color:red'>*</span>科室",
                    name:"DepartmentUid",
                    store:Ext.create("Admin.store.hospital.DepartmentStore"),
                    displayField:"Name",
                    valueField:"Uid",
                    itemId:"cmbDepartment",
                    editable:false,
                    allowBlank:false,
                    // disabled:true,
                    id: "cmbDepartment",
                    listeners:{
                        render : function(combo) {//渲染  
                            if (this.ownerCt.ownerCt.type == "Add") {}
                            else{
                                var store = combo.getStore();
                                var grid = this.ownerCt.ownerCt;
                                store.getProxy().extraParams = {
                                    hospitalId: grid.record.get("hospitalId")
                                };
                                store.removeAll();
                                store.loadPage(1);
                                var selectrow = grid.record.get("DepartmentUid");
                                // combo.setValue(selectrow);//第一个值
                                combo.select(selectrow);
                            }
                              
                        }
                    }
                },
                {
                    fieldLabel:"<span style='color:red'>*</span>姓名",
                    name:"Name",
                    allowBlank:false
                },
                {
                    fieldLabel:"<span style='color:red'>*</span>手机号码",
                    name:"Mobile",
                    allowBlank:false
                },{
                    xtype: 'fieldcontainer',
                    fieldLabel : "<span style='color:red'>*</span>性别",
                    defaultType: 'radiofield',
                    layout: 'hbox',
                    allowBlank:false,
                    items: [
                        {
                            boxLabel  : '男',
                            name      : 'Gender',
                            inputValue: '1'
                        }, {
                            boxLabel  : '女',
                            margin:"0 0 0 20",
                            name      : 'Gender',
                            inputValue: '2'
                        }
                    ]
                },{
                    xtype: 'fieldcontainer',
                    fieldLabel : "<span style='color:red'>*</span>演示账号",
                    defaultType: 'radiofield',
                    layout: 'hbox',
                    allowBlank:false,
                    items: [
                        {
                            boxLabel  : '不是',
                            name      : 'IsSSO',
                            inputValue: '1'
                        }, {
                            boxLabel  : '是',
                            margin:"0 0 0 20",
                            name      : 'IsSSO',
                            inputValue: '0'
                        }
                    ]
                },
                {
                    xtype: 'fieldcontainer',
                    fieldLabel : "<span style='color:red'>*</span>pc医生账号权限",
                    defaultType: 'radiofield',
                    layout: 'hbox',
                    allowBlank:false,
                    items: [
                        {
                            boxLabel  : '高级权限',
                            name      : 'Jurisdiction',
                            inputValue: '1'
                        }, {
                            boxLabel  : '低级权限',
                            margin:"0 0 0 20",
                            name      : 'Jurisdiction',
                            inputValue: '2'
                        }
                    ]
                },{
                    fieldLabel:"电子邮箱",
                    name:"Email"
                },{
                    fieldLabel:"职位",
                    name:"Title"
                },{
                    fieldLabel:"擅长",
                    name:"Expert"
                },{
                    fieldLabel:"备注",
                    name:"Comment"
                }
            ]
        }
    ],
    bbar:[
        {
            xtype:"container",
            flex:1,
            layout:{
                type:"hbox",
                pack:"center",
                align:"center"
            },
            padding:"0 0 10 0",
            items:[
                {
                    xtype:"tbspacer"
                },{
                    xtype:"button",
                    text:"提交",
                    handler:"onSubmitButtonClick"
                }
            ]
        }
    ]
});
