Ext.define('Admin.store.telemedicine.GroupStore', {
    extend: 'Admin.store.BaseStore',
    alias: 'store.group',

    fields: [
        {name:"Id", mapping: 'id'},
        {name:"Name", mapping: 'name'},
        {name:"CreateTime",type:"date"},
        {name:"UpdateTime",type:"date"},
        {name:"IsActive"}
    ],

    autoLoad: false,
    storeId: 'group',
    proxy: {
         type: 'ajax',
         cors:true,
         useDefaultXhrHeader:false,
         url: Admin.proxy.API.MgmtPath+"telemedicineGroup/findPage",
        //  api: {
        //     create  : 'http://localhost:49712/api/admin/hospital/query',
        //     read  : 'http://localhost:49712/api/admin/hospital/query',
        //     update  : '/controller/update',
        //     destroy : '/controller/destroy_action'
        // },
         reader: {
             type: 'json',
             rootProperty: 'data',
             totalProperty  : 'totalCount',
             successProperty: 'IsSuccess'
         }
     }
});