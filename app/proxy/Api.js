Ext.define('Admin.proxy.API', {
    statics:{
        // "BasePath":"http://api.tcm-ai.com/",
        // "BasePaths":"http://106.14.64.143:8082/",
        // "RbacPath":"http://api.tcm-ai.com/rbac/",
        // "MgmtPath": 'http://api.tcm-ai.com/mgmt/',
        // "PcDetail": "http://api.tcm-ai.com/mgmt/evaDetail/pc",
        // "AppDetail": "http://api.tcm-ai.com/mgmt/evaDetail/app",
        // "WechatDetail": "http://api.tcm-ai.com/mgmt/evaDetail/wechat",
        // "WebPath":" http://wap.tcm-ai.com/",
        // "WechatWebPath":"http://api.tcm-ai.com/wechatWebIndex",   
        "BasePath":"http://test.tcm-ai.com/",
        "BasePaths":"http://118.178.137.95:8082/",
        "RbacPath":"http://test.tcm-ai.com/rbac/",
        "MgmtPath": 'http://test.tcm-ai.com/mgmt/',
        "PcDetail": "http://test.tcm-ai.com/mgmt/evaDetail/pc",
        "AppDetail": "http://test.tcm-ai.com/mgmt/evaDetail/app",
        "WechatDetail": "http://test.tcm-ai.com/mgmt/evaDetail/wechat",
        "WebPath":" http://wap-test.tcm-ai.com/",
        "WechatWebPath":"http://test.tcm-ai.com/wechatWebIndex",       
    }
});