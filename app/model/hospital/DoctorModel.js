Ext.define('Admin.model.hospital.DoctorModel', {
    //extend: 'Admin.model.Base',
    extend:'Ext.data.Model',

    idProperty:"Id",
    fields: [
        {name:"Id", mapping: 'id'},
        {name:"HospitalId", mapping: 'hospitalId'},
        {name:"Name", mapping: 'name'},
        {name:"HospitalName",mapping:"hospitalName" },
        {name:"DepartmentUid", mapping: 'departmentUid'},
        {name:"Department", mapping: 'department'},
        {name:"Mobile", mapping: 'mobile'},
        {name:"Email", mapping: 'email'},
        {name:"Gender", mapping: 'gender'},
        {name:"Title", mapping: 'title'},
        {name:"Expert", mapping: 'expert'},
        {name:"Qrcode", mapping: 'qrcode'},
        {name:"Comment", mapping: 'comment'},
        {name:"IsSSO", mapping: 'isSSO'},
        {name:"CreateTime",mapping:"createTime"},
        {name:"UpdateTime",mapping:"updateTime"},
        {name:"Jurisdiction",mapping:"jurisdiction"}
    ]
});
