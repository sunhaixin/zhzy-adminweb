Ext.define('Admin.model.report.Stat2_1Model', {
    //extend: 'Admin.model.Base',
    extend:'Ext.data.Model',

    idProperty:"name",
    fields: [
        { name: "name"},
        { name: "value1"},
        { name: "value2"},
        { name: "value3"},
    ]
});
