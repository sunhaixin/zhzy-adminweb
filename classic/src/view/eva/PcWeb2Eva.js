Ext.define("Admin.view.eva.PcWeb2Eva",{
	extend: "Admin.view.main.BaseContainer",
	xtype: "pcweb2eva",
	requires: [
        "Admin.view.eva.EvaTypeCombobox",
        "Ext.form.field.ComboBox",
        "Ext.grid.Panel",
        "Ext.form.field.Text",
        "Ext.form.field.Date",
        "Ext.button.Button"
    ],
	layout: "fit",
	controller: 'EvaData',
	cls: "content-Panel-BorderStyle",
	margin:20,
	items: [
		{
			xtype: "grid",
			title:"PCWeb测评数据(无身份证号)",
			type: 'pcweb2',
			flex:1,
			tbar:{
				xtype: "container",
				padding: "10",
				style:"background:white",
				layout: "hbox",
				autoScroll:true,
				items: [
					{
						xtype: "combobox",
						store: Ext.create("Admin.store.data.HospitalStore"),
						changid: "pc_webeva2",
						allowBland:false,
						displayField:"Name",
						valueField: "Id",
						fieldLabel: "请选择医院",
						emptyText: "全部医院",
						labelWidth: 80,
						labelStyle: "font-size:10px",
						listeners: {
							change:"onHospitalItemClick",
							'beforequery':function(e){
	                            var combo = e.combo;  
	                            combo.getStore().clearFilter();
	                            if(!e.forceAll){  
	                                var input = e.query;  
	                                // 检索的正则
	                                if(input != ""){
	                                    var regExp = new RegExp(".*" + input + ".*");
	                                    // 执行检索
	                                    combo.store.filterBy(function(record,id){  
	                                        // 得到每个record的项目名称值
	                                        return regExp.test(record.get("Name"));
	                                    });
	                                    combo.expand();  
	                                    return false;
	                                }
	                                return false;
	                            }
	                        }
						}
					},
					{
						xtype: "EvaTypeCombobox",
						fieldLabel: "测评类型",
						id: "EvaTypeCombobox_pcweb2",
						labelStyle: "font-size:10px",
						margin: "0 10",
						labelWidth: 60
					},
					{
						xtype: 'datefield',
			            name: 'startDate',
			            id: 'PCWeb2_startDate',
			            fieldLabel: '起始时间',
			            value: "起始时间",
			            format: "Y-m-d",
			            width: 200,
			            labelStyle: "font-size:10px",
			            labelWidth: 60,
			            allowBlank: false,
			            listeners: {
			            	change: "onEvaDateChange"
			            }
					},
					{
						xtype: 'datefield',
			            name: 'endDate',
			            id: 'PCWeb2_endDate',
			            fieldLabel: '截止时间',
			            labelStyle: "font-size:10px",
			            format: "Y-m-d",
			            margin: '0 0 0 5',
			            labelWidth: 60,
			            width: 200,
			            allowBlank: false,
			            listeners: {
			            	change: "onEvaDateChange"
			            }
					}
				]
			},
			id:"grdPcWeb2Data",
			store:Ext.create("Admin.store.data.PcWeb2DataStore",{
				pageSize:100,
			}),
			viewConfig:{  
                enableTextSelection:true
            },
			columns: [
				{
					text: "记录类型",
					dataIndex: "Title",
					width: 150
				},
				{
					text: "医院",
					dataIndex: "HospitalName",
					flex: 1
				},
				{
					text: "姓名",
					dataIndex: "PatientName",
					width: 120
				},
				{
					text: "结论",
					dataIndex: "Result",
					flex: 1
				},
				{
					text: "测评日期",
					dataIndex: "CreateTime",
                    width:160,
				}
			],
			bbar:[
				{
					xtype: 'pagingtoolbar',
					store: Ext.data.StoreManager.lookup('pcweb2datatore'),
					displayInfo: true,
                    displayMsg: '显示第 {0} 条到 {1} 条记录，一共 {2} 条',
                    emptyMsg: "没有数据可以显示"
				}
			]

		}
		
	],
})
