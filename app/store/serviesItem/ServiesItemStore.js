Ext.define("Admin.store.serviesItem.ServiesItemStore",{
	extend: 'Admin.store.BaseStore',
    alias: 'store.serviesItem',

    fields: [
        {name:"Id",mapping:"id"},
        {name:"Name",mapping:"name"},
        {name:"IsActive",mapping:"isActive"},
        {name:"Eva",mapping:"serviceId"},
        {name:"CreateTime",type:"date",mapping:"createTime"}
    ],

    autoLoad: false,
    storeId: 'serviesItem',
    proxy: {
         type: 'ajax',
         cors:true,
         useDefaultXhrHeader:false,
         url: Admin.proxy.API.BasePaths+"api/admin/serviesItem/query",
         reader: {
             type: 'json',
             rootProperty: 'Data',
             totalProperty  : 'Total',
             successProperty: 'IsSuccess'
         }
     }
})