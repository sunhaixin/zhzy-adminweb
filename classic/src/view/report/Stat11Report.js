Ext.define("Admin.view.report.Stat11Report",{
    extend: 'Ext.panel.Panel',
    xtype: 'stat11report',
    height:"100%",
    layout:"hbox",
    controller:"report",
    cls:"content-Panel-BorderStyle",
    margin: 20,
    gridId: "stat11report_grid",
    listeners:{
        afterrender:"onReportRender"
    },
    items:[
        {
            xtype:"panel",
            title:"儿童偏颇体质与症状分布",
            flex:1,
            height:"100%",
            style:"border:1px solid #ccc",
            layout:{
                type:"hbox",
                pack:"center"
            },
            autoScroll:true,
            tbar: {
                xtype: "container",
                layout: {
                    type: "hbox",
                    pack: "center"
                },
                padding: 10, 
                items: [
                    {
                        xtype: 'combobox',
                        displayField:"Name",
                        valueField: "Name",
                        id: "stat11tizhi",
                        editable:false,
                        allowBlank:false,
                        emptyText: "气虚体",
                        store: {
                            fields:[
                                "Eva","Name"
                            ],
                            autoLoad: true,
                            data: [
                                { eva: 0, Name: "气虚体" },
                                { eva: 1, Name: "阳虚体" },
                                { eva: 2, Name: "痰湿体" },
                                { eva: 3, Name: "积滞体" },
                                { eva: 4, Name: "肝火体" },
                                { eva: 5, Name: "热盛体" },
                                { eva: 6, Name: "高敏体" },
                                { eva: 7, Name: "怯弱体" },
                            ]
                        },
                        listeners: {
                            change: "onstat11Change"
                        }
                    }
                ]
            },
            items:[
                {
                    width:1000,
                    height:500,
                    layout: {
                        type: 'hbox',
                    },
                    items:[
                        {
                            xtype:"grid",
                            autoHeight:true,
                            width: 220,
                            id: "stat11report_grid",
                            viewConfig:{
                                enableTextSelection:true, //可以复制单元格文字
                            },
                            store:Ext.create("Admin.store.report.Stat11Store",{autoLoad:false}),
                            columns:[
                                {
                                    text:"症状",
                                    dataIndex:"name",
                                    width: 150,
                                },{
                                    text:"人数",
                                    dataIndex:"value",
                                    flex: 1,
                                }
                            ],
                            style:"border:1px solid #ccc",
                            margin:10,
                            type: "气虚体"
                        },{
                            xtype: 'cartesian',
                            // flex:1,
                            reference: 'chart',
                            id: "stat11reportcanvas",
                            width: 650,
                            height: 400,
                            store:Ext.data.StoreManager.lookup("stat11report"),
                            axes: [{
                                type: 'numeric',
                                position: 'left',
                                minimum: 0
                            }, {
                                type: 'category',
                                position: 'bottom',
                                label: {
                                    rotate: {
                                        degrees: 75
                                    }
                                }
                            }],
                            series: [{
                                type: 'bar',
                                xField: 'name',
                                yField: 'value',
                                label: {
                                    field: 'value',
                                    display: 'insideEnd',
                                    orientation:"horizontal"
                                }
                            }]
                        }
                    ]
                }
            ]
        }
    ]
});