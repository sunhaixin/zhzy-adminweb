Ext.define("Admin.util.Submit",{

	submit:function(me,form,myurl,method){
		var view = me.getView();
        var frm = view.down(form);
        var valus = frm.getValues();
        if (method) {
        	method();
        }
        Ext.Ajax.request({
            url: Admin.proxy.API.BasePath+myurl,
            method:"Post",
            cors:true,//开启跨域模式
            useDefaultXhrHeader:false,//true的话header会带有X-Requested-With
            //binary:true, //为true后，后端将返回bytes
            //jsonData:{data:valus,token:"123456"},
            jsonData:valus,
            success:function(resp){
                var bean = Ext.decode(resp.responseText);
                if(bean.success){
                    Ext.Msg.alert("提示","添加成功");
                    var store = view.BindGrid.getStore();
                    store.reload();
                    view.close();      
                }else{
                    Ext.Msg.alert("提示",bean.Message);
                }
            }
        });
	}
})