Ext.define('Admin.Application', {
    extend: 'Ext.app.Application',
    
    name: 'Admin',

    stores: [
        'NavigationTree'
    ],
    requires: [
        'Ext.window.MessageBox',
        'Ext.plugin.Viewport',
        'Ext.tree.Panel',
        'Ext.container.Container',
        'Ext.chart.interactions.Rotate',
        'Ext.chart.interactions.ItemHighlight',
        'Ext.chart.CartesianChart',
        'Ext.chart.axis.Numeric',
        'Ext.chart.axis.Category',
        'Ext.chart.series.Bar',
        'Admin.view.main.MainContainer'
    ],
    //defaultToken : 'hospital',

    // The name of the initial view to create. This class will gain a "viewport" plugin
    // if it does not extend Ext.Viewport.
    //
    mainView: "Admin.view.main.MainContainer",
    //mainView:"Admin.view.authentication.Login",
    //mainView:'Admin.view.main.Main',
    //mainView:"Admin.Test",
    onAppUpdate: function () {
        Ext.Msg.confirm('Application Update', 'This application has an update, reload?',
            function (choice) {
                if (choice === 'yes') {
                    window.location.reload();
                }
            }
        );
    }
});
