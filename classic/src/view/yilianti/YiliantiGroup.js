Ext.define('Admin.view.yilianti.YiliantiGroup', {
    extend: 'Admin.view.main.BaseContainer',
    xtype: 'yilianti_group',
    requires: [
        'Ext.data.TreeStore',
        "Admin.view.yilianti.YiliantiHospitalItemMenu"
    ],
    controller: 'yilianti',
    layout: 'hbox',
    
    listeners:{
        activate:function(){
            var grid = this.down("[xtype=listpanel]");
            grid.getStore().load();
            var store = grid.getStore();
            var hid = sessionStorage.getItem("Hid");
            //当权限为只能查看自己的医联体时
            if (sessionStorage.getItem("MyPrvileges").indexOf("yilianti_group_see_self") !=-1) {
                grid.getStore().proxy.url = Admin.proxy.API.MgmtPath + "MedicalTreatmentCombination/queryByHid";
                store.getProxy().extraParams = {
                    "hid":hid,
                };
            }
        }
    },
    margin:20,
    items: [
        {
            xtype: "container",
            privileges:["yilianti_group_see_self","yilianti_group_see_all"],
            layout: 'vbox',
            cls:"content-Panel-BorderStyle",
            height:"100%",
            autoScroll:true,
            items: [
                {
                    xtype:"listpanel",
                    width:300,
                    title:"医联体名称",
                    id: "lstp-yiliantiHospital",
                    tbar:[
                        {
                            xtype:"container",
                            layout: 'vbox',
                            width:"100%",
                            items:[
                                {
                                    xtype:"button",
                                    text:"新增医联体",
                                    privileges:["yilianti_group_add"],
                                    width:"100%",
                                    handler:function(){
                                        var win = Ext.create("Admin.view.yilianti.WinYiliantiGroup");
                                        win.setTitle("添加医联体");
                                        win.BindGrid = this.ownerCt.ownerCt.ownerCt;
                                        win.type = "Add"
                                        win.show();
                                    }
                                },{
                                    xtype: "container",
                                    layout: 'hbox',
                                    margin: "10 0 0 0",
                                    width:"100%",
                                    items: [
                                        {
                                            xtype: "textfield",
                                            flex: 1,
                                            fieldLabel: '搜索',
                                            labelWidth: 50,
                                            listeners: {
                                                change: "onYiliantiHospitalFielts"
                                            }
                                        }
                                    ]
                                }
                            ]
                        }
                    ],
                    store:Ext.create("Admin.store.yilianti.YiliantiGroupStore",{
                        pageSize:1000,
                        autoLoad: false
                    }),
                    columns:[
                        {
                            xtype:"templatecolumn",
                            menuDisabled:true,
                            flex:1,
                            tpl:"<div style='height:30px;line-height:30px;cursor:pointer;'>{Name}</div>"
                        }
                    ],
                    listeners:{
                        itemClick:"onHospitalItemClick"
                    }
                }
            ]
            
        },
        {
            xtype:"treepanel",
            title:"医联体成员",
            cls:"content-Panel-BorderStyle",
            privileges:["yilianti_group_see_self","yilianti_group_see_all"],
            flex:1,
            margin:"0 0 0 20",
            height:"100%",
            itemId:"GridDepartment",
            id: "GridDepartment",
            style:"border:1px solid #d0d0d0 !important",
            rootVisible:false,
            viewConfig: {
			    plugins: {
                    treeviewdragdrop: {
                        containerScroll: true
                    },
			        ptype: 'treeviewdragdrop',
			        appendOnly: false   //只能拖拽到非叶节点上
			    },
			    listeners: {
			        drop: function (Node, data, overModel, dropPosition, options) {
			        }
			    }
			},
            tbar: [
                {
                    xtype: "container",
                    lyout: "hbox",
                    width: "100%",
                    items: [
                        {
                            xtype: "button",
                            text: "新增医院",
                            privileges:["yilianti_group_hospital_add"],
                            handler:function(){
                                var grid = Ext.getCmp("lstp-yiliantiHospital");
                                 var record = grid.getSelectionModel().getLastSelected();
                                var win = Ext.create("Admin.view.yilianti.WinYiliantiHospital");
                                win.setTitle("添加一级医院");
                                var frm = Ext.getCmp("idfrmYiliantiHospital");
                                frm.InitFormGrid = record;
                                win.BindGrid = this.ownerCt.ownerCt.ownerCt;
                                win.type = "Add"
                                win.show();
                            }
                        }
                    ]
                }
            ],
            listeners:{
				itemcontextmenu:function(cmp, record, item, index, e, eOpts ){
					var me = this;
					//禁止浏览器右键菜单事件
					e.preventDefault();  
					e.stopEvent();
                    var myprivileges = Ext.decode(sessionStorage.getItem("privileges"));
                    var privileges = [];
                    myprivileges.forEach(function(bean){
                        privileges.push(bean.code);
                    });
                    // yilianti_group_hospital_delete
                    if (privileges.indexOf("yilianti_group_lower_hospital_add") != -1 || privileges.indexOf("yilianti_group_hospital_delete") != -1) {
                        var menu = Ext.create("Admin.view.yilianti.YiliantiHospitalItemMenu").showAt(e.getXY());
                        if (privileges.indexOf("yilianti_group_lower_hospital_add") == -1) {
                            menu.down("[text=添加下级医院]").hide()
                        }
                        if (privileges.indexOf("yilianti_group_hospital_delete") == -1) {
                            menu.down("[text=删除该成员]").hide()
                        }
                        menu.selectedNodeId = record.data.id;
                        menu.raw = record;
                    }else{
                    }
					

				},
				itemclick: function () {
				    // this.ownerCt.OnItemsClick(arguments[1]);
				},
                render: function(){
                    this.getStore().remove();
                }

			},
            store: Ext.create("Admin.store.yilianti.YiliantiHospitalStore")
        }
    ]
});
