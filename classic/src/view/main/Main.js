Ext.define('Admin.view.main.Main', {
    extend: 'Ext.container.Container',
    xtype:"main",
    requires: [
        'Ext.button.Segmented',
        'Ext.list.Tree'
    ],
    controller: 'main',
    viewModel: 'main',
    itemId: 'mainView',
    layout:"border",
    height:"100%",
    items: [
        {
            xtype: 'toolbar',
            region:"north",
            cls: 'sencha-dash-dash-headerbar shadow',
            height: 76,
            itemId: 'headerBar',
            items: [
                {
                    xtype: 'component',
                    reference: 'senchaLogo',
                    cls: 'sencha-logo',
                    style: "background:#35BAF6",
                    height: 76,
                    html: '<div style="height:100%;width:100%;padding:10px 65px;"><img src="resources/images/logo.png" height="50px" width="100%"></div>'+
                    '<span style="position: absolute;right: 0;bottom: 0;">1.1.9 pre-release</span>',
                    width: 350
                },
                '->',
                {
                    xtype:"button",
                    text:"注销",
                    style: "border-radius:3px;",
                    handler:"onLogout"
                },
                {
                    xtype: 'tbtext',
                    text: 'Goff Smith',
                    cls: 'top-user-name',
                    listeners: {
                        render: function(){
                            this.setText(sessionStorage.getItem("UserName"))
                        }
                    }
                },
                {
                    xtype: 'image',
                    cls: 'header-right-profile-image',
                    height: 35,
                    width: 35,
                    alt:'current user image',
                    src: 'resources/images/user-profile/icon_avatar.jpg'
                }
            ]
        },
        {
            xtype:"maincontainerwrap",
            width:350,
            region:"west",
            autoScroll:false,
            style: "border-right: 1px solid #d0d0d0;",
            items:[
                {
                    xtype: 'treelist',
                    reference: 'navigationTreeList',
                    itemId: 'navigationTreeList',
                    ui: 'navigation',
                    store: 'NavigationTree',
                    width: 350,
                    id: "leftMenus",
                    autoScroll:false,
                    expanderFirst: false,
                    expanderOnly: false,
                    style:"padding-right: 10px;",
                    listeners: {
                        selectionchange: 'onNavigationTreeSelectionChange',
                        render:function(){
                        }
                    }
                }
            ]
        },
        {
            xtype: 'container',
            flex: 1,
            region:"center",
            reference: 'mainCardPanel',
            cls: 'sencha-dash-right-main-container',
            itemId: 'contentPanel',
            layout: {
                type: 'card',
                anchor: '100%'
            }
        }
    ]
});
