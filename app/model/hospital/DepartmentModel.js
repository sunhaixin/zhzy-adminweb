Ext.define('Admin.model.hospital.DepartmentModel', {
    //extend: 'Admin.model.Base',
    extend:'Ext.data.Model',

    idProperty:"Uid",
    fields: [
        {name:"Uid", mapping: 'uid'},
        {name:"HospitalId", mapping: 'hospitalId'},
        {name:"HospitalName"},
        {name:"Name", mapping: 'name'},
        {name:"Memo", mapping: 'memo'},
        {name:"DepartmentUid"},
        {name:"CreateTime"},
        {name:"UpdateTime"}
    ]
});
