Ext.define('Admin.model.system.AccountTypeModel', {
    extend:'Ext.data.Model',

    idProperty:"Id",
    fields: [
        {name:"Id",mapping:"id"},
        {name:"Name",mapping:"name"},
        {name:"Code",mapping:"code"}
    ]
});
