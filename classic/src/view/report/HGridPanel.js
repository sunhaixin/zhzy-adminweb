Ext.define("Admin.view.report.HGridPanel",{
    extend:"Ext.grid.Panel",
    xtype:"HGrid",
    columnLines:true,
    viewConfig:{
        enableTextSelection:true, //可以复制单元格文字
    },
    initComponent:function(){
        var me = this;
        me.callParent(arguments);
        var store = me.BindStore;
        var columns = [];
        data = {
            fields:[],
            data:[{}]
        };
        if (me.id == "stat11report_grid") {
            store.getProxy().extraParams = {
                    aid: sessionStorage.getItem("Aid"),
                    type: "气虚体"
                }
        }else if(me.id == "stat12report_1_grid"){
            store.getProxy().extraParams = {
                aid: sessionStorage.getItem("Aid"),
                type: "左手"
            };
        }else if(me.id == "stat12report_2_grid"){
            store.getProxy().extraParams = {
                aid: sessionStorage.getItem("Aid"),
                type: "右手"
            };
        }else if(me.id == "stat13report_1_grid"){
            store.getProxy().extraParams = {
                aid: sessionStorage.getItem("Aid"),
                type: "tongue"
            };
        }else if(me.id == "stat13report_2_grid"){
            store.getProxy().extraParams = {
                aid: sessionStorage.getItem("Aid"),
                type: "taise"
            };
        }else if(me.id == "stat13report_3_grid"){
            store.getProxy().extraParams = {
                aid: sessionStorage.getItem("Aid"),
                type: "taizhi"
            };
        }else if(me.id == "stat13report_4_grid"){
            store.getProxy().extraParams = {
                aid: sessionStorage.getItem("Aid"),
                type: "shexing"
            };
        }else if(me.id == "stat14report_1_grid"){
            store.getProxy().extraParams = {
                aid: sessionStorage.getItem("Aid"),
                type: "面色"
            };
        }else if(me.id == "stat14report_2_grid"){
            store.getProxy().extraParams = {
                aid: sessionStorage.getItem("Aid"),
                type: "光泽"
            };
        }else if(me.id == "stat14report_3_grid"){
            store.getProxy().extraParams = {
                aid: sessionStorage.getItem("Aid"),
                type: "唇色"
            };
        }else{
            store.getProxy().extraParams = {
                aid: sessionStorage.getItem("Aid")
            };
        }
        if (me.id == "stat6report_grid") {
            store.load({
                callback:function(records){
                    var index = 0;
                    var d = {
                        fields:[],
                        data:[]
                    };
                    d.data[0] = "";
                    d.data[1] = "";
                    records.forEach(function(bean){
                        columns[columns.length] = {
                            text:bean.get("name"),
                            dataIndex:"value"+index,
                            flex:1,
                            align:"center",
                            menuDisabled:true
                        };
                        eval("data.fields["+index+"] = 'value"+index+"';");
                        eval("data.data[0].value"+index+" = "+bean.get("value")+";");
                        // data.fields = ["value"+, "value0",value]
                        d.fields[index] = "value"+index;
                        
                        d.data[0] += "value" + index + ":" + bean.get("value")+ ",";
                        d.data[1] += "value" + index + ":" + bean.get("percent")+ ",";
                        index++;
                       
                    });
                    var value = d.data[0].split(",");
                    var a = {}, b = {};
                    var percent = d.data[1].split(",");
                    value.forEach(function(bean,index){
                         var key = bean.split(":")[0]
                         var val = bean.split(":")[1]
                         a[key] = val;
                         // a.val = va
                    })
                    percent.forEach(function(bean,index){
                         var key = bean.split(":")[0]
                         var val = bean.split(":")[1]
                         b[key] = val;
                         // a.val = va
                    })
                    d.data[0] = a;
                    d.data[1] = b;
                    if(me.getColumns().length == 0){
                        me.setColumns(columns);
                    }
                    me.setData(d);
                    me.setStore(d);
                }
            });
        }else{
            store.load({
                callback:function(records){
                    var index = 0;
                    records.forEach(function(bean){
                        columns[columns.length] = {
                            text:bean.get("name"),
                            dataIndex:"value"+index,
                            flex:1,
                            align:"center",
                            menuDisabled:true
                        };
                        eval("data.fields["+index+"] = 'value"+index+"';");
                        eval("data.data[0].value"+index+" = "+bean.get("value")+";");
                        index++;
                       
                    });
                    if(me.getColumns().length == 0){
                        me.setColumns(columns);
                    }
                    me.setData(data);
                    me.setStore(data);
                }
            });
        }
        
    }
})