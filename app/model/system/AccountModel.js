Ext.define('Admin.model.system.AccountModel', {
    extend:'Ext.data.Model',

    idProperty:"Id",
    fields: [
        {name:"Id",mapping:"id"},
        {name:"LoginId",mapping:"loginId"},
        {name:"AccountType",mapping:"accountType"},
        {name:"AccountId",mapping:"accountId"},
        // {name: "AccountTypeCode"},
        {name:"UserName",mapping:"userName"},
        {name:"Memo",mapping:"memo"},
        {name:"CreateTime",mapping:"createTime"},
        {name:"UpdateTime",mapping:"updateTime"},
        {name:"HospitalName",mapping:"hospitalName"},
        {name: "SysName",mapping:"sysName"},
        {name: "MapUrl",mapping:"mapUrl"}
    ]
});
