Ext.define('Admin.view.permission.serviesItem.WinServiesItem', {
    extend: 'Ext.window.Window',
    controller: 'serviesItem',
    width:500,
    height:490,
    layout:"fit",
    modal:true,
    items: [
        {
            xtype:"form",
            itemId:"frmServiesItem",
            layout: 'anchor',
            defaultType: 'textfield',
            autoScroll:true,
            padding:"10 20",
            method:"POST",
            jsonSubmit:true,
            defaults: {
                anchor: '100%',
                labelWidth:65
            },
            items:[
                {
                    fieldLabel:"<span style='color:red'>*</span>服务名称",
                    name:"name",
                    allowBlank:false,               //不允许为空
                    blankText:'该项不能为空!'
                },
                {
                    xtype: "numberfield",
                    fieldLabel:"<span style='color:red'>*</span>eva",
                    name:"eva",
                    allowBlank: false
                }
            ]
        }
    ],
    bbar:[
        {
            xtype:"container",
            flex:1,
            layout:{
                type:"hbox",
                pack:"center",
                align:"center"
            },
            padding:"0 0 10 0",
            items:[
                {
                    xtype:"tbspacer"
                },{
                    xtype:"button",
                    text:"提交",
                    handler:"onSubmitButtonClick"
                }
            ]
        }
    ]
});
