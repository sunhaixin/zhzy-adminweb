Ext.define("Admin.view.permission.privilegeMgr.PrivilegeMgrController",{
    extend: "Ext.app.ViewController",
    alias: "controller.privilegeMgr",
    onMenuSubmit: function(){
        var view = this.getView();
        var frm = view.down("#frmAddMenu");
        var valus = frm.getValues();

        if(!frm.isValid()){
            Ext.Msg.alert("提示","请检查必填项")
            return;
        }
        if (!frm.form.isValid()) {
            Ext.Msg.alert("提示","请检查必填项")
            return false;
        }
        var values = {
            name: valus.name,
            icon: "x-fa fa-cog",
            viewType: valus.viewType
        };

        if(view.type == "Add"){
            values.parentNode = view.parentid;
        }
        if (view.type == "RootAdd") {
            values.parentNode = view.parentid;
        }

        var controller = Ext.create("Admin.view.controller.admincontroller");
        var url = 'menu/add';
        var store = Ext.getCmp("grdPrivilegeTree").getStore();
        controller.post(values, url, store, view);
    },
    
    onSubMenuItemClick:function(){
        var view = this.getView();
        var dpGrid = view.down("#grdPrivilege");
        var menuId = arguments[1].get("id");
        dpGrid.getStore().getProxy().extraParams = {"menuId": menuId};
        dpGrid.getStore().remove();
        dpGrid.getStore().loadPage(1);
    },
    onPrivilegeSubmit: function(){
        var view = this.getView();
        if (view.type == "Add") {
            this.onPrivilegeAddSubmit(arguments);
        }
        if (view.type == "update") {
            this.onUpdatePrivilegeSubmit(arguments);
        }
    },
    onPrivilegeAddSubmit:function(){
        var view = this.getView();
        var frm = view.down("#frmAddPrivilege");
        var values = frm.getValues();
        values.menuId = frm.ownerCt.MenuId;
        if(!frm.isValid()){
            Ext.Msg.alert("提示","请检查必填项");
            return;
        }
        var controller = Ext.create("Admin.view.controller.admincontroller");
        var url = 'privilege/add';
        var store = view.BindGrid.getStore();
        controller.post(values, url, store, view);
    },
    onUpdatePrivilegeSubmit: function() {
        var view = this.getView();
        var frm = view.down("#frmAddPrivilege");
        var valus = frm.getValues();
        valus.menuId = view.record.get("menuId");
        valus.id = view.record.get("id");
        if(!frm.isValid()){
            Ext.Msg.alert("提示","请检查必填项");
            return;
        }

        var controller = Ext.create("Admin.view.controller.admincontroller");
        var url = 'privilege/update';
        var store = view.BindGrid.getStore();
        controller.post(valus, url, store, view);
    },
    onAccountTypeAddSubmit:function(){
        var view = this.getView();
        var frm = view.down("#frmAddAccountType");
        var values = frm.getValues();

        if(!frm.form.isValid()){
            Ext.Msg.alert("提示","请检查必填项")
            return;
        }

        var controller = Ext.create("Admin.view.controller.admincontroller");
        var url = 'accountType/add';
        var store = view.BindGrid.getStore();
        controller.post(values, url, store, view);
    },
    onSetAccountPermissions: function(){
        var view = this.getView();
        var frm = view.down("#AccountPermissions");
        var typeid = view.TypeId;
        var values = frm.getChecked();
        var ids = [];

        for (var i = 0; i <= values.length - 1 ; i++) {
            ids[i] = values[i].get("id")+"|"+values[i].get("code");
        }
        ids = ids.join(",");
        var valus = {
            ids: ids,
            accountTypeId: typeid,
            indexs: ''
        }
        var controller = Ext.create("Admin.view.controller.admincontroller");
        var url = '/permission/setPrivilege';
        var store = false;
        controller.post(valus, url, store, view, 'close');
    },
    onPrivilegeIndex: function(){
        var view = this.getView();
        var grid = view.down("#grdPrivilege");
        var store = grid.getStore();
        var count = store.getCount();
        var ids = [];
        var indexs = [];
        for (var i = 0; i < count; i++) {
            var record = store.getAt(i);
            var Id = record.data.Id;
            var Index = i;
            ids.push(Id);
            indexs.push(Index);
        }
        ids = ids.join(";");
        indexs = indexs.join(";");
        var values = {
            ids: ids,
            indexs: indexs
        };

        var controller = Ext.create("Admin.view.controller.admincontroller");
        var url = 'privilege/index';
        controller.post(values, url, store);
    }
})