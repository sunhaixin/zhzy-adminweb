Ext.define('Admin.model.hospital.HospitalGetModel', {
    extend:'Ext.data.Model',

    idProperty:"id",
    fields: [
        {name:"Id",mapping:"id"},
        {name:"Name",mapping:"name"},
    ]
});

