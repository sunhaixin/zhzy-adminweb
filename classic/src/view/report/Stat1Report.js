Ext.define("Admin.view.report.Stat1Report",{
    extend: 'Ext.container.Container',
    xtype: 'stat1report',
    requires: [
        'Ext.chart.PolarChart',
        'Ext.chart.series.Pie',
        'Ext.chart.interactions.Rotate'
    ],
    layout:"hbox",
    height:"100%",
    margin: 20,
    cls:"content-Panel-BorderStyle",
    controller:"report",
    gridId: "stat1report_grid",
    listeners:{
        afterrender:"onReportRender",
        activate: function(){
        }
    },
    items:[
        {
            xtype:"panel",
            title:"工作量统计",
            flex:1,
            height:"100%",
            style: "background: #fff",
            layout:{
                type:"hbox",
                pack:"center"
            },
            autoScroll:true,
            tbar: {
                xtype: "container",
                layout: {
                    type: "hbox",
                    pack: "center"
                },
                padding: 10, 
                items: [
                    {
                        xtype: 'datefield',
                        name: 'stat1_startDate',
                        id: 'stat1_startDate',
                        fieldLabel: '起始时间',
                        format: "Y-m-d",
                        width: 200,
                        // margin: '0 5 0 0',
                        labelWidth: 60,
                        allowBlank: false,
                        listeners: {
                            change: "onEndDate"
                        }
                    },
                    {
                        xtype: 'datefield',
                        name: 'stat1_endDate',
                        id: 'stat1_endDate',
                        fieldLabel: '截止时间',
                        format: "Y-m-d",
                        margin: '0 0 0 5',
                        labelWidth: 60,
                        width: 200,
                        allowBlank: false,
                        listeners: {
                            change: "onEndDate"
                        }
                    }
                ]
            },
            items:[
                {
                    width:900,
                    height:500,
                    layout:{
                        type:"hbox"
                    },
                    items:[
                        {
                            xtype:"grid",
                            width:300,
                            height:"100%",
                            id: "stat1report_grid",
                            store:Ext.create("Admin.store.report.Stat1Store",{
                                autoLoad:false,
                                baseParams:{
                                    "hasTotal": true
                                }
                            }),
                            // store:Ext.create("Admin.store.report.StatNo1Store",{autoLoad:false}),
                            style:"border:1px solid #ccc",
                            height:"100%",
                            margin:10,
                            columns:[
                                {
                                    text:"项目",
                                    dataIndex:"name",
                                    flex:1,
                                },{
                                    text:"工作量（检测数量）",
                                    dataIndex:"value",
                                    width:150
                                }
                            ]
                        },
                        {
                            xtype:"grid",
                            width:300,
                            height:"100%",
                            id: "stat1report_no_grid",
                            // store:Ext.create("Admin.store.report.Stat1Store",{autoLoad:false}),
                            store:Ext.create("Admin.store.report.StatNo1Store",{autoLoad:false}),
                            style:"border:1px solid #ccc",
                            height:"100%",
                            margin:10,
                            columns:[
                                {
                                    text:"项目",
                                    dataIndex:"name",
                                    flex:1,
                                },{
                                    text:"工作量（检测数量）",
                                    dataIndex:"value",
                                    width:115
                                }
                            ]
                        },
                        {
                            xtype: 'polar',
                            flex:1,
                            id: "Stat1Polar",
                            // width: 500,
                            height: "100%",
                            insetPadding: 20,
                            innerPadding: 20,
                            // store:Ext.data.StoreManager.lookup("stat1report"),
                            store:Ext.data.StoreManager.lookup("statno1report"),
                            // store: Ext.create("Admin.store.report.StatNo1Store"),
                            legend: {
                                orient: 'vertical',
                                // width: 160,
                                docked: 'right'
                            },
                            interactions: ['rotate'],
                            series: [{
                                type: 'pie',
                                angleField: 'value',
                                label: {
                                    field: 'name',
                                    calloutLine: {
                                        length: 10,
                                        width: 1
                                        // specifying 'color' is also possible here
                                    },
                                    renderer:function(name){
                                        var me = this;
                                        var store = Ext.data.StoreManager.lookup("statno1report");
                                        if(true){
                                            var count = 0;
                                            var records = store.getData().items;
                                            records.forEach(function(bean){
                                                count += bean.get("value");
                                                // count += 1;
                                            });
                                            me.TotalCount = count;

                                            me.records = records;
                                        }
                                        var val = me.records[arguments[4]];
                                        var percent = (val.get("value")/me.TotalCount)*100;
                                        // var percent = (1/me.TotalCount)*100;
                                        if (name.length > 5) {
                                            name = name.substring(0,name.length/2) + "\n" + name.substring(name.length/2, name.length);
                                        }
                                        return name+ "\n" + percent.toFixed(2)+"%";
                                    }
                                },
                                highlight: true,
                                tooltip: {
                                    trackMouse: true,
                                    renderer: function (tooltip, record, item) {
                                        var me = this;
                                        var store = Ext.data.StoreManager.lookup("statno1report");
                                        if(!me.TotalCount){
                                            var count = 0;
                                            var records = store.getData().items;
                                            records.forEach(function(bean){
                                                count += bean.get("value");
                                                // count += 1;
                                            });
                                            me.TotalCount = count;
                                        }
                                        
                                        //var percent = (record.get('value')/me.TotalCount)*100;
                                        //tooltip.setHtml(record.get('name') + ': ' + percent.toFixed(2) + '%');
                                        tooltip.setHtml(record.get('name') + ': ' + record.get("value"));
                                        // tooltip.setHtml(record.get('name') + ': ' + 1);
                                    }
                                }
                            }]
                        },
                        {
                            xtype: 'polar',
                            flex:1,
                            id: "Stat1Polar_no",
                            height: "100%",
                            insetPadding: 50,
                            innerPadding: 50,
                            store:Ext.create('Ext.data.JsonStore', {
                                                fields: ['name', 'value' ],
                                                data: [
                                                    { name: '暂无数据', value: 20 },
                                                ]
                                            }),
                            // store: Ext.create("Admin.store.data.PcDataStore"),
                            legend: {
                                docked: 'bottom'
                            },
                            interactions: ['rotate'],
                            series: [{
                                type: 'pie',
                                angleField: 'value',
                                label: {
                                    field: 'name',
                                    calloutLine: {
                                        length: 100,
                                        width: 1
                                        // specifying 'color' is also possible here
                                    },
                                    renderer:function(name){
                                        var me = this;
                                        var store = Ext.create('Ext.data.JsonStore', {
                                                fields: ['name', 'value' ],
                                                data: [
                                                    { name: '暂无数据', value: 20 },
                                                ]
                                            });
                                        if(!me.TotalCount){
                                            var count = 0;
                                            var records = store.getData().items;
                                            records.forEach(function(bean){
                                                count += bean.get("value");
                                                // count += 1;
                                            });
                                            me.TotalCount = count;

                                            me.records = records;
                                        }
                                        var val = me.records[arguments[4]];
                                        var percent = (val.get("value")/me.TotalCount)*100;
                                        // var percent = (1/me.TotalCount)*100;
                                        return name+"("+percent.toFixed(2)+"%)";
                                    }
                                },
                                highlight: true,
                                tooltip: {
                                    trackMouse: true,
                                    // renderer: function (tooltip, record, item) {
                                    //     var me = this;
                                    //     var store = Ext.data.StoreManager.lookup("stat1report");
                                    //     if(!me.TotalCount){
                                    //         var count = 0;
                                    //         var records = store.getData().items;
                                    //         records.forEach(function(bean){
                                    //             count += bean.get("value");
                                    //             // count += 1;
                                    //         });
                                    //         me.TotalCount = count;
                                    //     }
                                        
                                    //     //var percent = (record.get('value')/me.TotalCount)*100;
                                    //     //tooltip.setHtml(record.get('name') + ': ' + percent.toFixed(2) + '%');
                                    //     tooltip.setHtml(record.get('name') + ': ' + record.get("value"));
                                    //     // tooltip.setHtml(record.get('name') + ': ' + 1);
                                    // }
                                }
                            }]
                        }
                    ]
                }
            ]
        }
    ]
});