Ext.define("Admin.store.data.PcWeb2DataStore",{
    extend: 'Admin.store.BaseStore',
    alias: "store.pcweb2datatore",
    fields: [
        {name: "UId",mapping:"uid"},
        {name: "HospitalId",mapping:"hospitalId"},
        {name: "HospitalName",mapping:"hospitalName"},
        {name: "PatientId",mapping:"patientId"},
        {name: "DoctorId",mapping:"doctorId"},
        {name: "Code",mapping:"code"},
        {name: "Title",mapping:"title"},
        {name: "Result",mapping:"result"},
        {name: "Eva",mapping:"eva"},
        {name: "EvaUid",mapping:"evaUid"},
        {name: "DoctorName",mapping:"doctorName"},
        {name: "PatientName",mapping:"patientName"},
        {name: "CreateTime",mapping:"createTime"},
        {name: "UpdateTime",mapping:"updateTime"},
        {name: "IsFinish",mapping:"isFinish"}
    ],
    autoLoad: true,
    storeId: 'pcweb2datatore',
    proxy: {
         type: 'ajax',
         cors:true,
         useDefaultXhrHeader:false,
         url: Admin.proxy.API.MgmtPath+"commonPE/evaList?isFinish=1",
         reader: {
             type: 'json',
             rootProperty: 'data',
             totalProperty  : 'totalCount',
             successProperty: 'success'
         }
     }
})