Ext.define('Admin.store.hospital.DoctorStore', {
    extend: 'Admin.store.BaseStore',
    alias: 'store.doctor',

    model: 'Admin.model.hospital.DoctorModel',

    autoLoad: false,
    storeId: 'doctor',
    proxy: {
         type: 'ajax',
         cors:true,
         useDefaultXhrHeader:false,
         url: Admin.proxy.API.MgmtPath+"doctor/findPage",
         reader: {
             type: 'json',
             rootProperty: 'data',
             totalProperty  : 'totalCount',
             successProperty: 'success'
         }
     }
});
