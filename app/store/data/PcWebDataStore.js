Ext.define("Admin.store.data.PcWebDataStore",{
    extend: 'Admin.store.BaseStore',
    alias: "store.pcwebdatatore",
    fields: [
        {name: "UId",mapping:"uid"},
        {name: "HospitalId",mapping:"hospitalId"},
        {name: "HospitalName",mapping:"hospitalName"},
        {name: "PatientId",mapping:"patientId"},
        {name: "DoctorId",mapping:"doctorId"},
        {name: "Code",mapping:"code"},
        {name: "Title",mapping:"title"},
        {name: "Result",mapping:"result"},
        {name: "Eva",mapping:"eva"},
        {name: "EvaUid",mapping:"evaUid"},
        {name: "DoctorName",mapping:"doctorName"},
        {name: "PatientName",mapping:"patientName"},
        {name: "CreateTime",mapping:"createTime"},
        {name: "UpdateTime",mapping:"updateTime"},
        {name: "IsFinish",mapping:"isFinish"}
    ],
    autoLoad: true,
    storeId: 'pcwebdatatore',
    proxy: {
         type: 'ajax',
         cors:true,
         useDefaultXhrHeader:false,
         url: Admin.proxy.API.MgmtPath+"pcWeb/query",
        //  api: {
        //     create  : 'http://localhost:49712/api/admin/hospital/query',
        //     read  : 'http://localhost:49712/api/admin/hospital/query',
        //     update  : '/controller/update',
        //     destroy : '/controller/destroy_action'
        // },
         reader: {
             type: 'json',
             rootProperty: 'data',
             totalProperty  : 'totalCount',
             successProperty: 'success'
         }
     }
})